// HENIKA × ITMO [ one day ] .r1

const mainRenderer = new THREE.WebGLRenderer({ powerPreference: "high-performance" });
mainRenderer.setSize(window.clientWidth, window.innerHeight);
mainRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(mainRenderer.domElement);
mainRenderer.domElement.style.position = 'fixed';
mainRenderer.domElement.style.display = 'block';
mainRenderer.domElement.style.top = 0;
const uiRenderer = new THREE.WebGLRenderer({ alpha: true, antialias: true, powerPreference: "high-performance" });
uiRenderer.setSize(window.innerWidth, window.innerHeight);
uiRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(uiRenderer.domElement);
uiRenderer.domElement.style.position = 'fixed';
uiRenderer.domElement.style.display = 'block';
uiRenderer.domElement.style.top = 0;
const mainScene = new THREE.Scene();
mainScene.background = new THREE.Color(0x000000);
const uiScene = new THREE.Scene();
const mainCamera = new THREE.PerspectiveCamera(50, document.documentElement.clientWidth / document.documentElement.clientHeight, 0.01, 50);
const ambientLight = new THREE.AmbientLight(0xffffff, 1);
const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
mainScene.add(ambientLight, directionalLight);
directionalLight.position.set(-4, 5, 10);
pixelComposer = new THREE.EffectComposer(mainRenderer);
const renderPixelatedPass = new THREE.RenderPixelatedPass(window.innerWidth / 240 * window.devicePixelRatio, mainScene, mainCamera);
renderPixelatedPass.normalEdgeStrength = 0;
renderPixelatedPass.depthEdgeStrength = 0;
const filmPass = new THREE.FilmPass(0.4, 0, 0, false);
pixelComposer.addPass(renderPixelatedPass);
pixelComposer.addPass(filmPass);
glitchComposer = new THREE.EffectComposer(mainRenderer);
const renderPixelatedPass1 = new THREE.RenderPixelatedPass(window.innerWidth / 240 * window.devicePixelRatio, mainScene, mainCamera);
renderPixelatedPass1.normalEdgeStrength = 0;
renderPixelatedPass1.depthEdgeStrength = 0;
const filmPass1 = new THREE.FilmPass(0.4, 0, 0, false);
glitchComposer.addPass(renderPixelatedPass1);
glitchComposer.addPass(filmPass1);
const glitchPass = new THREE.GlitchPass();
glitchComposer.addPass(glitchPass);
let goGlitch = false;
let PressStart;
new THREE.FontLoader().load('./fonts/PressStart.json', function(font) {
	PressStart = font;
	goLoadingScreen();
});
const loadingBar = new THREE.Object3D();
function goLoadingScreen() {
	const barGeometry = [];
	let shape = new THREE.Shape();
	shape.moveTo(0.775, -0.1);
	shape.lineTo(0.775, 0.1);
	shape.lineTo(-0.775, 0.1);
	shape.lineTo(-0.775, -0.1);
	shape.lineTo(0.775, -0.1);
	shape.lineTo(0.755, -0.085);
	shape.lineTo(-0.755, -0.085);
	shape.lineTo(-0.755, 0.085);
	shape.lineTo(0.755, 0.085);
	shape.lineTo(0.755, -0.085);
	barGeometry[0] = new THREE.ShapeGeometry(shape, 1);
	shape = null;
	shape = new THREE.Shape();
  shape.moveTo(0.04, -0.07);
  shape.lineTo(0.04, 0.07);
  shape.lineTo(-0.04, 0.07);5
  shape.lineTo(-0.04, -0.07);
	for (let i = 1; i < 17; i++) {
	  barGeometry[i] = new THREE.ShapeGeometry(shape, 1);
	  barGeometry[i].translate(-0.7125 + 0.095 * (i - 1), 0, -0.02);
	}
	loadingBar.bar = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(barGeometry), new THREE.MeshBasicMaterial({ color: 0xE040FB, transparent: true }));
  loadingBar.plane = new THREE.Mesh(new THREE.PlaneGeometry(1.5, 0.2), new THREE.MeshBasicMaterial({ color: 0x000000 }));
  loadingBar.plane.position.z = -0.01;
  loadingBar.text = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`загрузка`, 0.1), 1), loadingBar.bar.material);
  loadingBar.text.geometry.computeBoundingBox();
  loadingBar.text.geometry.translate(-0.5 * loadingBar.text.geometry.boundingBox.max.x, -0.3, 0);
	loadingBar.add(loadingBar.bar, loadingBar.plane, loadingBar.text);
	loadingBar.position.z = -1;
	loadingBar.scale.set(0.32, 0.32, 1);
	mainScene.add(loadingBar);
	goLoad();
}
let loadingCount = 38;
const loadingFull = loadingCount;
let path = [];
let Shantell;
const gameSound = [];
function goLoad() {
	for (let i = 0; i < 19; i++) {
    loadSVG(`svg/svg${i}.svg`).then(data => {
      path[i] = data.paths;
      checkLoading();
    });
	}
  new THREE.FontLoader().load('./fonts/Shantell.json', function(font) {
  	Shantell = font;
  	checkLoading();
  });
  gameSound[0] = new Howl({
  	src: ['sounds/buttonSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	html5: true,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[1] = new Howl({
  	src: ['sounds/loopSound.mp3'],
  	usingWebAudio: true,
  	mute: false,
  	html5: true,
  	webAudio: true,
  	volume: 0.2,
  	loop: true,
  	onload: checkLoading
  });
  gameSound[2] = new Howl({
  	src: ['sounds/robotSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[3] = new Howl({
  	src: ['sounds/guardSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[4] = new Howl({
  	src: ['sounds/heroSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[5] = new Howl({
  	src: ['sounds/jumpSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.3,
  	onload: checkLoading
  });
  gameSound[6] = new Howl({
  	src: ['sounds/landingSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.6,
  	onload: checkLoading
  });
  gameSound[7] = new Howl({
  	src: ['sounds/hintSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[8] = new Howl({
  	src: ['sounds/bookSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.35,
  	onload: checkLoading
  });
  gameSound[9] = new Howl({
  	src: ['sounds/impactSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.7,
  	onload: checkLoading
  });
  gameSound[10] = new Howl({
  	src: ['sounds/hitSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[11] = new Howl({
  	src: ['sounds/fallSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[12] = new Howl({
  	src: ['sounds/slideSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[13] = new Howl({
  	src: ['sounds/flySound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.7,
  	onload: checkLoading
  });
  gameSound[14] = new Howl({
  	src: ['sounds/downSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 0.6,
  	onload: checkLoading
  });
  gameSound[15] = new Howl({
  	src: ['sounds/hurtSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[16] = new Howl({
  	src: ['sounds/finSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
  gameSound[17] = new Howl({
  	src: ['sounds/hatSound.mp3'],
  	usingWebAudio: false,
  	mute: false,
  	webAudio: true,
  	volume: 1,
  	onload: checkLoading
  });
}
function checkLoading() {
	loadingCount--;
	gsap.to(loadingBar.plane.position, { duration: 0.05, x: Math.round(1.5 / loadingFull * (loadingFull - loadingCount)), ease: "power1.inOut" });
	if (loadingCount == 0) {
	  createGraphics();
	}
}
const format = (mainRenderer.capabilities.isWebGL2) ? THREE.RedFormat : THREE.LuminanceFormat;
let gradientMap = [];
for (let i = 2; i <= 6; i++) {
  const colors = new Uint8Array(i);
  for (let c = 0; c <= colors.length; c++) {
	  colors[c] = (c / colors.length) * 256;
  }
  gradientMap[i] = new THREE.DataTexture(colors, colors.length, 1, format);
  gradientMap[i].needsUpdate = true;
}
const blackBasicMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });
const whiteBasicMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
const transparentMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000, transparent: true, opacity: 0.0000001 });
const normalMaterial = new THREE.MeshNormalMaterial();
const purpleBasicMaterial = new THREE.MeshBasicMaterial({ color: 0xD500F9 });
const blimMaterial = [];
for (let i = 0; i < 5; i++) {
  blimMaterial[i] = new THREE.MeshToonMaterial({ color: 0x000000, gradientMap: gradientMap[4] });
  gsap.to(blimMaterial[i].color, { duration: 0.3 + Math.random() * 0.3, setHex: 0xFFFFFF, ease: "none", repeat: -1 });
}
const blimBasicMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });
gsap.to(blimBasicMaterial.color, { duration: 0.6, setHex: 0xFFFFFF, ease: "none", repeat: -1 });
const lightHeroShirtMaterial = new THREE.MeshToonMaterial({ color: 0x6A1B9A, gradientMap: gradientMap[4], side: THREE.DoubleSide });
const darkHeroShirtMaterial = new THREE.MeshToonMaterial({ color: 0x4A148C, gradientMap: gradientMap[2] });
const heroPantsMaterial = new THREE.MeshToonMaterial({ color: 0x006064, gradientMap: gradientMap[4] });
const heroSleeveMaterial = new THREE.MeshToonMaterial({ color: 0x0277BD, gradientMap: gradientMap[4], side: THREE.DoubleSide });
const heroShoesMaterial = new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[3] });
const heroSoleMaterial = new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[3] });
const heroFinMaterial = [
	new THREE.MeshToonMaterial({ color: 0x4A148C, gradientMap: gradientMap[4], side: THREE.DoubleSide }),
  new THREE.MeshToonMaterial({ color: 0x6A1B9A, gradientMap: gradientMap[4] }),
  new THREE.MeshToonMaterial({ color: 0xF57C00, gradientMap: gradientMap[4] })
];
const heroSkinMaterial = new THREE.MeshToonMaterial({ color: 0xC37C4D, gradientMap: gradientMap[4] });
const heroSkinDoubleMaterial = new THREE.MeshToonMaterial({ color: 0xC37C4D, gradientMap: gradientMap[3], side: THREE.DoubleSide });
const heroHairMaterial = new THREE.MeshToonMaterial({ color: 0x3E2723, gradientMap: gradientMap[4] });
const toyNameBorderMaterial = new THREE.MeshToonMaterial({ color: 0x330044, gradientMap: gradientMap[4] });
const buildingMaterial1 = new THREE.MeshToonMaterial({ color: 0xFF5722, gradientMap: gradientMap[6] });
const buildingMaterial2 = new THREE.MeshToonMaterial({ color: 0x3E2723, gradientMap: gradientMap[6] });
const buildingMaterial3 = new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[6] });
const buildingMaterial4 = new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[6] });
const buildingMaterial5 = new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[6] });
const buildingMaterial6 = new THREE.MeshBasicMaterial({ color: 0x455A64 });
const buildingMaterial7 = new THREE.MeshBasicMaterial({ color: 0x212121 });
const buildingMaterial8 = new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[6] });
const buildingMaterial9 = new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[6] });
const buildingMaterial10 = new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[6] });
const buildingMaterial11 = new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[6] });
const robotWhiteMaterial = new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4], side: THREE.DoubleSide });
const robotBlueMaterial = new THREE.MeshToonMaterial({ color: 0x004565, gradientMap: gradientMap[4] });
const robotFaceMaterial = new THREE.MeshToonMaterial({ color: 0x111119, gradientMap: gradientMap[6] });
const robotEyesMaterial = new THREE.MeshToonMaterial({ color: 0x03A9F4, gradientMap: gradientMap[6] });
const robotBlimColor = new THREE.Color(0x3F51B5);
gsap.to(robotEyesMaterial.color, { duration: 0.05, r: robotBlimColor.r, g: robotBlimColor.g, b: robotBlimColor.b, ease: "none", repeat: -1, yoyo: true  });
const whiteColor = new THREE.Color(0xFFFFFF);
const blackColor = new THREE.Color(0x000000);
const purpleColor = new THREE.Color(0xD500F9);
const blueColor = new THREE.Color(0x00E5FF);
const yellowColor = new THREE.Color(0xFFFF00);
const daySkyColor = new THREE.Color(0x0288D1);
const darkBlueColor = new THREE.Color(0x01579B);
const darkGreyColor = new THREE.Color(0x455A64);
const darkBrownColor = new THREE.Color(0x5D4037);
const darkBlueColor1 = new THREE.Color(0x303F9F);
const tempGeometry = [];
const hero = new THREE.Object3D();
const robot = new THREE.Object3D();
const levelName = [];
const introContainer = new THREE.Object3D();
const rotationContainer = new THREE.Object3D();
const worldContainer = new THREE.Object3D();
const frame = new THREE.Object3D();
const toyName = new THREE.Object3D();
const building = new THREE.Object3D();
const levelEnvironment = [];
const button = [];
const speechBubble = [];
const guard = new THREE.Object3D();
let level = 0;
let onPlay = false;
let viewY = false;
let flyY = false;
const book = [];
const bookCounter = new THREE.Object3D();
let bookCount = 0;
let moveWorld = false;
function createGraphics() {
	robot.topBody = new THREE.Object3D();
	robot.topBody.torso = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), robotWhiteMaterial);
	robot.topBody.torso = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), robotWhiteMaterial);
	robot.topBody.torso.geometry.scale(1, 2, 0.8);
	robot.topBody.torso.geometry.rotateX(Math.PI);
	robot.topBody.bottom = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), robotWhiteMaterial);
	robot.topBody.bottom.geometry.scale(1, 0.1, 0.8);
  robot.topBody.logo = createSVG(0, 12, 0x004565);
  for (let l = 0; l < robot.topBody.logo.children.length; l++) {
  	tempGeometry[tempGeometry.length] = robot.topBody.logo.children[l].geometry.clone();
  	robot.topBody.logo.children[l].geometry.dispose();
  }
  robot.topBody.logo = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotBlueMaterial);
  clearTempGeometries();
  robot.topBody.logo.geometry.translate(-568, -400, 0);
  bendGeometry(robot.topBody.logo.geometry, "y", -0.004);
  robot.topBody.logo.scale.set(0.0012, -0.0012, 0.0012);
  robot.topBody.logo.position.set(0, 0.1, 0.198);
  robot.topBody.logo.rotation.x = -0.11;
  robot.topBody.neck = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.07, 0.2, 16, 1, true), robotBlueMaterial);
  robot.topBody.neck.position.y = 0.5;
  robot.topBody.collar = new THREE.Mesh(new THREE.TorusGeometry(0.09, 0.03, 10, 16), robotBlueMaterial);
	robot.topBody.collar.rotation.x = Math.PI * 0.5;
	robot.topBody.collar.position.y = 0.44;
	robot.topBody.head = new THREE.Object3D();
	robot.topBody.skull = new THREE.Mesh(new THREE.SphereGeometry(0.28, 36, 24), robotWhiteMaterial);
  let shape = new THREE.Shape();
  shape.absarc(0.08, 0.0001, 0.17, 0, Math.PI * 0.5);
  shape.absarc(-0.08, 0.0001, 0.17, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.15, 0, 0.1, Math.PI, Math.PI * 1.5);
  shape.absarc(0.15, 0, 0.1, Math.PI * 1.5, Math.PI * 2);
  robot.topBody.face = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0, curveSegments: 12, bevelEnabled: true, bevelThickness: 0.03, bevelSize: 0.03, bevelSegments: 8, bevelOffset: -0.03 }), robotFaceMaterial);
  robot.topBody.face.scale.set(0.94, 0.94, 1);
  robot.topBody.face2 = new THREE.Mesh(new THREE.SphereGeometry(0.1, 16, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), robotFaceMaterial);
  robot.topBody.face2.rotateX(-Math.PI * 0.5);
  robot.topBody.face2.scale.set(2, 0.8, 1.4);
  robot.topBody.face.geometry.translate(0, -0.06, 0);
  bendGeometry(robot.topBody.face.geometry, "y", -4);
  robot.topBody.face.position.z = 0.28;
  robot.topBody.face2.position.set(0, -0.02, 0.21);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.05, 20, 6, 0, Math.PI * 2, Math.PI * 0.4, Math.PI * 0.6);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1.1);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.08, 0, 0.054);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.16, 0, 0);
  robot.topBody.eyes = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotEyesMaterial);
  clearTempGeometries();
  bendGeometry(robot.topBody.eyes.geometry, "y", -3);
  robot.topBody.eyes.position.z = 0.238;
  robot.topBody.head.add(robot.topBody.eyes, robot.topBody.skull, robot.topBody.face, robot.topBody.face2);
	robot.topBody.head.position.y = 0.77;
	robot.topBody.head.rotation.set(0.05, 0.05, 0.05);
	gsap.to(robot.topBody.head.rotation, { duration: 1 + Math.random(), x: -0.05, ease: "power3.inOut", repeat: -1, yoyo: true });
  gsap.to(robot.topBody.head.rotation, { duration: 1 + Math.random(), y: -0.05, ease: "power3.inOut", repeat: -1, yoyo: true });
  gsap.to(robot.topBody.head.rotation, { duration: 1 + Math.random(), z: -0.05, ease: "power3.inOut", repeat: -1, yoyo: true });
  robot.rightHand = new THREE.Object3D();
	robot.rightHand.shoulder = new THREE.Object3D();
	robot.rightHand.shoulder.part = [];
	tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.15, 0.05, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.05, 0.05, 0.1, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-0.15, -0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.05, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.15, -0.25, 0);
  robot.rightHand.shoulder.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotBlueMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.1, 0.1, 12, 12, Math.PI * 0.6);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.05, -0.11, 0);
  robot.rightHand.shoulder.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotWhiteMaterial);
  clearTempGeometries();
  robot.rightHand.shoulder.add(robot.rightHand.shoulder.part[0], robot.rightHand.shoulder.part[1]);
  robot.rightHand.arm = new THREE.Object3D();
  robot.rightHand.arm.part = [];
  robot.rightHand.arm.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.05, 0.05, 0.1, 12, 1, true), robotBlueMaterial);
  robot.rightHand.arm.part[0].geometry.translate(0, -0.05, 0);
  robot.rightHand.arm.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.04, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), robotBlueMaterial);
  robot.rightHand.arm.part[1].position.y = -0.18;
  robot.rightHand.arm.part[2] = new THREE.Mesh(new THREE.SphereGeometry(0.08, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), robotWhiteMaterial);
	robot.rightHand.arm.part[2].geometry.scale(1, 2.8, 1);
  robot.rightHand.arm.position.set(-0.15, -0.25, 0);
  robot.rightHand.hand = new THREE.Object3D();
  robot.rightHand.hand.part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 8, 12, 0);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 1, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.02, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.03, 8, 8, 0);
  tempGeometry[tempGeometry.length - 1].translate(0.015, -0.005, 0.04);
  robot.rightHand.hand.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotBlueMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.017, 0.05, 4, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.025, 0);
  robot.rightHand.hand.part[1] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], robotBlueMaterial);
  robot.rightHand.hand.part[1].position.y = -0.075;
  robot.rightHand.hand.part[2] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], robotBlueMaterial);
  robot.rightHand.hand.part[2].position.set(0, -0.055, -0.035);
  robot.rightHand.hand.part[2].rotation.x = 0.2;
  robot.rightHand.hand.part[3] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], robotBlueMaterial);
  robot.rightHand.hand.part[3].position.set(0, -0.055, 0.035);
  robot.rightHand.hand.part[3].rotation.x = -0.2;
  robot.rightHand.hand.part[4] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], robotBlueMaterial);
  robot.rightHand.hand.part[4].scale.set(1.1, 0.9, 1.1);
  robot.rightHand.hand.part[4].position.set(0.015, -0.005, 0.05);
  robot.rightHand.hand.part[4].rotation.x = -0.5;
  robot.rightHand.hand.part[4].rotation.z = 0.3;
  clearTempGeometries();
  robot.rightHand.hand.add(robot.rightHand.hand.part[0], robot.rightHand.hand.part[1], robot.rightHand.hand.part[2], robot.rightHand.hand.part[3], robot.rightHand.hand.part[4]);
  robot.rightHand.hand.position.set(-0.01, -0.23, 0);
  robot.rightHand.arm.add(robot.rightHand.hand, robot.rightHand.arm.part[0], robot.rightHand.arm.part[2], robot.rightHand.arm.part[1]);
	robot.rightHand.add(robot.rightHand.shoulder, robot.rightHand.arm);
	robot.rightHand.rotation.z = -0.2;
	robot.rightHand.position.set(-0.11, 0.4, 0);
	robot.leftHand = robot.rightHand.clone();
	robot.leftHand.scale.x = -1;
	robot.leftHand.rotation.z = 0.2;
	robot.leftHand.position.set(0.11, 0.4, 0);
	robot.rightHand.rotation.x = -1;
	gsap.to(robot.rightHand.rotation, { duration: 1 + Math.random(), x: -1.1, ease: "power3.inOut", repeat: -1, yoyo: true });
  gsap.to(robot.leftHand.rotation, { duration: 1 + Math.random(), x: 0.2, ease: "power3.inOut", repeat: -1, yoyo: true });
  gsap.to(robot.leftHand.children[1].rotation, { duration: 1 + Math.random(), x: -0.4, ease: "power3.inOut", repeat: -1, yoyo: true });
	robot.rightHand.arm.rotation.x = -1.5;
	robot.rightHand.arm.rotation.z = -0.5;
	gsap.to(robot.rightHand.arm.rotation, { duration: 1 + Math.random(), z: -0.6, ease: "power3.inOut", repeat: -1, yoyo: true });
	robot.rightHand.hand.rotation.y = 0.9;
	gsap.to(robot.rightHand.hand.rotation, { duration: 1 + Math.random(), x: -0.2, ease: "power3.inOut", repeat: -1, yoyo: true });
  robot.topBody.rotation.z = -0.02;
  gsap.to(robot.topBody.rotation, { duration: 2 + Math.random(), z: 0.02, ease: "power3.inOut", repeat: -1, yoyo: true });
	robot.rightHand.hand.part[1].rotation.z = 1;
	robot.rightHand.hand.part[2].rotation.z = 1.3;
	robot.topBody.add(robot.rightHand, robot.leftHand, robot.topBody.head, robot.topBody.collar, robot.topBody.neck, robot.topBody.torso, robot.topBody.logo, robot.topBody.bottom);
	robot.bottom = new THREE.Object3D();
	robot.bottom.part = [];
	robot.bottom.part[0] = new THREE.Mesh(new THREE.SphereGeometry(0.23, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), robotWhiteMaterial);
 	robot.bottom.part[0].geometry.scale(1, 1, 0.8);
 	robot.bottom.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.23, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), robotWhiteMaterial);
 	robot.bottom.part[1].geometry.scale(1, 0.1, 0.8);
  robot.bottom.part[1].geometry.rotateX(Math.PI);
  robot.bottom.part[2] = new THREE.Mesh(new THREE.CylinderGeometry(0.16, 0.16, 0.2, 16, 1, true), robotBlueMaterial);
  robot.bottom.part[2].geometry.scale(1, 1, 0.8);
  robot.bottom.part[2].position.y = 0.05;
  robot.bottom.part[3] = new THREE.Mesh(new THREE.CylinderGeometry(0.08, 0.08, 0.3, 16, 1, true), robotBlueMaterial);
  robot.bottom.part[3].position.set(-0.13, -0.15, 0);
  robot.bottom.part[4] = new THREE.Mesh(new THREE.CylinderGeometry(0.08, 0.08, 0.3, 16, 1, true), robotBlueMaterial);
  robot.bottom.part[4].position.set(0.13, -0.15, 0);
  robot.bottom.add(robot.bottom.part[4], robot.bottom.part[3], robot.bottom.part[0], robot.bottom.part[1], robot.bottom.part[2]);
  robot.bottom.position.y = -0.1;
	robot.add(robot.bottom, robot.topBody);
  guard.material1 = new THREE.MeshToonMaterial({ color: 0x01579B, gradientMap: gradientMap[4] });
  guard.material2 = new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] });
  guard.material3 = new THREE.MeshToonMaterial({ color: 0x4E342E, gradientMap: gradientMap[4] });
  guard.material4 = new THREE.MeshToonMaterial({ color: 0x1A237E, gradientMap: gradientMap[4] });
  guard.material5 = new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] });
	guard.topBody = new THREE.Object3D();
	guard.topBody.torso = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), guard.material1);
	guard.topBody.torso.geometry.scale(1, 2.1, 0.9);
	guard.topBody.torso.geometry.rotateX(Math.PI);
  guard.topBody.buttocks = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), guard.material2);
  guard.topBody.buttocks.geometry.scale(1, 0.7, 0.9);
  guard.topBody.neck = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.07, 0.2, 16, 1, true), heroSkinMaterial);
  guard.topBody.neck.position.y = 0.54;
  guard.topBody.collar = new THREE.Mesh(new THREE.TorusGeometry(0.09, 0.035, 6, 16), guard.material4);
	guard.topBody.collar.rotation.x = Math.PI * 0.5;
	guard.topBody.collar.position.y = 0.47;
	guard.topBody.tie = new THREE.Mesh(new THREE.SphereGeometry(0.252, 3, 10, 0, Math.PI * 0.16, Math.PI * 0.55, Math.PI * 0.35), blackBasicMaterial);
  guard.topBody.tie.geometry.rotateY(-Math.PI * 0.58);
  guard.topBody.tie.geometry.scale(1, 2.1, 0.9);
  guard.topBody.tie.geometry.rotateX(Math.PI);
  guard.topBody.pocket = new THREE.Mesh(new THREE.SphereGeometry(0.252, 3, 3, 0, Math.PI * 0.2, Math.PI * 0.65, Math.PI * 0.1), guard.material4);
  guard.topBody.pocket.geometry.rotateY(-Math.PI * 0.82);
  guard.topBody.pocket.geometry.scale(1, 2.1, 0.9);
  guard.topBody.pocket.geometry.rotateX(Math.PI);
  guard.topBody.belt = new THREE.Mesh(new THREE.CylinderGeometry(0.26, 0.26, 0.05, 16, 1), heroShoesMaterial);
  guard.topBody.belt.scale.set(1, 1, 0.9)
	guard.leg = [];
	tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.14, 0.06, 0.54, 10, 1, true);
	tempGeometry[tempGeometry.length - 1].translate(-0.115, -0.27, 0);
	tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.14, 0.06, 0.54, 10, 1, true);
	tempGeometry[tempGeometry.length - 1].translate(0.115, -0.27, 0);
	guard.leg[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material2);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 10, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(-0.115, -0.62, 0.05);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.23, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.05, 0.05, 0.08, 10, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-0.115, -0.58, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.23, 0, 0);
  guard.leg[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material3);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.07, 0.07, 0.02, 10, 1);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(-0.115, -0.63, 0.05);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.23, 0, 0);
  guard.leg[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  guard.rightHand = new THREE.Object3D();
  guard.rightHand.shoulder = new THREE.Object3D();
  guard.rightHand.shoulder.part = [];
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.15, 0.1, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  guard.rightHand.shoulder.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material1);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.09, 0.1, 12, 1, true);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.09, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.05, 0);
  guard.rightHand.shoulder.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material1);
  clearTempGeometries();
  guard.rightHand.shoulder.part[1].position.set(-0.15, -0.2, 0);
  guard.rightHand.shoulder.add(guard.rightHand.shoulder.part[0], guard.rightHand.shoulder.part[1]);
  guard.rightHand.arm = new THREE.Object3D();
  guard.rightHand.arm.part = [];
  guard.rightHand.arm.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.07, 0.18, 12, 1, true), guard.material1);
  guard.rightHand.arm.part[0].geometry.translate(0, -0.09, 0);
  guard.rightHand.arm.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.06, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), heroSkinMaterial);
  guard.rightHand.arm.part[1].position.y = -0.18;
  guard.rightHand.arm.position.set(-0.15, -0.25, 0);
  guard.rightHand.hand = new THREE.Object3D();
  guard.rightHand.hand.part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 8, 12, 0);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 1, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.02, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.03, 8, 8, 0);
  tempGeometry[tempGeometry.length - 1].translate(0.015, -0.005, 0.04);
  guard.rightHand.hand.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.017, 0.05, 4, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.025, 0);
  guard.rightHand.hand.part[1] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  guard.rightHand.hand.part[1].position.y = -0.075;
  guard.rightHand.hand.part[2] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  guard.rightHand.hand.part[2].position.set(0, -0.055, -0.035);
  guard.rightHand.hand.part[2].rotation.x = 0.2;
  guard.rightHand.hand.part[3] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  guard.rightHand.hand.part[3].position.set(0, -0.055, 0.035);
  guard.rightHand.hand.part[3].rotation.x = -0.2;
  guard.rightHand.hand.part[4] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  guard.rightHand.hand.part[4].scale.set(1.1, 0.9, 1.1);
  guard.rightHand.hand.part[4].position.set(0.015, -0.005, 0.05);
  guard.rightHand.hand.part[4].rotation.x = -0.5;
  guard.rightHand.hand.part[4].rotation.z = 0.3;
  clearTempGeometries();
  guard.rightHand.hand.add(guard.rightHand.hand.part[0], guard.rightHand.hand.part[1], guard.rightHand.hand.part[2], guard.rightHand.hand.part[3], guard.rightHand.hand.part[4]);
  guard.rightHand.hand.scale.set(1.1, 1.1, 1.1);
  guard.rightHand.hand.position.set(-0.01, -0.21, 0);
  guard.rightHand.arm.add(guard.rightHand.hand, guard.rightHand.arm.part[0], guard.rightHand.arm.part[1]);
  guard.rightHand.add(guard.rightHand.shoulder, guard.rightHand.arm);
  guard.rightHand.rotation.z = -0.2;
  guard.rightHand.position.set(-0.12, 0.38, 0);
  guard.leftHand = guard.rightHand.clone();
  guard.leftHand.scale.x = -1;
  guard.leftHand.rotation.z = 0.2;
  guard.leftHand.position.set(0.12, 0.38, 0);
  guard.rightLeg = new THREE.Object3D();
  guard.rightLeg.part = [];
	guard.topBody.add(guard.leftHand, guard.rightHand, guard.topBody.belt, guard.topBody.neck, guard.topBody.pocket, guard.topBody.tie, guard.leg[2], guard.leg[1], guard.leg[0], guard.topBody.torso, guard.topBody.buttocks, guard.topBody.neck, guard.topBody.collar);
	guard.head = new THREE.Object3D();
	tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.33, 0.33, 0.4, 48, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.33, 48, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.07, 0.025, 12, 24, 5);
	tempGeometry[tempGeometry.length - 1].rotateZ(0.6);
  tempGeometry[tempGeometry.length - 1].translate(-0.36, 0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.07, 0.025, 12, 24, 5);
	tempGeometry[tempGeometry.length - 1].rotateZ(0.6);
	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.36, 0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.06, 0.1, 4, 8);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.1, 0.33);
	guard.head.scull = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinMaterial);
	clearTempGeometries();
	guard.head.scull.geometry.translate(0, 0, 0.08);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.07, 20, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.36, 0.01, 0.07);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.07, 20, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.36, 0.01, 0.07);
	guard.head.earsBack = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinDoubleMaterial);
	clearTempGeometries();
	guard.head.eyes = [];
	guard.head.eyes[0] = new THREE.Object3D();
	guard.head.eyes[0].part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.075, 30, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.14, 0, 0.054);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.28, 0, 0);
  guard.head.eyes[0].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
	bendGeometry(guard.head.eyes[0].part[0].geometry, "y", -3.5);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.04, 20, 5, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.4, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.13, 0, 0.069);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.26, 0, 0);
  guard.head.eyes[0].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
	bendGeometry(guard.head.eyes[0].part[1].geometry, "y", -3.5);
	guard.head.eyes[0].add(guard.head.eyes[0].part[0], guard.head.eyes[0].part[1]);
	guard.head.eyes[0].position.set(0, 0.16, 0.357);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.35, 48, 30, 0, Math.PI * 2, Math.PI * 0.43, Math.PI * 0.57);
  tempGeometry[tempGeometry.length - 1].scale(1.05, 1.51, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.36, 48, 24);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.5, 1);
	tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0.05);
	guard.head.hair = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material5);
  clearTempGeometries();
	guard.head.hair.position.set(0, 0.26, 0.03);
	guard.head.mouth = [];
  guard.head.mouth[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.02, 0.02, 0.1, 6, 10), blackBasicMaterial);
  guard.head.mouth[0].geometry.rotateZ(Math.PI * 0.5);
  bendGeometry(guard.head.mouth[0].geometry, "y", -4);
	guard.head.mouth[0].position.set(0, -0.09, 0.38);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.025, 0.025, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(1.4);
  tempGeometry[tempGeometry.length - 1].translate(-0.16, -0.02, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.025, 0.025, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(-1.4);
  tempGeometry[tempGeometry.length - 1].translate(0.16, -0.02, 0);
  guard.head.brows = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), guard.material5);
  clearTempGeometries();
  bendGeometry(guard.head.brows.geometry, "y", -3.5);
  guard.head.brows.position.set(0, 0.31, 0.41);
	guard.head.add(guard.head.brows, guard.head.mouth[0], guard.head.hair, guard.head.scull, guard.head.earsBack, guard.head.eyes[0]);
	guard.head.scale.set(0.85, 0.75, 0.85);
	guard.head.position.set(0, 0.7, 0);
	guard.add(guard.topBody, guard.head);
	guard.scale.set(1.1, 1.1, 1.1);
	guard.position.set(0, 0.7, -0.7);
	guard.rotation.y = 0.3;
  guard.aninationTween1 = gsap.to(guard.head.rotation, { duration: 0.6 + Math.random() * 0.6, x: -0.06, ease: "power1.inOut", repeat: -1, yoyo: true });
  guard.aninationTween2 = gsap.to(guard.head.rotation, { duration: 0.6 + Math.random() * 0.6, z: -0.06, ease: "power1.inOut", repeat: -1, yoyo: true });
	hero.topBody = new THREE.Object3D();
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.28, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.36);
	tempGeometry[tempGeometry.length - 1].scale(1, 2.1, 0.8);
	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.28, 0.28, 0.4, 24, 1, true);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.2, 0);
	hero.topBody.mantle = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[3]);
	clearTempGeometries();
	shape = null;
	shape = new THREE.Shape();
	shape.moveTo(0, -0.3);
	shape.absarc(0, 0, 0.2, Math.PI * 1.8, Math.PI * 3.2);
	shape.lineTo(0, -0.3);
	shape.lineTo(0, -0.15);
	shape.absarc(0, 0, 0.1, Math.PI * 3.2, Math.PI * 1.8, true);
	shape.lineTo(0, -0.15);
	shape.lineTo(0, -0.3);
	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 16);
	tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
	tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "x", -5.3);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -3.2);
  tempGeometry[tempGeometry.length - 1].rotateX(0.3);
  hero.topBody.mantle1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[4]);
	clearTempGeometries();
  hero.topBody.mantle1.position.set(0, 0.58, 0.05);
	hero.topBody.torso = new THREE.Mesh(new THREE.SphereGeometry(0.25, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38), lightHeroShirtMaterial);
	hero.topBody.torso.geometry.scale(1, 2.1, 0.8);
	hero.topBody.torso.geometry.rotateX(Math.PI);
	hero.topBody.torsoInside = new THREE.Mesh(new THREE.CircleGeometry(0.25, 24), darkHeroShirtMaterial);
	hero.topBody.torsoInside.geometry.scale(1, 0.8, 1);
	hero.topBody.torsoInside.geometry.rotateX(Math.PI * 0.5);
	hero.topBody.buttocks = new THREE.Mesh(new THREE.SphereGeometry(0.235, 24, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), heroPantsMaterial);
  hero.topBody.buttocks.geometry.scale(1, 0.7, 0.8);
	hero.topBody.buttocks.geometry.translate(0, 0.02, 0);
  hero.topBody.neck = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.07, 0.2, 16, 1, true), heroSkinMaterial);
  hero.topBody.neck.position.y = 0.54;
  hero.topBody.collar = new THREE.Mesh(new THREE.TorusGeometry(0.09, 0.025, 10, 16), heroSleeveMaterial);
	hero.topBody.collar.rotation.x = Math.PI * 0.5;
	hero.topBody.collar.position.y = 0.48;
	hero.rightHand = new THREE.Object3D();
	hero.rightHand.shoulder = new THREE.Object3D();
	hero.rightHand.shoulder.part = [];
	tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.15, 0.08, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  hero.rightHand.shoulder.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), lightHeroShirtMaterial);
	clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.065, 0.06, 0.1, 12, 1, true);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.05, 0);
  hero.rightHand.shoulder.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSleeveMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.15, 0.09, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  hero.rightHand.shoulder.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[3]);
	clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.09, 0.09, 0.1, 12, 1, true);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.09, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.05, 0);
  hero.rightHand.shoulder.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[3]);
  clearTempGeometries();
  hero.rightHand.shoulder.part[1].position.set(-0.15, -0.19, 0);
  hero.rightHand.shoulder.part[3].position.set(-0.15, -0.19, 0);
  hero.rightHand.shoulder.add(hero.rightHand.shoulder.part[3], hero.rightHand.shoulder.part[2], hero.rightHand.shoulder.part[0], hero.rightHand.shoulder.part[1]);
  hero.rightHand.arm = new THREE.Object3D();
  hero.rightHand.arm.part = [];
  hero.rightHand.arm.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.06, 0.05, 0.18, 12, 1, true), heroSleeveMaterial);
  hero.rightHand.arm.part[0].geometry.translate(0, -0.09, 0);
  hero.rightHand.arm.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.04, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), heroSkinMaterial);
  hero.rightHand.arm.part[1].position.y = -0.18;
  hero.rightHand.arm.part[2] = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.1, 0.22, 12, 1, true), blimMaterial[3]);
  hero.rightHand.arm.part[2].geometry.translate(0, -0.09, 0);
  hero.rightHand.arm.position.set(-0.15, -0.24, 0);
  hero.rightHand.hand = new THREE.Object3D();
  hero.rightHand.hand.part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 8, 12, 0);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 1, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.02, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.03, 8, 8, 0);
  tempGeometry[tempGeometry.length - 1].translate(0.015, -0.005, 0.04);
  hero.rightHand.hand.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.017, 0.05, 4, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.025, 0);
  hero.rightHand.hand.part[1] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  hero.rightHand.hand.part[1].position.y = -0.075;
  hero.rightHand.hand.part[2] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  hero.rightHand.hand.part[2].position.set(0, -0.055, -0.035);
  hero.rightHand.hand.part[2].rotation.x = 0.2;
  hero.rightHand.hand.part[3] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  hero.rightHand.hand.part[3].position.set(0, -0.055, 0.035);
  hero.rightHand.hand.part[3].rotation.x = -0.2;
  hero.rightHand.hand.part[4] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], heroSkinMaterial);
  hero.rightHand.hand.part[4].scale.set(1.1, 0.9, 1.1);
  hero.rightHand.hand.part[4].position.set(0.015, -0.005, 0.05);
  hero.rightHand.hand.part[4].rotation.x = -0.5;
  hero.rightHand.hand.part[4].rotation.z = 0.3;
  clearTempGeometries();
  hero.rightHand.hand.add(hero.rightHand.hand.part[0], hero.rightHand.hand.part[1], hero.rightHand.hand.part[2], hero.rightHand.hand.part[3], hero.rightHand.hand.part[4]);
  hero.rightHand.hand.scale.set(0.9, 0.9, 0.9);
  hero.rightHand.hand.position.set(-0.01, -0.21, 0);
  hero.rightHand.arm.add(hero.rightHand.arm.part[2], hero.rightHand.hand, hero.rightHand.arm.part[0], hero.rightHand.arm.part[1]);
	hero.rightHand.add(hero.rightHand.shoulder, hero.rightHand.arm);
	hero.rightHand.rotation.z = -0.15;
	hero.rightHand.position.set(-0.12, 0.39, 0);
	hero.leftHand = hero.rightHand.clone();
	hero.leftHand.scale.x = -1;
	hero.leftHand.rotation.z = 0.15;
	hero.leftHand.position.set(0.12, 0.39, 0);
	hero.rightLeg = new THREE.Object3D();
	hero.rightLeg.part = [];
	tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.12, 0.08, 0.28, 12, 1, true);
	tempGeometry[tempGeometry.length - 1].translate(0, -0.14, 0);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.08, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.28, 0);
  hero.rightLeg.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroPantsMaterial);
  clearTempGeometries();
  hero.rightLeg.part[1] = new THREE.Object3D();
  hero.rightLeg.part[1].part = [];
  hero.rightLeg.part[1].part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.08, 0.05, 0.24, 12, 1, true), heroPantsMaterial);
  hero.rightLeg.part[1].part[0].geometry.translate(0, -0.12, 0);
  hero.rightLeg.part[1].part[1] = new THREE.Mesh(new THREE.CylinderGeometry(0.04, 0.04, 0.1, 12, 1, true), heroSkinMaterial);
  hero.rightLeg.part[1].part[1].position.y = -0.24;
  hero.rightLeg.part[1].part[2] = new THREE.Mesh(new THREE.SphereGeometry(0.06, 12, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), heroShoesMaterial);
  hero.rightLeg.part[1].part[2].geometry.rotateX(Math.PI);
  hero.rightLeg.part[1].part[2].geometry.scale(1, 1, 1.6);
  hero.rightLeg.part[1].part[2].position.set(0, -0.34, 0.05);
  hero.rightLeg.part[1].part[3] = new THREE.Mesh(new THREE.CylinderGeometry(0.05, 0.05, 0.08, 12, 1, true), heroShoesMaterial);
  hero.rightLeg.part[1].part[3].position.y = -0.3;
  hero.rightLeg.part[1].part[4] = new THREE.Mesh(new THREE.BoxGeometry(0.03, 0.02, 0.06), whiteBasicMaterial);
  hero.rightLeg.part[1].part[4].position.set(0, -0.29, 0.06);
  hero.rightLeg.part[1].part[5] = new THREE.Mesh(new THREE.CylinderGeometry(0.07, 0.07, 0.02, 12, 1), heroSoleMaterial);
  hero.rightLeg.part[1].part[5].geometry.scale(1, 1, 1.6);
  hero.rightLeg.part[1].part[5].position.set(0, -0.35, 0.05);
  hero.rightLeg.part[1].add(hero.rightLeg.part[1].part[0], hero.rightLeg.part[1].part[1], hero.rightLeg.part[1].part[2], hero.rightLeg.part[1].part[3], hero.rightLeg.part[1].part[4], hero.rightLeg.part[1].part[5]);
  hero.rightLeg.part[1].position.y = -0.28;
  hero.rightLeg.add(hero.rightLeg.part[0], hero.rightLeg.part[1]);
  hero.rightLeg.position.x = -0.115;
	hero.leftLeg = hero.rightLeg.clone();
  hero.leftLeg.position.x = 0.115;
	hero.head = new THREE.Object3D();
	hero.head.hat = new THREE.Object3D();
	hero.head.hat.part = [];
	hero.head.hat.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.35, 0.35, 0.2, 16, 1, true), heroFinMaterial[0]);
  hero.head.hat.part[1] = new THREE.Mesh(new THREE.BoxGeometry(1, 0.03, 1), heroFinMaterial[0]);
  hero.head.hat.part[1].position.y = 0.115;
  hero.head.hat.part[1].rotation.y = 0.75;
  hero.head.hat.part[2] = new THREE.Mesh(new THREE.CylinderGeometry(0.34, 0.34, 0.2, 16, 1, true), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4], side: THREE.DoubleSide }));
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0, 0.06, 0.4, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 8, 4);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.18, 0);
  hero.head.hat.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroFinMaterial[2]);
  clearTempGeometries();
  hero.head.hat.part[3].position.set(0.35, 0.1, 0.35);
  gsap.to(hero.head.hat.part[3].rotation, { duration: 0.5, x: 0.3, ease: "power1.inOut", repeat: -1, yoyo: true });
	hero.head.hat.add(hero.head.hat.part[3], hero.head.hat.part[2], hero.head.hat.part[0], hero.head.hat.part[1]);
	hero.head.hat.position.set(0, 0.63, 0);
	hero.head.hat.rotation.x = -0.28
	hero.head.hat.visible = false;
	hero.topBody.mantle.visible = false;
	hero.topBody.mantle1.visible = false;
	hero.rightHand.shoulder.part[2].visible = false;
	hero.rightHand.shoulder.part[3].visible = false;
	hero.rightHand.arm.part[2].visible = false;
	hero.leftHand.children[0].children[0].visible = false;
	hero.leftHand.children[0].children[1].visible = false;
	hero.leftHand.children[1].children[0].visible = false;
	tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.33, 0.33, 0.4, 48, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.33, 48, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.07, 0.025, 12, 24, 5);
	tempGeometry[tempGeometry.length - 1].rotateZ(0.6);
  tempGeometry[tempGeometry.length - 1].translate(-0.36, 0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.07, 0.025, 12, 24, 5);
	tempGeometry[tempGeometry.length - 1].rotateZ(0.6);
	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.36, 0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.12, 16, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(-0.18, -0.02, 0);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "y", -3.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.24);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.04, 0);
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.06, 0.1, 4, 8);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.1, 0.33);
	hero.head.scull = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinMaterial);
	clearTempGeometries();
	hero.head.scull.geometry.translate(0, 0, 0.08);
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.07, 20, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.36, 0.01, 0.07);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.07, 20, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
	tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.36, 0.01, 0.07);
	hero.head.earsBack = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroSkinDoubleMaterial);
	clearTempGeometries();
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.35, 48, 30, 0, Math.PI * 2, Math.PI * 0.43, Math.PI * 0.57);
  tempGeometry[tempGeometry.length - 1].scale(1.05, 1.51, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.36, 48, 24);
	tempGeometry[tempGeometry.length - 1].scale(1.05, 0.5, 1.14);
	tempGeometry[tempGeometry.length - 1].rotateX(-0.2);
	tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0.055);
	hero.head.hair = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroHairMaterial);
  clearTempGeometries();
	hero.head.hair.position.set(0, 0.26, 0.03);
	hero.head.eyes = [];
	hero.head.eyes[0] = new THREE.Object3D();
	hero.head.eyes[0].part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.075, 30, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.14, 0, 0.054);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.28, 0, 0);
  hero.head.eyes[0].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
	bendGeometry(hero.head.eyes[0].part[0].geometry, "y", -3.5);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.04, 20, 5, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.4, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.13, 0, 0.069);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.26, 0, 0);
  hero.head.eyes[0].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
	bendGeometry(hero.head.eyes[0].part[1].geometry, "y", -3.5);
	hero.head.eyes[0].add(hero.head.eyes[0].part[0], hero.head.eyes[0].part[1]);
	hero.head.eyes[0].position.set(0, 0.16, 0.357);
	hero.head.eyes[0].visible = false;
  hero.head.eyes[1] = new THREE.Object3D();
  hero.head.eyes[1].part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.075, 30, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.14, 0, 0.054);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.075, 30, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", 3.5);
  tempGeometry[tempGeometry.length - 1].translate(0.14, 0, 0.054);
  hero.head.eyes[1].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  bendGeometry(hero.head.eyes[1].part[0].geometry, "y", -3.5);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.04, 20, 5, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.4, 1.2);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.13, 0, 0.069);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.26, 0, 0);
  hero.head.eyes[1].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  bendGeometry(hero.head.eyes[1].part[1].geometry, "y", -3.5);
  hero.head.eyes[1].add(hero.head.eyes[1].part[0], hero.head.eyes[1].part[1]);
	hero.head.eyes[1].position.set(0, 0.16, 0.357);
  hero.head.brows = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(-1.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.16, 0.03, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(1.45);
  tempGeometry[tempGeometry.length - 1].translate(0.16, -0.035, 0);
  hero.head.brows[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroHairMaterial);
  clearTempGeometries();
  bendGeometry(hero.head.brows[0].geometry, "y", -3.5);
  hero.head.brows[0].position.set(0, 0.31, 0.41);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(-1.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.16, 0.03, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(1.2);
  tempGeometry[tempGeometry.length - 1].translate(0.16, 0.03, 0);
  hero.head.brows[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroHairMaterial);
  clearTempGeometries();
  bendGeometry(hero.head.brows[1].geometry, "y", -3.5);
  hero.head.brows[1].position.set(0, 0.31, 0.41);
  hero.head.brows[1].visible = false;
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(1.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.16, 0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.12, 8, 10);
  tempGeometry[tempGeometry.length - 1].rotateZ(-1.5);
  tempGeometry[tempGeometry.length - 1].translate(0.16, 0.01, 0);
  hero.head.brows[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroHairMaterial);
  clearTempGeometries();
  bendGeometry(hero.head.brows[2].geometry, "y", -3.5);
  hero.head.brows[2].position.set(0, 0.31, 0.41);
  hero.head.brows[2].visible = false;
  hero.head.mouth = [];
  hero.head.mouth[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.015, 0.015, 0.1, 8, 10), blackBasicMaterial);
  hero.head.mouth[0].geometry.rotateZ(Math.PI * 0.5);
  bendGeometry(hero.head.mouth[0].geometry, "y", -4);
	hero.head.mouth[0].position.set(0, -0.09, 0.38);
	hero.head.mouth[1] = new THREE.Mesh(new THREE.SphereGeometry(0.04, 10, 12), blackBasicMaterial);
  hero.head.mouth[1].geometry.scale(1, 1.4, 0.4);
  hero.head.mouth[1].geometry.rotateX(0.5);
  hero.head.mouth[1].position.set(0, -0.09, 0.38);
  hero.head.mouth[1].visible = false;
  hero.head.mouth[2] = new THREE.Mesh(new THREE.CylinderGeometry(0.015, 0.015, 0.2, 8, 10), blackBasicMaterial);
  hero.head.mouth[2].geometry.rotateZ(Math.PI * 0.5);
  bendGeometry(hero.head.mouth[2].geometry, "z", 4);
  bendGeometry(hero.head.mouth[2].geometry, "y", -4);
  hero.head.mouth[2].geometry.rotateX(0.8);
	hero.head.mouth[2].position.set(0, -0.09, 0.38);
	hero.head.mouth[2].visible = false;
	hero.head.add(hero.head.hat, hero.head.mouth[2], hero.head.mouth[0], hero.head.mouth[1], hero.head.brows[2], hero.head.brows[0], hero.head.brows[1], hero.head.scull, hero.head.earsBack, hero.head.hair, hero.head.eyes[0], hero.head.eyes[1]);
	hero.head.scale.set(0.8, 0.8, 0.8);
	hero.head.position.set(0, 0.75, 0);
	hero.topBody.add(hero.topBody.mantle1, hero.topBody.mantle, hero.leftHand, hero.rightHand, hero.topBody.neck, hero.head, hero.topBody.collar, hero.topBody.torso, hero.topBody.torsoInside, hero.topBody.buttocks);
	hero.add(hero.topBody, hero.rightLeg, hero.leftLeg);
  building.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-1, 0);
  shape.lineTo(-1, 2.2);
  shape.lineTo(1, 2.2);
  shape.lineTo(1, 0);
  shape.lineTo(0.4, 0);
  shape.lineTo(0.4, 1.6);
  shape.lineTo(-0.4, 1.6);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-1.9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-1, 2.2);
  shape.lineTo(-1, 3.6);
  shape.lineTo(1, 3.6);
  shape.lineTo(1, 2.2);
  shape.lineTo(0.2, 2.2);
  shape.lineTo(0.2, 3);
  shape.lineTo(-0.2, 3);
  shape.lineTo(-0.2, 2.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-1.9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(11, 3.6)
  shape.lineTo(0, 3.6);
  shape.lineTo(0, 0);
  shape.lineTo(0.3, 0);
  shape.absarc(1.1, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(1.9, 0);
  shape.lineTo(2.5, 0);
  shape.absarc(3.3, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(4.1, 0);
  shape.lineTo(4.7, 0);
  shape.absarc(5.5, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(6.3, 0);
  shape.lineTo(6.9, 0);
  shape.absarc(7.7, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(8.5, 0);
  shape.lineTo(9.1, 0);
  shape.absarc(9.9, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(10.7, 0);
  shape.lineTo(11, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 0)
  shape.lineTo(0.2, 3.6);
  shape.lineTo(-0.2, 3.6);
  shape.lineTo(-0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-14.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0)
  shape.lineTo(0, 3.6);
  shape.lineTo(-6.3, 3.6);
  shape.lineTo(-6.3, 0);
  shape.lineTo(-5.3, 0);
  shape.absarc(-4.5, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(-3.7, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-13.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.95, 6.75);
  shape.lineTo(2.95, 7);
  shape.lineTo(-2.95, 7);
  shape.lineTo(-2.95, 6.75);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.05, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.76, 7.6);
  shape.lineTo(2.76, 7.66);
  shape.lineTo(-2.76, 7.66);
  shape.lineTo(-2.76, 7.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.86, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.36, 7.6);
  shape.lineTo(0.36, 7.66);
  shape.lineTo(-0.36, 7.66);
  shape.lineTo(-0.36, 7.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.36, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.6, 0, 0.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.2, 9.9);
  shape.lineTo(1.2, 12);
  shape.lineTo(-1.2, 12);
  shape.lineTo(-1.2, 9.9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-11, 7);
  shape.lineTo(-14, 7);
  shape.lineTo(-14, 8);
  shape.lineTo(-11, 8);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.9, 0, 0);
  building.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial1);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.06, 3.6);
  shape.lineTo(1.06, 3.68);
  shape.lineTo(-1.06, 3.68);
  shape.lineTo(-1.06, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.06, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-1.9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 3.6);
  shape.lineTo(11.06, 3.6);
  shape.lineTo(11.06, 3.68);
  shape.lineTo(0, 3.68);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.36, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 3.6);
  shape.lineTo(0, 3.68);
  shape.lineTo(-17.36, 3.68);
  shape.lineTo(-17.36, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.36, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.26, 3.6)
  shape.lineTo(0.26, 3.68);
  shape.lineTo(-0.26, 3.68);
  shape.lineTo(-0.26, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.06, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-14.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.47, 5.1);
  shape.lineTo(0.47, 5.15);
  shape.lineTo(-0.47, 5.15);
  shape.lineTo(-0.47, 5.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 5.15);
  shape.lineTo(0.2, 5.2);
  shape.lineTo(-0.2, 5.2);
  shape.lineTo(-0.2, 5.15);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.15, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(3.16, 7.2);
  shape.lineTo(3.16, 7.26);
  shape.lineTo(-3.16, 7.26);
  shape.lineTo(-3.16, 7.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.26, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 7.2);
  shape.lineTo(11.06, 7.2);
  shape.lineTo(11.06, 7.26);
  shape.lineTo(0, 7.26);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.56, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.8, 7.1);
  shape.lineTo(0, 7.55);
  shape.lineTo(-0.8, 7.1);
  shape.lineTo(-0.84, 7.14);
  shape.lineTo(0, 7.62);
  shape.lineTo(0.84, 7.14);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.36, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 7.2);
  shape.lineTo(18.06, 7.2);
  shape.lineTo(18.06, 7.26);
  shape.lineTo(0, 7.26);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.56, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-20.3, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.75, 5.23);
  shape.lineTo(0.75, 5.28);
  shape.lineTo(-0.75, 5.28);
  shape.lineTo(-0.75, 5.23);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.19, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.76, 9.1);
  shape.lineTo(2.76, 9.16);
  shape.lineTo(-2.76, 9.16);
  shape.lineTo(-2.76, 9.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.86, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.36, 9.1);
  shape.lineTo(0.36, 9.16);
  shape.lineTo(-0.36, 9.16);
  shape.lineTo(-0.36, 9.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.36, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.6, 0, 0.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0)
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.9, 9.9);
  shape.lineTo(1.9, 9.96);
  shape.lineTo(-1.9, 9.96);
  shape.lineTo(-1.9, 9.9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.9, 12.4);
  shape.lineTo(1.9, 12.45);
  shape.lineTo(-1.9, 12.45);
  shape.lineTo(-1.9, 12.4);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  building.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial2);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.8, 3.6)
  shape.lineTo(2.8, 5.4);
  shape.lineTo(-2.8, 5.4);
  shape.lineTo(-2.8, 3.6);
  shape.lineTo(-2.2, 3.6);
  shape.lineTo(-2.2, 4.8);
  shape.lineTo(-1.6, 4.8);
  shape.lineTo(-1.6, 3.6);
  shape.absarc(0, 3.6, 0.9, Math.PI, 0, true);
  shape.lineTo(1.6, 3.6);
  shape.lineTo(1.6, 4.8);
  shape.lineTo(2.2, 4.8);
  shape.lineTo(2.2, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.9, curveSegments: 8, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.8, 5.4)
  shape.lineTo(2.8, 7);
  shape.lineTo(-2.8, 7);
  shape.lineTo(-2.8, 5.4);
  shape.lineTo(-2.2, 5.4);
  shape.lineTo(-2.2, 6.4);
  shape.lineTo(-1.6, 6.4);
  shape.lineTo(-1.6, 5.4);
  shape.lineTo(1.6, 5.4);
  shape.lineTo(1.6, 6.4);
  shape.lineTo(2.2, 6.4);
  shape.lineTo(2.2, 5.4);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.9, curveSegments: 8, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 3.6)
  shape.lineTo(11, 3.6);
  shape.lineTo(11, 7);
  shape.lineTo(0, 7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 3.6);
  shape.lineTo(0, 7);
  shape.lineTo(-17.3, 7);
  shape.lineTo(-17.3, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.7, 7);
  shape.lineTo(2.7, 9);
  shape.lineTo(-2.7, 9);
  shape.lineTo(-2.7, 7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 7);
  shape.lineTo(0.3, 9);
  shape.lineTo(-0.3, 9);
  shape.lineTo(-0.3, 7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.6, 0, 0.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.8, 9);
  shape.lineTo(1.8, 9.8);
  shape.lineTo(-1.8, 9.8);
  shape.lineTo(-1.8, 9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 9.9);
  shape.lineTo(0.3, 12);
  shape.lineTo(-0.3, 12);
  shape.lineTo(-0.3, 9.9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-1.5, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.8, 12);
  shape.lineTo(1.8, 12.3);
  shape.lineTo(-1.8, 12.3);
  shape.lineTo(-1.8, 12);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  building.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial3);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 3.6);
  shape.lineTo(0.2, 7);
  shape.lineTo(-0.2, 7);
  shape.lineTo(-0.2, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.7, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5.4, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 3.6, 0.9, Math.PI, 0, true);
  shape.absarc(0, 3.6, 1.3, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.9);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.9, 6.5);
  shape.lineTo(2.9, 6.75);
  shape.lineTo(-2.9, 6.75);
  shape.lineTo(-2.9, 6.5);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.42, 3.6);
  shape.lineTo(0.42, 5.1);
  shape.lineTo(-0.42, 5.1);
  shape.lineTo(-0.42, 3.6);
  shape.lineTo(-0.25, 3.6);
  shape.lineTo(-0.25, 4.8);
  shape.lineTo(0.25, 4.8);
  shape.lineTo(0.25, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.05, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.1, 4.8);
  shape.lineTo(0.15, 5.15);
  shape.lineTo(-0.15, 5.15);
  shape.lineTo(-0.1, 4.8);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.15, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, 5.5);
  shape.lineTo(0.35, 6.7);
  shape.lineTo(-0.35, 6.7);
  shape.lineTo(-0.35, 5.5);
  shape.lineTo(-0.25, 5.6);
  shape.lineTo(-0.25, 6.6);
  shape.lineTo(0.25, 6.6);
  shape.lineTo(0.25, 5.6);
  shape.lineTo(-0.25, 5.6);
  shape.lineTo(-0.35, 5.5);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.05, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.1, 6.6);
  shape.lineTo(0.1, 6.8);
  shape.lineTo(-0.1, 6.8);
  shape.lineTo(-0.1, 6.6);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.29);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.76, 9);
  shape.lineTo(2.76, 9.1);
  shape.lineTo(-2.76, 9.1);
  shape.lineTo(-2.76, 9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.86, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.36, 9);
  shape.lineTo(0.36, 9.1);
  shape.lineTo(-0.36, 9.1);
  shape.lineTo(-0.36, 9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.36, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.6, 0, 0.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.6 / 3 * 2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.86, 9.8);
  shape.lineTo(1.86, 9.9);
  shape.lineTo(-1.86, 9.9);
  shape.lineTo(-1.86, 9.8);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.06, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.86, 12.3);
  shape.lineTo(1.86, 12.4);
  shape.lineTo(-1.86, 12.4);
  shape.lineTo(-1.86, 12.3);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.06, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-11.02, 3.6);
  shape.lineTo(-14, 3.6);
  shape.lineTo(-14, 7);
  shape.lineTo(-11.02, 7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.9, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-2.9, 0, 0);
  building.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial4);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(3.1, 7);
  shape.lineTo(3.1, 7.2);
  shape.lineTo(-3.1, 7.2);
  shape.lineTo(-3.1, 7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 7);
  shape.lineTo(11, 7);
  shape.lineTo(11, 7.2);
  shape.lineTo(0, 7.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.35, 7.1);
  shape.lineTo(0, 7.3);
  shape.lineTo(0.35, 7.1);
  shape.lineTo(0.8, 7.1);
  shape.lineTo(0, 7.55);
  shape.lineTo(-0.8, 7.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.3, curveSegments: 1, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 7);
  shape.lineTo(18, 7);
  shape.lineTo(18, 7.2);
  shape.lineTo(0, 7.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-20.3, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, 11.85);
  shape.lineTo(0.35, 12);
  shape.lineTo(-0.35, 12);
  shape.lineTo(-0.35, 11.85);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-1.5, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 11.85);
  shape.lineTo(0.2, 12);
  shape.lineTo(-0.2, 12);
  shape.lineTo(-0.2, 11.85);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.45, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.9, 0, 0);
  building.part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial5);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(3, 0);
  shape.lineTo(13.8, 0);
  shape.lineTo(13.8, 3.5);
  shape.lineTo(3, 3.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-3, 0);
  shape.lineTo(-20, 0);
  shape.lineTo(-20, 3.5);
  shape.lineTo(-3, 3.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 0);
  shape.lineTo(2.5, 0);
  shape.lineTo(2.5, 3.5);
  shape.lineTo(1, 3.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 3.6, 0.8, Math.PI, 0, true);
  shape.lineTo(0.8, 3.55);
  shape.lineTo(-0.8, 3.55);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.21);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.8, 3);
  shape.lineTo(-0.8, 3);
  shape.lineTo(-0.8, 2.2);
  shape.lineTo(0.8, 2.2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.205);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`УНИВЕРСИТЕТ ИТМО`, 0.2), 1)
  tempGeometry[tempGeometry.length - 1].translate(-10.5, 3.25, 0.32);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`UNIVERSITY ITMO`, 0.2), 1)
  tempGeometry[tempGeometry.length - 1].translate(6.4, 3.25, 0.32);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 3.6);
  shape.lineTo(0.4, 5);
  shape.lineTo(-0.4, 5);
  shape.lineTo(-0.4, 3.6);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.291);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 5.55);
  shape.lineTo(0.3, 6.65);
  shape.lineTo(-0.3, 6.65);
  shape.lineTo(-0.3, 5.55);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.291);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.3, 3.6);
  shape.lineTo(0.3, 3.6);
  shape.lineTo(0.3, 6.8);
  shape.lineTo(-0.3, 6.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.5, 5.2);
  shape.lineTo(0.5, 5.2);
  shape.lineTo(0.5, 7);
  shape.lineTo(-0.5, 7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.07);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 7.9);
  shape.lineTo(0.25, 7.9);
  shape.lineTo(0.25, 8.5);
  shape.lineTo(-0.25, 8.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-1.75, 0, 0.81);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.3, 10.1);
  shape.lineTo(0.3, 10.1);
  shape.lineTo(0.3, 11.9);
  shape.lineTo(-0.3, 11.9);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.59);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 12.4);
  shape.lineTo(0.25, 12.4);
  shape.lineTo(0.25, 13.8);
  shape.lineTo(-0.25, 13.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  building.part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 0.7);
  shape.lineTo(0.2, 0.7);
  shape.lineTo(0.2, 1.8);
  shape.lineTo(-0.2, 1.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 2.1);
  shape.lineTo(0.2, 2.1);
  shape.lineTo(0.2, 3.4);
  shape.lineTo(-0.2, 3.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.28, 0.7);
  shape.lineTo(0.72, 0.7);
  shape.lineTo(0.72, 1.8);
  shape.lineTo(0.28, 1.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.28, 0.7);
  shape.lineTo(-0.72, 0.7);
  shape.lineTo(-0.72, 1.8);
  shape.lineTo(-0.28, 1.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.28, 2.1);
  shape.lineTo(0.72, 2.1);
  shape.lineTo(0.72, 3.4);
  shape.lineTo(0.28, 3.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.28, 2.1);
  shape.lineTo(-0.72, 2.1);
  shape.lineTo(-0.72, 3.4);
  shape.lineTo(-0.28, 3.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 0.7);
  shape.lineTo(0.3, 1.5);
  shape.lineTo(-0.3, 1.5);
  shape.lineTo(-0.3, 0.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.81);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.15, 2.95);
  shape.lineTo(0.15, 2.65);
  shape.lineTo(-0.15, 2.65);
  shape.lineTo(-0.15, 2.95);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.81);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 3.6, 0.75, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.22);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.75, 2.95);
  shape.lineTo(-0.75, 2.95);
  shape.lineTo(-0.75, 2.25);
  shape.lineTo(0.75, 2.25);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.22);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 0.7);
  shape.lineTo(0.5, 1.7);
  shape.lineTo(-0.5, 1.7);
  shape.lineTo(-0.5, 0.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-16.5, 0, 0.31);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 2.8);
  shape.lineTo(0.5, 2.1);
  shape.lineTo(-0.5, 2.1);
  shape.lineTo(-0.5, 2.8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-16.5, 0, 0.31);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 4.45);
  shape.lineTo(-0.2, 4.75);
  shape.lineTo(0.2, 4.75);
  shape.lineTo(0.2, 4.45);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 4.4);
  shape.lineTo(-0.2, 3.7);
  shape.lineTo(-0.025, 3.7);
  shape.lineTo(-0.025, 4.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 4.4);
  shape.lineTo(0.2, 3.7);
  shape.lineTo(0.025, 3.7);
  shape.lineTo(0.025, 4.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 6.3);
  shape.lineTo(0.2, 6.55);
  shape.lineTo(-0.2, 6.55);
  shape.lineTo(-0.2, 6.3);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 6.25);
  shape.lineTo(0.2, 5.65);
  shape.lineTo(0.025, 5.65);
  shape.lineTo(0.025, 6.25);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 6.25);
  shape.lineTo(-0.2, 5.65);
  shape.lineTo(-0.025, 5.65);
  shape.lineTo(-0.025, 6.25);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 0, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 6.1);
  shape.lineTo(0.25, 6.1);
  shape.lineTo(0.25, 6.35);
  shape.lineTo(-0.25, 6.35);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 6.05);
  shape.lineTo(-0.025, 6.05);
  shape.lineTo(-0.025, 5.5);
  shape.lineTo(-0.25, 5.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.25, 6.05);
  shape.lineTo(0.025, 6.05);
  shape.lineTo(0.025, 5.5);
  shape.lineTo(0.25, 5.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 4.45);
  shape.lineTo(0.25, 4.45);
  shape.lineTo(0.25, 4.75);
  shape.lineTo(-0.25, 4.75);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 4.4);
  shape.lineTo(-0.025, 4.4);
  shape.lineTo(-0.025, 3.7);
  shape.lineTo(-0.25, 3.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.25, 4.4);
  shape.lineTo(0.025, 4.4);
  shape.lineTo(0.025, 3.7);
  shape.lineTo(0.25, 3.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.9, 0, 0.71);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, 6.2);
  shape.lineTo(0.35, 6.95);
  shape.lineTo(-0.35, 6.95);
  shape.lineTo(-0.35, 6.2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.08);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, 5.35);
  shape.lineTo(0.025, 5.35);
  shape.lineTo(0.025, 6.15);
  shape.lineTo(0.35, 6.15);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.08);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.35, 5.35);
  shape.lineTo(-0.025, 5.35);
  shape.lineTo(-0.025, 6.15);
  shape.lineTo(-0.35, 6.15);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 7.95);
  shape.lineTo(-0.025, 7.95);
  shape.lineTo(-0.025, 8.45);
  shape.lineTo(-0.2, 8.45);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-1.75, 0, 0.82);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, 7.95);
  shape.lineTo(0.025, 7.95);
  shape.lineTo(0.025, 8.45);
  shape.lineTo(0.2, 8.45);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-1.75, 0, 0.82);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.75, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 11.3);
  shape.lineTo(0.25, 11.3);
  shape.lineTo(0.25, 11.85);
  shape.lineTo(-0.25, 11.85);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.585);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 11.25);
  shape.lineTo(0.25, 11.25);
  shape.lineTo(0.25, 10.7);
  shape.lineTo(-0.25, 10.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.585);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.25, 10.65);
  shape.lineTo(0.25, 10.65);
  shape.lineTo(0.25, 10.15);
  shape.lineTo(-0.25, 10.15);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.585);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 13.25);
  shape.lineTo(0.2, 13.25);
  shape.lineTo(0.2, 13.75);
  shape.lineTo(-0.2, 13.75);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.02);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.2, 12.35);
  shape.lineTo(0.2, 12.35);
  shape.lineTo(0.2, 13.2);
  shape.lineTo(-0.2, 13.2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.02);
  building.part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial6);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 0);
  shape.lineTo(1, 5.6);
  shape.lineTo(-1, 5.6);
  shape.lineTo(-1, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.2);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 0);
  shape.lineTo(0.5, 2);
  shape.lineTo(-0.5, 2);
  shape.lineTo(-0.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-15.5, 0, 0.31);
  building.part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial2);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.85, 0);
  shape.lineTo(0.85, 1.7);
  shape.lineTo(0.025, 1.7);
  shape.lineTo(0.025, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.4);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.85, 0);
  shape.lineTo(-0.85, 1.7);
  shape.lineTo(-0.025, 1.7);
  shape.lineTo(-0.025, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.4);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.025, 1.7);
  shape.lineTo(0.025, 1.7);
  shape.lineTo(0.025, 2);
  shape.lineTo(-0.025, 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0.4);
  for (let i = 1; i < 9; i++) {
    tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.15, 0);
  shape.lineTo(0.15, 0.3);
  shape.lineTo(-0.15, 0.3);
  shape.lineTo(-0.15, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3.3, 7.15, 0.31);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  building.part[8] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial7);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0.3, 0);
  shape.absarc(1.1, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(1.9, 0);
  shape.lineTo(2.5, 0);
  shape.absarc(3.3, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(4.1, 0);
  shape.lineTo(4.7, 0);
  shape.absarc(5.5, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(6.3, 0);
  shape.lineTo(6.9, 0);
  shape.absarc(7.7, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(8.5, 0);
  shape.lineTo(9.1, 0);
  shape.absarc(9.9, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(10.7, 0);
  shape.lineTo(11, 0);
  shape.absarc(9.9, 2.1, 1.1, 0, Math.PI);
  shape.absarc(7.7, 2.1, 1.1, 0, Math.PI);
  shape.absarc(5.5, 2.1, 1.1, 0, Math.PI);
  shape.absarc(3.3, 2.1, 1.1, 0, Math.PI);
  shape.absarc(1.1, 2.1, 1.1, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(2.9, 0, 0.31);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-5.6, 0);
  shape.lineTo(-5.3, 0);
  shape.absarc(-4.5, 2.1, 0.8, Math.PI, 0, true);
  shape.lineTo(-3.7, 0);
  shape.lineTo(-3.4, 0);
  shape.absarc(-4.5, 2.1, 1.1, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(-13.9, 0, 0.31);
  building.part[9] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial8);
  clearTempGeometries();
  building.part[10] = createSVG(0, 12, 0xFFFFFF);
  building.part[10].position.set(-3.12, 5, 0.9);
  building.part[10].scale.set(0.0055, -0.0055, 1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.13, 0.13, 1.8, 10, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-0.5, 6.1, 1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.16, 0.28);
  tempGeometry[tempGeometry.length - 1].translate(0, 5.2, 1);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.25, 0.16, 0.16);
  tempGeometry[tempGeometry.length - 1].translate(-0.5, 5.1, 1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.4, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(-0.5, 7, 1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1, 0, 0);
  building.part[11] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial5);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.15, 0.15, 2.2, 12, 1, true)
  tempGeometry[tempGeometry.length - 1].translate(-0.45, 10.9, -0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.9, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1.06, 1.06, 0.07, 16, 1)
  tempGeometry[tempGeometry.length - 1].translate(0, 14.5, -1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.5, 4, 1, true)
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 8);
  tempGeometry[tempGeometry.length - 1].translate(Math.cos(Math.PI / 8) * 0.96, 13.5 , -1 + Math.sin(Math.PI / 8) * 0.96);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.5, 4, 1, true)
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 8 * 3);
  tempGeometry[tempGeometry.length - 1].translate(Math.cos(Math.PI / 8 * 3) * 0.96, 13.5 , -1 + Math.sin(Math.PI / 8 * 3) * 0.96);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.5, 4, 1, true)
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 8 * 5);
  tempGeometry[tempGeometry.length - 1].translate(Math.cos(Math.PI / 8 * 5) * 0.96, 13.5 , -1 + Math.sin(Math.PI / 8 * 5) * 0.96);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.5, 4, 1, true)
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 8 * 7);
  tempGeometry[tempGeometry.length - 1].translate(Math.cos(Math.PI / 8 * 7) * 0.96, 13.5 , -1 + Math.sin(Math.PI / 8 * 7) * 0.96);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(3.6, 0.07)
  tempGeometry[tempGeometry.length - 1].translate(0, 12.7, 0.05);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.07, 0.4)
  tempGeometry[tempGeometry.length - 1].translate(-1.7, 12.6, 0.05);
  for (let i = 1; i < 18; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  }
  building.part[12] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial4);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1, 1, 2.5, 16, 1, true)
  tempGeometry[tempGeometry.length - 1].translate(0, 13.5, -1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1.18, 1.1, 0.2, 16, 1)
  tempGeometry[tempGeometry.length - 1].translate(0, 14.8, -1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.9, 0.9, 1, 16, 1, true)
  tempGeometry[tempGeometry.length - 1].translate(0, 15, -1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1, 0.95, 0.1, 16, 1)
  tempGeometry[tempGeometry.length - 1].translate(0, 15.5, -1);
  building.part[13] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial3);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1.23, 1.23, 0.05, 16, 1)
  tempGeometry[tempGeometry.length - 1].translate(0, 14.9, -1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(1.05, 1.05, 0.05, 16, 1)
  tempGeometry[tempGeometry.length - 1].translate(0, 15.55, -1);
  building.part[14] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial2);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.9, 12, 10, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0, 15.55, -1);
  building.part[15] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial9);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.25, 0);
  shape.lineTo(0.25, 0.4);
  shape.lineTo(0, 0.5);
  shape.lineTo(-0.25, 0.4);
  shape.lineTo(-0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(3.3, 7.15, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(1.13, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-16.77, 0, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-1.13, 0, 0);
  }
  building.part[16] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial9);
  clearTempGeometries();
  building.ground1 = new THREE.Mesh(new THREE.BoxGeometry(80, 0.02, 2), buildingMaterial10);
  building.ground1.position.z = 1.3;
  building.ground2 = new THREE.Mesh(new THREE.PlaneGeometry(80, 15), new THREE.MeshBasicMaterial({ color: 0x6D4C41 }));
  building.ground2.rotation.x = -Math.PI * 0.5;
  building.ground2.position.z = 7.2;
  building.cloud = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.5, 24, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].scale(4, 1, 1.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(2, 0.8, 1);
  tempGeometry[tempGeometry.length - 1].translate(-2, -0.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.5, 0.8, 1);
  tempGeometry[tempGeometry.length - 1].translate(1.7, -0.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(2, 1, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(-1, -0.2, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.5, 1, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(1, -0.3, 0);
  tempGeometry[0].scale(1.8, 1.3, 1.2);
  tempGeometry[0].translate(0.2, 0, 0);
  for (let i = 0; i < 6; i++) {
    building.cloud[i] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial11);
  	building.add(building.cloud[i]);
  }
  clearTempGeometries();
  building.cloud[0].scale.set(3, 3, 3);
  building.cloud[0].position.set(-3, 17, -4);
  building.cloud[1].scale.set(-2.5, 2.5, 2.5);
  building.cloud[1].position.set(10, 14, -4);
  building.cloud[2].scale.set(2.5, 2.5, 2.5);
  building.cloud[2].position.set(-15, 12, -4);
  building.cloud[3].scale.set(-2, 2, 2);
  building.cloud[3].position.set(-20, 20, -4);
  building.cloud[4].scale.set(2, 2, 2);
  building.cloud[4].position.set(9, 22, -4);
  building.cloud[5].scale.set(-2, 2, 2);
  building.cloud[5].position.set(-8, 24, -4);
  for (let i = 0; i < 6; i++) {
  	gsap.to(building.cloud[i].position, { duration: 20, x: building.cloud[i].position.x + 5, ease: "none" });
  }
  building.car = new THREE.Object3D();
  building.car.material1 = new THREE.MeshToonMaterial({ color: 0x006064, gradientMap: gradientMap[4] });
  building.car.material2 = new THREE.MeshToonMaterial({ color: 0xAD1457, gradientMap: gradientMap[4] });
  building.car.material3 = new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] });
  building.car.wheel = [];
  building.car.wheel[0] = new THREE.Object3D();
  building.car.wheel[0].part = [];
  building.car.wheel[0].part[0] = new THREE.Mesh(new THREE.TorusGeometry(0.19, 0.1, 12, 24), building.car.material3);
  building.car.wheel[0].part[1] = new THREE.Mesh(new THREE.CircleGeometry(0.2, 8), whiteBasicMaterial);
  building.car.wheel[0].part[2] = new THREE.Mesh(new THREE.CircleGeometry(0.05, 6), building.car.material1);
  building.car.wheel[0].part[2].position.z = 0.01;
  building.car.wheel[0].add(building.car.wheel[0].part[0], building.car.wheel[0].part[2], building.car.wheel[0].part[1]);
  building.car.wheel[0].position.set(0.55, 0, -0.5);
  building.car.wheel[1] = building.car.wheel[0].clone();
  building.car.wheel[2] = building.car.wheel[0].clone();
  building.car.wheel[3] = building.car.wheel[0].clone();
  building.car.wheel[1].position.set(0.55, 0, 0.5);
  building.car.wheel[2].position.set(-0.75, 0, -0.5);
  building.car.wheel[3].position.set(-0.75, 0, 0.5);
  building.car.wheel[0].rotation.y = Math.PI;
  building.car.wheel[2].rotation.y = Math.PI;
  building.car.body = new THREE.Object3D();
  building.car.body.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-1.2, 0);
  shape.lineTo(-1.2, 0.4);
  shape.lineTo(-0.6, 0.5);
  shape.lineTo(-0.4, 0.9);
  shape.lineTo(0.5, 0.9);
  shape.lineTo(0.6, 0.5);
  shape.lineTo(1, 0.4);
  shape.lineTo(1, 0);
  shape.absarc(0.55, 0, 0.3, 0, Math.PI);
  shape.absarc(-0.75, 0, 0.3, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.6);
  building.car.body.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.car.material1);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.35, 0.05, 4, 8, Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.55, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.3, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.3, 0, 0);
  building.car.body.part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.car.material1);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.52, 0.5);
  shape.lineTo(-0.35, 0.85);
  shape.lineTo(0.1, 0.85);
  shape.lineTo(0.1, 0.5);
  building.car.body.part[1] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.22, curveSegments: 1, bevelEnabled: false }), normalMaterial);
  building.car.body.part[1].geometry.translate(0, 0, -0.61);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.15, 0.5);
  shape.lineTo(0.15, 0.85);
  shape.lineTo(0.45, 0.85);
  shape.lineTo(0.52, 0.5);
  building.car.body.part[2] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.22, curveSegments: 1, bevelEnabled: false }), normalMaterial);
  building.car.body.part[2].geometry.translate(0, 0, -0.61);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.61, 0.5);
  shape.lineTo(-0.43, 0.85);
  shape.lineTo(0.52, 0.85);
  shape.lineTo(0.61, 0.5);
  building.car.body.part[3] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.1, curveSegments: 1, bevelEnabled: false }), normalMaterial);
  building.car.body.part[3].geometry.translate(0, 0, -0.55);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.15, 0.1, 1.25);
  tempGeometry[tempGeometry.length - 1].translate(-1.15, 0.05, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.1, 0, 0);
  building.car.body.part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), buildingMaterial2);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.12, 0.12, 0.05, 16, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0.25, 0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.8);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.3, 0.16, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0.08, 0);
  building.car.body.part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.08, 0.08, 0.05, 16, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1, 0.28, 0.45);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.9);
  building.car.body.part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.car.material2);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.26, 0.46);
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0.25, 0);
  building.car.body.part[8] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.car.material3);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.4, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.12, 0.04, 1.21);
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0.4, 0);
  building.car.body.part[9] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  building.car.body.add(building.car.body.part[9], building.car.body.part[8], building.car.body.part[7], building.car.body.part[6], building.car.body.part[5], building.car.body.part[4], building.car.body.part[3], building.car.body.part[2], building.car.body.part[0], building.car.body.part[1]);
  building.car.add(building.car.wheel[1], building.car.wheel[2], building.car.wheel[3], building.car.wheel[0], building.car.body);
  building.car.position.set(20, 0.29, 2.6);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.04, 2);
  tempGeometry[tempGeometry.length - 1].translate(0.9, 0.1, 3.5);
  for (let i = 0; i < 60; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.16, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 0, 0);
  for (let i = 0; i < 60; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.16, 0, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(100, 0.04);
  tempGeometry[tempGeometry.length - 1].translate(50.9, 0.15, 3.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-101.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.7, 0);
  building.fence = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  building.three = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.15, 0.2, 4, 10, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 2, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.6, 0.1, 8, 12, Math.PI * .5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.12, 2.6, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.8, 0.1, 8, 12, Math.PI * .5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.12, 2.6, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 2);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(1, 0.1, 8, 12, Math.PI * .5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.12, 2.6, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 4);
  building.three[0] = new THREE.Object3D();
  building.three[1] = new THREE.Object3D();
  building.three[2] = new THREE.Object3D();
  building.three[3] = new THREE.Object3D();
  building.three[0].material1 = new THREE.MeshToonMaterial({ color: 0x00695C, gradientMap: gradientMap[6] });
  building.three[0].material2 = new THREE.MeshToonMaterial({ color: 0x004D40, gradientMap: gradientMap[6] });
  building.three[0].material3 = new THREE.MeshToonMaterial({ color: 0x2E7D32, gradientMap: gradientMap[6] });
  building.three[0].material4 = new THREE.MeshToonMaterial({ color: 0x1B5E20, gradientMap: gradientMap[6] });
  building.three[0].material5 = new THREE.MeshToonMaterial({ color: 0x4E342E, gradientMap: gradientMap[6] });
  building.three[0].part = [];
  building.three[1].part = [];
  building.three[2].part = [];
  building.three[3].part = [];
  building.three[0].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material5);
  building.three[1].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material5);
  building.three[2].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material5);
  building.three[3].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material5);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.5, 24, 12);
  tempGeometry[tempGeometry.length - 1].translate(0, 4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.2, 1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(0.7, -1.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1.3, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(1, -2.4, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 4);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.4, 1.4, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0.9, -2.4, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(2, 2, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, -4.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.2, 1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(0.9, -2.2, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1);
  tempGeometry[tempGeometry.length - 1].translate(1, -1.5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.2, 1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(0.9, -2.3, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 5);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1.3, 1.3);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.2, 1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1.3, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.3, 0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1.3, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.3, -0.4, -0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.5, 1.5, 1.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, -1.9, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.5, 1.5, 1.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.9, -2.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1.3, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.8, -1.3, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 1.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 1.7);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 1.7);
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.8, 0.4, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 3 * 1.7);
  building.three[0].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material1);
  building.three[1].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material2);
  building.three[2].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material3);
  building.three[3].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.three[0].material4);
  clearTempGeometries();
  building.three[0].add(building.three[0].part[0], building.three[0].part[1]);
  building.three[1].add(building.three[1].part[0], building.three[1].part[1]);
  building.three[2].add(building.three[2].part[0], building.three[2].part[1]);
  building.three[3].add(building.three[3].part[0], building.three[3].part[1]);
  building.three[0].position.set(1.8, 0, 12);
  building.three[1].position.set(-3.5, 0, 8);
  building.three[1].rotation.y = 2;
  building.three[2].position.set(-2.7, 0, 14);
  building.three[3].position.set(7, 0, 5);
  building.three[3].rotation.y = 1;
  building.lantern = [];
  building.lantern[0] = new THREE.Object3D();
  building.lantern[0].part = [];
  building.lantern[0].material1 = new THREE.MeshToonMaterial({ color: 0xFFCC80, gradientMap: gradientMap[6] });
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.15, 0.3, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.15, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.07, 0.1, 0.6, 8, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.45, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1, 12, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.75, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.05, 1.5, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.05, 10, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.02, 6, 8, Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 2.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.05, 0.02, 6, 6, Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.45, 2.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 0.1, 4, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.45, 2.15, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.15, 10, 16);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.3, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.45, 2.1, 0);
  building.lantern[0].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.car.material3);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.13, 10, 16, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.45, 2.1, 0);
  building.lantern[0].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), building.lantern[0].material1);
  clearTempGeometries();
  building.lantern[0].add(building.lantern[0].part[0], building.lantern[0].part[1]);
  building.lantern[1] = building.lantern[0].clone();
  building.lantern[0].position.set(-1.6, 0, 11);
  building.lantern[1].position.set(1.6, 0, 6);
  building.lantern[1].rotation.y = Math.PI;
  building.add(building.lantern[1], building.lantern[0], building.three[0], building.three[1], building.three[2], building.three[3], building.fence, building.car, building.ground2, building.ground1, building.part[16], building.part[15], building.part[14], building.part[13], building.part[12], building.part[11], building.part[10], building.part[9], building.part[8], building.part[0], building.part[1], building.part[2], building.part[3], building.part[4], building.part[5], building.part[6], building.part[7]);
  building.position.z = 2.5;
  building.scale.set(1.4, 1.4, 1.4);
	levelEnvironment[0] = new THREE.Object3D();
	levelEnvironment[0].wall = [];
	levelEnvironment[1] = new THREE.Object3D();
	levelEnvironment[1].wall = [];
	levelEnvironment[2] = new THREE.Object3D();
	levelEnvironment[2].wall = [];
	levelEnvironment[3] = new THREE.Object3D();
	levelEnvironment[3].wall = [];
	levelEnvironment[4] = new THREE.Object3D();
	levelEnvironment[4].wall = [];
	levelEnvironment[0].greenBlimMaterial = new THREE.MeshToonMaterial({ color: 0x64DD17, gradientMap: gradientMap[4] })
	levelEnvironment[0].greenBlimColor = new THREE.Color(0x76FF03);
	gsap.to(levelEnvironment[0].greenBlimMaterial.color, { duration: 0.1, r: levelEnvironment[0].greenBlimColor.r, g: levelEnvironment[0].greenBlimColor.g, b: levelEnvironment[0].greenBlimColor.b, ease: "none", repeat: -1, yoyo: true });
 	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-10, 0);
  shape.lineTo(-10, 10);
  shape.lineTo(10, 10);
  shape.lineTo(10, 0);
  shape.lineTo(4.5, 0);
  shape.lineTo(4.5, 6);
  shape.lineTo(2.5, 6);
  shape.lineTo(2.5, 1.5);
  shape.lineTo(4.5, 1.5);
  shape.lineTo(4.5, 0);
  shape.lineTo(1.2, 0);
  shape.absarc(0, 5, 1.2, 0, Math.PI);
  shape.lineTo(-1.2, 0);
  shape.lineTo(-2.5, 0);
  shape.lineTo(-2.5, 6);
  shape.lineTo(-4.5, 6);
  shape.lineTo(-4.5, 1.5);
  shape.lineTo(-2.5, 1.5);
  shape.lineTo(-2.5, 0);
	levelEnvironment[0].wall[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 8, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  levelEnvironment[0].wall[0].position.x = -1;
  levelEnvironment[0].wall[0].rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].wall[1] = new THREE.Mesh(new THREE.PlaneGeometry(51.8, 10), new THREE.MeshBasicMaterial({ color: 0x607D8B }));
  levelEnvironment[0].wall[1].position.set(24.9, 5, -6);
  levelEnvironment[0].wall[2] = new THREE.Mesh(new THREE.BoxGeometry(5, 10, 4), new THREE.MeshToonMaterial({ color: 0x00897B, gradientMap: gradientMap[4] }));
  levelEnvironment[0].wall[2].position.set(48, 5, -4);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-10, 0);
  shape.lineTo(-10, 20);
  shape.lineTo(10, 20);
  shape.lineTo(10, 0);
  shape.lineTo(1, 0);
  shape.absarc(0, 5.5, 1, 0, Math.PI);
  shape.lineTo(-1, 0);
  levelEnvironment[0].wall[3] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 8, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  levelEnvironment[0].wall[3].rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].wall[3].position.set(50.9, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(48.5, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(24.25, 0.1, -5.99);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(5.02, 0.2, 4.02);
  tempGeometry[tempGeometry.length - 1].translate(48, 0.1, -4);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.2, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(50.65, 0.1, -5.09);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.2, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(50.65, 0.1, 5.09);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.2, 7.79);
  tempGeometry[tempGeometry.length - 1].translate(-1.24, 0.1, 5.09);
  levelEnvironment[0].wall[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x546E7A }));
  clearTempGeometries();
  levelEnvironment[0].wall[5] = new THREE.Mesh(new THREE.BoxGeometry(1, 0.2, 7.79), levelEnvironment[0].wall[4].material);
  levelEnvironment[0].wall[5].geometry.translate(-0.5, 0, 0);
  levelEnvironment[0].wall[5].position.set(-0.99, 0.1, -5.09);
  levelEnvironment[0].window = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.2, 1);
  shape.absarc(0, 6, 2.2, 0, Math.PI);
  shape.lineTo(-2.2, 1);
  shape.lineTo(2.2, 1);
  shape.lineTo(2.1, 1.1);
  shape.lineTo(-2.1, 1.1);
  shape.absarc(0, 6, 2.1, Math.PI, 0, true);
  shape.lineTo(2.1, 1.1);
  shape.lineTo(2.2, 1);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(3, 0, -5.98);
  for (let i = 1; i < 7; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(6.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.7, 1);
  shape.lineTo(-0.8, 1);
  shape.lineTo(-0.8, 8);
  shape.lineTo(-0.7, 8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3, 0, -5.98);
  for (let i = 1; i < 7; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(6.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.7, 1);
  shape.lineTo(0.8, 1);
  shape.lineTo(0.8, 8);
  shape.lineTo(0.7, 8);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3, 0, -5.98);
  for (let i = 1; i < 7; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(6.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-2.2, 6);
  shape.lineTo(2.2, 6);
  shape.lineTo(2.2, 5.9);
  shape.lineTo(-2.2, 5.9);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(3, 0, -5.98);
  for (let i = 1; i < 7; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(6.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-1.2, 6);
  shape.lineTo(1.2, 6);
  shape.lineTo(1.2, 0);
  shape.lineTo(-1.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, -3.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 7);
  levelEnvironment[0].window[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2.15, 1);
  shape.absarc(0, 6, 2.15, 0, Math.PI);
  shape.lineTo(-2.15, 1);
  shape.lineTo(2.15, 1);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(3, 0, -5.99);
  for (let i = 1; i < 7; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(6.2, 0, 0);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.8, 5.8);
  shape.lineTo(-0.05, 5.8);
  shape.lineTo(-0.05, 4.5);
  shape.lineTo(-0.8, 4.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.19, 0, -3.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 7);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.8, 5.8);
  shape.lineTo(0.05, 5.8);
  shape.lineTo(0.05, 4.5);
  shape.lineTo(0.8, 4.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.19, 0, -3.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 7);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.8, 4.4);
  shape.lineTo(0.8, 4.4);
  shape.lineTo(0.8, 0.2);
  shape.lineTo(-0.8, 0.2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.19, 0, -3.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 7);
  levelEnvironment[0].window[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x00838F }));
  clearTempGeometries();
  levelEnvironment[0].floor = new THREE.Object3D();
  levelEnvironment[0].floor.back = new THREE.Mesh(new THREE.PlaneGeometry(53.4, 30), new THREE.MeshBasicMaterial({ color: 0xA1887F }));
  levelEnvironment[0].floor.back.rotation.x = -Math.PI * 0.5;
  levelEnvironment[0].floor.back.position.y = -0.01;
  levelEnvironment[0].floor.back.position.x = -37.98;
  levelEnvironment[0].floor.back.geometry.translate(26.7, 0, 0);
  for (let i = 1; i < 405; i++) {
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.15, 1.15);
    tempGeometry[tempGeometry.length - 1].translate(-38 + 1.2 * (i % 45), -1.2 * Math.floor(i / 45), 0);
  }
  levelEnvironment[0].floor.tiles = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xBCAAA4 }));
  clearTempGeometries();
  levelEnvironment[0].floor.tiles.rotation.x = -Math.PI * 0.5;
  levelEnvironment[0].floor.tiles.position.z = -5.4;
  levelEnvironment[0].floor.add(levelEnvironment[0].floor.back, levelEnvironment[0].floor.tiles);
  levelEnvironment[0].floor.position.x = 35;
  levelEnvironment[0].itmoWord = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`#ITMO`, 1.5), { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x0D47A1, gradientMap: gradientMap[4] }));
  levelEnvironment[0].itmoWord.position.set(1.2, -0.3, -3);
  levelEnvironment[0].itmoWord.rotation.y = 0.3;
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(51.8, 20);
  tempGeometry[tempGeometry.length - 1].translate(-0.4, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1, 14, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(-24.4, 0, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  levelEnvironment[0].ceiling = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].ceiling.rotation.x = Math.PI * 0.5;
  levelEnvironment[0].ceiling.position.set(25.3, 10, 0);
  levelEnvironment[0].add(levelEnvironment[0].itmoWord, levelEnvironment[0].window[1], levelEnvironment[0].wall[5], levelEnvironment[0].wall[4], levelEnvironment[0].window[0], levelEnvironment[0].wall[3], levelEnvironment[0].wall[2], levelEnvironment[0].ceiling, levelEnvironment[0].wall[1], levelEnvironment[0].floor, levelEnvironment[0].wall[0]);
  levelEnvironment[0].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle = []
  levelEnvironment[0].obstacle[0] = new THREE.Object3D();
  levelEnvironment[0].obstacle[0].part = [];
  levelEnvironment[0].monitor1 = new THREE.Object3D();
  levelEnvironment[0].monitor1.part = [];
  levelEnvironment[0].monitor2 = new THREE.Object3D();
  levelEnvironment[0].monitor2.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.8, 0.55, 0.04, 0, Math.PI * 0.5);
  shape.absarc(-0.8, 0.55, 0.04, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.8, -0.55, 0.04, Math.PI, Math.PI * 1.5);
  shape.absarc(0.8, -0.55, 0.04, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[0].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 3, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[0].part[0].geometry.rotateX(-0.1);
  levelEnvironment[0].obstacle[0].part[0].geometry.translate(0, 0.9, 0);
  levelEnvironment[0].monitor1.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 3, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  levelEnvironment[0].monitor1.part[0].geometry.rotateX(-0.1);
  levelEnvironment[0].monitor1.part[0].geometry.translate(0, 0.9, 0);
  levelEnvironment[0].monitor2.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 3, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  levelEnvironment[0].monitor2.part[0].geometry.rotateX(-0.1);
  levelEnvironment[0].monitor2.part[0].geometry.translate(0, 0.9, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.4, 1, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.35, 0.35, 0.05, 16, 1);
  levelEnvironment[0].obstacle[0].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.4, 1, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.35, 0.35, 0.05, 16, 1);
  levelEnvironment[0].monitor1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.4, 1, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.35, 0.35, 0.05, 16, 1);
  levelEnvironment[0].monitor2.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x42A5F5, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.75, 0.5, 0.02, 0, Math.PI * 0.5);
  shape.absarc(-0.75, 0.5, 0.02, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.75, -0.4, 0.02, Math.PI, Math.PI * 1.5);
  shape.absarc(0.75, -0.4, 0.02, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[0].part[2] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 3), heroShoesMaterial);
  levelEnvironment[0].obstacle[0].part[2].geometry.rotateX(-0.1);
  levelEnvironment[0].obstacle[0].part[2].geometry.translate(0, 0.9, 0.21);
  for (let i = 0; i < 8; i++) {
    const lineLength = 0.6 + Math.random() * 0.7;
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(lineLength, 0.04);
    tempGeometry[tempGeometry.length - 1].translate(lineLength * 0.5 - 0.67, 1.3 - 0.1 * i, 0.3);
    levelEnvironment[0].obstacle[0].part[i + 3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[Math.floor(Math.random() * 5)]);
    clearTempGeometries();
    levelEnvironment[0].obstacle[0].part[i + 3].geometry.rotateX(-0.1);
    levelEnvironment[0].obstacle[0].add(levelEnvironment[0].obstacle[0].part[i + 3]);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.75, 0.5, 0.02, 0, Math.PI * 0.5);
  shape.absarc(-0.75, 0.5, 0.02, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.75, -0.4, 0.02, Math.PI, Math.PI * 1.5);
  shape.absarc(0.75, -0.4, 0.02, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[0].part[2] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 3), heroShoesMaterial);
  levelEnvironment[0].obstacle[0].part[2].geometry.rotateX(-0.1);
  levelEnvironment[0].obstacle[0].part[2].geometry.translate(0, 0.9, 0.21);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.75, 0.5, 0.02, 0, Math.PI * 0.5);
  shape.absarc(-0.75, 0.5, 0.02, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.75, -0.4, 0.02, Math.PI, Math.PI * 1.5);
  shape.absarc(0.75, -0.4, 0.02, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].monitor1.part[2] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 3), heroShoesMaterial);
  levelEnvironment[0].monitor1.part[2].geometry.rotateX(-0.1);
  levelEnvironment[0].monitor1.part[2].geometry.translate(0, 0.9, 0.21);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.75, 0.5, 0.02, 0, Math.PI * 0.5);
  shape.absarc(-0.75, 0.5, 0.02, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.75, -0.4, 0.02, Math.PI, Math.PI * 1.5);
  shape.absarc(0.75, -0.4, 0.02, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].monitor2.part[2] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 3), heroShoesMaterial);
  levelEnvironment[0].monitor2.part[2].geometry.rotateX(-0.1);
  levelEnvironment[0].monitor2.part[2].geometry.translate(0, 0.9, 0.21);
  for (let i = 0; i < 8; i++) {
  	const lineLength = 0.6 + Math.random() * 0.7;
  	tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(lineLength, 0.04);
  	tempGeometry[tempGeometry.length - 1].translate(lineLength * 0.5 - 0.67, 1.3 - 0.1 * i, 0.3);
  	levelEnvironment[0].monitor1.part[i + 3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[Math.floor(Math.random() * 5)]);
  	clearTempGeometries();
  	levelEnvironment[0].monitor1.part[i + 3].geometry.rotateX(-0.1);
  	levelEnvironment[0].monitor1.add(levelEnvironment[0].monitor1.part[i + 3]);
  }
  for (let i = 0; i < 8; i++) {
  	const lineLength = 0.6 + Math.random() * 0.7;
  	tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(lineLength, 0.04);
  	tempGeometry[tempGeometry.length - 1].translate(lineLength * 0.5 - 0.67, 1.3 - 0.1 * i, 0.3);
  	levelEnvironment[0].monitor2.part[i + 3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[Math.floor(Math.random() * 5)]);
  	clearTempGeometries();
  	levelEnvironment[0].monitor2.part[i + 3].geometry.rotateX(-0.1);
  	levelEnvironment[0].monitor2.add(levelEnvironment[0].monitor2.part[i + 3]);
  }
  levelEnvironment[0].monitor1.add(levelEnvironment[0].monitor1.part[2], levelEnvironment[0].monitor1.part[0], levelEnvironment[0].monitor1.part[1]);
  levelEnvironment[0].monitor1.part[0].material.color.setHex(0x303F9F);
  levelEnvironment[0].monitor1.part[1].material.color.setHex(0x303F9F);
  levelEnvironment[0].monitor1.position.set(31, 0, -3);
  levelEnvironment[0].monitor1.rotation.y = 0.3;
  levelEnvironment[0].monitor1.scale.set(3, 3, 3);
  levelEnvironment[0].monitor2.add(levelEnvironment[0].monitor2.part[2], levelEnvironment[0].monitor2.part[0], levelEnvironment[0].monitor2.part[1]);
  levelEnvironment[0].monitor2.part[0].material.color.setHex(0x01579B);
  levelEnvironment[0].monitor2.part[1].material.color.setHex(0x01579B);
  levelEnvironment[0].monitor2.position.set(41, 0, -4);
  levelEnvironment[0].monitor2.rotation.y = -0.3;
  levelEnvironment[0].monitor2.scale.set(4, 4, 4);
  levelEnvironment[0].sofa1 = new THREE.Object3D();
  levelEnvironment[0].sofa1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.3, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.2, 0.8);
  tempGeometry[tempGeometry.length - 1].rotateX(-1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.65, -0.4);
  levelEnvironment[0].sofa1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xD81B60, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.3, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(-1, 0.15, 0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2, 0, 0);
  levelEnvironment[0].sofa1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x616161, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].sofa1.add(levelEnvironment[0].sofa1.part[0], levelEnvironment[0].sofa1.part[1]);
  levelEnvironment[0].sofa1.position.set(12.5, 0, -4);
  levelEnvironment[0].sofa2 = levelEnvironment[0].sofa1.clone();
  levelEnvironment[0].sofa2.children[0].material = new THREE.MeshToonMaterial({ color: 0x673AB7, gradientMap: gradientMap[4] });
  levelEnvironment[0].sofa2.position.set(14.3, 0, -3);
  levelEnvironment[0].sofa2.rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].sofa3 = levelEnvironment[0].sofa1.clone();
  levelEnvironment[0].sofa3.children[0].material = new THREE.MeshToonMaterial({ color: 0xFF6F00, gradientMap: gradientMap[4] });
  levelEnvironment[0].sofa3.position.set(33, 0, -4);
  levelEnvironment[0].armchair1 = new THREE.Object3D();
  levelEnvironment[0].armchair1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.3, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.2, 0.8);
  tempGeometry[tempGeometry.length - 1].rotateX(-1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.65, -0.4);
  levelEnvironment[0].armchair1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8F00, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.3, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.15, 0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, 0);
  levelEnvironment[0].armchair1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x616161, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].armchair1.add(levelEnvironment[0].armchair1.part[0], levelEnvironment[0].armchair1.part[1]);
  levelEnvironment[0].armchair1.position.set(11, 0, -3);
  levelEnvironment[0].armchair1.rotation.y = Math.PI * 0.5;
  levelEnvironment[0].armchair2 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair2.children[0].material = new THREE.MeshToonMaterial({ color: 0x512DA8, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair2.position.set(4.5, 0, -3);
  levelEnvironment[0].armchair2.rotation.y = 1;
  levelEnvironment[0].armchair3 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair3.children[0].material = new THREE.MeshToonMaterial({ color: 0x388E3C, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair3.position.set(7, 0, -3);
  levelEnvironment[0].armchair3.rotation.y = -1;
  levelEnvironment[0].armchair4 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair4.children[0].material = new THREE.MeshToonMaterial({ color: 0x0D47A1, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair4.position.set(25, 0, -3);
  levelEnvironment[0].armchair4.rotation.y = 1;
  levelEnvironment[0].armchair5 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair5.children[0].material = new THREE.MeshToonMaterial({ color: 0x6A1B9A, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair5.position.set(27.5, 0, -3);
  levelEnvironment[0].armchair5.rotation.y = -1;
  levelEnvironment[0].armchair6 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair6.children[0].material = new THREE.MeshToonMaterial({ color: 0xF57F17, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair6.position.set(36, 0, -3);
  levelEnvironment[0].armchair6.rotation.y = 0;
  levelEnvironment[0].armchair7 = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].armchair7.children[0].material = new THREE.MeshToonMaterial({ color: 0x006064, gradientMap: gradientMap[4] });
  levelEnvironment[0].armchair7.position.set(35, 0, -2);
  levelEnvironment[0].armchair7.rotation.y = Math.PI * 0.5;
  levelEnvironment[0].chair1 = new THREE.Object3D();
  levelEnvironment[0].chair1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.6, 0.05, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.6, 0.05, 0.6);
  tempGeometry[tempGeometry.length - 1].rotateX(-1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.7, -0.32);
  levelEnvironment[0].chair1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xD81B60, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.4, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(-0.2, 0.2, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.4, 0, 0);
  levelEnvironment[0].chair1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x616161, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].chair1.add(levelEnvironment[0].chair1.part[0], levelEnvironment[0].chair1.part[1]);
  levelEnvironment[0].chair1.position.set(17, 0, -2);
  levelEnvironment[0].chair1.rotation.y = Math.PI * 0.5;
  levelEnvironment[0].chair2 = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].chair2.position.set(18, 0, -3);
  levelEnvironment[0].chair2.rotation.y = 0;
  levelEnvironment[0].chair3 = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].chair3.position.set(19, 0, -2);
  levelEnvironment[0].chair3.rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].chair4 = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].chair4.position.set(21, 0, -1.5);
  levelEnvironment[0].chair4.rotation.y = Math.PI * 0.5;
  levelEnvironment[0].chair5 = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].chair5.position.set(22, 0, -2.5);
  levelEnvironment[0].chair5.rotation.y = 0;
  levelEnvironment[0].chair6 = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].chair6.position.set(23, 0, -1.5);
  levelEnvironment[0].chair6.rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].table1 = new THREE.Object3D();
  levelEnvironment[0].table1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.05, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.7, 0)
  levelEnvironment[0].table1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.7, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(-0.5, 0.35, 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1, 0, 0);
  levelEnvironment[0].table1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x616161, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].table1.add(levelEnvironment[0].table1.part[0], levelEnvironment[0].table1.part[1]);
  levelEnvironment[0].table1.position.set(12.5, 0, -2.7);
  levelEnvironment[0].table2 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table2.position.set(5.75, 0, -2);
  levelEnvironment[0].table3 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table3.position.set(18, 0, -2);
  levelEnvironment[0].table4 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table4.position.set(22, 0, -1.5);
  levelEnvironment[0].table5 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table5.position.set(26.25, 0, -2);
  levelEnvironment[0].table6 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table6.position.set(33, 0, -3);
  levelEnvironment[0].table7 = levelEnvironment[0].table1.clone();
  levelEnvironment[0].table7.position.set(36, 0, -2);
  levelEnvironment[0].grass1 = new THREE.Object3D();
  levelEnvironment[0].grass1.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0.4);
  shape.lineTo(-0.4, 0.4);
  shape.lineTo(-0.4, -0.4);
  shape.lineTo(0.4, -0.4);
  shape.lineTo(0.35, -0.35);
  shape.lineTo(-0.35, -0.35);
  shape.lineTo(-0.35, 0.35);
  shape.lineTo(0.35, 0.35);
  shape.lineTo(0.35, -0.35);
  shape.lineTo(0.4, -0.4);
  levelEnvironment[0].grass1.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 0, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x5D4037, gradientMap: gradientMap[4] }));
  levelEnvironment[0].grass1.part[0].geometry.rotateX(-Math.PI * 0.5);
  levelEnvironment[0].grass1.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(0.6, 0.6), new THREE.MeshToonMaterial({ color: 0x3E2723, gradientMap: gradientMap[4] }));
  levelEnvironment[0].grass1.part[1].geometry.rotateX(-Math.PI * 0.5);
  levelEnvironment[0].grass1.part[1].geometry.translate(0, 0.7, 0);
  levelEnvironment[0].grassMaterial = [
  	new THREE.MeshBasicMaterial({ color: 0x558B2F }),
  	new THREE.MeshBasicMaterial({ color: 0x388E3C }),
  	new THREE.MeshBasicMaterial({ color: 0x2E7D32 })
  ];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.05, 0);
  shape.lineTo(0, 0.7);
  shape.lineTo(-0.05, 0);
  for (let i = 0; i < 3; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  	for (let j = 0; j < 12; j++) {
  	  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  	  tempGeometry[tempGeometry.length - 1].scale(1, 1 + Math.random() * 0.6, 1);
    	tempGeometry[tempGeometry.length - 1].translate(-0.3 + Math.random() * 0.6, 0, -0.35 + Math.random() * 0.7);
    }
  	levelEnvironment[0].grass1.part[i + 2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), levelEnvironment[0].grassMaterial[i]);
  	clearTempGeometries();
  	levelEnvironment[0].grass1.part[i + 2].position.y = 0.7;
  	levelEnvironment[0].grass1.add(levelEnvironment[0].grass1.part[i + 2]);
  }
  levelEnvironment[0].grass1.add(levelEnvironment[0].grass1.part[0], levelEnvironment[0].grass1.part[1]);
  levelEnvironment[0].grass1.position.set(0, 0, -3);
  levelEnvironment[0].grass2 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass2.position.set(9, 0, -2);
  levelEnvironment[0].grass3 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass3.position.set(19, 0, -4.5);
  levelEnvironment[0].grass4 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass4.position.set(22.5, 0, -4.5);
  levelEnvironment[0].grass5 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass5.position.set(26, 0, -4.5);
  levelEnvironment[0].grass6 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass6.position.set(20, 0, -1.5);
  levelEnvironment[0].grass7 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass7.position.set(32, 0, -1.5);
  levelEnvironment[0].grass8 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass8.position.set(38, 0, -1.5);
  levelEnvironment[0].grass9 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass9.position.set(44, 0, -4);
  levelEnvironment[0].grass10 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass10.position.set(49.5, 0, -1.5);
  levelEnvironment[0].grass13 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass13.position.set(40, -0.8, 5);
  levelEnvironment[0].grass14 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass14.position.set(21, -0.8, 5);
  levelEnvironment[0].grass12 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass12.position.set(15, -0.8, 5);
  levelEnvironment[0].grass11 = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].grass11.position.set(10, -0.8, 5);
  levelEnvironment[0].column1 = new THREE.Object3D();
  levelEnvironment[0].column1.part = [];
  levelEnvironment[0].column1.part[0] = new THREE.Mesh(new THREE.BoxGeometry(0.6, 10, 0.6), levelEnvironment[0].wall[0].material);
  levelEnvironment[0].column1.part[0].geometry.translate(0, 5, 0);
  levelEnvironment[0].column1.part[1] = new THREE.Mesh(new THREE.BoxGeometry(0.62, 0.2, 0.62), levelEnvironment[0].wall[4].material);
  levelEnvironment[0].column1.part[1].geometry.translate(0, 0.1, 0);
  levelEnvironment[0].column1.add(levelEnvironment[0].column1.part[0], levelEnvironment[0].column1.part[1]);
  levelEnvironment[0].column1.position.set(2.5, 0, -2);
  levelEnvironment[0].column2 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column2.position.set(8, 0, -1);
  levelEnvironment[0].column3 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column3.position.set(16, 0, -4);
  levelEnvironment[0].column4 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column4.position.set(21, 0, -4);
  levelEnvironment[0].column5 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column5.position.set(28, 0, -4);
  levelEnvironment[0].column6 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column6.position.set(43, 0, -1.5);
  levelEnvironment[0].column7 = levelEnvironment[0].column1.clone();
  levelEnvironment[0].column7.position.set(37, 0, -4);
  levelEnvironment[0].roundLamp1 = new THREE.Object3D();
  levelEnvironment[0].roundLamp1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.5, 0.5, 0.1, 16, 1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 6, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 3, 0);
  levelEnvironment[0].roundLamp1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].roundLamp1.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.5, 16, 4, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), whiteBasicMaterial);
  levelEnvironment[0].roundLamp1.part[1].geometry.translate(0, -0.1, 0);
  levelEnvironment[0].roundLamp1.part[1].scale.y = 0.3;
  levelEnvironment[0].roundLamp1.add(levelEnvironment[0].roundLamp1.part[0], levelEnvironment[0].roundLamp1.part[1]);
  levelEnvironment[0].roundLamp1.position.set(5, 7, 0);
  levelEnvironment[0].roundLamp2 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp2.position.set(21, 8, 0);
  levelEnvironment[0].roundLamp3 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp3.position.set(27, 8, 0);
  levelEnvironment[0].roundLamp4 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp4.position.set(37, 7, 0);
  levelEnvironment[0].roundLamp5 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp5.position.set(41, 8, 0);
  levelEnvironment[0].roundLamp7 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp7.position.set(9, 4, -4);
  levelEnvironment[0].roundLamp8 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp8.position.set(23, 4, -5);
  levelEnvironment[0].roundLamp9 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp9.position.set(25, 5, -3);
  levelEnvironment[0].roundLamp6 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp6.position.set(45, 7, -4);
  levelEnvironment[0].roundLamp10 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[0].roundLamp10.position.set(36, 4, -2);
  levelEnvironment[0].circleLamp1 = new THREE.Object3D();
  levelEnvironment[0].circleLamp1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.8, 0.1, 6, 20);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 6, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.8, 3, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1.6);
  levelEnvironment[0].circleLamp1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  levelEnvironment[0].circleLamp1.part[0].geometry.rotateY(Math.PI * 0.25)
  clearTempGeometries();
  levelEnvironment[0].circleLamp1.part[1] = new THREE.Mesh(new THREE.TorusGeometry(0.8, 0.12, 4, 20), whiteBasicMaterial);
  levelEnvironment[0].circleLamp1.part[1].geometry.rotateX(Math.PI * 0.5);
  levelEnvironment[0].circleLamp1.part[1].geometry.translate(0, -0.23, 0);
  levelEnvironment[0].circleLamp1.part[1].scale.y = 0.3;
  levelEnvironment[0].circleLamp1.add(levelEnvironment[0].circleLamp1.part[0], levelEnvironment[0].circleLamp1.part[1]);
  levelEnvironment[0].circleLamp1.position.set(0.5, 6, 0);
  levelEnvironment[0].circleLamp2 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp2.position.set(12, 7, 0);
  levelEnvironment[0].circleLamp3 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp3.position.set(16, 8, 0);
  levelEnvironment[0].circleLamp4 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp4.position.set(33, 8, 0);
  levelEnvironment[0].circleLamp5 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp5.position.set(47, 7, 0);
  levelEnvironment[0].circleLamp7 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp7.position.set(5.7, 5, -3);
  levelEnvironment[0].circleLamp8 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp8.position.set(12.5, 4, -3);
  levelEnvironment[0].circleLamp9 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp9.position.set(18, 6, -4);
  levelEnvironment[0].circleLamp10 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp10.position.set(31, 7, -4);
  levelEnvironment[0].circleLamp6 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[0].circleLamp6.position.set(40, 7, -5);
  levelEnvironment[0].matrixNum = [];
  levelEnvironment[0].matrixMaterial = [];
  for (let i = 0; i < 5; i++) {
  	levelEnvironment[0].matrixMaterial[i] = new THREE.MeshBasicMaterial({ color: 0x76FF03, transparent: true });
  	gsap.to(levelEnvironment[0].matrixMaterial[i], { duration: 0.05 + Math.random() * 0.1, opacity: 0, ease: "none", repeat: -1, yoyo: true });
  }
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`0`, 0.4), 1);
  tempGeometry[tempGeometry.length - 1].center();
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`1`, 0.4), 1);
  tempGeometry[tempGeometry.length - 1].center();
  for (let i = 0; i < 20; i++) {
  	levelEnvironment[0].matrixNum[i] = new THREE.Mesh(tempGeometry[Math.round(Math.random())], levelEnvironment[0].matrixMaterial[Math.floor(Math.random() * 5)]);
  	levelEnvironment[0].matrixNum[i].position.y = 9.8;
  	levelEnvironment[0].matrixNum[i].tween = gsap.to(levelEnvironment[0].matrixNum[i].position, { duration: 2 + Math.random(), y: 0.2, ease: "none", repeat: -1 });
  	levelEnvironment[0].add(levelEnvironment[0].matrixNum[i]);
  }
  clearTempGeometries();
  levelEnvironment[0].matrixNum[0].position.x = 28;
  levelEnvironment[0].matrixNum[0].position.z = -1;
  levelEnvironment[0].matrixNum[1].position.x = 31;
  levelEnvironment[0].matrixNum[1].position.z = -1;
  levelEnvironment[0].matrixNum[2].position.x = 31.5;
  levelEnvironment[0].matrixNum[2].position.z = -2;
  levelEnvironment[0].matrixNum[3].position.x = 35;
  levelEnvironment[0].matrixNum[3].position.z = -1;
  levelEnvironment[0].matrixNum[4].position.x = 36;
  levelEnvironment[0].matrixNum[4].position.z = -2;
  levelEnvironment[0].matrixNum[5].position.x = 36.5;
  levelEnvironment[0].matrixNum[5].position.z = -1;
  levelEnvironment[0].matrixNum[6].position.x = 38;
  levelEnvironment[0].matrixNum[6].position.z = -3;
  levelEnvironment[0].matrixNum[7].position.x = 39;
  levelEnvironment[0].matrixNum[7].position.z = -1;
  levelEnvironment[0].matrixNum[8].position.x = 39.5;
  levelEnvironment[0].matrixNum[8].position.z = -1.5;
  levelEnvironment[0].matrixNum[9].position.x = 41;
  levelEnvironment[0].matrixNum[9].position.z = -1.5;
  levelEnvironment[0].matrixNum[10].position.x = 41.5;
  levelEnvironment[0].matrixNum[10].position.z = -1;
  levelEnvironment[0].matrixNum[11].position.x = 42;
  levelEnvironment[0].matrixNum[11].position.z = -1;
  levelEnvironment[0].matrixNum[12].position.x = 44;
  levelEnvironment[0].matrixNum[12].position.z = -2;
  levelEnvironment[0].matrixNum[13].position.x = 44.5;
  levelEnvironment[0].matrixNum[13].position.z = -1;
  levelEnvironment[0].matrixNum[14].position.x = 45.5;
  levelEnvironment[0].matrixNum[14].position.z = -1;
  levelEnvironment[0].matrixNum[15].position.x = 46;
  levelEnvironment[0].matrixNum[15].position.z = -5;
  levelEnvironment[0].matrixNum[16].position.x = 47;
  levelEnvironment[0].matrixNum[16].position.z = -4;
  levelEnvironment[0].matrixNum[17].position.x = 49;
  levelEnvironment[0].matrixNum[17].position.z = -2;
  levelEnvironment[0].matrixNum[18].position.x = 34;
  levelEnvironment[0].matrixNum[18].position.z = -5;
  levelEnvironment[0].matrixNum[19].position.x = 33;
  levelEnvironment[0].matrixNum[19].position.z = -4;
  for (let i = 0; i < 20; i++) {
  	levelEnvironment[0].matrixNum[i].position.x -= 4;
  }
  levelEnvironment[0].robotIcon = new THREE.Object3D();
  levelEnvironment[0].robotIcon.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(1, 1, 0.2, 0, Math.PI * 0.5);
  shape.absarc(-1, 1, 0.2, Math.PI * 0.5, Math.PI);
  shape.absarc(-1, -1, 0.2, Math.PI, Math.PI * 1.5);
  shape.absarc(1, -1, 0.2, Math.PI * 1.5, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 2, 0.3, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.1, 1);
  shape.lineTo(0.1, 2);
  shape.lineTo(-0.1, 2);
  shape.lineTo(-0.1, 1);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  levelEnvironment[0].robotIcon.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x546E7A }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.2, -0.7);
  shape.absarc(1.4, -0.5, 0.2, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(1.4, 0.5, 0.2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  shape.lineTo(1.2, 0.7);
  shape.lineTo(1.2, -0.7);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 2, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.2, -0.2);
  shape.lineTo(0, 0.2);
  shape.lineTo(-0.2, -0.2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  levelEnvironment[0].robotIcon.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xD81B60 }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.55, 0.4, 0.4, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.55, 0.4, 0.4, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.55, -0.6, 0.25, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(0.55, -0.6, 0.25, Math.PI * 1.5, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.01);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.55, 0.4, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.03);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.55, 0.4, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.03);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.025, -0.4);
  shape.lineTo(-0.025, -0.85);
  shape.lineTo(0.025, -0.85);
  shape.lineTo(0.025, -0.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(-0.5, 0, 0.05);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 0);
  shape.lineTo(0.5, 0.5);
  shape.lineTo(0.42, 0.42);
  shape.lineTo(0.8, 0.05);
  shape.lineTo(-1, 0.05);
  shape.lineTo(-1, -0.05);
  shape.lineTo(0.8, -0.05);
  shape.lineTo(0.42, -0.42);
  shape.lineTo(0.5, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -1.9, 0);
  levelEnvironment[0].robotIcon.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.55, 0.4, 0.35, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.02);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.55, 0.4, 0.35, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.02);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.55, -0.6, 0.2, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(0.55, -0.6, 0.2, Math.PI * 1.5, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.02);
  levelEnvironment[0].robotIcon.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  levelEnvironment[0].robotIcon.add(levelEnvironment[0].robotIcon.part[3], levelEnvironment[0].robotIcon.part[2], levelEnvironment[0].robotIcon.part[1], levelEnvironment[0].robotIcon.part[0]);
  levelEnvironment[0].robotIcon.position.set(48, 4, -1.99);
  levelEnvironment[0].add(levelEnvironment[0].roundLamp10, levelEnvironment[0].robotIcon, levelEnvironment[0].roundLamp7, levelEnvironment[0].roundLamp8, levelEnvironment[0].roundLamp9, levelEnvironment[0].roundLamp10, levelEnvironment[0].circleLamp7, levelEnvironment[0].circleLamp8, levelEnvironment[0].circleLamp9, levelEnvironment[0].circleLamp10, levelEnvironment[0].circleLamp5, levelEnvironment[0].circleLamp6, levelEnvironment[0].roundLamp4, levelEnvironment[0].roundLamp5, levelEnvironment[0].circleLamp4, levelEnvironment[0].roundLamp2, levelEnvironment[0].roundLamp3, levelEnvironment[0].circleLamp3, levelEnvironment[0].circleLamp2, levelEnvironment[0].circleLamp1, levelEnvironment[0].roundLamp1, levelEnvironment[0].column7, levelEnvironment[0].column4, levelEnvironment[0].column6, levelEnvironment[0].column5, levelEnvironment[0].column3, levelEnvironment[0].column2, levelEnvironment[0].column1, levelEnvironment[0].grass13, levelEnvironment[0].grass14, levelEnvironment[0].grass11, levelEnvironment[0].grass12, levelEnvironment[0].grass8, levelEnvironment[0].grass9, levelEnvironment[0].grass10, levelEnvironment[0].grass6, levelEnvironment[0].grass7, levelEnvironment[0].grass3, levelEnvironment[0].grass4, levelEnvironment[0].grass5, levelEnvironment[0].grass2, levelEnvironment[0].grass1, levelEnvironment[0].armchair7, levelEnvironment[0].armchair6, levelEnvironment[0].table7, levelEnvironment[0].table6, levelEnvironment[0].sofa3, levelEnvironment[0].table5, levelEnvironment[0].armchair4, levelEnvironment[0].armchair5, levelEnvironment[0].table4, levelEnvironment[0].chair4, levelEnvironment[0].chair5, levelEnvironment[0].chair6, levelEnvironment[0].chair3, levelEnvironment[0].chair2, levelEnvironment[0].table3, levelEnvironment[0].chair1, levelEnvironment[0].table2, levelEnvironment[0].armchair3, levelEnvironment[0].armchair2, levelEnvironment[0].armchair1, levelEnvironment[0].sofa2, levelEnvironment[0].table1, levelEnvironment[0].sofa1, levelEnvironment[0].monitor1, levelEnvironment[0].monitor2, levelEnvironment[0].itmoWord, levelEnvironment[0].window[1], levelEnvironment[0].wall[5], levelEnvironment[0].wall[4], levelEnvironment[0].window[0], levelEnvironment[0].wall[3], levelEnvironment[0].wall[2], levelEnvironment[0].ceiling, levelEnvironment[0].wall[1], levelEnvironment[0].floor, levelEnvironment[0].wall[0]);
  levelEnvironment[0].obstacle[0].box = [1.344, 1.192];
  levelEnvironment[0].obstacle[0].ready = true;
  levelEnvironment[0].obstacle[0].rotation.y = 1.6;
  levelEnvironment[0].obstacle[0].scale.set(0.8, 0.8, 0.8);
  levelEnvironment[0].obstacle[0].add(levelEnvironment[0].obstacle[0].part[2], levelEnvironment[0].obstacle[0].part[0], levelEnvironment[0].obstacle[0].part[1]);
  levelEnvironment[0].obstacle[0].position.set(0, 0, -5.8);
  levelEnvironment[0].obstacle[1] = new THREE.Object3D();
  levelEnvironment[0].obstacle[1].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(1, 0.2, 0.15, 0, Math.PI * 0.5);
  shape.absarc(-1, 0.2, 0.15, Math.PI * 0.5, Math.PI);
  shape.absarc(-1, -0.2, 0.15, Math.PI, Math.PI * 1.5);
  shape.absarc(1, -0.2, 0.15, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[1].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 4, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 1, bevelOffset: -0.1 }), new THREE.MeshToonMaterial({ color: 0x66BB6A, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[1].part[0].geometry.translate(0, 0.35, -0.3);
  levelEnvironment[0].obstacle[1].part[1] = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`Enter`, 0.24), 1), heroShoesMaterial);
  levelEnvironment[0].obstacle[1].part[1].geometry.center();
  levelEnvironment[0].obstacle[1].part[1].geometry.translate(0, 0.4, 0.231);
  levelEnvironment[0].obstacle[1].box = [2.3, 0.7];
  levelEnvironment[0].obstacle[1].ready = true;
  levelEnvironment[0].obstacle[1].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[1].position.set(0, 0.5, -11.5);
  levelEnvironment[0].obstacle[1].add(levelEnvironment[0].obstacle[1].part[0], levelEnvironment[0].obstacle[1].part[1]);
  levelEnvironment[0].obstacle[2] = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`HELLO,`, 0.5), { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false }), levelEnvironment[0].greenBlimMaterial);
  levelEnvironment[0].obstacle[2].geometry.computeBoundingBox();
  levelEnvironment[0].obstacle[2].geometry.translate(-0.5 * levelEnvironment[0].obstacle[2].geometry.boundingBox.max.x, 0, -1);
  levelEnvironment[0].obstacle[2].box = [levelEnvironment[0].obstacle[2].geometry.boundingBox.max.x * 2, levelEnvironment[0].obstacle[2].geometry.boundingBox.max.y];
  levelEnvironment[0].obstacle[2].ready = true;
  levelEnvironment[0].obstacle[2].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[2].position.set(0, 2, -18.4);
  levelEnvironment[0].obstacle[3] = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`WORLD!`, 0.5), { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false }), levelEnvironment[0].greenBlimMaterial);
  levelEnvironment[0].obstacle[3].geometry.computeBoundingBox();
  levelEnvironment[0].obstacle[3].geometry.translate(-0.5 * levelEnvironment[0].obstacle[3].geometry.boundingBox.max.x, 0, -1);
  levelEnvironment[0].obstacle[3].box = [levelEnvironment[0].obstacle[3].geometry.boundingBox.max.x * 2, levelEnvironment[0].obstacle[3].geometry.boundingBox.max.y];
  levelEnvironment[0].obstacle[3].ready = true;
  levelEnvironment[0].obstacle[3].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[3].position.set(0, 2, -24.3);
  levelEnvironment[0].obstacle[4] = new THREE.Object3D();
  levelEnvironment[0].obstacle[4].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.2, 0, 0.1, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(0.2, 0, 0.1, Math.PI * 1.5, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 8, bevelEnabled: false })
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  levelEnvironment[0].obstacle[4].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[4].part[1] = new THREE.Mesh(new THREE.BoxGeometry(0.5, 0.4, 0.1), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[4].part[1].geometry.translate(0, 0.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.12, 0.12);
  tempGeometry[tempGeometry.length - 1].translate(-0.125, 0.8, 0.06);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.48, 0.08);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.005, 0);
  levelEnvironment[0].obstacle[4].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  levelEnvironment[0].obstacle[4].part[3] = new THREE.Mesh(new THREE.CylinderGeometry(0.09, 0.09, 0.5, 10, 1, true), levelEnvironment[0].obstacle[4].part[0].material);
  levelEnvironment[0].obstacle[4].part[4] = new THREE.Mesh(new THREE.CylinderGeometry(0.06, 0.06, 2, 10, 1, true), levelEnvironment[0].obstacle[4].part[0].material);
  levelEnvironment[0].obstacle[4].part[4].position.y = -1;
  levelEnvironment[0].obstacle[4].part[5] = createSVG(3, 1, 0xBDBDBD);
  levelEnvironment[0].obstacle[4].part[5].position.set(-0.12, 0.08, 0.11);
  levelEnvironment[0].obstacle[4].part[5].rotation.z = Math.PI * 0.5;
  levelEnvironment[0].obstacle[4].part[5].scale.set(0.001, -0.001, 0.001);
  levelEnvironment[0].obstacle[4].box = [0.48, 0.8];
  levelEnvironment[0].obstacle[4].ready = true;
  levelEnvironment[0].obstacle[4].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[4].position.set(0, 0.5, -29.6);
  levelEnvironment[0].obstacle[4].add(levelEnvironment[0].obstacle[4].part[5], levelEnvironment[0].obstacle[4].part[4], levelEnvironment[0].obstacle[4].part[3], levelEnvironment[0].obstacle[4].part[2], levelEnvironment[0].obstacle[4].part[1], levelEnvironment[0].obstacle[4].part[0]);
  levelEnvironment[0].obstacle[4].scale.set(0.9, 0.9, 0.9);
  levelEnvironment[0].obstacle[5] = levelEnvironment[0].obstacle[4].clone();
  levelEnvironment[0].obstacle[5].box = [0.48, 0.8];
  levelEnvironment[0].obstacle[5].ready = true;
  levelEnvironment[0].obstacle[5].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[5].position.set(0, 1, -33.9);
  levelEnvironment[0].obstacle[6] = new THREE.Object3D();
  levelEnvironment[0].obstacle[6].box = [0, 0];
  levelEnvironment[0].obstacle[6].ready = false;
  levelEnvironment[0].obstacle[7] = new THREE.Object3D();
  levelEnvironment[0].obstacle[7].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.8, 0.2, 0.15, 0, Math.PI * 0.5);
  shape.absarc(-0.8, 0.2, 0.15, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.8, -0.2, 0.15, Math.PI, Math.PI * 1.5);
  shape.absarc(0.8, -0.2, 0.15, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[7].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 4, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 1, bevelOffset: -0.1 }), new THREE.MeshToonMaterial({ color: 0xFFEE58, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[7].part[0].geometry.translate(0, 0.35, -0.3);
  levelEnvironment[0].obstacle[7].part[1] = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`Ctrl`, 0.24), 1), heroShoesMaterial);
  levelEnvironment[0].obstacle[7].part[1].geometry.center();
  levelEnvironment[0].obstacle[7].part[1].geometry.translate(0, 0.4, 0.231);
  levelEnvironment[0].obstacle[7].box = [1.9, 0.7];
  levelEnvironment[0].obstacle[7].ready = true;
  levelEnvironment[0].obstacle[7].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[7].position.set(0, 2, -38.9);
  levelEnvironment[0].obstacle[7].add(levelEnvironment[0].obstacle[7].part[0], levelEnvironment[0].obstacle[7].part[1]);
  levelEnvironment[0].obstacle[8] = new THREE.Object3D();
  levelEnvironment[0].obstacle[8].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.6, 0.2, 0.15, 0, Math.PI * 0.5);
  shape.absarc(-0.6, 0.2, 0.15, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.6, -0.2, 0.15, Math.PI, Math.PI * 1.5);
  shape.absarc(0.6, -0.2, 0.15, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[8].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 4, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 1, bevelOffset: -0.1 }), new THREE.MeshToonMaterial({ color: 0xFFEE58, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[8].part[0].geometry.translate(0, 0.35, -0.3);
  levelEnvironment[0].obstacle[8].part[1] = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`Alt`, 0.24), 1), heroShoesMaterial);
  levelEnvironment[0].obstacle[8].part[1].geometry.center();
  levelEnvironment[0].obstacle[8].part[1].geometry.translate(0, 0.4, 0.231);
  levelEnvironment[0].obstacle[8].box = [1.5, 0.7];
  levelEnvironment[0].obstacle[8].ready = true;
  levelEnvironment[0].obstacle[8].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[8].position.set(0, 2, -43.3);
  levelEnvironment[0].obstacle[8].add(levelEnvironment[0].obstacle[8].part[0], levelEnvironment[0].obstacle[8].part[1]);
  levelEnvironment[0].obstacle[9] = new THREE.Object3D();
  levelEnvironment[0].obstacle[9].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.6, 0.2, 0.15, 0, Math.PI * 0.5);
  shape.absarc(-0.6, 0.2, 0.15, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.6, -0.2, 0.15, Math.PI, Math.PI * 1.5);
  shape.absarc(0.6, -0.2, 0.15, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[9].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 4, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 1, bevelOffset: -0.1 }), new THREE.MeshToonMaterial({ color: 0xE57373, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[9].part[0].geometry.translate(0, 0.35, -0.3);
  levelEnvironment[0].obstacle[9].part[1] = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`Del`, 0.24), 1), heroShoesMaterial);
  levelEnvironment[0].obstacle[9].part[1].geometry.center();
  levelEnvironment[0].obstacle[9].part[1].geometry.translate(0, 0.4, 0.231);
  levelEnvironment[0].obstacle[9].box = [1.5, 0.7];
  levelEnvironment[0].obstacle[9].ready = true;
  levelEnvironment[0].obstacle[9].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[9].position.set(0, 0.75, -47.7);
  levelEnvironment[0].obstacle[9].add(levelEnvironment[0].obstacle[9].part[0], levelEnvironment[0].obstacle[9].part[1]);
  levelEnvironment[1].wall[0] = new THREE.Mesh(new THREE.PlaneGeometry(47.6, 20), new THREE.MeshBasicMaterial({ color: 0xA1887F }));
  levelEnvironment[1].wall[0].position.set(23.8, 9, -5);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-10, 0);
  shape.lineTo(-10, 20);
  shape.lineTo(10, 20);
  shape.lineTo(10, 0);
  shape.lineTo(1, 0);
  shape.lineTo(1, 9);
  shape.lineTo(-1, 9);
  shape.lineTo(-1, 0);
  levelEnvironment[1].wall[1] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 8, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x3F51B5, gradientMap: gradientMap[4] }));
  levelEnvironment[1].wall[1].rotation.y = -Math.PI * 0.5;
  levelEnvironment[1].wall[1].position.set(47.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(47.6, 0.21);
  tempGeometry[tempGeometry.length - 1].translate(23.28, 0.09, -4.98);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.21, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(47.35, 0.09, -5.09);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.21, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(47.35, 0.09, 5.09);
  levelEnvironment[1].wall[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  clearTempGeometries();
  levelEnvironment[1].wall[3] = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 20), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  levelEnvironment[1].wall[3].position.set(-0.1, 9, 5.98);
  levelEnvironment[1].wall[4] = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 20), new THREE.MeshBasicMaterial({ color: 0x3F51B5 }));
  levelEnvironment[1].wall[4].position.set(47.35, 9, 5.98);
  levelEnvironment[1].floorBack = new THREE.Mesh(new THREE.PlaneGeometry(48.3, 12), new THREE.MeshBasicMaterial({ color: 0x78909C }));
  levelEnvironment[1].floorBack.rotation.x = -Math.PI * 0.5;
  levelEnvironment[1].floorBack.position.set(23.45, -0.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.95, 11);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  for (let i = 1; i < 48; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.998, 0, 0);
  }
  levelEnvironment[1].floorTop = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x90A4AE }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(47.6, 12);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1, 14, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(-22.4, 0, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4, 0, 0);
  levelEnvironment[1].ceiling = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[1].ceiling.rotation.x = Math.PI * 0.5;
  levelEnvironment[1].ceiling.position.set(23.8, 11, 0);
  levelEnvironment[1].plug = new THREE.Object3D();
  levelEnvironment[1].plug.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.4, 0)
  shape.absarc(0, -0.2, 0.4, Math.PI, Math.PI * 2);
  shape.lineTo(0.4, 0)
  shape.lineTo(-0.4, 0)
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 5, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 4, bevelOffset: -0.1 });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.8, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.55, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.8, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.4, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.06, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 4.5, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.15, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  levelEnvironment[1].plug.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[1].plug.part[1] = new THREE.Mesh(new THREE.CircleGeometry(0.09, 6), new THREE.MeshBasicMaterial({ color: 0x424242 }));
  levelEnvironment[1].plug.part[1].geometry.translate(0, 4.5, 0.21);
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.08, 0.6, 3, 6);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 4.9, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.5, 0, 0);
  levelEnvironment[1].plug.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[1].plug.add(levelEnvironment[1].plug.part[2], levelEnvironment[1].plug.part[1], levelEnvironment[1].plug.part[0]);
  levelEnvironment[1].plug.position.set(4, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 0);
  for (let i = 1; i < 7; i++) {
    shape.lineTo(0.3 * Math.cos(Math.PI / 3 * i), 0.3 * Math.sin(Math.PI / 3 * i));
  }
  let holePath = new THREE.Path()
  holePath.absarc(0, 0, 0.13, 0, Math.PI * 1.999);
	shape.holes.push(holePath);
  levelEnvironment[0].obstacle[10] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 24, bevelEnabled: true, bevelSize: 0.03, bevelThickness: 0.03, bevelOffset: -0.025, bevelSegments: 1 }), new THREE.MeshToonMaterial({ color: 0x3E2723, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[10].geometry.translate(0, 0.28, 0);
  levelEnvironment[0].obstacle[10].box = [0.7, 1.05];
  levelEnvironment[0].obstacle[10].ready = true;
  levelEnvironment[0].obstacle[10].scale.set(2, 2, 2);
  levelEnvironment[0].obstacle[10].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[10].position.set(0, 0, -59.7);
  levelEnvironment[0].obstacle[11] = new THREE.Object3D();
  levelEnvironment[0].obstacle[11].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.5, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.45, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.06, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.25, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, -0.3);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.8);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, 0.2);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.08, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.05, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.3, 0);
  levelEnvironment[0].obstacle[11].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE64A19, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[11].part[0].geometry.translate(0.25, 0, 0);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.35, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.3, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0.56, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.12, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.3, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1.6, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(0.1, 0);
  shape.lineTo(0.06, -0.2);
  shape.lineTo(0.06, -0.05);
  shape.lineTo(0.03, -0.05);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].scale(2, 2, 2);
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.25, -0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 0, 0);
  levelEnvironment[0].obstacle[11].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF57C00, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[11].part[1].geometry.translate(0.25, 0, 0);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.25, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1.6, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.6, -0.1);
  levelEnvironment[0].obstacle[11].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xD32F2F, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[11].part[2].geometry.translate(0.25, 0, 0);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.05, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.6, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.21, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.41, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1.6, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.1, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.31, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.6, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.1, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.23, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.05, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.43, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1.6, -0.1);
  levelEnvironment[0].obstacle[11].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[11].part[3].geometry.translate(0.25, 0, 0);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 0.9, 0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.1, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.42, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1.6, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.05, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.32, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 1.6, -0.1);
  levelEnvironment[0].obstacle[11].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotWhiteMaterial);
  levelEnvironment[0].obstacle[11].part[4].geometry.translate(0.25, 0, 0);
  clearTempGeometries();
  levelEnvironment[0].obstacle[11].add(levelEnvironment[0].obstacle[11].part[4], levelEnvironment[0].obstacle[11].part[3], levelEnvironment[0].obstacle[11].part[2], levelEnvironment[0].obstacle[11].part[1], levelEnvironment[0].obstacle[11].part[0]);
  levelEnvironment[0].obstacle[11].box = [1.5 * 1.2, 1.7 * 1.2];
  levelEnvironment[0].obstacle[11].ready = true;
  levelEnvironment[0].obstacle[11].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[0].obstacle[11].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[11].position.set(0, 0, -64.9);
  levelEnvironment[0].obstacle[12] = new THREE.Object3D();
  levelEnvironment[0].obstacle[12].part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 12, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.2, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  levelEnvironment[0].obstacle[12].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFBC02D, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.07, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.21, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.25, 0.25, 0.06, 0, Math.PI * 0.5);
  shape.absarc(-0.25, 0.25, 0.06, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.25, -0.25, 0.06, Math.PI, Math.PI * 1.5);
  shape.absarc(0.25, -0.25, 0.06, Math.PI * 1.5, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.95, -0.2);
  levelEnvironment[0].obstacle[12].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(0.4, 0.4);
  shape.lineTo(0.2, 0.8);
  shape.lineTo(0.25, 0.42);
  shape.lineTo(0, 0.2);
  shape.lineTo(-0.25, 0.42);
  shape.lineTo(-0.2, 0.8);
  shape.lineTo(-0.4, 0.4);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4, -0.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.1);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 0.6, 0.6);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0.85, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.25, 1.6);
  shape.lineTo(0.95, 1.6);
  shape.lineTo(0.95, 0.7);
  shape.lineTo(0.25, 0.7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.2);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.13, 0);
  shape.lineTo(0.13, 0.1);
  shape.lineTo(-0.13, 0.1);
  shape.lineTo(-0.13, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, -0.15);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  levelEnvironment[0].obstacle[12].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x0D47A1, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.13, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.13, -0.11);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.1, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.3, 4.4, -0.11);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.2, 1.95, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.08, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.16, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1.12, 0.77, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.06, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.16, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1.12, 0.6, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.4);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.12, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.28, 0.14, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.05, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  levelEnvironment[0].obstacle[12].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.4, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.9, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.1, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 1.7, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.1, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.95, 1.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.025, 0.025, 0.8, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 2.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.8, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.4, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(1.2, 1.05, 0);
  levelEnvironment[0].obstacle[12].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1, 8, 3);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 2.6, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.08, 0);
  shape.lineTo(0, 0.1);
  shape.lineTo(-0.08, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.88, 0.21);
  levelEnvironment[0].obstacle[12].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.11, 12);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 2.05, 0.21);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.32, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.05, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 1.75, 0.21);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  levelEnvironment[0].obstacle[12].part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[0]);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, -0.05);
  shape.absarc(0, 0, 0.3, 0, Math.PI);
  shape.lineTo(-0.3, -0.05);
  shape.lineTo(0.3, -0.05);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.21);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.06, 12);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 2.05, 0.22);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.32, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.025, 0);
  shape.lineTo(0.025, 0.2);
  shape.lineTo(-0.025, 0.2);
  shape.lineTo(-0.025, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.23);
  levelEnvironment[0].obstacle[12].part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.25, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.22);
  levelEnvironment[0].obstacle[12].part[8] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  levelEnvironment[0].obstacle[12].add(levelEnvironment[0].obstacle[12].part[8], levelEnvironment[0].obstacle[12].part[7], levelEnvironment[0].obstacle[12].part[6], levelEnvironment[0].obstacle[12].part[5], levelEnvironment[0].obstacle[12].part[4], levelEnvironment[0].obstacle[12].part[2], levelEnvironment[0].obstacle[12].part[1], levelEnvironment[0].obstacle[12].part[3], levelEnvironment[0].obstacle[12].part[0]);
  levelEnvironment[0].obstacle[12].box = [0.01, 5];
  levelEnvironment[0].obstacle[12].ready = true;
  levelEnvironment[0].obstacle[12].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[12].position.set(0, 0, -70.75);
  for (let i = 0; i < 13; i++) {
  	levelEnvironment[0].obstacle[i].dir = 3;
  }
  levelEnvironment[0].obstacle[12].dir = 4;
  levelEnvironment[0].obstacle[13] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0.4);
  shape.lineTo(-0.4, 0.4);
  shape.lineTo(-0.4, -0.4);
  shape.lineTo(0.4, -0.4);
  shape.lineTo(0.35, -0.35);
  shape.lineTo(-0.35, -0.35);
  shape.lineTo(-0.35, 0.35);
  shape.lineTo(0.35, 0.35);
  shape.lineTo(0.35, -0.35);
  shape.lineTo(0.4, -0.4);
  levelEnvironment[0].obstacle[13].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 0, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[13].part[0].geometry.scale(1.25, 1.25, 1.25);
  levelEnvironment[0].obstacle[13].part[0].geometry.translate(0, 1.8, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.9, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.8, 0.4);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.06, 12);
  tempGeometry[tempGeometry.length - 1].translate(-0.21, 1.9, 0.42);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.42, 0, 0);
  levelEnvironment[0].obstacle[13].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x212121 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.17, 12);
  tempGeometry[tempGeometry.length - 1].translate(-0.21, 1.9, 0.41);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.42, 0, 0);
  levelEnvironment[0].obstacle[13].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[0]);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.3, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.15, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.03, 1, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.85, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.03, 1.1, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.08, 0.85, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.38, 0.425, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.76, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.36, 0, 0);
  levelEnvironment[0].obstacle[13].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.4, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.95, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.35, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.275, 0.85, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.55, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.35, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.275, 0.45, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.55, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.3, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.275, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.55, 0, 0);
  levelEnvironment[0].obstacle[13].part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[13].part[4] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part[4].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0);
  shape.lineTo(0, 0.6);
  shape.lineTo(-0.4, 0);
  levelEnvironment[0].obstacle[13].part[4].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x5D4037, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[13].part[4].part[0].geometry.translate(0, 0, -0.1);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.09, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0.01);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.02, 1.05);
  tempGeometry[tempGeometry.length - 1].translate(-0.045, 0.7, 0.03);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.045, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.045, 0, 0);
  levelEnvironment[0].obstacle[13].part[4].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.12, 0.6, 0.04);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.75, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.16, 0.3, 0.04);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.1, 0);
  levelEnvironment[0].obstacle[13].part[4].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[13].part[4].add(levelEnvironment[0].obstacle[13].part[4].part[2], levelEnvironment[0].obstacle[13].part[4].part[1], levelEnvironment[0].obstacle[13].part[4].part[0]);
  levelEnvironment[0].obstacle[13].part[4].scale.set(1.5, 1.5, 1.5);
  levelEnvironment[0].obstacle[13].part[4].position.set(-0.5, 0.6, 0.4);
  levelEnvironment[0].obstacle[13].part[4].rotation.z = -1.1;
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.18, 0);
  shape.lineTo(0.18, 0.1);
  shape.lineTo(-0.18, 0.1);
  shape.lineTo(-0.18, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 0, bevelEnabled: false })
  tempGeometry[tempGeometry.length - 1].translate(-0.275, 0, -0.15);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.55, 0, 0);
  levelEnvironment[0].obstacle[13].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[13].part[7] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part[7].part = [];
  levelEnvironment[0].obstacle[13].part[7].part[0] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part[7].part[0].part = [];
  levelEnvironment[0].obstacle[13].part[7].part[0].part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.1, 0.1, 0.2, 6, 1), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[13].part[7].part[0].part[0].rotation.x = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[0].part[1] = new THREE.Mesh(new THREE.CylinderGeometry(0.06, 0.06, 0.5, 6, 1), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[13].part[7].part[0].part[1].position.y = 0.25;
  levelEnvironment[0].obstacle[13].part[7].part[0].add(levelEnvironment[0].obstacle[13].part[7].part[0].part[1], levelEnvironment[0].obstacle[13].part[7].part[0].part[0]);
  levelEnvironment[0].obstacle[13].part[7].part[1] = levelEnvironment[0].obstacle[13].part[7].part[0].clone();
  levelEnvironment[0].obstacle[13].part[7].part[1].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[3] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part[7].part[3].part = [];
  levelEnvironment[0].obstacle[13].part[7].part[3].part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.1, 0.1, 0.4, 6, 1), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[13].part[7].part[3].part[0].rotation.x = Math.PI * 0.5;
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.4, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.08);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.08);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.08);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.06, 0.2, 0.06);
  tempGeometry[tempGeometry.length - 1].rotateX(0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.1, 0.24);
  levelEnvironment[0].obstacle[13].part[7].part[3].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[13].part[7].part[3].add(levelEnvironment[0].obstacle[13].part[7].part[3].part[1], levelEnvironment[0].obstacle[13].part[7].part[3].part[0]);
  levelEnvironment[0].obstacle[13].part[7].part[3].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[3].position.y = 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[2] = new THREE.Object3D();
  levelEnvironment[0].obstacle[13].part[7].part[2].add(levelEnvironment[0].obstacle[13].part[7].part[3], levelEnvironment[0].obstacle[13].part[7].part[1]);
  levelEnvironment[0].obstacle[13].part[7].part[2].position.y = 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[2].rotation.x = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].part[7].part[2].rotation.z = -Math.PI * 0.3;
  levelEnvironment[0].obstacle[13].part[7].add(levelEnvironment[0].obstacle[13].part[7].part[0], levelEnvironment[0].obstacle[13].part[7].part[2]);
  levelEnvironment[0].obstacle[13].part[7].rotation.z = Math.PI * 0.1;
  levelEnvironment[0].obstacle[13].part[7].rotation.x = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].part[7].position.set(-0.55, 1.2, 0);
  levelEnvironment[0].obstacle[13].part[8] = levelEnvironment[0].obstacle[13].part[7].clone();
  levelEnvironment[0].obstacle[13].part[8].scale.x = -1;
  levelEnvironment[0].obstacle[13].part[8].position.set(0.55, 1.2, 0);
  levelEnvironment[0].obstacle[13].part[8].rotation.x = Math.PI * 0.85;
  levelEnvironment[0].obstacle[13].part[8].rotation.z = -Math.PI * 0.1;
  levelEnvironment[0].obstacle[13].part[8].children[1].rotation.z = Math.PI;
  levelEnvironment[0].obstacle[13].part[8].children[1].children[0].rotation.x = 1.2;
  levelEnvironment[0].obstacle[13].add(levelEnvironment[0].obstacle[13].part[8], levelEnvironment[0].obstacle[13].part[7], levelEnvironment[0].obstacle[13].part[5], levelEnvironment[0].obstacle[13].part[6], levelEnvironment[0].obstacle[13].part[4], levelEnvironment[0].obstacle[13].part[3], levelEnvironment[0].obstacle[13].part[2], levelEnvironment[0].obstacle[13].part[1], levelEnvironment[0].obstacle[13].part[0]);
  levelEnvironment[0].obstacle[13].box = [1, 2.3];
  levelEnvironment[0].obstacle[13].ready = true;
  levelEnvironment[0].obstacle[13].dir = 3;
  levelEnvironment[0].obstacle[13].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[13].position.set(0, 0, -81.3);
  levelEnvironment[0].obstacle[14] = new THREE.Object3D();
  levelEnvironment[0].obstacle[14].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.04, 7);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.02, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.24, 0);
  levelEnvironment[0].obstacle[14].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.7, 0.2, 6.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.14, -1.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.2, 2, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, -1, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  levelEnvironment[0].obstacle[14].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.08, 0.08);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.14, 1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.08, 0.08, 0.08);
  tempGeometry[tempGeometry.length - 1].translate(-0.65, 0.14, 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.3, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, -1.9, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.1, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, -0.05, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  levelEnvironment[0].obstacle[14].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x3949AB, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[14].add(levelEnvironment[0].obstacle[14].part[2], levelEnvironment[0].obstacle[14].part[1], levelEnvironment[0].obstacle[14].part[0]);
  levelEnvironment[0].obstacle[14].box = [1.8, 0.26];
  levelEnvironment[0].obstacle[14].ready = true;
  levelEnvironment[0].obstacle[14].dir = 3;
  levelEnvironment[0].obstacle[14].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[14].position.set(0, 2, -76.7);
  levelEnvironment[0].obstacle[15] = new THREE.Object3D();
  levelEnvironment[0].obstacle[15].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.04, 7);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.02, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.24, 0);
  levelEnvironment[0].obstacle[15].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.7, 0.2, 6.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.14, -1.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.2, 4.5, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(0, -1.25, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  levelEnvironment[0].obstacle[15].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00796B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.08, 0.08);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.14, 1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.08, 0.08, 0.08);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 1.14, 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.3, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(0, -3.4, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.1, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.95, -2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2);
  levelEnvironment[0].obstacle[15].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x006064, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(0.35, -0.1);
  shape.lineTo(0.35, -0.4);
  shape.lineTo(0.15, -0.5);
  shape.lineTo(0.15, -0.45);
  shape.lineTo(0.25, -0.37);
  shape.lineTo(0.25, -0.2);
  shape.lineTo(0, -0.13);
  shape.lineTo(-0.25, -0.2);
  shape.lineTo(-0.25, -0.37);
  shape.lineTo(-0.15, -0.45);
  shape.lineTo(-0.15, -0.5);
  shape.lineTo(-0.35, -0.4);
  shape.lineTo(-0.35, -0.1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 0, bevelEnabled: false })
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, -0.2);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.3, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.04, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.74, 0);
  levelEnvironment[0].obstacle[15].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE65100, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.17, 0.17, 0.25, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.83, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.13, 0.13, 0.25, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, 0);
  levelEnvironment[0].obstacle[15].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x4A148C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.12, 0.12, 0.42, 8, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.45, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.08, 0.08, 0.42, 8, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.29, 0.35, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.58, 0, 0);
  levelEnvironment[0].obstacle[15].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[15].add(levelEnvironment[0].obstacle[15].part[5], levelEnvironment[0].obstacle[15].part[4], levelEnvironment[0].obstacle[15].part[3], levelEnvironment[0].obstacle[15].part[2], levelEnvironment[0].obstacle[15].part[1], levelEnvironment[0].obstacle[15].part[0]);
  levelEnvironment[0].obstacle[15].box = [0.8, 1.26];
  levelEnvironment[0].obstacle[15].ready = true;
  levelEnvironment[0].obstacle[15].dir = 4;
  levelEnvironment[0].obstacle[15].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[15].position.set(0, 3.5, -87.2);
  levelEnvironment[0].obstacle[16] = levelEnvironment[0].obstacle[11].clone();
  levelEnvironment[0].obstacle[16].box = [1.5 * 1.5, 1.7 * 1.5];
  levelEnvironment[0].obstacle[16].ready = true;
  levelEnvironment[0].obstacle[16].scale.set(-1.5, 1.5, 1.5);
  levelEnvironment[0].obstacle[16].dir = 4;
  levelEnvironment[0].obstacle[16].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[16].position.set(0, 0, -93);
  levelEnvironment[0].obstacle[17] = levelEnvironment[0].obstacle[10].clone();
  levelEnvironment[0].obstacle[17].box = [0.63, 0.945];
  levelEnvironment[0].obstacle[17].ready = false;
  levelEnvironment[0].obstacle[17].scale.set(1.8, 1.8, 1.8);
  levelEnvironment[0].obstacle[17].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[17].position.set(-5, 4.7, -87.2);
  levelEnvironment[0].obstacle[17].dir = 3;
  levelEnvironment[1].window = new THREE.Object3D();
  levelEnvironment[1].window.part = [];
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(4, 6.6);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 4.7, -4.99);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  levelEnvironment[1].window.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(2.6, 7.3, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 7.3, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(5.2, 7.3, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(2.6, 6, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 6, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(5.2, 6, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(2.6, 4.7, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 4.7, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(5.2, 4.7, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(2.6, 3.4, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 3.4, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(5.2, 3.4, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(2.6, 2.1, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(3.9, 2.1, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.2, 1.2);
  tempGeometry[tempGeometry.length - 1].translate(5.2, 2.1, -4.98);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(11, 0, 0);
  levelEnvironment[1].window.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x5C6BC0 }));
  clearTempGeometries();
  levelEnvironment[1].window.add(levelEnvironment[1].window.part[0], levelEnvironment[1].window.part[1]);
  levelEnvironment[1].robot = robot.clone();
  levelEnvironment[1].robot.scale.set(1.5, 1.5, 1.5);
  levelEnvironment[1].robot.position.set(3, 1, -3);
  levelEnvironment[1].bigTable = new THREE.Object3D();
  levelEnvironment[1].bigTable.part = [];
  levelEnvironment[1].bigTable.part[0] = new THREE.Mesh(new THREE.BoxGeometry(2.5, 0.05, 1), new THREE.MeshToonMaterial({ color: 0x9E9E9E, gradientMap: gradientMap[4] }));
  levelEnvironment[1].bigTable.part[0].geometry.translate(0, 0.8, 0);
  levelEnvironment[1].bigTable.part[1] = new THREE.Mesh(new THREE.BoxGeometry(2.3, 0.8, 0.8), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[4] }));
  levelEnvironment[1].bigTable.part[1].geometry.translate(0, 0.4, 0);
  levelEnvironment[1].bigTable.add(levelEnvironment[1].bigTable.part[0], levelEnvironment[1].bigTable.part[1]);
  levelEnvironment[1].bigTable.position.set(3, 0, -2.2);
  levelEnvironment[1].bigTable1 = levelEnvironment[1].bigTable.clone();
  levelEnvironment[1].bigTable1.position.set(12, 0, -2);
  levelEnvironment[1].bigTable2 = levelEnvironment[1].bigTable.clone();
  levelEnvironment[1].bigTable2.position.set(19, 0, -3);
  levelEnvironment[1].bigTable3 = levelEnvironment[1].bigTable.clone();
  levelEnvironment[1].bigTable3.position.set(34, 0, -2);
  levelEnvironment[1].cabinet = new THREE.Object3D();
  levelEnvironment[1].cabinet.part = [];
  levelEnvironment[1].cabinet.part[0] = new THREE.Mesh(new THREE.BoxGeometry(2, 3, 0.6), new THREE.MeshToonMaterial({ color: 0x1976D2, gradientMap: gradientMap[4] }));
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.85, 2.8, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(-0.475, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.95, 0, 0);
  levelEnvironment[1].cabinet.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x1565C0, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.3, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(-0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  levelEnvironment[1].cabinet.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[1].cabinet.add(levelEnvironment[1].cabinet.part[2], levelEnvironment[1].cabinet.part[1], levelEnvironment[1].cabinet.part[0]);
  levelEnvironment[1].cabinet.rotation.y = Math.PI * 0.5;
  levelEnvironment[1].cabinet.position.set(0.3, 1.5, -3);
  levelEnvironment[1].cabinet1 = levelEnvironment[1].cabinet.clone();
  levelEnvironment[1].cabinet1.rotation.y = 0;
  levelEnvironment[1].cabinet1.position.set(7.2, 1.5, -4.7);
  levelEnvironment[1].cabinet2 = levelEnvironment[1].cabinet.clone();
  levelEnvironment[1].cabinet2.rotation.y = 0;
  levelEnvironment[1].cabinet2.position.set(35, 1.5, -4.7);
  levelEnvironment[1].cabinet3 = levelEnvironment[1].cabinet.clone();
  levelEnvironment[1].cabinet3.rotation.y = 0;
  levelEnvironment[1].cabinet3.position.set(28.5, 1.5, -4.7);
  levelEnvironment[1].robotIcon = levelEnvironment[0].robotIcon.clone();
  levelEnvironment[1].robotIcon.position.set(38.5, 1.2, -4.99);
  levelEnvironment[1].server = new THREE.Object3D();
  levelEnvironment[1].server.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 1.5);
  shape.lineTo(-0.5, 1.5);
  shape.lineTo(-0.5, -1.5);
  shape.lineTo(0.5, -1.5);
  shape.lineTo(0.45, -1.45);
  shape.lineTo(-0.45, -1.45);
  shape.lineTo(-0.45, 1.45);
  shape.lineTo(0.45, 1.45);
  shape.lineTo(0.45, -1.45);
  shape.lineTo(0.5, -1.5);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 0, bevelEnabled: false });
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0);
  shape.lineTo(0.4, 0.23125);
  shape.lineTo(-0.4, 0.23125);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 0, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 1.17, 0);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(0, -0.28125, 0);
  }
  levelEnvironment[1].server.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(-0.3, 1.3, 1.01);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.28125, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.1, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 1.3, 1.01);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.28125, 0);
  }
  levelEnvironment[1].server.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[1]);
  clearTempGeometries();
  levelEnvironment[1].server.add(levelEnvironment[1].server.part[0], levelEnvironment[1].server.part[1]);
  levelEnvironment[1].server.position.set(13.5, 1.5, -5);
  levelEnvironment[1].server1 = levelEnvironment[1].server.clone();
  levelEnvironment[1].server1.rotation.y = -Math.PI * 0.25;
  levelEnvironment[1].server1.position.set(16, 1.5, -3.5);
  levelEnvironment[1].server2 = levelEnvironment[1].server.clone();
  levelEnvironment[1].server2.rotation.y = -Math.PI * 0.25;
  levelEnvironment[1].server2.position.set(24.5, 1.5, -4.5);
  levelEnvironment[1].server3 = levelEnvironment[1].server.clone();
  levelEnvironment[1].server3.position.set(46, 1.5, -5);
  levelEnvironment[1].table = levelEnvironment[0].table1.clone();
  levelEnvironment[1].table.position.set(8, 0, -3);
  levelEnvironment[1].table1 = levelEnvironment[0].table1.clone();
  levelEnvironment[1].table1.position.set(22, 0, -2);
  levelEnvironment[1].table2 = levelEnvironment[0].table1.clone();
  levelEnvironment[1].table2.position.set(40, 0, -2);
  levelEnvironment[0].robotHand = levelEnvironment[0].obstacle[11].clone();
  levelEnvironment[0].robotHand.children[4].material = new THREE.MeshToonMaterial({ color: 0x01579B, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand.children[3].material = new THREE.MeshToonMaterial({ color: 0x00838F, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand.children[2].material = new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand.children[1].material = new THREE.MeshBasicMaterial({ color: 0x212121 });
  levelEnvironment[0].robotHand.children[0].material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
  levelEnvironment[0].robotHand.scale.set(-0.7, 0.7, 0.7);
  levelEnvironment[0].robotHand.rotation.y = -0.4;
  levelEnvironment[0].robotHand.position.set(2.3, 0.825, -2.2);
  levelEnvironment[0].robotHand1 = levelEnvironment[0].obstacle[11].clone();
  levelEnvironment[0].robotHand1.children[4].material = new THREE.MeshToonMaterial({ color: 0x01579B, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand1.children[3].material = new THREE.MeshToonMaterial({ color: 0x00838F, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand1.children[2].material = new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand1.children[1].material = new THREE.MeshBasicMaterial({ color: 0x212121 });
  levelEnvironment[0].robotHand1.children[0].material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
  levelEnvironment[0].robotHand1.scale.set(0.9, 0.9, 0.9);
  levelEnvironment[0].robotHand1.rotation.y = -0.6;
  levelEnvironment[0].robotHand1.position.set(19.5, 0.825, -3);
  levelEnvironment[0].robotHand2 = levelEnvironment[0].obstacle[11].clone();
  levelEnvironment[0].robotHand2.children[4].material = new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand2.children[3].material = new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand2.children[2].material = new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] });
  levelEnvironment[0].robotHand2.children[1].material = new THREE.MeshBasicMaterial({ color: 0x212121 });
  levelEnvironment[0].robotHand2.children[0].material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
  levelEnvironment[0].robotHand2.scale.set(-3, 3, 3);
  levelEnvironment[0].robotHand2.rotation.y = -0.6;
  levelEnvironment[0].robotHand2.position.set(31, 0, -3);
  levelEnvironment[0].robot1 = new THREE.Object3D();
  levelEnvironment[0].robot1.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.25, 0.25, 0.06, 0, Math.PI * 0.5);
  shape.absarc(-0.25, 0.25, 0.06, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.25, -0.25, 0.06, Math.PI, Math.PI * 1.5);
  shape.absarc(0.25, -0.25, 0.06, Math.PI * 1.5, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.95, -0.2);
  levelEnvironment[0].robot1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(0.4, 0.4);
  shape.lineTo(0.2, 0.8);
  shape.lineTo(0.25, 0.42);
  shape.lineTo(0, 0.2);
  shape.lineTo(-0.25, 0.42);
  shape.lineTo(-0.2, 0.8);
  shape.lineTo(-0.4, 0.4);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.1);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 0.6, 0.6);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0.85, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.25, 1.6);
  shape.lineTo(0.95, 1.6);
  shape.lineTo(0.95, 0.7);
  shape.lineTo(0.25, 0.7);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.2);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.13, 0);
  shape.lineTo(0.13, 0.1);
  shape.lineTo(-0.13, 0.1);
  shape.lineTo(-0.13, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, -0.15);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  levelEnvironment[0].robot1.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.2, 1.95, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.08, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.16, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1.12, 0.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.06, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.16, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1.12, 0.6, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.4);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.12, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.28, 0.14, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.05, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.22, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0.4, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  levelEnvironment[0].robot1.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.4, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.1, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 1.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.1, 12, 12, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.95, 1.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.025, 0.025, 0.8, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 2.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.8, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.4, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.4, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(1.2, 1.05, 0);
  levelEnvironment[0].robot1.part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1, 8, 3);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 2.6, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.08, 0);
  shape.lineTo(0, 0.1);
  shape.lineTo(-0.08, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.88, 0.21);
  levelEnvironment[0].robot1.part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.11, 12);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 2.05, 0.21);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.32, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.05, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 1.75, 0.21);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.08, 0, 0);
  levelEnvironment[0].robot1.part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[0]);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, -0.05);
  shape.absarc(0, 0, 0.3, 0, Math.PI);
  shape.lineTo(-0.3, -0.05);
  shape.lineTo(0.3, -0.05);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.21);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.06, 12);
  tempGeometry[tempGeometry.length - 1].translate(0.44, 2.05, 0.22);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.32, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.025, 0);
  shape.lineTo(0.025, 0.2);
  shape.lineTo(-0.025, 0.2);
  shape.lineTo(-0.025, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.23);
  levelEnvironment[0].robot1.part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.25, 0, Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 1.2, 0.22);
  levelEnvironment[0].robot1.part[8] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  levelEnvironment[0].robot1.add(levelEnvironment[0].robot1.part[8], levelEnvironment[0].robot1.part[7], levelEnvironment[0].robot1.part[6], levelEnvironment[0].robot1.part[5], levelEnvironment[0].robot1.part[4], levelEnvironment[0].robot1.part[2], levelEnvironment[0].robot1.part[1], levelEnvironment[0].robot1.part[3]);
  levelEnvironment[0].robot1.scale.set(2, 2, 2);
  levelEnvironment[0].robot1.rotation.y = -0.4;
  levelEnvironment[0].robot1.position.set(42, 0, -3);
  levelEnvironment[0].robot2 = levelEnvironment[0].robot1.clone();
  levelEnvironment[0].robot2.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[0].robot2.rotation.y = 0.3;
  levelEnvironment[0].robot2.position.set(9.7, 0, -3);
  levelEnvironment[0].hand = levelEnvironment[0].obstacle[13].part[7].part[2].clone();
  levelEnvironment[0].hand.rotation.set(0, 0, 0);
  levelEnvironment[0].hand.position.set(25.9, 2.65, -4);
  levelEnvironment[0].hand.scale.set(2, 2, 2);
  levelEnvironment[0].hand.part1 = new THREE.Mesh(tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.15, 0.15, 0.2, 8, 1), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[4] }));
  levelEnvironment[0].hand.part1.position.y = -0.1;
  levelEnvironment[0].hand.part2 = new THREE.Mesh(tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.2, 0.05, 8, 1), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  levelEnvironment[0].hand.part2.position.y = -0.2;
  levelEnvironment[0].hand.add(levelEnvironment[0].hand.part1, levelEnvironment[0].hand.part2);
  levelEnvironment[0].hand1 = levelEnvironment[0].hand.clone();
  levelEnvironment[0].hand1.position.set(12, 1.05, -2);
  levelEnvironment[0].hand1.scale.set(1, 1, 1);
  levelEnvironment[0].hand2 = levelEnvironment[0].hand.clone();
  levelEnvironment[0].hand2.position.set(33.5, 0.8, -3);
  levelEnvironment[0].hand2.scale.set(3, 3, 3);
  levelEnvironment[1].monitor = levelEnvironment[0].obstacle[0].clone();
  levelEnvironment[1].monitor.rotation.y = 3;
  levelEnvironment[1].monitor.scale.set(1.2, 1.2, 1.2);
  levelEnvironment[1].monitor.position.set(8, 0.75, -3);
  levelEnvironment[1].monitor1 = levelEnvironment[0].obstacle[0].clone();
  levelEnvironment[1].monitor1.rotation.y = 3;
  levelEnvironment[1].monitor1.position.set(34, 0.825, -2);
  levelEnvironment[1].monitor2 = levelEnvironment[0].obstacle[0].clone();
  levelEnvironment[1].monitor2.rotation.y = 3;
  levelEnvironment[1].monitor2.scale.set(1.2, 1.2, 1.2);
  levelEnvironment[1].monitor2.position.set(40, 0.75, -2);
  levelEnvironment[1].monitor3 = levelEnvironment[0].obstacle[0].clone();
  levelEnvironment[1].monitor3.rotation.y = 3;
  levelEnvironment[1].monitor3.position.set(22, 0.825, -2);
  levelEnvironment[1].chair = levelEnvironment[0].chair1.clone();
  levelEnvironment[1].chair.children[0].material = new THREE.MeshToonMaterial({ color: 0x7E57C2, gradientMap: gradientMap[4] });
  levelEnvironment[1].chair.rotation.y = 2;
  levelEnvironment[1].chair.position.set(7, 0, -2);
  levelEnvironment[1].chair1 = levelEnvironment[1].chair.clone();
  levelEnvironment[1].chair1.rotation.y = 2;
  levelEnvironment[1].chair1.position.set(21, 0, -2);
  levelEnvironment[1].chair2 = levelEnvironment[1].chair.clone();
  levelEnvironment[1].chair2.rotation.y = -2;
  levelEnvironment[1].chair2.position.set(23, 0, -2);
  levelEnvironment[1].chair3 = levelEnvironment[1].chair.clone();
  levelEnvironment[1].chair3.rotation.y = 5;
  levelEnvironment[1].chair3.position.set(41, 0, -1.5);
  levelEnvironment[1].chair4 = levelEnvironment[1].chair.clone();
  levelEnvironment[1].chair4.rotation.y = -5;
  levelEnvironment[1].chair4.position.set(39, 0, -1.5);
  levelEnvironment[1].grass = levelEnvironment[0].grass1.clone();
  levelEnvironment[1].grass.position.set(1.5, 0, 5);
  levelEnvironment[1].grass1 = levelEnvironment[0].grass1.clone();
  levelEnvironment[1].grass1.position.set(42, 0, -4);
  levelEnvironment[1].grass2 = levelEnvironment[0].grass1.clone();
  levelEnvironment[1].grass2.position.set(5, 0, -2);
  levelEnvironment[1].grass3 = levelEnvironment[0].grass1.clone();
  levelEnvironment[1].grass3.position.set(10, 0, -4);
  levelEnvironment[1].column1 = new THREE.Object3D();
  levelEnvironment[1].column1.part = [];
  levelEnvironment[1].column1.part[0] = new THREE.Mesh(new THREE.BoxGeometry(0.6, 11, 0.6), new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] }));
  levelEnvironment[1].column1.part[0].geometry.translate(0, 5.5, 0);
  levelEnvironment[1].column1.part[1] = new THREE.Mesh(new THREE.BoxGeometry(0.62, 0.2, 0.62), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  levelEnvironment[1].column1.part[1].geometry.translate(0, 0.1, 0);
  levelEnvironment[1].column1.add(levelEnvironment[1].column1.part[0], levelEnvironment[1].column1.part[1]);
  levelEnvironment[1].column1.position.set(5, 0, -4);
  levelEnvironment[1].column2 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column2.position.set(10, 0, -2);
  levelEnvironment[1].column3 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column3.position.set(17, 0, -2);
  levelEnvironment[1].column4 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column4.position.set(19.5, 0, -1.5);
  levelEnvironment[1].column5 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column5.position.set(29, 0, -2);
  levelEnvironment[1].column6 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column6.position.set(40, 0, -4);
  levelEnvironment[1].column7 = levelEnvironment[1].column1.clone();
  levelEnvironment[1].column7.position.set(45, 0, -1.5);
  levelEnvironment[1].blueLamp = new THREE.Object3D();
  levelEnvironment[1].blueLamp.part = [];
  levelEnvironment[1].blueLamp.part[0] = new THREE.Mesh(new THREE.ConeGeometry(0.5, 0.5, 24, 1, true), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  levelEnvironment[1].blueLamp.part[1] = new THREE.Mesh(new THREE.CylinderGeometry(0.025, 0.025, 4, 6, 1, true), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  levelEnvironment[1].blueLamp.part[1].position.y = 2.25;
  levelEnvironment[1].blueLamp.part[2] = new THREE.Mesh(new THREE.ConeGeometry(0.49, 0.49, 24, 1, true), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4], side: THREE.BackSide }));
  levelEnvironment[1].blueLamp.add(levelEnvironment[1].blueLamp.part[2], levelEnvironment[1].blueLamp.part[0], levelEnvironment[1].blueLamp.part[1]);
  levelEnvironment[1].blueLamp.position.set(20.5, 8.5, 0);
  clearTempGeometries();
  levelEnvironment[1].longLamp1 = new THREE.Object3D();
  levelEnvironment[1].longLamp1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(3, 0.15, 0.3);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 7, 7, 1);
  tempGeometry[tempGeometry.length - 1].translate(-1.4, 3.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.8, 0, 0);
  levelEnvironment[1].longLamp1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[1].longLamp1.part[1] = new THREE.Mesh(new THREE.BoxGeometry(2.99, 0.3, 0.29), whiteBasicMaterial);
  levelEnvironment[1].longLamp1.part[1].geometry.translate(0, -0.15, 0);
  levelEnvironment[1].longLamp1.part[1].scale.y = 0.3;
  levelEnvironment[1].longLamp1.add(levelEnvironment[1].longLamp1.part[0], levelEnvironment[1].longLamp1.part[1]);
  levelEnvironment[1].longLamp1.position.set(2.2, 5, -3);
  levelEnvironment[1].longLamp2 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp2.position.set(8, 5, -4);
  levelEnvironment[1].longLamp3 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp3.position.set(14, 4, -2);
  levelEnvironment[1].longLamp4 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp4.position.set(12, 6, -4);
  levelEnvironment[1].longLamp5 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp5.position.set(23, 5, -3);
  levelEnvironment[1].longLamp6 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp6.rotation.y = Math.PI * 0.5;
  levelEnvironment[1].longLamp6.position.set(26.3, 6, -3);
  levelEnvironment[1].longLamp7 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp7.rotation.y = Math.PI * 0.5;
  levelEnvironment[1].longLamp7.position.set(36.7, 10, -4);
  levelEnvironment[1].longLamp8 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp8.position.set(33, 5, -3);
  levelEnvironment[1].longLamp9 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp9.position.set(40, 4, -3);
  levelEnvironment[1].longLamp10 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp10.position.set(42, 8, -1);
  levelEnvironment[1].longLamp11 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[1].longLamp11.position.set(44, 6, -2);
  levelEnvironment[1].roundLamp1 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp1.position.set(3, 7, -1);
  levelEnvironment[1].roundLamp2 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp2.position.set(6, 6, -2);
  levelEnvironment[1].roundLamp3 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp3.position.set(9, 7, -1);
  levelEnvironment[1].roundLamp4 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp4.position.set(12, 7, -1);
  levelEnvironment[1].roundLamp5 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp5.position.set(16, 6, -4);
  levelEnvironment[1].roundLamp6 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp6.position.set(23, 7, -1.5);
  levelEnvironment[1].roundLamp7 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp7.position.set(33, 6, -2);
  levelEnvironment[1].roundLamp8 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp8.position.set(36.7, 10, -1);
  levelEnvironment[1].roundLamp9 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp9.position.set(40, 5, -2);
  levelEnvironment[1].roundLamp10 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp10.position.set(44, 6, -4);
  levelEnvironment[1].roundLamp11 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[1].roundLamp11.position.set(46, 8, -2);
  levelEnvironment[2].pBag = new THREE.Object3D();
  levelEnvironment[2].pBag.part = [];
  levelEnvironment[2].pBag.part[0] = new THREE.Mesh(new THREE.CapsuleGeometry(0.4, 3, 5, 18), heroShoesMaterial);
  levelEnvironment[2].pBag.part[0].geometry.scale(1, 0.5, 1);
  levelEnvironment[2].pBag.part[0].geometry.translate(0, 0.95, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.12, 0.12);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.7, 0.41);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.25);
  for (let i = 1; i < 4; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5 * i);
  }
  levelEnvironment[2].pBag.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.03, 0.8, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.7, 0.41);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.25);
  for (let i = 1; i < 4; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5 * i);
  }
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.03, 6, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 5.2, 0);
  levelEnvironment[2].pBag.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x424242, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].pBag.add(levelEnvironment[2].pBag.part[0], levelEnvironment[2].pBag.part[1], levelEnvironment[2].pBag.part[2]);
  levelEnvironment[2].pBag.position.set(2.4, 1.1, 0);
  levelEnvironment[0].obstacle[18] = new THREE.Object3D();
  levelEnvironment[0].obstacle[18].part = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.3, 0.3, 0.04, 18, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.02, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.25, 0.25, 0.24, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.12, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.08, 0.08, 0.5, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  levelEnvironment[0].obstacle[18].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 18, 10, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.4, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.3, 18, 10, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.4, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.78, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.3, 18, 8, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.38);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.4, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.775, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.3, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.17, 16, 8);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.2, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.1, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.15, 0.1, 0.3, 14, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.19, 0.745, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  levelEnvironment[0].obstacle[18].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x1B5E20, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[18].add(levelEnvironment[0].obstacle[18].part[0], levelEnvironment[0].obstacle[18].part[1]);
  levelEnvironment[0].obstacle[18].box = [0.4 * 1.5, 1.2 * 1.5];
  levelEnvironment[0].obstacle[18].ready = true;
  levelEnvironment[0].obstacle[18].scale.set(-1.5, 1.5, 1.5);
  levelEnvironment[0].obstacle[18].dir = 3;
  levelEnvironment[0].obstacle[18].rotation.y = Math.PI * 0.5 - 0.4;
  levelEnvironment[0].obstacle[18].position.set(0, 0, -105.3);
  levelEnvironment[0].obstacle[19] = levelEnvironment[2].pBag.clone();
  levelEnvironment[0].obstacle[19].children[0].material = new THREE.MeshToonMaterial({ color: 0x1A237E });
  levelEnvironment[0].obstacle[19].box = [0.2, 10];
  levelEnvironment[0].obstacle[19].ready = true;
  levelEnvironment[0].obstacle[19].dir = 2;
  levelEnvironment[0].obstacle[19].position.set(0, 1.1, -108.8);
  levelEnvironment[0].obstacle[20] = new THREE.Object3D();
  levelEnvironment[0].obstacle[20].part = [];
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.35, 0.03, 6, 24);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.015, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.05, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.01, -0.4);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.4, 0.3, 0.01);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.01, -0.46);
  levelEnvironment[0].obstacle[20].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF44336, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.02, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.35, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(0.3);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.4);
  tempGeometry[tempGeometry.length - 1].translate(-0.35, 0, 0);
  for (let i = 1; i < 16; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 8 * i);
  }
  levelEnvironment[0].obstacle[20].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFFFFF, gradientMap: gradientMap[4], side: THREE.DoubleSide }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[20].part[2] = new THREE.Mesh(new THREE.PlaneGeometry(2, 1.5), new THREE.MeshBasicMaterial({ color: 0x00ACC1, transparent: true, opacity: 0.5 }));
  levelEnvironment[0].obstacle[20].part[2].geometry.translate(0, 0.4, -0.45);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, -0.75);
  shape.lineTo(1, 0.75);
  shape.lineTo(-1, 0.75);
  shape.lineTo(-1, -0.75);
  shape.lineTo(1, -0.75);
  shape.lineTo(0.9, -0.65);
  shape.lineTo(-0.9, -0.65);
  shape.lineTo(-0.9, 0.65);
  shape.lineTo(0.9, 0.65);
  shape.lineTo(0.9, -0.65);
  shape.lineTo(0.1, -0.75);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, -0.44);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, -0.3);
  shape.lineTo(0.5, 0.3);
  shape.lineTo(-0.5, 0.3);
  shape.lineTo(-0.5, -0.3);
  shape.lineTo(0.5, -0.3);
  shape.lineTo(0.4, -0.2);
  shape.lineTo(-0.4, -0.2);
  shape.lineTo(-0.4, 0.2);
  shape.lineTo(0.4, 0.2);
  shape.lineTo(0.4, -0.2);
  shape.lineTo(0.1, -0.3);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.44);
  levelEnvironment[0].obstacle[20].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x37474F }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.6, 0.5, 0.01);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.01, -0.46);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.2, 0.2, 0.8);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.5, -0.82);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.2, 5, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(0, -3.23, -1.1);
  levelEnvironment[0].obstacle[20].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x795548, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 0.8, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(0, -4.6, -1.1);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.05, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, -4.95, -1.1);
  levelEnvironment[0].obstacle[20].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[20].add(levelEnvironment[0].obstacle[20].part[5], levelEnvironment[0].obstacle[20].part[4], levelEnvironment[0].obstacle[20].part[3], levelEnvironment[0].obstacle[20].part[2], levelEnvironment[0].obstacle[20].part[1], levelEnvironment[0].obstacle[20].part[0]);
  levelEnvironment[0].obstacle[20].box = [0.4, 0.03];
  levelEnvironment[0].obstacle[20].ready = true;
  levelEnvironment[0].obstacle[20].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[20].dir = 4;
  levelEnvironment[0].obstacle[20].position.set(0, 5, -114.3);
  levelEnvironment[0].obstacle[21] = new THREE.Object3D();
  levelEnvironment[0].obstacle[21].part = [];
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.5, 0.1, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.4, 0.07, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.3, 0.4, 0.3, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.35, 0.8, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.7, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.3, 0.1, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.1, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 16, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.1, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.4, 20, 10);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.6, 0);
  levelEnvironment[0].obstacle[21].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  levelEnvironment[0].obstacle[21].add(levelEnvironment[0].obstacle[21].part[0]);
  levelEnvironment[0].obstacle[21].box = [0.5, 1.9];
  levelEnvironment[0].obstacle[21].ready = true;
  levelEnvironment[0].obstacle[21].dir = 3;
  levelEnvironment[0].obstacle[21].position.set(0, 0, -119.4);
  levelEnvironment[0].obstacle[22] = new THREE.Object3D();
  levelEnvironment[0].obstacle[22].part = [];
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.5, 0.1, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.4, 0.15, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.35, 0.05, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.45, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.3, 0.4, 0.3, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.4, 0.07, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.35, 1.7, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.3, 0.1, 8, 24);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.05, 8, 24);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.6, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.45, 0.2, 0.3, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.45, 16, 6, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.4, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.95, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 20, 10);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.3, 0);
  levelEnvironment[0].obstacle[22].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  levelEnvironment[0].obstacle[22].add(levelEnvironment[0].obstacle[22].part[0]);
  levelEnvironment[0].obstacle[22].box = [0.4, 2.4];
  levelEnvironment[0].obstacle[22].ready = true;
  levelEnvironment[0].obstacle[22].dir = 3;
  levelEnvironment[0].obstacle[22].position.set(0, 1.77, -124.7);
  levelEnvironment[2].chessFloor = [];
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.77, 1.77);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 7.08, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.77, 1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0);
  levelEnvironment[2].chessFloor[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotWhiteMaterial);
  levelEnvironment[2].chessFloor[0].geometry.rotateX(-Math.PI * 0.5);
  levelEnvironment[2].chessFloor[0].position.x = 21.05;
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].translate(5.31, -0.885, -0.885);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 1.77, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(4.425, 0, -0.885);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 1.77, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(6.195, 0, -0.885);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 1.77, 0);
  levelEnvironment[2].chessFloor[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessFloor[1].geometry.rotateX(-Math.PI * 0.5);
  levelEnvironment[2].chessFloor[1].position.x = 21.05
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].translate(1.77, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.77, -1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 7.08, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, 1.77, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0);
  levelEnvironment[2].chessFloor[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessFloor[2].geometry.rotateX(-Math.PI * 0.5);
  levelEnvironment[2].chessFloor[2].position.x = 21.05;
  clearTempGeometries();
  levelEnvironment[2].chessFloor[3] = new THREE.Object3D();
  levelEnvironment[2].chessFloor[3].part = [];
  levelEnvironment[2].chessFloor[3].part[0] = new THREE.Mesh(new THREE.BoxGeometry(1.6, 3.5, 1.6), new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessFloor[3].part[1] = new THREE.Mesh(new THREE.BoxGeometry(1.77, 0.1, 1.77), new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessFloor[3].part[1].geometry.translate(0, 1.72, 0);
  levelEnvironment[2].chessFloor[3].add(levelEnvironment[2].chessFloor[3].part[1], levelEnvironment[2].chessFloor[3].part[0]);
  levelEnvironment[2].chessFloor[3].position.set(22.82, -1, -1.77);
  levelEnvironment[2].chessFigure = levelEnvironment[0].obstacle[21].clone();
  levelEnvironment[2].chessFigure.children[0].material = new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] });
  levelEnvironment[2].chessFigure.position.set(22.82, 0.77, -1.77);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.5, 0.1, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.35, 0.15, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.3, 0.05, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.3, 0.4, 0.3, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.1, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.4, 0.07, 8, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.35, 1.1, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.1, 8, 24);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.25, 0.05, 8, 24);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.32, 16, 10);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.5, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.8, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.15, 12, 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.35, 0);
  levelEnvironment[2].chessFigure1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].chessFigure1.position.set(24.59, 0, -3.54);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.77, 1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0.885, -5.31);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 3.54, -0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 3.54, 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, 1.77, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, -1.77, 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, 1.77, 0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -3.54, -0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -3.54, 0.3);
  levelEnvironment[2].chessWall = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessWall.position.x = 21.05;
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.77, 1.77, 1.77);
  tempGeometry[tempGeometry.length - 1].translate(1.77, 0.885, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.54, 0, -0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 3.54, 0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 3.54, -0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.77, 1.77, 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.54, 0, -0.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -3.54, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -3.54, 0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.77, 5.31, -0.8);
  levelEnvironment[2].chessWall1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4] }));
  levelEnvironment[2].chessWall1.position.x = 21.05;
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(3.54, 0.3, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(2.655, 6.93, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5.01, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.3, 5.31, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(1.035, 4.425, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.24, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.2, 5.31, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(2.1, 4.425, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.05, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(3.54, 0.2, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(2.665, 5.6, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -1.2, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -1.2, 0);
  levelEnvironment[2].window = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].window.position.x = 21.05;
  levelEnvironment[2].window1 = new THREE.Mesh(new THREE.PlaneGeometry(4, 6), new THREE.MeshBasicMaterial({ color: 0x00838F }));
  levelEnvironment[2].window1.position.set(23.715, 4, -4.99);
  levelEnvironment[0].obstacle[23] = new THREE.Object3D();
  levelEnvironment[0].obstacle[23].part = [];
  levelEnvironment[0].obstacle[23].part[0] = new THREE.Mesh(new THREE.BoxGeometry(1.6, 3.5, 1.6), new THREE.MeshToonMaterial({ color: 0x795548, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[23].part[1] = new THREE.Mesh(new THREE.BoxGeometry(1.77, 0.1, 1.77), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[23].part[1].geometry.translate(0, 1.72, 0);
  levelEnvironment[0].obstacle[23].add(levelEnvironment[0].obstacle[23].part[1], levelEnvironment[0].obstacle[23].part[0]);
  levelEnvironment[0].obstacle[23].box = [1.6, 1.77];
  levelEnvironment[0].obstacle[23].ready = true;
  levelEnvironment[0].obstacle[23].dir = 3;
  levelEnvironment[0].obstacle[23].position.set(0, 0, -124.7);
  levelEnvironment[0].obstacle[24] = new THREE.Object3D();
  levelEnvironment[0].obstacle[24].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 10, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 3.9, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.1, 0.1, 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 8, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.08, 0.4, 0.08);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.1, 0.1, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.6, -1.05, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.08, 0.08, 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.6, -1.05, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.2, 0, 0);
  levelEnvironment[0].obstacle[24].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x616161, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.05, 0.05, 2.4, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  levelEnvironment[0].obstacle[24].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.4, 0.4, 0.08, 16, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.8, 0.2, 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.2, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0, 0);
  levelEnvironment[0].obstacle[24].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 8, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(1.1, 0.2, 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(2.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-2.2, 0, 0);
  levelEnvironment[0].obstacle[24].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[24].add(levelEnvironment[0].obstacle[24].part[3], levelEnvironment[0].obstacle[24].part[2], levelEnvironment[0].obstacle[24].part[1], levelEnvironment[0].obstacle[24].part[0]);
  levelEnvironment[0].obstacle[24].box = [0.1, 9];
  levelEnvironment[0].obstacle[24].ready = true;
  levelEnvironment[0].obstacle[24].dir = 2;
  levelEnvironment[0].obstacle[24].position.set(0, 1.1, -129.2);
  levelEnvironment[0].obstacle[25] = new THREE.Object3D();
  levelEnvironment[0].obstacle[25].part = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.06, 0.06, 4.2, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(2.7, 0.2, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.4, 0, 0);
  levelEnvironment[0].obstacle[25].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xEF6C00, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(4.6, 0.06, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.95, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -2, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.06, 2, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(-2.27, 0.95, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4.54, 0, 0);
  levelEnvironment[0].obstacle[25].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.02, 2);
  tempGeometry[tempGeometry.length - 1].translate(-2.1, 0.95, 0);
  for (let i = 0; i < 28; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(0.15, 0, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(4.58, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.85, 0);
  for (let i = 0; i < 12; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  }
  levelEnvironment[0].obstacle[25].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x212121, side: THREE.DoubleSide }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.4);
  tempGeometry[tempGeometry.length - 1].translate(-2.7, 2.15, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.9, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  levelEnvironment[0].obstacle[25].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[25].add(levelEnvironment[0].obstacle[25].part[3], levelEnvironment[0].obstacle[25].part[2], levelEnvironment[0].obstacle[25].part[1], levelEnvironment[0].obstacle[25].part[0]);
  levelEnvironment[0].obstacle[25].box = [0.1, 2];
  levelEnvironment[0].obstacle[25].ready = true;
  levelEnvironment[0].obstacle[25].dir = 3;
  levelEnvironment[0].obstacle[25].position.set(0, 2, -133.3);
  levelEnvironment[0].obstacle[26] = new THREE.Object3D();
  levelEnvironment[0].obstacle[26].part = [];
  levelEnvironment[0].obstacle[26].part[0] = new THREE.Mesh(new THREE.BoxGeometry(1.6, 0.06, 2.7), new THREE.MeshToonMaterial({ color: 0x1B5E20, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[26].part[0].geometry.translate(0, 0.7, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.6, 0.06);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.74, -1.32);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 2.64);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.06, 2.7);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.74, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.77, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.54, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.75, 0.04, 0.01);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.03, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 0.8, 0.76);
  levelEnvironment[0].obstacle[26].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.69, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.4, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(-0.9, 0.89, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.8, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.2, 0.04, 12, 1);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(0.3, 0.75, 0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, -1.6);
  levelEnvironment[0].obstacle[26].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), heroShoesMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.75, 0.015);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.97, 0);
  for (let i = 0; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.05, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.015, 0.26);
  tempGeometry[tempGeometry.length - 1].translate(-0.85, 0.9, 0);
  for (let i = 0; i < 34; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.05, 0, 0);
  }
  levelEnvironment[0].obstacle[26].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x212121 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.05, 0.3);
  tempGeometry[tempGeometry.length - 1].translate(0.3, 0.75, 1.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
 	tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, -2.4);
  levelEnvironment[0].obstacle[26].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.2, 12);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1.3);
  tempGeometry[tempGeometry.length - 1].translate(0.3, 0.77, 0.8);
  levelEnvironment[0].obstacle[26].part[5] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], new THREE.MeshBasicMaterial({ color: 0xE53935 }));
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.6, 0, -1.6);
  levelEnvironment[0].obstacle[26].part[6] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], new THREE.MeshBasicMaterial({ color: 0x0D47A1 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.06, 0.7, 0.06);
  tempGeometry[tempGeometry.length - 1].translate(0.7, 0.34, 1.25);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -2.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.05, 0.05, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.7, 0.3, 0.75);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.4, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(-0.7, 0.35, 1.25);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 2.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  levelEnvironment[0].obstacle[26].part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x283593, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[26].add(levelEnvironment[0].obstacle[26].part[7], levelEnvironment[0].obstacle[26].part[5], levelEnvironment[0].obstacle[26].part[6], levelEnvironment[0].obstacle[26].part[4], levelEnvironment[0].obstacle[26].part[3], levelEnvironment[0].obstacle[26].part[2], levelEnvironment[0].obstacle[26].part[1], levelEnvironment[0].obstacle[26].part[0]);
  levelEnvironment[0].obstacle[26].box = [1.6, 0.73];
  levelEnvironment[0].obstacle[26].ready = true;
  levelEnvironment[0].obstacle[26].dir = 3;
  levelEnvironment[0].obstacle[26].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[26].position.set(0.5, 0, -114.3);
  levelEnvironment[0].obstacle[27] = new THREE.Object3D();
  levelEnvironment[0].obstacle[27].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.6, 0.6, 0.3, 0, Math.PI * 0.5);
  shape.absarc(-0.6, 0.6, 0.3, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.6, -0.6, 0.3, Math.PI, Math.PI * 1.5);
  shape.absarc(0.6, -0.6, 0.3, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.6, -0.6, 0.2, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-0.6, -0.6, 0.2, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-0.6, 0.6, 0.2, Math.PI, Math.PI * 0.5, true);
  shape.absarc(0.6, 0.6, 0.2, Math.PI * 0.5, 0, true);
  shape.lineTo(0.8, -0.6);
  shape.lineTo(0.9, -0.6);
  levelEnvironment[0].obstacle[27].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 4, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[27].part[0].geometry.translate(0, 0.9, -0.25);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.6, 0.6, 0.25, 0, Math.PI * 0.5);
  shape.absarc(-0.6, 0.6, 0.25, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.6, -0.6, 0.25, Math.PI, Math.PI * 1.5);
  shape.absarc(0.6, -0.6, 0.25, Math.PI * 1.5, Math.PI * 2);
  levelEnvironment[0].obstacle[27].part[1] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 2), new THREE.MeshBasicMaterial({ color: 0xFFAB91 }));
  levelEnvironment[0].obstacle[27].part[1].geometry.translate(0, 0.9, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 0);
  shape.lineTo(0.6, 0.4);
  shape.lineTo(0.52, 0.32);
  shape.lineTo(0.8, 0.05);
  shape.lineTo(-1.3, 0.05);
  shape.lineTo(-1.3, -0.05);
  shape.lineTo(0.8, -0.05);
  shape.lineTo(0.52, -0.32);
  shape.lineTo(0.6, -0.4);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].scale(0.6, 0.6, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0.1, 0.45, 0.01);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`ВХОД`, 0.24), 1)
  tempGeometry[tempGeometry.length - 1].center();
  tempGeometry[tempGeometry.length - 1].translate(0, 1, 0.01);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`НИЗКИЙ`, 0.16), 1)
  tempGeometry[tempGeometry.length - 1].center();
  tempGeometry[tempGeometry.length - 1].translate(0, 1.4, 0.01);
  levelEnvironment[0].obstacle[27].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x4A148C, transparent: true }));
  clearTempGeometries();
  gsap.to(levelEnvironment[0].obstacle[27].part[2].material, { duration: 0.05, opacity: 0.7, ease: "none", repeat: -1, yoyo: true });
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.04, 0.04, 10, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(-0.7, 6.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.4, 0, 0);
  levelEnvironment[0].obstacle[27].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x3E2723 }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[27].add(levelEnvironment[0].obstacle[27].part[3], levelEnvironment[0].obstacle[27].part[2], levelEnvironment[0].obstacle[27].part[1], levelEnvironment[0].obstacle[27].part[0]);
  levelEnvironment[0].obstacle[27].box = [1.7, 10];
  levelEnvironment[0].obstacle[27].ready = true;
  levelEnvironment[0].obstacle[27].dir = 2;
  levelEnvironment[0].obstacle[27].position.set(0, 1.3, -138.8);//143.8
  levelEnvironment[0].obstacle[27].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].ring = new THREE.Object3D();
  levelEnvironment[0].ring.part = [];
  levelEnvironment[0].ring.part[0] = new THREE.Mesh(new THREE.BoxGeometry(5.2, 0.8, 5.2), new THREE.MeshToonMaterial({ color: 0x1976D2, gradientMap: gradientMap[4] }));
  levelEnvironment[0].ring.part[0].geometry.translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 2.4, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(2.65, 1.2, 2.65);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5.3, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 5.3);
  levelEnvironment[0].ring.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.13, 0.13, 1.4, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(-2.3, 1.7, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(4.6, 0, -4.6);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.03, 0.03);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(-2.3, 2.3, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(3, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(-0.8, 2.3, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-4.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -4.6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(4.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.03, 1.2, 0.03);
  tempGeometry[tempGeometry.length - 1].translate(-0.7, 1.7, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  levelEnvironment[0].ring.part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), robotWhiteMaterial);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.13, 0.13, 1.4, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(2.3, 1.7, 2.3);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(1.5, 2.3, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 1.5);
  tempGeometry[tempGeometry.length - 1].translate(4.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.03, 0.03);
  tempGeometry[tempGeometry.length - 1].translate(0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(2.3, 2.3, 2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  levelEnvironment[0].ring.part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.13, 0.13, 1.4, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(-2.3, 1.7, -2.3);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.05, 0.05);
  tempGeometry[tempGeometry.length - 1].translate(-1.5, 2.3, -2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 1.5);
  tempGeometry[tempGeometry.length - 1].translate(-4.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.03, 0.03);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].translate(-2.3, 2.3, -2.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -0.4, 0);
  levelEnvironment[0].ring.part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x3949AB, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, 0);
  shape.lineTo(0.6, 0.2);
  shape.lineTo(0.4, 0.2);
  shape.lineTo(0.4, 0.4);
  shape.lineTo(0.2, 0.4);
  shape.lineTo(0.2, 0.6);
  shape.lineTo(0, 0.6);
  shape.lineTo(0, 0);
  levelEnvironment[0].ring.part[5] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  levelEnvironment[0].ring.part[5].rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].ring.part[5].position.set(2, 0, 2.6);
  levelEnvironment[0].ring.add(levelEnvironment[0].ring.part[5], levelEnvironment[0].ring.part[4], levelEnvironment[0].ring.part[3], levelEnvironment[0].ring.part[2], levelEnvironment[0].ring.part[1], levelEnvironment[0].ring.part[0]);
  levelEnvironment[0].ring.position.set(8, 0, -5);
  levelEnvironment[0].ring.rotation.y = 0.2;
  levelEnvironment[2].tribune1 = new THREE.Object3D();
  levelEnvironment[2].tribune1.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8, 0.6, 10);
  for (let i = 1; i < 8; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  }
  levelEnvironment[2].tribune1.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, 0);
  shape.absarc(0, 0.4, 0.35, 0, Math.PI);
  shape.lineTo(-0.35, 0);
  shape.lineTo(0.35, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0, curveSegments: 6, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 4, bevelOffset: -0.1 });
  tempGeometry[tempGeometry.length - 1].translate(-3.6, 0.3, 4.2);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.6, 4.05);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(-0.8, 0, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, -0.8);
  for (let i = 1; i < 10; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.8, 0, 0);
  }
  levelEnvironment[2].tribune1.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x3F51B5, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].tribune1.add(levelEnvironment[2].tribune1.part[1], levelEnvironment[2].tribune1.part[0]);
  levelEnvironment[2].tribune1.position.set(16, 0.1, -8);
  levelEnvironment[2].wall[0] = new THREE.Mesh(new THREE.PlaneGeometry(20, 10), new THREE.MeshBasicMaterial({ color: 0xFF8A65 }));
  levelEnvironment[2].wall[0].position.set(10, 5, -8.7);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-10, 0);
  shape.lineTo(-10, 20);
  shape.lineTo(10, 20);
  shape.lineTo(10, 0);
  shape.lineTo(1.1, 0);
  shape.lineTo(1.1, 1.1);
  shape.lineTo(-1.1, 1.1);
  shape.lineTo(-1.1, 0);
  levelEnvironment[2].wall[1] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 8, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x6A1B9A, gradientMap: gradientMap[4] }));
  levelEnvironment[2].wall[1].rotation.y = -Math.PI * 0.5;
  levelEnvironment[2].wall[1].position.set(42, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1, 0.21);
  tempGeometry[tempGeometry.length - 1].translate(20.5, 0.09, -4.99);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(20, 0.21);
  tempGeometry[tempGeometry.length - 1].translate(10, 0.09, -8.69);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(4.02, 0.21, 8.02);
  tempGeometry[tempGeometry.length - 1].translate(2, 0.09, -6);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.02, 0.21, 10.02);
  tempGeometry[tempGeometry.length - 1].translate(41, 0.09, -6.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.02, 0.21, 10.02);
  tempGeometry[tempGeometry.length - 1].translate(27.5, 0.09, -9.5);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(20, 0.21);
  tempGeometry[tempGeometry.length - 1].translate(31, 0.09, -10.69);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.21, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(41.74, 0.09, -5.09);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.52, 0.21, 8.2);
  tempGeometry[tempGeometry.length - 1].translate(41.74, 0.09, 5.09);
  levelEnvironment[2].wall[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x3F51B5 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(42, 22);
  levelEnvironment[2].ceiling = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].ceiling.rotation.x = Math.PI * 0.5;
  levelEnvironment[2].ceiling.position.set(21, 10, 0);
  levelEnvironment[2].wall[3] = new THREE.Mesh(new THREE.BoxGeometry(4, 10, 8), new THREE.MeshBasicMaterial({ color: 0x5C6BC0 }));
  levelEnvironment[2].wall[3].position.set(2, 5, -6);
  levelEnvironment[2].wall[5] = new THREE.Mesh(new THREE.BoxGeometry(1, 10, 4), new THREE.MeshBasicMaterial({ color: 0x5C6BC0 }));
  levelEnvironment[2].wall[5].position.set(20.5, 5, -7);
  levelEnvironment[2].wall[6] = new THREE.Mesh(new THREE.BoxGeometry(9, 1.8, 1), new THREE.MeshBasicMaterial({ color: 0x1976D2 }));
  levelEnvironment[2].wall[6].position.set(15, 7, -8.7);
  levelEnvironment[2].wall[7] = new THREE.Mesh(new THREE.BoxGeometry(1, 10, 10), new THREE.MeshBasicMaterial({ color: 0x5C6BC0 }));
  levelEnvironment[2].wall[7].position.set(41, 5, -6.5);
  levelEnvironment[2].wall[8] = new THREE.Mesh(new THREE.BoxGeometry(1, 10, 10), new THREE.MeshBasicMaterial({ color: 0x5C6BC0 }));
  levelEnvironment[2].wall[8].position.set(27.5, 5, -9.5);
  levelEnvironment[2].wall[9] = new THREE.Mesh(new THREE.PlaneGeometry(20, 10), new THREE.MeshBasicMaterial({ color: 0x78909C }));
  levelEnvironment[2].wall[9].position.set(31, 5, -10.7);
  levelEnvironment[2].wall[10] = new THREE.Mesh(new THREE.BoxGeometry(18, 1.2, 1), new THREE.MeshBasicMaterial({ color: 0x2196F3 }));
  levelEnvironment[2].wall[10].position.set(32, 8.5, -10.7);
  levelEnvironment[2].wall[4] = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 20), new THREE.MeshBasicMaterial({ color: 0x3F51B5 }));
  levelEnvironment[2].wall[4].position.set(41.75, 9, 5.98);
  levelEnvironment[2].floorBack = new THREE.Mesh(new THREE.PlaneGeometry(9, 15), new THREE.MeshBasicMaterial({ color: 0xFF8A65 }));
  levelEnvironment[2].floorBack.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorBack.position.set(16.5, -0.01, 2);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.0095, 11);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.6, 0, 0);
  for (let i = 0; i < 110; i++) {
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.15, 1.15);
    tempGeometry[tempGeometry.length - 1].translate(0.6 + 1.2 * (i % 10), 8 - 1.2 * Math.floor(i / 10), 0);
  }
  levelEnvironment[2].floorTop = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xCE93D8 }));
  clearTempGeometries();
  levelEnvironment[2].floorTop.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorBox = new THREE.Mesh(new THREE.PlaneGeometry(12, 18), new THREE.MeshBasicMaterial({ color: 0xBA68C8 }));
  levelEnvironment[2].floorBox.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorBox.position.set(6, -0.01, 0);
  levelEnvironment[2].logo = createSVG(4, 5, 0xFFFFFF);
  levelEnvironment[2].logo.position.set(-0.4, 7, -1.99);
  levelEnvironment[2].logo.scale.set(0.0028, -0.0028, 0.0028);
  levelEnvironment[2].logo1 = createSVG(5, 5, 0xFFFFFF);
  levelEnvironment[2].logo1.position.set(4.01, 7, -2);
  levelEnvironment[2].logo1.rotation.y = Math.PI * 0.5;
  levelEnvironment[2].logo1.scale.set(0.004, -0.004, 0.004);
  levelEnvironment[2].logo2 = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`KRONBARS`, 0.7), 1), whiteBasicMaterial);
  levelEnvironment[2].logo2.geometry.center();
  levelEnvironment[2].logo2.position.set(15, 7, -8.19);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(5.6, 4);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.1, 3.9);
  tempGeometry[tempGeometry.length - 1].translate(-1.9, 0, 0.02);
  for (let i = 1; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.78, 0, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(5.5, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.15, 0.02);
  for (let i = 1; i < 4; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.78, 0);
  }
  levelEnvironment[2].window1part1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  levelEnvironment[2].window1part1.position.set(7.2, 6, -8.69);
  levelEnvironment[2].window1part2 = new THREE.Mesh(new THREE.PlaneGeometry(5.4, 3.8), new THREE.MeshBasicMaterial({ color: 0x546E7A }))
  levelEnvironment[2].window1part2.position.set(7.2, 6, -8.68);
  levelEnvironment[2].pBag1 = levelEnvironment[2].pBag.clone();
  levelEnvironment[2].pBag1.children[0].material = new THREE.MeshToonMaterial({ color: 0xC62828 });
  levelEnvironment[2].pBag1.position.set(5, 1.1, -7.7);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(8.2, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.4, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.2, 8);
  tempGeometry[tempGeometry.length - 1].translate(-4.4, -4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(7.99, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.RingGeometry(1, 1.2, 16, 1, 0, Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(-0.55, -1, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.6, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.55, -1, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.2, 4);
  tempGeometry[tempGeometry.length - 1].translate(-2.25, -2, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.4, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(3.6, 0.2);
  tempGeometry[tempGeometry.length - 1].translate(-0.55, -4, 0);
  tempGeometry[tempGeometry.length] = new THREE.RingGeometry(1.2, 1.4, 16, 1, 0, Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(-0.55, -4, 0);
  levelEnvironment[2].basketFloor = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), whiteBasicMaterial);
  clearTempGeometries();
  levelEnvironment[2].basketFloor.position.set(16.5, 0, -2);
  levelEnvironment[2].basketFloor.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].soccerGate = new THREE.Object3D();
  levelEnvironment[2].soccerGate.part = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.07, 0.07, 2.6, 8, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-2.5, 1.25, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.07, 0.07, 5, 8, 1, true);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.04, 0.04, 2.55, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-2.5, 1.25, -1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.04, 0.04, 5, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.5, -1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -2.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.04, 0.04, 1, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-2.5, 2.5, -0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -2.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-5, 0, 0);
  levelEnvironment[2].soccerGate.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(5, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.4, -1);
  for (let i = 1; i < 16; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.02, 2.5);
  tempGeometry[tempGeometry.length - 1].translate(-2.4, 1.25, -1);
  for (let i = 1; i < 33; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0.15, 0, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.02, 2.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-2.5, 1.25, -0.1);
  for (let i = 1; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.15);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  for (let i = 1; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.15);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(5, 0.02);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.5, -0.1);
  for (let i = 1; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.15);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1, 0.02);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-2.5, 2.4, -0.5);
  for (let i = 1; i < 16; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, -0.15, 0);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(5, 0, 0);
  for (let i = 1; i < 16; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(0, 0.15, 0);
  }
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(10, 0.2);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 4);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(6, 0.2);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.5);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.2, 1.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-3, 0, 0.75);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.2, 4);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-5, 0, 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(10, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.RingGeometry(2.2, 2.4, 16, 1, 0, Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 4);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.15, 10);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 3);
  levelEnvironment[2].soccerGate.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, side: THREE.DoubleSide }));
  clearTempGeometries();
  levelEnvironment[2].soccerGate.add(levelEnvironment[2].soccerGate.part[1], levelEnvironment[2].soccerGate.part[0]);
  levelEnvironment[2].soccerGate.position.set(37, 0, -3);
  levelEnvironment[2].soccerGate.rotation.y = -0.3;
  levelEnvironment[2].tribune2 = levelEnvironment[2].tribune1.clone();
  levelEnvironment[2].tribune2.children[1].material = new THREE.MeshToonMaterial({ color: 0x7986CB, gradientMap: gradientMap[4] });
  levelEnvironment[2].tribune2.children[0].material = new THREE.MeshToonMaterial({ color: 0xAB47BC, gradientMap: gradientMap[4] });
  levelEnvironment[2].tribune2.position.set(41.3, 0.1, -10);
  levelEnvironment[2].tribune3 = levelEnvironment[2].tribune1.clone();
  levelEnvironment[2].tribune3.children[1].material = new THREE.MeshToonMaterial({ color: 0x7986CB, gradientMap: gradientMap[4] });
  levelEnvironment[2].tribune3.children[0].material = new THREE.MeshToonMaterial({ color: 0x00695C, gradientMap: gradientMap[4] });
  levelEnvironment[2].tribune3.position.set(32, 0.1, -10);
  levelEnvironment[2].floorBox1 = new THREE.Mesh(new THREE.PlaneGeometry(6, 40), new THREE.MeshBasicMaterial({ color: 0x00838F }));
  levelEnvironment[2].floorBox1.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorBox1.position.set(30.2, -0.02, 0);
  for (let i = 0; i < 45; i++) {
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(1.15, 1.15);
    tempGeometry[tempGeometry.length - 1].translate(27.8 + 1.2 * (i % 5), 5.2 - 1.2 * Math.floor(i / 5), 0);
  }
  levelEnvironment[2].floorTop1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x0097A7 }));
  clearTempGeometries();
  levelEnvironment[2].floorTop1.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorTop1.position.y = -0.01;
  levelEnvironment[2].floorBox2 = new THREE.Mesh(new THREE.PlaneGeometry(8, 40), new THREE.MeshBasicMaterial({ color: 0x8D6E63 }));
  levelEnvironment[2].floorBox2.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorBox2.position.set(37.2, -0.02, 0);
  for (let i = 0; i < 9; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.95, 10);
  	tempGeometry[tempGeometry.length - 1].translate(33.7 + 0.978 * i, 0, 0);
  }
  levelEnvironment[2].floorTop2 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xA1887F }));
  clearTempGeometries();
  levelEnvironment[2].floorTop2.rotation.x = -Math.PI * 0.5;
  levelEnvironment[2].floorTop2.position.y = -0.01;
  levelEnvironment[2].logo3 = new THREE.Object3D();
  levelEnvironment[2].logo3.box = new THREE.Mesh(new THREE.BoxGeometry(6, 0.8, 0.2), new THREE.MeshToonMaterial({ color: 0x1565C0, gradientMap: gradientMap[4] }));
  levelEnvironment[2].logo3.logo = levelEnvironment[2].logo2.clone();
  levelEnvironment[2].logo3.logo.scale.set(0.6, 0.6, 1);
  levelEnvironment[2].logo3.logo.position.set(0, 0, 0.11);
  levelEnvironment[2].logo3.add(levelEnvironment[2].logo3.box, levelEnvironment[2].logo3.logo);
  levelEnvironment[2].logo3.position.set(32, 0.4, -4);
  levelEnvironment[2].logo3.rotation.y = 0.2;
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0.6, 1);
  shape.lineTo(0.3, 1);
  shape.lineTo(0, 1.3);
  shape.lineTo(-0.3, 1);
  shape.lineTo(-0.6, 1);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].scale(0.8, 0.8, 1);
  for (let i = 1; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(2.4, 0, 0);
  }
  levelEnvironment[2].logo4 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  clearTempGeometries();
  levelEnvironment[2].logo4.position.set(28.9, 7.9, -10.19);
  levelEnvironment[2].logo5 = levelEnvironment[2].logo4.clone();
  levelEnvironment[2].logo5.material = new THREE.MeshBasicMaterial({ color: 0x1976D2 });
  levelEnvironment[2].logo5.position.set(30.1, 7.9, -10.19);
  levelEnvironment[2].logo6 = levelEnvironment[2].logo1.clone();
  levelEnvironment[2].logo6.rotation.y = -Math.PI * 0.5;
  levelEnvironment[2].logo6.scale.set(0.004, -0.004, 0.004);
  levelEnvironment[2].logo6.position.set(40.49, 8, -9);
  levelEnvironment[2].logo7 = levelEnvironment[2].logo.clone();
  levelEnvironment[2].logo7.rotation.y = Math.PI * 0.5;
  levelEnvironment[2].logo7.scale.set(0.004, -0.004, 0.004);
  levelEnvironment[2].logo7.position.set(28.01, 8.5, -3.5);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(7, 2.6);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(6.9, 0.1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0.02);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.1, 2.5);
  tempGeometry[tempGeometry.length - 1].translate(-1.7, 0, 0.02);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.7, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(1.7, 0, 0);
  levelEnvironment[2].window2frame = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  clearTempGeometries();
  levelEnvironment[2].window2frame.position.set(32, 6.3, -10.69);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(6.8, 2.4);
  levelEnvironment[2].window2glass = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x009688 }));
  clearTempGeometries();
  levelEnvironment[2].window2glass.position.set(32, 6.3, -10.68);
  levelEnvironment[2].ball = new THREE.Object3D();
  levelEnvironment[2].ball.part = [];
  levelEnvironment[2].ball.part[0] = new THREE.Mesh(new THREE.SphereGeometry(0.5, 16, 8), new THREE.MeshToonMaterial({ color: 0xE65100, gradientMap: gradientMap[4] }));
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.51, 0.51, 0.05, 16, 1, true);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.6, 0.03, 6, 16);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "y", -1);
  tempGeometry[tempGeometry.length - 1].scale(0.83, 0.94, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  levelEnvironment[2].ball.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x37474F }));
  clearTempGeometries();
  levelEnvironment[2].ball.add(levelEnvironment[2].ball.part[0], levelEnvironment[2].ball.part[1]);
  levelEnvironment[2].ball.scale.set(0.6, 0.6, 0.6);
  levelEnvironment[2].ball.position.set(13.5, 0.3, -2);
  levelEnvironment[2].ball1 = levelEnvironment[2].ball.clone();
  levelEnvironment[2].ball1.position.set(17, 0.3, -2.5);
  levelEnvironment[2].ball2 = new THREE.Object3D();
  levelEnvironment[2].ball2.part = [];
  levelEnvironment[2].ball2.part[0] = new THREE.Mesh(new THREE.SphereGeometry(0.5, 16, 8), new THREE.MeshToonMaterial({ color: 0x64B5F6, gradientMap: gradientMap[4] }));
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.7, 0.04, 12, 1);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "y", -2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.51);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 6);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 6);
  let newTempGeometry = new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = newTempGeometry;
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 6 * 1.5);
  newTempGeometry.dispose();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  levelEnvironment[2].ball2.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x303F9F }));
  clearTempGeometries();
  levelEnvironment[2].ball2.add(levelEnvironment[2].ball2.part[0], levelEnvironment[2].ball2.part[1]);
  levelEnvironment[2].ball2.scale.set(0.6, 0.6, 0.6);
  levelEnvironment[2].ball2.position.set(32, 0.3, -3);
  levelEnvironment[2].ball3 = levelEnvironment[2].ball2.clone();
  levelEnvironment[2].ball3.position.set(34, 0.3, -2);
  levelEnvironment[2].ball4 = new THREE.Object3D();
  levelEnvironment[2].ball4.part = [];
  levelEnvironment[2].ball4.part[0] = new THREE.Mesh(new THREE.SphereGeometry(0.5, 16, 8), new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4] }));
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.51, 5, 3, 0, Math.PI * 2, Math.PI * 0.88, Math.PI * 0.12);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2.5);
  newTempGeometry = new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = newTempGeometry;
  newTempGeometry.dispose();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  levelEnvironment[2].ball4.part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x37474F }));
  clearTempGeometries();
  levelEnvironment[2].ball4.add(levelEnvironment[2].ball4.part[0], levelEnvironment[2].ball4.part[1]);
  levelEnvironment[2].ball4.scale.set(0.6, 0.6, 0.6);
  levelEnvironment[2].ball4.position.set(37, 0.3, -2);
  levelEnvironment[2].column1 = levelEnvironment[0].column1.clone();
  levelEnvironment[2].column1.children[0].material = new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] });
  levelEnvironment[2].column1.children[1].material = new THREE.MeshBasicMaterial({ color: 0x3F51B5 });
  levelEnvironment[2].column1.position.set(5, 0, -2);
  levelEnvironment[2].column2 = levelEnvironment[2].column1.clone();
  levelEnvironment[2].column2.position.set(20.1, 0, -1.5);
  levelEnvironment[2].column3 = levelEnvironment[2].column1.clone();
  levelEnvironment[2].column3.position.set(28, 0, -2.5);
  levelEnvironment[2].column4 = levelEnvironment[2].column1.clone();
  levelEnvironment[2].column4.position.set(36.8, 0, -7);
  levelEnvironment[2].squareLamp = new THREE.Object3D();
  levelEnvironment[2].squareLamp.part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.15, 2);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.02, 0.02, 7, 7, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.9, 3.5, 0.9);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -1.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 1.8);
  levelEnvironment[2].squareLamp.part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[2].squareLamp.part[1] = new THREE.Mesh(new THREE.BoxGeometry(1.9, 0.3, 1.9), whiteBasicMaterial);
  levelEnvironment[2].squareLamp.part[1].geometry.translate(0, -0.15, 0);
  levelEnvironment[2].squareLamp.part[1].scale.y = 0.3;
  levelEnvironment[2].squareLamp.add(levelEnvironment[2].squareLamp.part[0], levelEnvironment[2].squareLamp.part[1]);
  levelEnvironment[2].squareLamp.position.set(1.2, 7, 0);
  levelEnvironment[2].squareLamp1 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp1.position.set(8, 5, -4);
  levelEnvironment[2].squareLamp2 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp2.position.set(13, 8, -2);
  levelEnvironment[2].squareLamp3 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp3.position.set(18, 8, -5);
  levelEnvironment[2].squareLamp4 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp4.position.set(25, 6, -3);
  levelEnvironment[2].squareLamp5 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp5.position.set(22, 9, 0);
  levelEnvironment[2].squareLamp6 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp6.position.set(33, 7, -2);
  levelEnvironment[2].squareLamp7 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp7.position.set(30, 6, -7);
  levelEnvironment[2].squareLamp8 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp8.position.set(35, 7, -8);
  levelEnvironment[2].squareLamp9 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp9.position.set(38, 9, -2);
  levelEnvironment[2].squareLamp10 = levelEnvironment[2].squareLamp.clone();
  levelEnvironment[2].squareLamp10.position.set(42, 8, -9);
  levelEnvironment[2].longLamp1 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp1.position.set(6.5, 6, -1);
  levelEnvironment[2].longLamp2 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp2.position.set(12, 5, -3);
  levelEnvironment[2].longLamp3 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp3.position.set(16, 7, -6);
  levelEnvironment[2].longLamp4 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp4.position.set(25, 7, -1);
  levelEnvironment[2].longLamp7 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp7.position.set(30.5, 5, -2);
  levelEnvironment[2].longLamp5 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp5.position.set(37, 7, -4);
  levelEnvironment[2].longLamp6 = levelEnvironment[1].longLamp1.clone();
  levelEnvironment[2].longLamp6.position.set(20, 5, -3);
  for (let i = 0; i < 108; i++) {
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(2.3, 2.3);
    tempGeometry[tempGeometry.length - 1].translate(1.175 + 2.35 * (i % 18), 1.175 + 2.35 * Math.floor(i / 18), 0);
  }
  levelEnvironment[2].ceiling2 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x424242 }));
  clearTempGeometries();
  levelEnvironment[2].ceiling2.position.set(0, 9.95, -10.7);
  levelEnvironment[2].ceiling2.rotation.x = Math.PI * 0.5;
  levelEnvironment[2].tweenAnimation = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(10, 10);
  shape.lineTo(10, -10);
  shape.lineTo(-5.98, -10);
  shape.lineTo(-5.98, 10);
  shape.lineTo(10, 10);
  shape.lineTo(1.1, 0);
  shape.lineTo(-1.1, 0);
  shape.lineTo(-1.1, -1.1);
  shape.lineTo(1.1, -1.1);
  shape.lineTo(1.1, 0);
  shape.lineTo(10, 10);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 1, bevelEnabled: false });
  levelEnvironment[3].wall[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), normalMaterial);
  clearTempGeometries();
  levelEnvironment[3].wall[0].position.y = 1.1;
  levelEnvironment[3].wall[0].rotation.y = Math.PI * 0.5;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 7.5, Math.PI * 1.5, Math.PI * 2.5);
  shape.absarc(0, 0, 6.5, Math.PI * 2.5, Math.PI * 1.5, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  for (let i = 1; i < 14; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  }
  levelEnvironment[3].wall[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), normalMaterial);
  clearTempGeometries();
  levelEnvironment[3].wall[1].position.set(0.8, 0.4, 4);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 6.5, Math.PI * 1.5, Math.PI * 2.5);
  shape.absarc(0, 0, 6.3, Math.PI * 2.5, Math.PI * 1.5, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.3);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  for (let i = 1; i < 14; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  }
  levelEnvironment[3].wall[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[2].position.set(0.8, 0.4, 4);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.25, 0.25, 0.2, 6, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.3, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 24);
  for (let j = 0; j < 14; j++) {
    if (j > 0) {
    	tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI);
    	tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
    }
    for (let i = 0; i < 12; i++) {
  	  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 12);
    }
  }
  levelEnvironment[3].wall[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x283593, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[3].position.set(0.8, 0.4, 4);
  levelEnvironment[3].animationTween = [];
  levelEnvironment[3].materialColor = [];
  levelEnvironment[3].yellowBlimColor = new THREE.Color(0xF9A825);
  levelEnvironment[3].blueBlimColor = new THREE.Color(0x0091EA);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.16, 8, 1, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.2, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 24);
  for (let j = 0; j < 14; j++) {
  	if (j > 0) {
  		tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI);
  		tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  	}
  	for (let i = 0; i < 12; i++) {
  		tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  		tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 12);
  	}
  }
  levelEnvironment[3].wall[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x4A148C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[4].position.set(0.8, 0.4, 4);
  levelEnvironment[0].obstacle[28] = new THREE.Object3D();
  levelEnvironment[0].obstacle[28].part = [];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.6, 0.6, 1.2, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.6, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.4, 0.4, 1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.7, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.2, 0.2, 10, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.6, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.25, 0);
  levelEnvironment[0].obstacle[28].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF6F00, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.7, 0.7, 0.1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.5, 0.5, 0.1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 2.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.4, 0.4, 0.2, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.3, 0);
  levelEnvironment[0].obstacle[28].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x004D40, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.2, 0.2, 4, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.2, 0);
  levelEnvironment[0].obstacle[28].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x01579B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.4, 0.2, 8, 12);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 3, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.4, 0.4, 0.1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  levelEnvironment[0].obstacle[28].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xC2185B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.1, 0.1, 3, 8, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 7.8, 0);
  levelEnvironment[0].obstacle[28].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.4, 16, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 9.5, 0);
  levelEnvironment[0].obstacle[28].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF1744, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[28].part[6] = new THREE.Mesh(new THREE.TorusGeometry(0.2, 0.1, 8, 12), new THREE.MeshToonMaterial({ color: 0xFF1744, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[28].part[6].geometry.rotateX(Math.PI * 0.5);
  levelEnvironment[0].obstacle[28].part[6].geometry.translate(0, 8.7, 0);
  levelEnvironment[0].obstacle[28].part[7] = new THREE.Mesh(new THREE.TorusGeometry(0.3, 0.1, 8, 12), new THREE.MeshToonMaterial({ color: 0xFF1744, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[28].part[7].geometry.rotateX(Math.PI * 0.5);
  levelEnvironment[0].obstacle[28].part[7].geometry.translate(0, 8.4, 0);
  levelEnvironment[0].obstacle[28].part[8] = new THREE.Mesh(new THREE.TorusGeometry(0.4, 0.1, 8, 12), new THREE.MeshToonMaterial({ color: 0xFF1744, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[28].part[8].geometry.rotateX(Math.PI * 0.5);
  levelEnvironment[0].obstacle[28].part[8].geometry.translate(0, 8.1, 0);
  levelEnvironment[0].obstacle[28].add(levelEnvironment[0].obstacle[28].part[8], levelEnvironment[0].obstacle[28].part[7], levelEnvironment[0].obstacle[28].part[6], levelEnvironment[0].obstacle[28].part[5], levelEnvironment[0].obstacle[28].part[4], levelEnvironment[0].obstacle[28].part[3], levelEnvironment[0].obstacle[28].part[2], levelEnvironment[0].obstacle[28].part[1], levelEnvironment[0].obstacle[28].part[0]);
  levelEnvironment[0].obstacle[28].scale.set(0.8, 0.8, 0.8);
  levelEnvironment[0].obstacle[28].box = [0.6 * 0.8, 9.6 * 0.8];
  levelEnvironment[0].obstacle[28].ready = true;
  levelEnvironment[0].obstacle[28].dir = 2.3;
  levelEnvironment[0].obstacle[28].position.set(0, -6.8, -144.16);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 7, Math.PI * 1.5, Math.PI * 2.5);
  shape.absarc(0, 0, 6.9, Math.PI * 2.5, Math.PI * 1.5, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.6, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.8);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  for (let i = 1; i < 14; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].translate(3, 0, 0);
  }
  levelEnvironment[3].wall[20] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[20].position.set(0.8, 0.4, 4);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(41, 0.3, 1);
  tempGeometry[tempGeometry.length - 1].translate(20.5, 6.7, 0);
  for (let i = 0; i < 12; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI / 12);
  }
  levelEnvironment[3].wall[21] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[21].position.set(0, 0.4, 4);
  levelEnvironment[0].obstacle[29] = new THREE.Object3D();
  levelEnvironment[0].obstacle[29].part = [];
  levelEnvironment[0].obstacle[29].part[0] = new THREE.Object3D();
  levelEnvironment[0].obstacle[29].part[0].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.4, 0)
  shape.absarc(0, -0.2, 0.4, Math.PI, Math.PI * 2);
  shape.lineTo(0.4, 0)
  shape.lineTo(-0.4, 0)
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 5, bevelEnabled: true, bevelThickness: 0.1, bevelSize: 0.1, bevelSegments: 4, bevelOffset: -0.1 });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.8, -0.1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.55, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.8, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.15, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.4, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.06, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 7, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -3, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 6, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -3, 0);
  levelEnvironment[0].obstacle[29].part[0].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[29].part[0].part[1] = new THREE.Mesh(new THREE.CircleGeometry(0.09, 6), new THREE.MeshBasicMaterial({ color: 0x424242 }));
  levelEnvironment[0].obstacle[29].part[0].part[1].geometry.translate(0, 4.5, 0.21);
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(0.08, 0.6, 3, 6);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 4.9, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.5, 0, 0);
  levelEnvironment[0].obstacle[29].part[0].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[29].part[0].add(levelEnvironment[0].obstacle[29].part[0].part[2], levelEnvironment[0].obstacle[29].part[0].part[1], levelEnvironment[0].obstacle[29].part[0].part[0]);
  levelEnvironment[0].obstacle[29].part[0].rotation.set(Math.PI, Math.PI * 0.5, 0);
  levelEnvironment[0].obstacle[29].part[0].position.set(0, 5, 0);
  levelEnvironment[0].obstacle[29].add(levelEnvironment[0].obstacle[29].part[0]);
  levelEnvironment[0].obstacle[29].box = [0.5, 9.6];
  levelEnvironment[0].obstacle[29].ready = true;
  levelEnvironment[0].obstacle[29].dir = 2.5;
  levelEnvironment[0].obstacle[29].position.set(0, -1.5, -148.1);
  levelEnvironment[0].obstacle[30] = new THREE.Object3D();
  levelEnvironment[0].obstacle[30].box = [0.5, 0.4];
  levelEnvironment[0].obstacle[30].ready = false;
  levelEnvironment[0].obstacle[30].dir = 2.5;
  levelEnvironment[0].obstacle[30].position.set(0, 400, -148.1);
  levelEnvironment[0].obstacle[31] = new THREE.Object3D();
  levelEnvironment[0].obstacle[31].part = [];
  levelEnvironment[0].obstacle[31].part[0] = new THREE.Object3D();
  levelEnvironment[0].obstacle[31].part[0].part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.6, 0, Math.PI * 0.5);
  shape.absarc(0, 0, 0.2, Math.PI * 0.5, 0, true);
  shape.lineTo(0.2, -0.5)
  shape.lineTo(0.6, -0.5)
  levelEnvironment[0].obstacle[31].part[0].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x0D47A1, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[31].part[0].part[0].geometry.translate(0, 0, -0.2);
  levelEnvironment[0].obstacle[31].part[0].part[0].geometry.rotateY(Math.PI * 0.5);
  levelEnvironment[0].obstacle[31].part[0].part[1] = new THREE.Mesh(levelEnvironment[0].obstacle[31].part[0].part[0].geometry.clone(), new THREE.MeshToonMaterial({ color: 0xC62828, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[31].part[0].part[1].geometry.rotateY(Math.PI);
  levelEnvironment[0].obstacle[31].part[0].add(levelEnvironment[0].obstacle[31].part[0].part[1], levelEnvironment[0].obstacle[31].part[0].part[0]);
  levelEnvironment[0].obstacle[31].part[0].position.y = 0.5;
  levelEnvironment[0].obstacle[31].add(levelEnvironment[0].obstacle[31].part[0]);
  levelEnvironment[0].obstacle[31].scale.set(0.8, 0.8, 0.8);
  levelEnvironment[0].obstacle[31].box = [1.2 * 0.8, 1.2 * 0.8];
  levelEnvironment[0].obstacle[31].ready = true;
  levelEnvironment[0].obstacle[31].dir = 1.6;
  levelEnvironment[0].obstacle[31].position.set(0, -2.1, -151.3);
  levelEnvironment[0].obstacle[32] = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`F=ma`, 0.5), { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x689F38, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[32].geometry.computeBoundingBox();
  levelEnvironment[0].obstacle[32].geometry.translate(-0.5 * levelEnvironment[0].obstacle[32].geometry.boundingBox.max.x, 0, -1);
  levelEnvironment[0].obstacle[32].box = [levelEnvironment[0].obstacle[32].geometry.boundingBox.max.x * 1.8, levelEnvironment[0].obstacle[32].geometry.boundingBox.max.y];
  levelEnvironment[0].obstacle[32].ready = true;
  levelEnvironment[0].obstacle[32].dir = 2;
  levelEnvironment[0].obstacle[32].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[32].position.set(0, -1.3, -155.8);
  levelEnvironment[0].obstacle[33] = levelEnvironment[0].obstacle[31].clone();
  levelEnvironment[0].obstacle[33].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[0].obstacle[33].box = [1.2 * 1.2, 1.2 * 1.2];
  levelEnvironment[0].obstacle[33].ready = true;
  levelEnvironment[0].obstacle[33].dir = 2.1;
  levelEnvironment[0].obstacle[33].position.set(0, 1.8, -152.8);
  levelEnvironment[0].obstacle[34] = levelEnvironment[0].obstacle[28].clone();
  levelEnvironment[0].obstacle[34].children[4].material = new THREE.MeshToonMaterial({ color: 0x3949AB, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[6].material = new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[8].material = new THREE.MeshToonMaterial({ color: 0x1B5E20, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[5].material = new THREE.MeshToonMaterial({ color: 0xE65100, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[0].material = new THREE.MeshToonMaterial({ color: 0xD50000, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[1].material = new THREE.MeshToonMaterial({ color: 0xD50000, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[2].material = new THREE.MeshToonMaterial({ color: 0xD50000, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].children[3].material = new THREE.MeshToonMaterial({ color: 0xD50000, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[34].scale.set(0.8, 0.8, 0.8);
  levelEnvironment[0].obstacle[34].box = [0.6 * 0.8, 9.6 * 0.8];
  levelEnvironment[0].obstacle[34].ready = true;
  levelEnvironment[0].obstacle[34].dir = 2;
  levelEnvironment[0].obstacle[34].position.set(0, -11, -160.1);
  levelEnvironment[0].obstacle[35] = levelEnvironment[0].obstacle[31].clone();
  levelEnvironment[0].obstacle[35].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[35].box = [1.2 * 0.7, 1.2 * 0.7];
  levelEnvironment[0].obstacle[35].ready = true;
  levelEnvironment[0].obstacle[35].dir = 2.1;
  levelEnvironment[0].obstacle[35].position.set(0, -3, -155.6);
  levelEnvironment[0].obstacle[36] = new THREE.Object3D();
  levelEnvironment[0].obstacle[36].part = [];
  levelEnvironment[0].obstacle[36].part[0] = levelEnvironment[0].obstacle[34].clone();
  levelEnvironment[0].obstacle[36].part[0].rotation.x = Math.PI;
  levelEnvironment[0].obstacle[36].part[0].position.set(0, 9.6 * 0.8, 0);
  levelEnvironment[0].obstacle[36].add(levelEnvironment[0].obstacle[36].part[0]);
  levelEnvironment[0].obstacle[36].box = [0.6 * 0.8, 9.6 * 0.8];
  levelEnvironment[0].obstacle[36].ready = true;
  levelEnvironment[0].obstacle[36].dir = 2;
  levelEnvironment[0].obstacle[36].position.set(0, 0, -160.1);
  levelEnvironment[0].obstacle[37] = new THREE.Object3D();
  levelEnvironment[0].obstacle[37].part = [];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.25, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(-0.2, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.3, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.3, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.25, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.2, 10, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.2, 0);
  levelEnvironment[0].obstacle[37].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), normalMaterial);
  clearTempGeometries();
  levelEnvironment[0].obstacle[37].part[1] = new THREE.Object3D();
  levelEnvironment[0].obstacle[37].part[1].orbit = new THREE.Mesh(new THREE.TorusGeometry(1.1, 0.04, 4, 20), blimMaterial[0]);
  levelEnvironment[0].obstacle[37].part[1].part = new THREE.Mesh(new THREE.SphereGeometry(0.25, 10, 5), normalMaterial);
  levelEnvironment[0].obstacle[37].part[1].part.geometry.translate(0, 1.1, 0);
  levelEnvironment[0].obstacle[37].part[1].add(levelEnvironment[0].obstacle[37].part[1].orbit, levelEnvironment[0].obstacle[37].part[1].part);
  levelEnvironment[0].obstacle[37].part[2] = new THREE.Object3D();
  levelEnvironment[0].obstacle[37].part[2].orbit = new THREE.Mesh(new THREE.TorusGeometry(1.2, 0.04, 4, 20), blimMaterial[1]);
  levelEnvironment[0].obstacle[37].part[2].part = new THREE.Mesh(new THREE.SphereGeometry(0.25, 10, 5), normalMaterial);
  levelEnvironment[0].obstacle[37].part[2].add(levelEnvironment[0].obstacle[37].part[2].orbit, levelEnvironment[0].obstacle[37].part[2].part);
  levelEnvironment[0].obstacle[37].part[2].part.geometry.translate(0, 1.2, 0);
  levelEnvironment[0].obstacle[37].part[3] = new THREE.Object3D();
  levelEnvironment[0].obstacle[37].part[3].orbit = new THREE.Mesh(new THREE.TorusGeometry(1.3, 0.04, 4, 20), blimMaterial[2]);
  levelEnvironment[0].obstacle[37].part[3].part = new THREE.Mesh(new THREE.SphereGeometry(0.25, 10, 5), normalMaterial);
  levelEnvironment[0].obstacle[37].part[3].add(levelEnvironment[0].obstacle[37].part[3].orbit, levelEnvironment[0].obstacle[37].part[3].part);
  levelEnvironment[0].obstacle[37].part[3].part.geometry.translate(0, 1.3, 0);
  levelEnvironment[0].obstacle[37].part[2].rotation.set(2, 2, 2);
  levelEnvironment[0].obstacle[37].part[3].rotation.set(-1, -3, -1);
  levelEnvironment[0].obstacle[37].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[37].container.position.y = 1;
  levelEnvironment[0].obstacle[37].container.add(levelEnvironment[0].obstacle[37].part[3], levelEnvironment[0].obstacle[37].part[2], levelEnvironment[0].obstacle[37].part[1], levelEnvironment[0].obstacle[37].part[0]);
  levelEnvironment[0].obstacle[37].add(levelEnvironment[0].obstacle[37].container);
  levelEnvironment[0].obstacle[37].scale.set(0.8, 0.8, 0.8);
  levelEnvironment[0].obstacle[37].box = [1.8 * 0.8, 1.8 * 0.8];
  levelEnvironment[0].obstacle[37].ready = true;
  levelEnvironment[0].obstacle[37].dir = 2.5;
  levelEnvironment[0].obstacle[37].position.set(0, -2.5, -164.1);
  levelEnvironment[0].obstacle[38] = levelEnvironment[0].obstacle[37].clone();
  levelEnvironment[0].obstacle[38].scale.set(0.6, 0.6, 0.6);
  levelEnvironment[0].obstacle[38].box = [1.8 * 0.6, 1.8 * 0.6];
  levelEnvironment[0].obstacle[38].ready = true;
  levelEnvironment[0].obstacle[38].dir = 2.5;
  levelEnvironment[0].obstacle[38].position.set(0, 1.8, -166.1);
  levelEnvironment[0].obstacle[39] = new THREE.Object3D();
  levelEnvironment[0].obstacle[39].part = [];
  levelEnvironment[0].obstacle[39].part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`E=mc`, 0.6), { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x689F38, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[39].part[0].geometry.computeBoundingBox();
  levelEnvironment[0].obstacle[39].part[0].geometry.translate(-0.5 * levelEnvironment[0].obstacle[39].part[0].geometry.boundingBox.max.x, 0, -1);
  levelEnvironment[0].obstacle[39].part[1] = new THREE.Mesh(new THREE.ExtrudeGeometry(PressStart.generateShapes(`2`, 0.3), { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x689F38, gradientMap: gradientMap[4] }));
  levelEnvironment[0].obstacle[39].part[1].geometry.translate(1.9, 0.7, -1);
  levelEnvironment[0].obstacle[39].add(levelEnvironment[0].obstacle[39].part[1], levelEnvironment[0].obstacle[39].part[0]);
  levelEnvironment[0].obstacle[39].box = [levelEnvironment[0].obstacle[39].part[0].geometry.boundingBox.max.x * 1.8, levelEnvironment[0].obstacle[39].part[0].geometry.boundingBox.max.y];
  levelEnvironment[0].obstacle[39].ready = true;
  levelEnvironment[0].obstacle[39].dir = 1.5;
  levelEnvironment[0].obstacle[39].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[39].position.set(0, -0.1, -168.6);
  levelEnvironment[0].obstacle[40] = levelEnvironment[0].obstacle[37].clone();
  levelEnvironment[0].obstacle[40].scale.set(0.9, 0.9, 0.9);
  levelEnvironment[0].obstacle[40].box = [1.8 * 0.9, 1.8 * 0.9];
  levelEnvironment[0].obstacle[40].ready = true;
  levelEnvironment[0].obstacle[40].dir = 1.5;
  levelEnvironment[0].obstacle[40].position.set(0, -3.7, -172.3);
  levelEnvironment[0].obstacle[41] = levelEnvironment[0].obstacle[37].clone();
  levelEnvironment[0].obstacle[41].scale.set(0.6, 0.6, 0.6);
  levelEnvironment[0].obstacle[41].box = [1.8 * 0.6, 1.8 * 0.6];
  levelEnvironment[0].obstacle[41].ready = true;
  levelEnvironment[0].obstacle[41].dir = 1.5;
  levelEnvironment[0].obstacle[41].position.set(0, -0.3, -173.6);
  levelEnvironment[0].obstacle[42] = levelEnvironment[0].obstacle[36].clone();
  levelEnvironment[0].obstacle[42].children[0].children[4].material = new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[6].material = new THREE.MeshToonMaterial({ color: 0x00695C, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[8].material = new THREE.MeshToonMaterial({ color: 0x4E342E, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[5].material = new THREE.MeshToonMaterial({ color: 0x4527A0, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[0].material = new THREE.MeshToonMaterial({ color: 0x33691E, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[1].material = new THREE.MeshToonMaterial({ color: 0x33691E, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[2].material = new THREE.MeshToonMaterial({ color: 0x33691E, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].children[0].children[3].material = new THREE.MeshToonMaterial({ color: 0x33691E, gradientMap: gradientMap[4] });
  levelEnvironment[0].obstacle[42].box = [0.6 * 0.8, 9.6 * 0.8];
  levelEnvironment[0].obstacle[42].ready = true;
  levelEnvironment[0].obstacle[42].dir = 2;
  levelEnvironment[0].obstacle[42].position.set(0, -0.6, -177.3);
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 0, 1, 4.5, 0, Math.PI * 2);
  shape.lineTo(10, 0);
  shape.lineTo(10, -10);
  shape.lineTo(-5.97, -10);
  shape.lineTo(-5.97, 10);
  shape.lineTo(10, 10);
  shape.lineTo(10, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 48, bevelEnabled: false });
  levelEnvironment[0].wall[22] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), normalMaterial);
  clearTempGeometries();
  levelEnvironment[0].wall[22].position.set(40.6, 0.4, 0)
  levelEnvironment[0].wall[22].rotation.y = Math.PI * 0.5;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2.6, 0, Math.PI * 2);
  shape.absarc(0, 0, 1.1, Math.PI * 2, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 6, bevelEnabled: false });
  levelEnvironment[3].wall[23] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[23].rotation.y = Math.PI * 0.5;
  levelEnvironment[3].wall[23].position.set(0, 0.4, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2.3, 0, Math.PI * 2);
  shape.absarc(0, 0, 1.3, Math.PI * 2, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.5, curveSegments: 6, bevelEnabled: false });
  levelEnvironment[3].wall[24] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), normalMaterial);
  clearTempGeometries();
  levelEnvironment[3].wall[24].rotation.y = Math.PI * 0.5;
  levelEnvironment[3].wall[24].position.set(0, 0.4, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2.1, 0, Math.PI * 2);
  shape.absarc(0, 0, 1.5, Math.PI * 2, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 6, bevelEnabled: false });
  levelEnvironment[3].wall[25] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x263238, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[25].rotation.y = Math.PI * 0.5;
  levelEnvironment[3].wall[25].position.set(0, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.25, 0.25, 0.4, 6, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.75, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 12);
  for (let j = 0; j < 16; j++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 6);
  }
  levelEnvironment[3].wall[26] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x1A237E, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[3].wall[26].rotation.y = Math.PI * 0.5;
  levelEnvironment[3].wall[26].position.set(0.5, 0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.16, 8, 1, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 1.75, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 12);
  for (let k = 0; k < 16; k++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 6);
  }
  levelEnvironment[3].wall[27] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), levelEnvironment[3].wall[4].material);
  levelEnvironment[3].wall[27].rotation.y = Math.PI * 0.5;
  levelEnvironment[3].wall[27].position.set(0.7, 0.4, 0);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(41, 4);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(20.5, 7, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 16);
  for (let k = 0; k < 16; k++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 8);
  }
  levelEnvironment[3].wall[28] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x4A148C }));
  levelEnvironment[3].wall[28].position.set(0, 0.4, 4);
  clearTempGeometries();
  levelEnvironment[3].greenlimColor = new THREE.Color(0x000000);
  levelEnvironment[3].pendulum = new THREE.Object3D();
  levelEnvironment[3].pendulum.part = [];
  levelEnvironment[3].pendulum.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.4, 0.4, 2, 16, 1), new THREE.MeshToonMaterial({ color: 0x3E2723, gradientMap: gradientMap[4] }));
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, Math.PI, Math.PI * 2);
  levelEnvironment[3].pendulum.part[1] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  levelEnvironment[3].pendulum.part[1].geometry.translate(0, -1, -0.1);
  levelEnvironment[3].pendulum.part[2] = new THREE.Object3D();
  levelEnvironment[3].pendulum.part[2].part = [];
  levelEnvironment[3].pendulum.part[2].part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.04, 0.04, 5, 4, 1), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[4] }));
  levelEnvironment[3].pendulum.part[2].part[0].geometry.translate(0, -2.5, 0);
  levelEnvironment[3].pendulum.part[2].part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.6, 18, 10), new THREE.MeshToonMaterial({ color: 0x4E342E, gradientMap: gradientMap[4] }));
  levelEnvironment[3].pendulum.part[2].part[1].geometry.translate(0, -5.6, 0);
  levelEnvironment[3].pendulum.part[2].add(levelEnvironment[3].pendulum.part[2].part[1], levelEnvironment[3].pendulum.part[2].part[0]);
  levelEnvironment[3].pendulum.part[2].rotation.z = 0.5;
  levelEnvironment[3].pendulum.part[2].position.y = -1;
  levelEnvironment[3].pendulum.add(levelEnvironment[3].pendulum.part[2], levelEnvironment[3].pendulum.part[1], levelEnvironment[3].pendulum.part[0]);
  levelEnvironment[3].pendulum.position.set(6.8, 4, -1.5);
  levelEnvironment[3].pendulum1 = levelEnvironment[3].pendulum.clone();
  levelEnvironment[3].pendulum1.scale.set(0.8, 0.8, 0.8);
  levelEnvironment[3].pendulum1.position.set(33.8, 4, -1.5);
  levelEnvironment[3].pic = [];
  levelEnvironment[3].pic[0] = createSVG(6, 3, 0x000000);
  levelEnvironment[3].pic[0].position.set(0.3, 2, -1.5);
  levelEnvironment[3].pic[0].scale.set(0.006, -0.006, 1);
  levelEnvironment[3].pic[9] = createSVG(14, 3, 0x000000);
  levelEnvironment[3].pic[9].position.set(35, 0, -1);
  levelEnvironment[3].pic[9].scale.set(0.006, -0.006, 1);
  levelEnvironment[3].pic[8] = createSVG(16, 3, 0x000000);
  levelEnvironment[3].pic[8].position.set(33.5, 3, -1.5);
  levelEnvironment[3].pic[8].scale.set(0.006, -0.006, 1);
  levelEnvironment[3].pic[11] = createSVG(18, 2, 0x000000);
  levelEnvironment[3].pic[11].position.set(33.5, -0.5, -1.5);
  levelEnvironment[3].pic[11].rotation.z = -0.5;
  levelEnvironment[3].pic[11].scale.set(0.0025, -0.0025, 1);
  levelEnvironment[3].pic[5] = createSVG(7, 3, 0x000000);
  levelEnvironment[3].pic[5].position.set(28.3, 2, -1.5);
  levelEnvironment[3].pic[5].scale.set(0.007, -0.007, 1);
  levelEnvironment[3].pic[12] = createSVG(13, 2, 0x000000);
  levelEnvironment[3].pic[12].position.set(24.7, -0.4, -1);
  levelEnvironment[3].pic[12].rotation.z = -0.26;
  levelEnvironment[3].pic[12].scale.set(0.005, -0.005, 1);
  levelEnvironment[3].pic[6] = createSVG(8, 3, 0x000000);
  levelEnvironment[3].pic[6].position.set(24, 3, -1.5);
  levelEnvironment[3].pic[6].scale.set(0.007, -0.007, 1);
  levelEnvironment[3].pic[4] = createSVG(10, 3, 0x000000);
  levelEnvironment[3].pic[4].position.set(19, 1.9, -1.5);
  levelEnvironment[3].pic[4].scale.set(0.007, -0.007, 1);
  levelEnvironment[3].pic[2] = createSVG(15, 4, 0x000000);
  levelEnvironment[3].pic[2].position.set(16.5, 5, -1.5);
  levelEnvironment[3].pic[2].rotation.z = -1;
  levelEnvironment[3].pic[2].scale.set(0.007, -0.007, 1);
  levelEnvironment[3].pic[1] = createSVG(11, 5, 0x000000);
  levelEnvironment[3].pic[1].position.set(8, 2, -1.5);
  levelEnvironment[3].pic[1].scale.set(0.008, -0.008, 1);
  levelEnvironment[3].pic[3] = createSVG(12, 2, 0x000000);
  levelEnvironment[3].pic[3].position.set(13.6, 0.2, -1);
  levelEnvironment[3].pic[3].rotation.z = -0.5;
  levelEnvironment[3].pic[3].scale.set(0.007, -0.007, 1);
  levelEnvironment[3].pic[7] = createSVG(9, 3, 0x000000);
  levelEnvironment[3].pic[7].position.set(4, -1.4, -0.5);
  levelEnvironment[3].pic[7].scale.set(0.0045, -0.0045, 1);
  levelEnvironment[3].pic[10] = createSVG(17, 3, 0x000000);
  levelEnvironment[3].pic[10].position.set(6, 4.5, -1);
  levelEnvironment[3].pic[10].rotation.z = -0.5;
  levelEnvironment[3].pic[10].scale.set(0.005, -0.005, 1);
  levelEnvironment[3].sparkle = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.1, 0);
  shape.lineTo(0.03, 0.03);
  shape.lineTo(0, 0.1);
  shape.lineTo(-0.03, 0.03);
  shape.lineTo(-0.1, 0);
  shape.lineTo(-0.03, -0.03);
  shape.lineTo(0, -0.1);
  shape.lineTo(0.03, -0.03);
  introContainer.sparkle = [];
  for (let i = 0; i < 38; i++) {
    levelEnvironment[3].sparkle[i] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
    levelEnvironment[3].sparkle[i].scale.set(0, 0, 1);
    levelEnvironment[3].add(levelEnvironment[3].sparkle[i]);
  }
  levelEnvironment[3].sparkle[0].position.set(1, 2, -0.9);
  levelEnvironment[3].sparkle[1].position.set(3, -3, -0.9);
  levelEnvironment[3].sparkle[2].position.set(5, 1, -0.9);
  levelEnvironment[3].sparkle[3].position.set(7, 3.5, -0.9);
  levelEnvironment[3].sparkle[4].position.set(9, -2, -0.9);
  levelEnvironment[3].sparkle[5].position.set(11, 0, -0.9);
  levelEnvironment[3].sparkle[6].position.set(13, -3, -0.9);
  levelEnvironment[3].sparkle[7].position.set(15, 1, -0.9);
  levelEnvironment[3].sparkle[8].position.set(17, 2.5, -0.9);
  levelEnvironment[3].sparkle[9].position.set(19, -3, -0.9);
  levelEnvironment[3].sparkle[10].position.set(21, -1, -0.9);
  levelEnvironment[3].sparkle[11].position.set(23, 3.5, -0.9);
  levelEnvironment[3].sparkle[12].position.set(25, -2, -0.9);
  levelEnvironment[3].sparkle[13].position.set(27, -1, -0.9);
  levelEnvironment[3].sparkle[14].position.set(29, 3, -0.9);
  levelEnvironment[3].sparkle[15].position.set(31, 0, -0.9);
  levelEnvironment[3].sparkle[16].position.set(34, -2, -0.9);
  levelEnvironment[3].sparkle[17].position.set(36, 3.5, -0.9);
  levelEnvironment[3].sparkle[18].position.set(38, 0, -0.9);
  levelEnvironment[3].sparkle[19].position.set(2, 0, -0.9);
  levelEnvironment[3].sparkle[20].position.set(4, 3, -0.9);
  levelEnvironment[3].sparkle[21].position.set(6, -2, -0.9);
  levelEnvironment[3].sparkle[22].position.set(8, -1, -0.9);
  levelEnvironment[3].sparkle[23].position.set(10, 2, -0.9);
  levelEnvironment[3].sparkle[24].position.set(12, -1, -0.9);
  levelEnvironment[3].sparkle[25].position.set(14, 3, -0.9);
  levelEnvironment[3].sparkle[26].position.set(16, -2, -0.9);
  levelEnvironment[3].sparkle[27].position.set(18, 0, -0.9);
  levelEnvironment[3].sparkle[28].position.set(20, 3.5, -0.9);
  levelEnvironment[3].sparkle[29].position.set(22, 1, -0.9);
  levelEnvironment[3].sparkle[30].position.set(24, 0, -0.9);
  levelEnvironment[3].sparkle[31].position.set(26, 1, -0.9);
  levelEnvironment[3].sparkle[32].position.set(28, -2, -0.9);
  levelEnvironment[3].sparkle[33].position.set(30, -3, -0.9);
  levelEnvironment[3].sparkle[34].position.set(32, 2, -0.9);
  levelEnvironment[3].sparkle[35].position.set(34, 0, -0.9);
  levelEnvironment[3].sparkle[36].position.set(37, -3, -0.9);
  levelEnvironment[3].sparkle[37].position.set(39, 1, -0.9);
  levelEnvironment[3].position.z = -140.35;
  levelEnvironment[3].add(levelEnvironment[3].pic[12], levelEnvironment[3].pic[11], levelEnvironment[3].pic[10], levelEnvironment[3].pic[9], levelEnvironment[3].pic[8], levelEnvironment[3].pic[7], levelEnvironment[3].pic[6], levelEnvironment[3].pic[5], levelEnvironment[3].pic[4], levelEnvironment[3].pic[3], levelEnvironment[3].pic[2], levelEnvironment[3].pic[1], levelEnvironment[3].pic[0], levelEnvironment[3].pendulum1, levelEnvironment[3].pendulum, levelEnvironment[3].wall[28], levelEnvironment[0].wall[22], levelEnvironment[3].wall[27], levelEnvironment[3].wall[26], levelEnvironment[3].wall[25], levelEnvironment[3].wall[24], levelEnvironment[3].wall[23], levelEnvironment[3].wall[4], levelEnvironment[3].wall[21], levelEnvironment[3].wall[20], levelEnvironment[3].wall[2], levelEnvironment[3].wall[3], levelEnvironment[3].wall[0], levelEnvironment[3].wall[1]);
  levelEnvironment[3].rotation.y = Math.PI * 0.5;
  levelEnvironment[4].topBlack = new THREE.Mesh(new THREE.PlaneGeometry(30, 20), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  levelEnvironment[4].topBlack.geometry.translate(15, 0, 0);
  levelEnvironment[4].topBlack.position.z = 5.98;
  levelEnvironment[4].backBlack = levelEnvironment[4].topBlack.clone();
  levelEnvironment[4].backBlack.position.z = -10;
  levelEnvironment[4].roundLamp = new THREE.Object3D();
  levelEnvironment[4].roundLamp.part = [];
  levelEnvironment[4].roundLamp.part[0] = new THREE.Mesh(new THREE.CylinderGeometry(0.51, 0.51, 0.1, 16, 1), new THREE.MeshToonMaterial({ color: 0x212121, gradientMap: gradientMap[4] }));
  levelEnvironment[4].roundLamp.part[2] = new THREE.Mesh(new THREE.CylinderGeometry(0.02, 0.02, 1, 6, 1), levelEnvironment[4].roundLamp.part[0].material);
  levelEnvironment[4].roundLamp.part[2].geometry.translate(0, 0.5, 0);
  levelEnvironment[4].roundLamp.part[1] = new THREE.Mesh(new THREE.SphereGeometry(0.5, 16, 4, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5), whiteBasicMaterial);
  levelEnvironment[4].roundLamp.part[1].geometry.translate(0, -0.1, 0);
  levelEnvironment[4].roundLamp.part[1].scale.y = 0.3;
  levelEnvironment[4].roundLamp.add(levelEnvironment[4].roundLamp.part[2], levelEnvironment[4].roundLamp.part[0], levelEnvironment[4].roundLamp.part[1]);
  levelEnvironment[4].tower = new THREE.Object3D();
  levelEnvironment[4].tower.wall = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2, 0);
  shape.lineTo(2, 5);
  shape.lineTo(-2, 5);
  shape.lineTo(-2, 0);
  shape.lineTo(-1, 1);
  shape.absarc(0, 3, 1, Math.PI, 0, true);
  shape.lineTo(1, 1);
  shape.lineTo(-1, 1);
  shape.lineTo(-2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2, 0);
  shape.lineTo(2, 5);
  shape.lineTo(-2, 5);
  shape.lineTo(-2, 0);
  shape.lineTo(-0.8, 1.2);
  shape.lineTo(-0.8, 3.8);
  shape.lineTo(0.8, 3.8);
  shape.lineTo(0.8, 1.2);
  shape.lineTo(-0.8, 1.2);
  shape.lineTo(-2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2, 0);
  shape.lineTo(2, 5);
  shape.lineTo(-2, 5);
  shape.lineTo(-2, 0);
  shape.lineTo(-0.5, 1.2);
  shape.lineTo(-0.5, 3.5);
  shape.lineTo(0.5, 3.5);
  shape.lineTo(0.5, 1.2);
  shape.lineTo(-0.5, 1.2);
  shape.lineTo(-2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(2, 0);
  shape.lineTo(2, 5);
  shape.lineTo(-2, 5);
  shape.lineTo(-2, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  levelEnvironment[4].tower.wall[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x757575, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 1);
  shape.absarc(0, 3, 1, 0, Math.PI);
  shape.lineTo(-1, 1);
  shape.lineTo(1, 1);
  shape.lineTo(0.8, 1.2);
  shape.lineTo(-0.8, 1.2);
  shape.absarc(0, 3, 0.8, Math.PI, 0, true);
  shape.lineTo(0.8, 1.2);
  shape.lineTo(1, 1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.05, 1);
  shape.lineTo(0.05, 4);
  shape.lineTo(-0.05, 4);
  shape.lineTo(-0.05, 1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 2.9);
  shape.lineTo(1, 3);
  shape.lineTo(-1, 3);
  shape.lineTo(-1, 2.9);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 2);
  shape.lineTo(1, 2.1);
  shape.lineTo(-1, 2.1);
  shape.lineTo(-1, 2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.05, 1);
  shape.lineTo(0.05, 4);
  shape.lineTo(-0.05, 4);
  shape.lineTo(-0.05, 1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.8, 1.2);
  shape.lineTo(0.8, 3.8);
  shape.lineTo(-0.8, 3.8);
  shape.lineTo(-0.8, 1.2);
  shape.lineTo(0.8, 1.2);
  shape.lineTo(0.6, 1.4);
  shape.lineTo(-0.6, 1.4);
  shape.lineTo(-0.6, 3.6);
  shape.lineTo(0.6, 3.6);
  shape.lineTo(0.6, 1.4);
  shape.lineTo(0.8, 1.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.2);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 2.05);
  shape.lineTo(1, 2.15);
  shape.lineTo(-1, 2.15);
  shape.lineTo(-1, 2.05);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 2.95);
  shape.lineTo(1, 2.85);
  shape.lineTo(-1, 2.85);
  shape.lineTo(-1, 2.95);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 1.2);
  shape.lineTo(0.5, 3.5);
  shape.lineTo(-0.5, 3.5);
  shape.lineTo(-0.5, 1.2);
  shape.lineTo(0.5, 1.2);
  shape.lineTo(0.3, 1.4);
  shape.lineTo(-0.3, 1.4);
  shape.lineTo(-0.3, 3.3);
  shape.lineTo(0.3, 3.3);
  shape.lineTo(0.3, 1.4);
  shape.lineTo(0.5, 1.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.2);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1, 2.4);
  shape.lineTo(1, 2.3);
  shape.lineTo(-1, 2.3);
  shape.lineTo(-1, 2.4);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.1);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  levelEnvironment[4].tower.wall[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(1.1, 1);
  shape.lineTo(1.1, 1.1);
  shape.lineTo(-1.1, 1.1);
  shape.lineTo(-1.1, 1);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.2, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  levelEnvironment[4].tower.wall[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.9, 1.2);
  shape.lineTo(0.9, 1.3);
  shape.lineTo(-0.9, 1.3);
  shape.lineTo(-0.9, 1.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  levelEnvironment[4].tower.wall[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, 1.2);
  shape.lineTo(0.6, 1.3);
  shape.lineTo(-0.6, 1.3);
  shape.lineTo(-0.6, 1.2);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.2, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -5.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 10, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4 * 3);
  levelEnvironment[4].tower.wall[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.2, 0, Math.PI);
  shape.absarc(0, 0, 5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.08, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.95, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -35, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0, 0.08);
  shape.lineTo(-0.4, 0.08);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 10, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-4.2, 4.95, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.8, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-8.8, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.25, 0, Math.PI);
  shape.absarc(0, 0, 5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.05, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -34.45, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0, 0.05);
  shape.lineTo(-0.4, 0.05);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 10, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-4.25, 4.7, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(8.9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -4.45, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-8.9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 0);
  shape.lineTo(0.5, 0.08);
  shape.lineTo(-0.5, 0.08);
  shape.lineTo(-0.5, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.61, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.32, -5.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5 - Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.78, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5.1, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0.5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  levelEnvironment[4].tower.wall[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.3, 0, Math.PI);
  shape.absarc(0, 0, 5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.24, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.71, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -34.7, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.45, 0, Math.PI);
  shape.absarc(0, 0, 5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.3, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.49, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0, 0.24);
  shape.lineTo(-0.4, 0.24);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 10, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-4.3, 4.71, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(9, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -4.7, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-9, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0, 0.3);
  shape.lineTo(-0.4, 0.3);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 10, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-4.45, 4.49, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(9.3, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0);
  shape.lineTo(0.4, 0.7);
  shape.lineTo(-0.4, 0.7);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.6, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.4, -5.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5 - Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -34.6, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0);
  shape.lineTo(0.4, 0.08);
  shape.lineTo(-0.4, 0.08);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.61, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 5.72, -5.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5 - Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  levelEnvironment[4].tower.wall[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x78909C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.4, 0, Math.PI);
  shape.absarc(0, 0, 5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.14, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 4.57, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -34.3, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(0, 0.14);
  shape.lineTo(-0.4, 0.14);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 10, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(-4.4, 4.57, -5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(9.2, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -4.3, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-9.2, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.4, 0);
  shape.lineTo(0.4, 0.08);
  shape.lineTo(-0.4, 0.08);
  shape.lineTo(-0.4, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.61, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 3.88, -5.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5 - Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -32.9, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  levelEnvironment[4].tower.wall[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x9E9E9E, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[4].tower.wall[8] = new THREE.Mesh(new THREE.SphereGeometry(4.5, 12, 8, 0, Math.PI, Math.PI * 0.5, Math.PI * 0.5), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[6], side: THREE.BackSide }));
  levelEnvironment[4].tower.wall[8].rotation.x = Math.PI;
  levelEnvironment[4].tower.wall[8].position.y = 35;
  levelEnvironment[4].tower.wall[9] = new THREE.Mesh(new THREE.CylinderGeometry(4.5, 4.5, 10, 16, 1, true, Math.PI * 0.5, Math.PI), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[6], side: THREE.BackSide }));
  levelEnvironment[4].tower.wall[9].rotation.x = Math.PI * 0.5;
  levelEnvironment[4].tower.wall[9].position.y = 35;
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.34, 0.34, 35, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(4.5 * Math.cos(-Math.PI / 8), 17.5, -4.5 * Math.sin(-Math.PI / 8));
  for (let i = 1; i < 6; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[0].clone();
    tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4 * i);
  }
  levelEnvironment[4].tower.wall[10] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.36, -0.5);
  shape.lineTo(0.36, 1.4);
  shape.lineTo(-0.36, 1.4);
  shape.lineTo(-0.36, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 1.55, curveSegments: 1, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4.4, -5.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5 - Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 5, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, -34.8, 0);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 4);
  }
  levelEnvironment[4].tower.wall[11] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xA1887F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.15, 0, Math.PI * 0.5);
  shape.absarc(0, 0, 5, Math.PI * 0.5, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.8, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 35.1, -0.4);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  levelEnvironment[4].tower.wall[12] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x37474F, gradientMap: gradientMap[6] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 4.1, 0, Math.PI * 0.5);
  shape.absarc(0, 0, 5, Math.PI * 0.5, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 8, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 35.1, -0.2);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI / 8);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 4);
  }
  levelEnvironment[4].tower.wall[13] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[6] }));
  clearTempGeometries();
  levelEnvironment[4].tower.cloud = building.cloud[0].clone();
  levelEnvironment[4].tower.cloud.position.set(4, 8, -15);
  levelEnvironment[4].tower.cloud.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[4].tower.cloud1 = building.cloud[0].clone();
  levelEnvironment[4].tower.cloud1.position.set(-7, 13, -25);
  levelEnvironment[4].tower.cloud1.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[4].tower.cloud2 = building.cloud[0].clone();
  levelEnvironment[4].tower.cloud2.position.set(0, 18, -20);
  levelEnvironment[4].tower.cloud2.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[4].tower.cloud3 = building.cloud[0].clone();
  levelEnvironment[4].tower.cloud3.position.set(7, 28, -20);
  levelEnvironment[4].tower.cloud3.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[4].tower.cloud4 = building.cloud[0].clone();
  levelEnvironment[4].tower.cloud4.position.set(-5, 33, -15);
  levelEnvironment[4].tower.cloud4.scale.set(2.5, 2.5, 2.5);
  levelEnvironment[4].tower.pic = new THREE.Object3D();
  levelEnvironment[4].tower.pic.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic.part[0].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0x42A5F5 }));
  levelEnvironment[4].tower.pic.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic.part[1].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic.part[2] = new THREE.Mesh(new THREE.CircleGeometry(0.3, 12), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  levelEnvironment[4].tower.pic.part[2].position.z = -4.48;
  levelEnvironment[4].tower.pic.part[2].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic.part[3] = new THREE.Mesh(new THREE.PlaneGeometry(1, 0.6), new THREE.MeshBasicMaterial({ color: 0x4CAF50 }));
  levelEnvironment[4].tower.pic.part[3].position.set(0, -0.6, -4.47);
  levelEnvironment[4].tower.pic.part[3].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic.add(levelEnvironment[4].tower.pic.part[3], levelEnvironment[4].tower.pic.part[2], levelEnvironment[4].tower.pic.part[1], levelEnvironment[4].tower.pic.part[0]);
  levelEnvironment[4].tower.pic.rotation.y = Math.PI / 4;
  levelEnvironment[4].tower.pic.position.y = 2.5;
  levelEnvironment[4].tower.pic1 = new THREE.Object3D();
  levelEnvironment[4].tower.pic1.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic1.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic1.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic1.part[0].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic1.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0xB0BEC5 }));
  levelEnvironment[4].tower.pic1.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic1.part[1].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic1.part[2] = new THREE.Mesh(new THREE.PlaneGeometry(0.7, 0.7), new THREE.MeshBasicMaterial({ color: 0x424242 }));
  levelEnvironment[4].tower.pic1.part[2].position.z = -4.48;
  levelEnvironment[4].tower.pic1.part[2].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic1.add(levelEnvironment[4].tower.pic1.part[2], levelEnvironment[4].tower.pic1.part[1], levelEnvironment[4].tower.pic1.part[0]);
  levelEnvironment[4].tower.pic1.position.y = 7.5;
  levelEnvironment[4].tower.pic2 = new THREE.Object3D();
  levelEnvironment[4].tower.pic2.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic2.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic2.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic2.part[0].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic2.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0x80CBC4 }));
  levelEnvironment[4].tower.pic2.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic2.part[1].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic2.part[2] = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 0.5), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  levelEnvironment[4].tower.pic2.part[2].rotation.z = Math.PI * 0.25;
  levelEnvironment[4].tower.pic2.part[2].position.set(0, 0.2, -4.48);
  levelEnvironment[4].tower.pic2.part[2].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic2.part[3] = new THREE.Mesh(new THREE.PlaneGeometry(0.4, 0.4), new THREE.MeshBasicMaterial({ color: 0x757575 }));
  levelEnvironment[4].tower.pic2.part[3].rotation.z = Math.PI * 0.25;
  levelEnvironment[4].tower.pic2.part[3].position.set(0, -0.1, -4.47);
  levelEnvironment[4].tower.pic2.part[3].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic2.part[4] = new THREE.Mesh(new THREE.PlaneGeometry(0.3, 0.3), new THREE.MeshBasicMaterial({ color: 0xFFB74D }));
  levelEnvironment[4].tower.pic2.part[4].rotation.z = Math.PI * 0.25;
  levelEnvironment[4].tower.pic2.part[4].position.set(0, -0.4, -4.46);
  levelEnvironment[4].tower.pic2.part[4].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic2.add(levelEnvironment[4].tower.pic2.part[4], levelEnvironment[4].tower.pic2.part[3], levelEnvironment[4].tower.pic2.part[2], levelEnvironment[4].tower.pic2.part[1], levelEnvironment[4].tower.pic2.part[0]);
  levelEnvironment[4].tower.pic2.rotation.y = -Math.PI / 4;
  levelEnvironment[4].tower.pic2.position.y = 12.5;
  levelEnvironment[4].tower.pic3 = new THREE.Object3D();
  levelEnvironment[4].tower.pic3.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic3.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x8D6E63, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic3.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic3.part[0].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic3.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0x80CBC4 }));
  levelEnvironment[4].tower.pic3.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic3.part[1].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic3.part[2] = new THREE.Mesh(new THREE.CircleGeometry(0.2, 10), new THREE.MeshBasicMaterial({ color: 0x607D8B }));
  levelEnvironment[4].tower.pic3.part[2].position.set(0, 0.3, -4.48);
  levelEnvironment[4].tower.pic3.part[2].scale.set(1.2, 1.2, 1.2);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, -0.4);
  shape.lineTo(0, 0.1);
  shape.lineTo(-0.35, -0.4);
  levelEnvironment[4].tower.pic3.part[3] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), new THREE.MeshBasicMaterial({ color: 0xFFB74D }));
  levelEnvironment[4].tower.pic3.part[3].position.set(0, -0.1, -4.47);
  levelEnvironment[4].tower.pic3.part[3].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic3.add(levelEnvironment[4].tower.pic3.part[3], levelEnvironment[4].tower.pic3.part[2], levelEnvironment[4].tower.pic3.part[1], levelEnvironment[4].tower.pic3.part[0]);
  levelEnvironment[4].tower.pic3.rotation.y = Math.PI / 4;
  levelEnvironment[4].tower.pic3.position.y = 17.5;
  levelEnvironment[4].tower.pic4 = new THREE.Object3D();
  levelEnvironment[4].tower.pic4.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic4.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic4.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic4.part[0].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic4.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0xA5D6A7 }));
  levelEnvironment[4].tower.pic4.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic4.part[1].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic4.part[2] = new THREE.Mesh(new THREE.PlaneGeometry(0.4, 0.4), new THREE.MeshBasicMaterial({ color: 0xEF5350 }));
  levelEnvironment[4].tower.pic4.part[2].position.set(0.1, -0.35, -4.48);
  levelEnvironment[4].tower.pic4.part[2].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic4.part[3] = new THREE.Mesh(new THREE.PlaneGeometry(0.3, 0.3), new THREE.MeshBasicMaterial({ color: 0xAB47BC }));
  levelEnvironment[4].tower.pic4.part[3].position.set(-0.2, 0.1, -4.47);
  levelEnvironment[4].tower.pic4.part[3].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic4.part[4] = new THREE.Mesh(new THREE.PlaneGeometry(0.2, 0.2), new THREE.MeshBasicMaterial({ color: 0xEC407A }));
  levelEnvironment[4].tower.pic4.part[4].position.set(0, 0.4, -4.46);
  levelEnvironment[4].tower.pic4.part[4].scale.set(1.3, 1.3, 1.3);
  levelEnvironment[4].tower.pic4.add(levelEnvironment[4].tower.pic4.part[4], levelEnvironment[4].tower.pic4.part[3], levelEnvironment[4].tower.pic4.part[2], levelEnvironment[4].tower.pic4.part[1], levelEnvironment[4].tower.pic4.part[0]);
  levelEnvironment[4].tower.pic4.position.y = 22.5;
  levelEnvironment[4].tower.pic5 = new THREE.Object3D();
  levelEnvironment[4].tower.pic5.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.6, -0.8);
  shape.lineTo(0.6, 0.8);
  shape.lineTo(-0.6, 0.8);
  shape.lineTo(-0.6, -0.8);
  shape.lineTo(0.6, -0.8);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(-0.4, -0.6);
  shape.lineTo(-0.4, 0.6);
  shape.lineTo(0.4, 0.6);
  shape.lineTo(0.4, -0.6);
  shape.lineTo(0.6, -0.8);
  levelEnvironment[4].tower.pic5.part[0] = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.1, curveSegments: 1, bevelEnabled: false }), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  levelEnvironment[4].tower.pic5.part[0].position.z = -4.5;
  levelEnvironment[4].tower.pic5.part[0].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic5.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(1, 1.5), new THREE.MeshBasicMaterial({ color: 0x673AB7 }));
  levelEnvironment[4].tower.pic5.part[1].position.z = -4.49;
  levelEnvironment[4].tower.pic5.part[1].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic5.part[2] = new THREE.Mesh(new THREE.CircleGeometry(0.2, 12), new THREE.MeshBasicMaterial({ color: 0xFFAB91 }));
  levelEnvironment[4].tower.pic5.part[2].position.set(0, 0.3, -4.48);
  levelEnvironment[4].tower.pic5.part[2].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic5.part[3] = new THREE.Mesh(new THREE.PlaneGeometry(1, 0.6), new THREE.MeshBasicMaterial({ color: 0xEC407A }));
  levelEnvironment[4].tower.pic5.part[3].position.set(0, -0.6, -4.47);
  levelEnvironment[4].tower.pic5.part[3].scale.set(1.2, 1.2, 1.2);
  levelEnvironment[4].tower.pic5.add(levelEnvironment[4].tower.pic5.part[3], levelEnvironment[4].tower.pic5.part[2], levelEnvironment[4].tower.pic5.part[1], levelEnvironment[4].tower.pic5.part[0]);
  levelEnvironment[4].tower.pic5.rotation.y = -Math.PI / 4;
  levelEnvironment[4].tower.pic5.position.y = 27.5;
  levelEnvironment[4].tower.grass = levelEnvironment[0].grass1.clone();
  levelEnvironment[4].tower.grass.position.set(-2, 0, -2);
  levelEnvironment[4].tower.grass1 = levelEnvironment[0].grass1.clone();
  levelEnvironment[4].tower.grass1.position.set(2.5, 0, 2);
  levelEnvironment[4].tower.floor = new THREE.Mesh(new THREE.PlaneGeometry(10, 10), new THREE.MeshBasicMaterial({ color: 0x607D8B }));
  levelEnvironment[4].tower.floor.rotation.x = -Math.PI * 0.5;
  levelEnvironment[4].tower.floor.position.y = -0.01;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.5, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 8);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.54, 0, Math.PI * 0.33);
  shape.absarc(0, 0, 1.5, Math.PI * 0.3, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.1);
  for (let i = 0; i < 5; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI / 3);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 1.54, 0, Math.PI * 0.19);
  shape.absarc(0, 0, 2.5, Math.PI * 0.19, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.3);
  for (let i = 0; i < 9; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI / 5);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2.54, 0, Math.PI * 0.12);
  shape.absarc(0, 0, 3.5, Math.PI * 0.12, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.1);
  for (let i = 0; i < 15; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI / 8);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 3.54, 0, Math.PI * 0.095);
  shape.absarc(0, 0, 4.5, Math.PI * 0.095, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.4);
  for (let i = 0; i < 19; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI / 10);
  }
  levelEnvironment[4].tower.floorTop = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x78909C }));
  clearTempGeometries();
  levelEnvironment[4].tower.floorTop.rotation.x = -Math.PI * 0.5;
  levelEnvironment[4].tower.armchair = levelEnvironment[0].armchair1.clone();
  levelEnvironment[4].tower.armchair.position.set(1.5, 0, -3);
  levelEnvironment[4].tower.armchair.rotation.y = -1;
  levelEnvironment[4].tower.table = levelEnvironment[0].table1.clone();
  levelEnvironment[4].tower.table.position.set(0.5, 0, -3);
  levelEnvironment[4].tower.lamp1 = levelEnvironment[0].circleLamp1.clone();
  levelEnvironment[4].tower.lamp1.position.y = 36;
  levelEnvironment[4].tower.lamp2 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[4].tower.lamp2.position.set(2.5, 34, -1);
  levelEnvironment[4].tower.lamp3 = levelEnvironment[0].roundLamp1.clone();
  levelEnvironment[4].tower.lamp3.position.set(-2.5, 34, -1);
  levelEnvironment[4].tower.add(levelEnvironment[4].tower.lamp2, levelEnvironment[4].tower.lamp3, levelEnvironment[4].tower.lamp1, levelEnvironment[4].tower.wall[4], levelEnvironment[4].tower.wall[3], levelEnvironment[4].tower.wall[2], levelEnvironment[4].tower.table, levelEnvironment[4].tower.armchair, levelEnvironment[4].tower.grass1, levelEnvironment[4].tower.floorTop, levelEnvironment[4].tower.grass, levelEnvironment[4].tower.pic4, levelEnvironment[4].tower.pic5, levelEnvironment[4].tower.pic3, levelEnvironment[4].tower.pic2, levelEnvironment[4].tower.pic1, levelEnvironment[4].tower.pic, levelEnvironment[4].tower.cloud4, levelEnvironment[4].tower.cloud3, levelEnvironment[4].tower.cloud2, levelEnvironment[4].tower.cloud1, levelEnvironment[4].tower.cloud, levelEnvironment[4].tower.wall[1], levelEnvironment[4].tower.wall[0], levelEnvironment[4].tower.wall[13], levelEnvironment[4].tower.wall[12], levelEnvironment[4].tower.wall[11], levelEnvironment[4].tower.wall[10], levelEnvironment[4].tower.wall[9], levelEnvironment[4].tower.wall[8], levelEnvironment[4].tower.wall[7], levelEnvironment[4].tower.wall[6], levelEnvironment[4].tower.wall[5], levelEnvironment[4].tower.floor);
  levelEnvironment[4].tower.position.set(0.95, 0, 0);
  levelEnvironment[4].tower.visible = false;
  levelEnvironment[0].obstacle[43] = new THREE.Object3D();
  levelEnvironment[0].obstacle[43].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(3, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.9, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[43].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x303F9F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.8, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.4, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[43].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.6, 0.14, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.9, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[43].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE57373, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[43].part[3] = levelEnvironment[0].table1.clone();
  levelEnvironment[0].obstacle[43].part[3].position.set(-0.5, 0.5, -1);
  levelEnvironment[0].obstacle[43].part[4] = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].obstacle[43].part[4].position.set(0.5, 0.5, -1);
  levelEnvironment[0].obstacle[43].part[4].rotation.y = -Math.PI * 0.5;
  levelEnvironment[0].obstacle[43].part[5] = levelEnvironment[1].monitor1.clone();
  levelEnvironment[0].obstacle[43].part[5].position.set(-0.3, 1.3, -1);
  levelEnvironment[0].obstacle[43].part[5].scale.set(0.6, 0.6, 0.6);
  levelEnvironment[0].obstacle[43].part[5].rotation.y = 1;
  levelEnvironment[0].obstacle[43].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[43].container.add(levelEnvironment[0].obstacle[43].part[5], levelEnvironment[0].obstacle[43].part[4], levelEnvironment[0].obstacle[43].part[3], levelEnvironment[0].obstacle[43].part[2], levelEnvironment[0].obstacle[43].part[1], levelEnvironment[0].obstacle[43].part[0]);
  levelEnvironment[0].obstacle[43].add(levelEnvironment[0].obstacle[43].container);
  levelEnvironment[0].obstacle[43].box = [3, 0.5];
  levelEnvironment[0].obstacle[43].ready = true;
  levelEnvironment[0].obstacle[43].dir = 0;
  levelEnvironment[0].obstacle[43].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[44] = new THREE.Object3D();
  levelEnvironment[0].obstacle[44].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.8, 0.08, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.7, 0.05, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  levelEnvironment[0].obstacle[44].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5D4037, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.6, 0.24, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.1, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  levelEnvironment[0].obstacle[44].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00796B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.4, 0.14, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.7, 0.1, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, 0);
  levelEnvironment[0].obstacle[44].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00897B, gradientMap: gradientMap[4] }));
  clearTempGeometries();  levelEnvironment[0].obstacle[44].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[44].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[44].container.add(levelEnvironment[0].obstacle[44].part[2], levelEnvironment[0].obstacle[44].part[1], levelEnvironment[0].obstacle[44].part[0]);
  levelEnvironment[0].obstacle[44].add(levelEnvironment[0].obstacle[44].container);
  levelEnvironment[0].obstacle[44].box = [2.8, 0.5];
  levelEnvironment[0].obstacle[44].ready = true;
  levelEnvironment[0].obstacle[44].dir = 0;
  levelEnvironment[0].obstacle[44].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[45] = new THREE.Object3D();
  levelEnvironment[0].obstacle[45].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.6, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.5, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[45].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.4, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[45].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF5722, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.14, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.5, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[45].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[45].part[3] = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].obstacle[45].part[3].position.set(0.7, 0.5, -1);
  levelEnvironment[0].obstacle[45].part[4] = levelEnvironment[0].armchair1.clone();
  levelEnvironment[0].obstacle[45].part[4].position.set(0.-0.5, 0.5, -1);
  levelEnvironment[0].obstacle[45].part[4].rotation.y = 1;
  levelEnvironment[0].obstacle[45].part[5] = levelEnvironment[4].roundLamp.clone();
  levelEnvironment[0].obstacle[45].part[5].position.set(0.6, -0.6, -0.8);
  levelEnvironment[0].obstacle[45].part[5].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[45].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[45].container.add(levelEnvironment[0].obstacle[45].part[5], levelEnvironment[0].obstacle[45].part[4], levelEnvironment[0].obstacle[45].part[3], levelEnvironment[0].obstacle[45].part[2], levelEnvironment[0].obstacle[45].part[1], levelEnvironment[0].obstacle[45].part[0]);
  levelEnvironment[0].obstacle[45].add(levelEnvironment[0].obstacle[45].container);
  levelEnvironment[0].obstacle[45].box = [2.6, 0.5];
  levelEnvironment[0].obstacle[45].ready = true;
  levelEnvironment[0].obstacle[45].dir = 0;
  levelEnvironment[0].obstacle[45].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[46] = new THREE.Object3D();
  levelEnvironment[0].obstacle[46].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.4, 0.08, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.3, 0.05, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  levelEnvironment[0].obstacle[46].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00695C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.24, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.1, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  levelEnvironment[0].obstacle[46].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.14, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.3, 0.1, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, 0);
  levelEnvironment[0].obstacle[46].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x795548, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[46].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[46].container.add(levelEnvironment[0].obstacle[46].part[2], levelEnvironment[0].obstacle[46].part[1], levelEnvironment[0].obstacle[46].part[0]);
  levelEnvironment[0].obstacle[46].add(levelEnvironment[0].obstacle[46].container);
  levelEnvironment[0].obstacle[46].box = [2.4, 0.5];
  levelEnvironment[0].obstacle[46].ready = true;
  levelEnvironment[0].obstacle[46].dir = 0;
  levelEnvironment[0].obstacle[46].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[47] = new THREE.Object3D();
  levelEnvironment[0].obstacle[47].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.2, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.1, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[47].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x303F9F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[47].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00796B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.14, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2.1, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[47].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00897B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[47].part[3] = levelEnvironment[0].sofa1.clone();
  levelEnvironment[0].obstacle[47].part[3].rotation.y = 0.1;
  levelEnvironment[0].obstacle[47].part[3].position.set(0, 0.5, -0.8);
  levelEnvironment[0].obstacle[47].part[4] = levelEnvironment[4].roundLamp.clone();
  levelEnvironment[0].obstacle[47].part[4].position.set(-0.4, -0.6, -0.7);
  levelEnvironment[0].obstacle[47].part[4].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[47].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[47].container.add(levelEnvironment[0].obstacle[47].part[4], levelEnvironment[0].obstacle[47].part[3], levelEnvironment[0].obstacle[47].part[1], levelEnvironment[0].obstacle[47].part[2], levelEnvironment[0].obstacle[47].part[0]);
  levelEnvironment[0].obstacle[47].add(levelEnvironment[0].obstacle[47].container);
  levelEnvironment[0].obstacle[47].box = [2.2, 0.5];
  levelEnvironment[0].obstacle[47].ready = true;
  levelEnvironment[0].obstacle[47].dir = 0;
  levelEnvironment[0].obstacle[47].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[48] = new THREE.Object3D();
  levelEnvironment[0].obstacle[48].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(2, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.9, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[48].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5D4037, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[48].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.14, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.9, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[48].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[48].part[3] = levelEnvironment[0].table1.clone();
  levelEnvironment[0].obstacle[48].part[3].position.set(0, 0.5, -1);
  levelEnvironment[0].obstacle[48].part[4] = levelEnvironment[1].monitor1.clone();
  levelEnvironment[0].obstacle[48].part[4].position.set(0, 1.3, -1);
  levelEnvironment[0].obstacle[48].part[4].scale.set(0.6, 0.6, 0.6);
  levelEnvironment[0].obstacle[48].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[48].container.add(levelEnvironment[0].obstacle[48].part[3], levelEnvironment[0].obstacle[48].part[4], levelEnvironment[0].obstacle[48].part[2], levelEnvironment[0].obstacle[48].part[1], levelEnvironment[0].obstacle[48].part[0]);
  levelEnvironment[0].obstacle[48].add(levelEnvironment[0].obstacle[48].container);
  levelEnvironment[0].obstacle[48].box = [2, 0.5];
  levelEnvironment[0].obstacle[48].ready = true;
  levelEnvironment[0].obstacle[48].dir = 0;
  levelEnvironment[0].obstacle[48].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[49] = new THREE.Object3D();
  levelEnvironment[0].obstacle[49].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.9, 0.08, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.05, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  levelEnvironment[0].obstacle[49].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.7, 0.24, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.3, 0.1, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  levelEnvironment[0].obstacle[49].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.5, 0.14, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.1, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, 0);
  levelEnvironment[0].obstacle[49].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE57373, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[49].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[49].container.add(levelEnvironment[0].obstacle[49].part[2], levelEnvironment[0].obstacle[49].part[1], levelEnvironment[0].obstacle[49].part[0]);
  levelEnvironment[0].obstacle[49].add(levelEnvironment[0].obstacle[49].container);
  levelEnvironment[0].obstacle[49].box = [1.9, 0.5];
  levelEnvironment[0].obstacle[49].ready = true;
  levelEnvironment[0].obstacle[49].dir = 0;
  levelEnvironment[0].obstacle[49].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[50] = new THREE.Object3D();
  levelEnvironment[0].obstacle[50].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.8, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.7, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[50].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x00695C, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.2, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[50].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF5722, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.14, 1.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.7, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[50].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF7043, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[50].part[3] = levelEnvironment[0].chair1.clone();
  levelEnvironment[0].obstacle[50].part[3].position.set(0, 0.5, -1);
  levelEnvironment[0].obstacle[50].part[3].rotation.y = -1;
  levelEnvironment[0].obstacle[50].part[4] = levelEnvironment[4].roundLamp.clone();
  levelEnvironment[0].obstacle[50].part[4].position.set(0.4, -0.6, -0.7);
  levelEnvironment[0].obstacle[50].part[4].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[50].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[50].container.add(levelEnvironment[0].obstacle[50].part[4], levelEnvironment[0].obstacle[50].part[3], levelEnvironment[0].obstacle[50].part[2], levelEnvironment[0].obstacle[50].part[1], levelEnvironment[0].obstacle[50].part[0]);
  levelEnvironment[0].obstacle[50].add(levelEnvironment[0].obstacle[50].container);
  levelEnvironment[0].obstacle[50].box = [1.8, 0.5];
  levelEnvironment[0].obstacle[50].ready = true;
  levelEnvironment[0].obstacle[50].dir = 0;
  levelEnvironment[0].obstacle[50].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[51] = new THREE.Object3D();
  levelEnvironment[0].obstacle[51].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.7, 0.08, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.05, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  levelEnvironment[0].obstacle[51].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x303F9F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.5, 0.24, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.1, 0.1, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  levelEnvironment[0].obstacle[51].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.3, 0.14, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.1, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, 0);
  levelEnvironment[0].obstacle[51].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x795548, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[51].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[51].container.add(levelEnvironment[0].obstacle[51].part[2], levelEnvironment[0].obstacle[51].part[1], levelEnvironment[0].obstacle[51].part[0]);
  levelEnvironment[0].obstacle[51].add(levelEnvironment[0].obstacle[51].container);
  levelEnvironment[0].obstacle[51].box = [1.7, 0.5];
  levelEnvironment[0].obstacle[51].ready = true;
  levelEnvironment[0].obstacle[51].dir = 0;
  levelEnvironment[0].obstacle[51].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[52] = new THREE.Object3D();
  levelEnvironment[0].obstacle[52].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.6, 0.08, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.5, 0.05, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, 0);
  levelEnvironment[0].obstacle[52].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5D4037, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.24, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1, 0.1, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  levelEnvironment[0].obstacle[52].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x3F51B5, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.2, 0.14, 0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.5, 0.1, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, 0);
  levelEnvironment[0].obstacle[52].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x5C6BC0, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[52].part[3] = levelEnvironment[4].roundLamp.clone();
  levelEnvironment[0].obstacle[52].part[3].position.set(-0.2, -0.6, 0);
  levelEnvironment[0].obstacle[52].part[3].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[52].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[52].container.add(levelEnvironment[0].obstacle[52].part[3], levelEnvironment[0].obstacle[52].part[2], levelEnvironment[0].obstacle[52].part[1], levelEnvironment[0].obstacle[52].part[0]);
  levelEnvironment[0].obstacle[52].add(levelEnvironment[0].obstacle[52].container);
  levelEnvironment[0].obstacle[52].box = [1.6, 0.5];
  levelEnvironment[0].obstacle[52].ready = true;
  levelEnvironment[0].obstacle[52].dir = 0;
  levelEnvironment[0].obstacle[52].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[53] = new THREE.Object3D();
  levelEnvironment[0].obstacle[53].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.5, 0.08, 2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.05, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -0.5);
  levelEnvironment[0].obstacle[53].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x455A64, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.3, 0.24, 1.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.9, 0.1, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -0.5);
  levelEnvironment[0].obstacle[53].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xEF5350, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.1, 0.14, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -0.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.4, 0.1, 1.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -0.5);
  levelEnvironment[0].obstacle[53].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE57373, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[53].part[3] = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].obstacle[53].part[3].position.set(0, 0.5, -1);
  levelEnvironment[0].obstacle[53].part[4] = levelEnvironment[4].roundLamp.clone();
  levelEnvironment[0].obstacle[53].part[4].position.set(0, -0.6, 0);
  levelEnvironment[0].obstacle[53].part[4].scale.set(0.7, 0.7, 0.7);
  levelEnvironment[0].obstacle[53].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[53].container.add(levelEnvironment[0].obstacle[53].part[4], levelEnvironment[0].obstacle[53].part[3], levelEnvironment[0].obstacle[53].part[2], levelEnvironment[0].obstacle[53].part[1], levelEnvironment[0].obstacle[53].part[0]);
  levelEnvironment[0].obstacle[53].add(levelEnvironment[0].obstacle[53].container);
  levelEnvironment[0].obstacle[53].box = [1.5, 0.5];
  levelEnvironment[0].obstacle[53].ready = true;
  levelEnvironment[0].obstacle[53].dir = 0;
  levelEnvironment[0].obstacle[53].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[54] = new THREE.Object3D();
  levelEnvironment[0].obstacle[54].part = [];
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(9, 0.08, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.46, -2);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8.9, 0.05, 4.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.2, -2);
  levelEnvironment[0].obstacle[54].part[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x6D4C41, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8.8, 0.24, 4.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.3, -2);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8.4, 0.1, 4.4);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, -2);
  levelEnvironment[0].obstacle[54].part[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8.6, 0.14, 4.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.11, -2);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(8.9, 0.1, 4.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.39, -2);
  levelEnvironment[0].obstacle[54].part[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.3, 0.3, 3.5, 12, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 2.25, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  levelEnvironment[0].obstacle[54].part[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2.1, 0, Math.PI);
  shape.absarc(0, 0, 1.7, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.4, curveSegments: 12, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4, -1.7);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.15, -0.15);
  shape.lineTo(0.4, 0.8);
  shape.lineTo(-0.4, 0.8);
  shape.lineTo(-0.15, -0.15);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.7, curveSegments: 12, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 5.5, -1.85);
  levelEnvironment[0].obstacle[54].part[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x607D8B, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 1.7, 0, Math.PI);
  shape.absarc(0, 0, 1.5, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.2, curveSegments: 12, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4, -1.6);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 2, 0, Math.PI);
  shape.absarc(0, 0, 1.8, Math.PI, 0, true);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.6, curveSegments: 12, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 4, -1.8);
  levelEnvironment[0].obstacle[54].part[5] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xA1887F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.3, 0);
  shape.lineTo(0.6, 0.7);
  shape.lineTo(-0.6, 0.7);
  shape.lineTo(-0.3, 0);
  tempGeometry[tempGeometry.length] = new THREE.ExtrudeGeometry(shape, { steps: 1, depth: 0.62, curveSegments: 12, bevelEnabled: false });
  tempGeometry[tempGeometry.length - 1].translate(0, 5.4, -1.81);
  levelEnvironment[0].obstacle[54].part[6] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.7, 0.6, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 0.8, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.7, 0.4, 0.7);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 3.8, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  levelEnvironment[0].obstacle[54].part[8] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFF8A65, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.62, 0.6, 0.62);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 1.1, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.62, 0.4, 0.62);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 3.6, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  levelEnvironment[0].obstacle[54].part[9] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xA1887F, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.1, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 0.55, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.75, 0.06, 0.75);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 1.1, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 2.5, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.67, 0.04, 0.67);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 1.4, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 2, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.8, 0.06, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(-1.8, 4, -1.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(3.6, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(1.3, 0.06, 0.65);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.1, -1.5);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.9, 0.06, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 6.3, -1.5);
  levelEnvironment[0].obstacle[54].part[7] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0x546E7A, gradientMap: gradientMap[4] }));
  clearTempGeometries();
  levelEnvironment[0].obstacle[54].part[10] = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].obstacle[54].part[10].position.set(2.7, 0.5, -1.5);
  levelEnvironment[0].obstacle[54].part[11] = levelEnvironment[0].grass1.clone();
  levelEnvironment[0].obstacle[54].part[11].position.set(-2.7, 0.5, -1.5);
  levelEnvironment[0].obstacle[54].container = new THREE.Object3D();
  levelEnvironment[0].obstacle[54].container.add(levelEnvironment[0].obstacle[54].part[11], levelEnvironment[0].obstacle[54].part[10], levelEnvironment[0].obstacle[54].part[9], levelEnvironment[0].obstacle[54].part[8], levelEnvironment[0].obstacle[54].part[7], levelEnvironment[0].obstacle[54].part[6], levelEnvironment[0].obstacle[54].part[5], levelEnvironment[0].obstacle[54].part[4], levelEnvironment[0].obstacle[54].part[3], levelEnvironment[0].obstacle[54].part[2], levelEnvironment[0].obstacle[54].part[1], levelEnvironment[0].obstacle[54].part[0]);
  levelEnvironment[0].obstacle[54].add(levelEnvironment[0].obstacle[54].container);
  levelEnvironment[0].obstacle[54].box = [9, 0.5];
  levelEnvironment[0].obstacle[54].ready = true;
  levelEnvironment[0].obstacle[54].dir = 0;
  levelEnvironment[0].obstacle[54].rotation.y = Math.PI * 0.5;
  levelEnvironment[0].obstacle[54].position.set(-1, 29.5, -181.9);
  levelEnvironment[0].obstacle[54].ghost = -181.9;
  for (let i = 43; i < levelEnvironment[0].obstacle.length - 1; i++) {
    levelEnvironment[0].obstacle[i].position.z = -200;
  }
  levelEnvironment[4].hat = hero.head.hat.clone();
  levelEnvironment[4].hat.visible = true;
  gsap.to(levelEnvironment[4].hat.children[0].rotation, { duration: 0.5, x: 0.3, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].hat.scale.set(1.5, 1.5, 1.5);
  levelEnvironment[4].hat.position.set(0.9, 33, 0);
  levelEnvironment[4].hat.rotation.y = -0.1
  levelEnvironment[4].hat.tween = [];
  levelEnvironment[4].hat.tween[0] = gsap.to(levelEnvironment[4].hat.rotation, { duration: 1.5, y: 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].hat.tween[1] = gsap.to(levelEnvironment[4].hat.position, { duration: 1.2, x: 1, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].hat.tween[2] = gsap.to(levelEnvironment[4].hat.position, { duration: 1.1, y: levelEnvironment[4].hat.position.y + 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].position.z = -180.95;
  levelEnvironment[4].add(levelEnvironment[4].hat, levelEnvironment[4].tower, levelEnvironment[4].topBlack, levelEnvironment[4].backBlack);
  levelEnvironment[4].rotation.y = Math.PI * 0.5;
  levelEnvironment[1].add(levelEnvironment[1].roundLamp10, levelEnvironment[1].roundLamp11, levelEnvironment[1].roundLamp8, levelEnvironment[1].roundLamp9, levelEnvironment[1].roundLamp6, levelEnvironment[1].roundLamp7, levelEnvironment[1].roundLamp4, levelEnvironment[1].roundLamp5, levelEnvironment[1].roundLamp3, levelEnvironment[1].roundLamp2, levelEnvironment[1].roundLamp1, levelEnvironment[1].longLamp11, levelEnvironment[1].longLamp9, levelEnvironment[1].longLamp10, levelEnvironment[1].longLamp8, levelEnvironment[1].longLamp6, levelEnvironment[1].longLamp7, levelEnvironment[1].longLamp5, levelEnvironment[1].longLamp4, levelEnvironment[1].longLamp3, levelEnvironment[1].longLamp2, levelEnvironment[1].longLamp1, levelEnvironment[1].blueLamp, levelEnvironment[1].column5, levelEnvironment[1].column6, levelEnvironment[1].column7, levelEnvironment[1].column3, levelEnvironment[1].column4, levelEnvironment[1].column2, levelEnvironment[1].column1, levelEnvironment[1].grass3, levelEnvironment[1].grass2, levelEnvironment[1].grass1, levelEnvironment[1].grass, levelEnvironment[1].chair4, levelEnvironment[1].chair3, levelEnvironment[1].chair1, levelEnvironment[1].chair2, levelEnvironment[1].chair, levelEnvironment[1].monitor2, levelEnvironment[1].monitor3, levelEnvironment[1].monitor1, levelEnvironment[1].monitor, levelEnvironment[0].hand2, levelEnvironment[0].hand1, levelEnvironment[0].hand, levelEnvironment[0].robot1, levelEnvironment[0].robot2, levelEnvironment[0].robotHand2, levelEnvironment[0].robotHand1, levelEnvironment[0].robotHand, levelEnvironment[1].table2, levelEnvironment[1].table1, levelEnvironment[1].table, levelEnvironment[1].server3, levelEnvironment[1].server2, levelEnvironment[1].server1, levelEnvironment[1].server, levelEnvironment[1].robotIcon, levelEnvironment[1].cabinet2, levelEnvironment[1].cabinet3, levelEnvironment[1].cabinet1, levelEnvironment[1].cabinet, levelEnvironment[1].bigTable3, levelEnvironment[1].bigTable2, levelEnvironment[1].bigTable1, levelEnvironment[1].bigTable, levelEnvironment[1].robot, levelEnvironment[1].window, levelEnvironment[1].ceiling, levelEnvironment[1].floorTop, levelEnvironment[1].wall[4], levelEnvironment[1].wall[3], levelEnvironment[1].wall[2], levelEnvironment[1].wall[1], levelEnvironment[1].plug, levelEnvironment[1].wall[0], levelEnvironment[1].floorBack);
  levelEnvironment[1].rotation.y = Math.PI * 0.5;
  levelEnvironment[1].position.z = -50.75;
  levelEnvironment[2].add(levelEnvironment[2].ceiling2, levelEnvironment[2].longLamp1, levelEnvironment[2].longLamp2, levelEnvironment[2].longLamp3, levelEnvironment[2].longLamp4, levelEnvironment[2].longLamp5, levelEnvironment[2].longLamp6, levelEnvironment[2].longLamp7, levelEnvironment[2].squareLamp9, levelEnvironment[2].squareLamp10, levelEnvironment[2].squareLamp8, levelEnvironment[2].squareLamp6, levelEnvironment[2].squareLamp7, levelEnvironment[2].squareLamp4, levelEnvironment[2].squareLamp5, levelEnvironment[2].squareLamp3, levelEnvironment[2].squareLamp2, levelEnvironment[2].squareLamp1, levelEnvironment[2].squareLamp, levelEnvironment[2].column4, levelEnvironment[2].column3, levelEnvironment[2].column2, levelEnvironment[2].column1, levelEnvironment[2].ball4, levelEnvironment[2].ball3, levelEnvironment[2].ball2, levelEnvironment[2].ball1, levelEnvironment[2].ball, levelEnvironment[2].window2glass, levelEnvironment[2].window2frame, levelEnvironment[2].logo7, levelEnvironment[2].logo6, levelEnvironment[2].logo5, levelEnvironment[2].logo4, levelEnvironment[2].wall[10], levelEnvironment[2].logo3, levelEnvironment[2].wall[9], levelEnvironment[2].wall[8], levelEnvironment[2].floorTop2, levelEnvironment[2].floorBox2, levelEnvironment[2].floorTop1, levelEnvironment[2].floorBox1, levelEnvironment[2].tribune3, levelEnvironment[2].tribune2, levelEnvironment[2].wall[7], levelEnvironment[2].soccerGate, levelEnvironment[2].basketFloor, levelEnvironment[2].pBag1, levelEnvironment[2].window1part2, levelEnvironment[2].window1part1, levelEnvironment[2].logo2, levelEnvironment[2].wall[6], levelEnvironment[2].logo1, levelEnvironment[2].logo, levelEnvironment[2].floorBox, levelEnvironment[2].wall[5], levelEnvironment[2].wall[0], levelEnvironment[2].tribune1, levelEnvironment[0].ring, levelEnvironment[2].window1, levelEnvironment[2].window, levelEnvironment[2].chessWall1, levelEnvironment[2].chessWall, levelEnvironment[2].chessFigure1, levelEnvironment[2].chessFigure, levelEnvironment[2].chessFloor[3], levelEnvironment[2].chessFloor[1], levelEnvironment[2].chessFloor[0], levelEnvironment[2].chessFloor[2], levelEnvironment[2].pBag, levelEnvironment[2].wall[1], levelEnvironment[2].wall[2], levelEnvironment[2].wall[3], levelEnvironment[2].wall[4], levelEnvironment[2].floorBack, levelEnvironment[2].floorTop, levelEnvironment[2].ceiling);
  levelEnvironment[2].rotation.y = Math.PI * 0.5;
  levelEnvironment[2].position.z = -98.35;
	hero.animationTween = [];
	hero.floor = 0.64;
	hero.visible = false;
	hero.position.set(-0.5, 0.64, 24);
	hero.rotation.y = 0.3;
  hero.animation = `idle`;
  robot.position.set(0.6, -1.8, -2.2);
  robot.rotation.y = -0.3;
  robot.rotation.x = -0.2;
  robot.visible = false;
  robot.scale.set(1.6, 1.6, 1.6);
  goHeroIdleAnimation();
	rotationContainer.add(worldContainer);
	rotationContainer.position.set(0, -7.4, -25);
	rotationContainer.rotation.x = -0.23;
	introContainer.logo = createSVG(0, 12, 0x000000);
  introContainer.logo.position.set(-0.001, 0.14, -2);
  const tessellateModifier = new THREE.TessellateModifier(3, 4);
  for (let l = 0; l < introContainer.logo.children.length; l++) {
  	introContainer.logo.children[l].position.x -= 568;
  	introContainer.logo.children[l].position.y -= 400;
  	introContainer.logo.children[l].geometry = tessellateModifier.modify(introContainer.logo.children[l].geometry);
  	introContainer.logo.children[l].geometry.needsUpdate = true;
  }
  introContainer.logo.scale.set(0.0045, -0.0045, 0.0045);
  introContainer.logoTween1 = gsap.to(introContainer.logo.position, { duration: 0.2 + Math.random() * 0.2, x: 0.001, ease: "power1.inOut", repeat: -1, yoyo: true });
  introContainer.logoTween2 = gsap.to(introContainer.logo.position, { duration: 0.2 + Math.random() * 0.2, y: 0.142, ease: "power1.inOut", repeat: -1, yoyo: true });
  introContainer.logo2 = createSVG(1, 12, 0x000000);
  introContainer.logo2.position.set(-0.87, 0.049, -2);
  introContainer.logo2.scale.set(0.0012, -0.0012, 0.0012);
  const sparkleShape = new THREE.Shape();
  sparkleShape.moveTo(0.1, 0);
  sparkleShape.lineTo(0.03, 0.03);
  sparkleShape.lineTo(0, 0.1);
  sparkleShape.lineTo(-0.03, 0.03);
  sparkleShape.lineTo(-0.1, 0);
  sparkleShape.lineTo(-0.03, -0.03);
  sparkleShape.lineTo(0, -0.1);
  sparkleShape.lineTo(0.03, -0.03);
  introContainer.sparkle = [];
  for (let i = 0; i < 3; i++) {
    introContainer.sparkle[i] = new THREE.Mesh(new THREE.ShapeGeometry(sparkleShape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
    introContainer.sparkle[i].scale.set(0, 0, 1);
    mainScene.add(introContainer.sparkle[i]);
  }
  introContainer.sparkle[0].position.set(0.2, 1.05, -2);
  introContainer.sparkle[1].position.set(-0.7, -0.45, -2);
  introContainer.sparkle[2].position.set(0.7, -0.65, -2);
  introContainer.orbit = [];
  introContainer.orbit[0] = new THREE.Object3D();
  introContainer.orbit[0].part = [];
  introContainer.orbit[0].part[0] = new THREE.Mesh(new THREE.RingGeometry(0.6, 0.607, 24), new THREE.MeshBasicMaterial({ color:0xD500F9, transparent: true, opacity: 1 }));
  introContainer.orbit[0].part[1] = new THREE.Mesh(new THREE.CircleGeometry(0.05, 24), new THREE.MeshBasicMaterial({ color:0x00E5FF, transparent: true, opacity: 1 }));
  introContainer.orbit[0].part[1].position.set(0.604, 0, 0.001);
  introContainer.orbit[1] = new THREE.Mesh(new THREE.RingGeometry(0.4, 0.408, 24), new THREE.MeshBasicMaterial({ color:0xD500F9, transparent: true, opacity: 1 }));
  introContainer.orbit[2] = new THREE.Mesh(new THREE.RingGeometry(0.2, 0.208, 24), new THREE.MeshBasicMaterial({ color:0xD500F9, transparent: true, opacity: 1 }));
  introContainer.orbitTween = gsap.to([introContainer.orbit[0].rotation, introContainer.orbit[1].rotation, introContainer.orbit[2].rotation], { duration: 4, z: Math.PI * 2, ease: "none", repeat: -1 });
  introContainer.orbit[0].add(introContainer.orbit[0].part[0], introContainer.orbit[0].part[1]);
  introContainer.orbit[0].position.set(-0.9, 1.5, -2);
  introContainer.orbit[1].position.set(-0.9, 1.5, -2);
  introContainer.orbit[2].position.set(-0.9, 1.5, -2);
  introContainer.orbit[0].scale.set(0, 0, 1);
  introContainer.orbit[1].scale.set(0, 0, 1);
  introContainer.orbit[2].scale.set(0, 0, 1);
  const buttonTileGeometry = new THREE.PlaneGeometry(0.04, 0.04);
  button[0] = new THREE.Object3D();
  button[0].tile = [];
  button[0].tileMaterial = [];
  button[0].tween = [];
  button[0].name = `ИГРАТЬ`;
  button[0].ready = false;
  button[0].plane = new THREE.Mesh(new THREE.PlaneGeometry(0.96, 0.2), transparentMaterial);
  for (let i = 0; i < 110; i++) {
  	if (i % 5 == 0) {
  		button[0].tileMaterial[button[0].tileMaterial.length] = new THREE.MeshBasicMaterial({ color: 0xE040FB });
  	}
  	button[0].tile[i] = new THREE.Mesh(buttonTileGeometry, button[0].tileMaterial[button[0].tileMaterial.length - 1]);
  	button[0].tile[i].position.set(0.46 - 0.02 * (i % 5) - 0.04 * Math.floor(i / 5), 0.08 - 0.04 * (i % 5), 0);
  	button[0].tile[i].scale.set(0, 0, 1);
  	button[0].add(button[0].tile[i]);
  }
  for (let i = 0; i < button[0].tileMaterial.length; i++) {
  	button[0].tween[i] = gsap.to(button[0].tileMaterial[i].color, { duration: 0.4, r: blueColor.r, g: blueColor.g, b: blueColor.b, ease: "power1.inOut", repeat: -1, yoyo: true, delay: 0.04 * (button[0].tileMaterial.length - i) });
  }
  button[0].text = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`ИГРАТЬ`, 0.08), 1), blackBasicMaterial);
  button[0].text.geometry.computeBoundingBox();
  button[0].text.position.set(-0.5 * button[0].text.geometry.boundingBox.max.x, -0.5 * button[0].text.geometry.boundingBox.max.y, 0.001);
  button[0].text.geometry.dispose();
  button[0].text.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(``, 0.08), 1);
  button[0].add(button[0].text, button[0].plane);
  button[0].position.set(0, -0.7, -2);
  button[1] = new THREE.Object3D();
  button[1].tile = [];
  button[1].tileMaterial = new THREE.MeshBasicMaterial({ color: 0xEA80FC });
  button[1].tween = [];
  button[1].name = `ещё раз`;
  button[1].ready = false;
  button[1].plane = new THREE.Mesh(new THREE.PlaneGeometry(0.96, 0.16), transparentMaterial);
  for (let i = 0; i < 92; i++) {
  	button[1].tile[i] = new THREE.Mesh(buttonTileGeometry, button[1].tileMaterial);
  	button[1].tile[i].position.set(0.47 - 0.02 * (i % 4) - 0.04 * Math.floor(i / 4), 0.06 - 0.04 * (i % 4), 0);
  	button[1].tile[i].scale.set(0, 0, 1);
  	button[1].add(button[1].tile[i]);
  }
  for (let i = 0; i < button[1].tileMaterial.length; i++) {
  	button[1].tween[i] = gsap.to(button[1].tileMaterial[i].color, { duration: 0.4, r: blueColor.r, g: blueColor.g, b: blueColor.b, ease: "power1.inOut", repeat: -1, yoyo: true, delay: 0.04 * (button[0].tileMaterial.length - i) });
  }
  button[1].text = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`ещё раз`, 0.076), 1), blackBasicMaterial);
  button[1].text.geometry.computeBoundingBox();
  button[1].text.position.set(-0.5 * button[1].text.geometry.boundingBox.max.x, -0.5 * button[1].text.geometry.boundingBox.max.y + 0.007, 0.001);
  button[1].text.geometry.dispose();
  button[1].text.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(``, 0.076), 1);
  button[1].add(button[1].text, button[1].plane);
  button[1].position.set(0, -1.2, -2);
  button[2] = new THREE.Object3D();
  button[2].tile = [];
  button[2].tileMaterial = [];
  button[2].tween = [];
  button[2].name = `В ИТМО!`;
  button[2].ready = false;
  button[2].plane = new THREE.Mesh(new THREE.PlaneGeometry(1.44, 0.28), transparentMaterial);
  button[2].plane.position.y = 0.02;
  button[2].blimColor = new THREE.Color(0xFFFF00);
  for (let i = 0; i < 245; i++) {
  	if (i % 7 == 0) {
  		button[2].tileMaterial[button[2].tileMaterial.length] = new THREE.MeshBasicMaterial({ color: 0x76FF03 });
  	}
  	button[2].tile[i] = new THREE.Mesh(buttonTileGeometry, button[2].tileMaterial[button[2].tileMaterial.length - 1]);
  	button[2].tile[i].position.set(0.74 - 0.02 * (i % 7) - 0.04 * Math.floor(i / 7), 0.14 - 0.04 * (i % 7), 0);
  	button[2].tile[i].scale.set(0, 0, 1);
  	button[2].add(button[2].tile[i]);
  }
  for (let i = 0; i < button[2].tileMaterial.length; i++) {
  	button[2].tween[i] = gsap.to(button[2].tileMaterial[i].color, { duration: 0.4, r: button[2].blimColor.r, g: button[2].blimColor.g, b: button[2].blimColor.b, ease: "power1.inOut", repeat: -1, yoyo: true, delay: 0.04 * (button[2].tileMaterial.length - i) });
  }
  button[2].text = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`В ИТМО!`, 0.12), 1), blackBasicMaterial);
  button[2].text.geometry.computeBoundingBox();
  button[2].text.position.set(-0.5 * button[2].text.geometry.boundingBox.max.x + 0.005, -0.5 * button[2].text.geometry.boundingBox.max.y + 0.012, 0.001);
  button[2].text.geometry.dispose();
  button[2].text.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(``, 0.076), 1);
  button[2].add(button[2].text, button[2].plane);
  button[2].position.set(0, -0.8, -2);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.06, 0);
  shape.lineTo(-0.06, 0.03);
  shape.lineTo(0, 0.03);
  shape.lineTo(0, 0.05);
  shape.lineTo(0.02, 0.05);
  shape.lineTo(0.02, 0.09);
  shape.lineTo(0.06, 0.09);
  shape.lineTo(0.06, 0.05);
  shape.lineTo(0.02, 0.05);
  shape.lineTo(0.02, 0.03);
  shape.lineTo(0, 0.03);
  shape.lineTo(0, -0.03);
  shape.lineTo(0.02, -0.03);
  shape.lineTo(0.02, -0.05);
  shape.lineTo(0.06, -0.05);
  shape.lineTo(0.06, -0.09);
  shape.lineTo(0.02, -0.09);
  shape.lineTo(0.02, -0.05);
  shape.lineTo(0, -0.05);
  shape.lineTo(0, -0.03);
  shape.lineTo(-0.06, -0.03);
  button[3] = new THREE.Object3D();
  button[3].icon = new THREE.Mesh(new THREE.ShapeGeometry(shape, 0), new THREE.MeshBasicMaterial({ color: 0x18FFFF }));
  button[3].icon.scale.set(1.2, 1.2, 1);
  button[3].plane = new THREE.Mesh(new THREE.PlaneGeometry(0.3, 0.3), transparentMaterial);
  button[3].add(button[3].icon, button[3].plane);
  button[3].position.set(-0.75, -1.17, -2);
  button[3].ready = false;
  button[3].scale.set(0, 0, 1);
  button[4] = new THREE.Object3D();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.1, 0);
  shape.lineTo(-0.1, 0.03);
  shape.lineTo(-0.06, 0.03);
  shape.lineTo(-0.06, 0.05);
  shape.lineTo(-0.04, 0.05);
  shape.lineTo(-0.04, 0.07);
  shape.lineTo(-0.02, 0.07);
  shape.lineTo(-0.02, 0.09);
  shape.lineTo(0, 0.09);
  shape.lineTo(0, -0.09);
  shape.lineTo(-0.02, -0.09);
  shape.lineTo(-0.02, -0.07);
  shape.lineTo(-0.04, -0.07);
  shape.lineTo(-0.04, -0.05);
  shape.lineTo(-0.06, -0.05);
  shape.lineTo(-0.06, -0.03);
  shape.lineTo(-0.1, -0.03);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.02, 0.05);
  shape.lineTo(0.04, 0.05);
  shape.lineTo(0.04, 0.03);
  shape.lineTo(0.06, 0.03);
  shape.lineTo(0.06, 0.01);
  shape.lineTo(0.08, 0.01);
  shape.lineTo(0.08, 0.03);
  shape.lineTo(0.1, 0.03);
  shape.lineTo(0.1, 0.05);
  shape.lineTo(0.12, 0.05);
  shape.lineTo(0.12, 0.03);
  shape.lineTo(0.1, 0.03);
  shape.lineTo(0.1, 0.01);
  shape.lineTo(0.08, 0.01);
  shape.lineTo(0.08, -0.01);
  shape.lineTo(0.1, -0.01);
  shape.lineTo(0.1, -0.03);
  shape.lineTo(0.12, -0.03);
  shape.lineTo(0.12, -0.05);
  shape.lineTo(0.1, -0.05);
  shape.lineTo(0.1, -0.03);
  shape.lineTo(0.08, -0.03);
  shape.lineTo(0.08, -0.01);
  shape.lineTo(0.06, -0.01);
  shape.lineTo(0.06, -0.03);
  shape.lineTo(0.04, -0.03);
  shape.lineTo(0.04, -0.05);
  shape.lineTo(0.02, -0.05);
  shape.lineTo(0.02, -0.03);
  shape.lineTo(0.04, -0.03);
  shape.lineTo(0.04, -0.01);
  shape.lineTo(0.06, -0.01);
  shape.lineTo(0.06, 0.01);
  shape.lineTo(0.04, 0.01);
  shape.lineTo(0.04, 0.03);
  shape.lineTo(0.02, 0.03);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.1, 0);
  shape.lineTo(-0.1, 0.03);
  shape.lineTo(-0.06, 0.03);
  shape.lineTo(-0.06, 0.05);
  shape.lineTo(-0.04, 0.05);
  shape.lineTo(-0.04, 0.07);
  shape.lineTo(-0.02, 0.07);
  shape.lineTo(-0.02, 0.09);
  shape.lineTo(0, 0.09);
  shape.lineTo(0, -0.09);
  shape.lineTo(-0.02, -0.09);
  shape.lineTo(-0.02, -0.07);
  shape.lineTo(-0.04, -0.07);
  shape.lineTo(-0.04, -0.05);
  shape.lineTo(-0.06, -0.05);
  shape.lineTo(-0.06, -0.03);
  shape.lineTo(-0.1, -0.03);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  button[4].icon = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x18FFFF }));
  clearTempGeometries();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-0.1, 0);
  shape.lineTo(-0.1, 0.03);
  shape.lineTo(-0.06, 0.03);
  shape.lineTo(-0.06, 0.05);
  shape.lineTo(-0.04, 0.05);
  shape.lineTo(-0.04, 0.07);
  shape.lineTo(-0.02, 0.07);
  shape.lineTo(-0.02, 0.09);
  shape.lineTo(0, 0.09);
  shape.lineTo(0, -0.09);
  shape.lineTo(-0.02, -0.09);
  shape.lineTo(-0.02, -0.07);
  shape.lineTo(-0.04, -0.07);
  shape.lineTo(-0.04, -0.05);
  shape.lineTo(-0.06, -0.05);
  shape.lineTo(-0.06, -0.03);
  shape.lineTo(-0.1, -0.03);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.04, 0.01);
  shape.lineTo(0.06, 0.01);
  shape.lineTo(0.06, -0.01);
  shape.lineTo(0.04, -0.01);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.06, 0.07);
  shape.lineTo(0.08, 0.07);
  shape.lineTo(0.08, 0.05);
  shape.lineTo(0.1, 0.05);
  shape.lineTo(0.1, 0.03);
  shape.lineTo(0.12, 0.03);
  shape.lineTo(0.12, -0.03);
  shape.lineTo(0.1, -0.03);
  shape.lineTo(0.1, -0.05);
  shape.lineTo(0.08, -0.05);
  shape.lineTo(0.08, -0.07);
  shape.lineTo(0.06, -0.07);
  shape.lineTo(0.06, -0.05);
  shape.lineTo(0.08, -0.05);
  shape.lineTo(0.08, -0.03);
  shape.lineTo(0.1, -0.03);
  shape.lineTo(0.1, 0.03);
  shape.lineTo(0.08, 0.03);
  shape.lineTo(0.08, 0.05);
  shape.lineTo(0.06, 0.05);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  button[4].icon1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x18FFFF }));
  clearTempGeometries();
  button[4].plane = new THREE.Mesh(new THREE.PlaneGeometry(0.3, 0.3), transparentMaterial);
  button[4].add(button[4].icon1, button[4].icon, button[4].plane);
  button[4].position.set((Math.tan(mainCamera.fov * Math.PI / 360) / document.body.clientHeight * document.body.clientWidth - 0.1) * 2, (Math.tan(mainCamera.fov * Math.PI / 360) - 0.08) * 2, -2);
  button[4].ready = true;
  button[4].scale.set(0, 0, 1);
  button[4].icon.visible = false;
  introContainer.add(introContainer.logo, introContainer.logo2);
  frame.materialColor = new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true });
  frame.ring = new THREE.Mesh(new THREE.RingGeometry(2.3, 100, 24), frame.materialColor);
  frame.particles = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.01, 0);
  shape.lineTo(-0.01, 0.01);
  shape.lineTo(-0.01, -0.01);
  frame.particleGeometry = new THREE.CircleGeometry(0.008, 8);
  for (let j = 0; j < 2; j++) {
    for (let i = 0; i < 300; i++) {
    	tempGeometry[tempGeometry.length] = frame.particleGeometry.clone();
    	const angle = Math.random() * Math.PI * 2;
    	const dist = 1.7 + Math.random() * 0.6;
    	tempGeometry[tempGeometry.length - 1].scale((dist - 1.7) * 20, (dist - 1.7) * 20, 1);
      tempGeometry[tempGeometry.length - 1].rotateZ(Math.random() * 6);
  	  tempGeometry[tempGeometry.length - 1].translate(Math.cos(angle) * dist, Math.sin(angle) * dist, 0);
    }
    frame.particles[j] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), frame.materialColor);
    clearTempGeometries();
  }
  for (let i = 0; i < 300; i++) {
  	tempGeometry[tempGeometry.length] = frame.particleGeometry.clone();
  	const angle = Math.random() * Math.PI * 2;
  	const randomScale = 7 + Math.random() * 7;
  	tempGeometry[tempGeometry.length - 1].scale(randomScale, randomScale, 1);
  	tempGeometry[tempGeometry.length - 1].rotateZ(Math.random() * 6);
  	tempGeometry[tempGeometry.length - 1].translate(Math.cos(angle) * 2.3, Math.sin(angle) * 2.3, 0);
  }
  frame.particles[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), frame.materialColor);
  clearTempGeometries();
  gsap.to(frame.particles[0].rotation, { duration: 90, z: Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(frame.particles[1].rotation, { duration: 100, z: -Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(frame.particles[2].rotation, { duration: 200, z: -Math.PI * 2, ease: "none", repeat: -1 });
  frame.back = new THREE.Mesh(new THREE.PlaneGeometry(10000, 10000), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  frame.back.position.z = -2.1;
  frame.add(frame.back, frame.ring, frame.particles[0], frame.particles[1], frame.particles[2]);
  frame.position.z = -0.011;
  frame.scale.set(0.004, 0.004, 1);
  toyName.symbol = [];
  toyName.text = `ОдинденьвИТМО`;
  for (let i = 0; i < toyName.text.length; i++) {
  	toyName.symbol[i] = new THREE.Object3D();
  	toyName.symbol[i].top = new THREE.Mesh(new THREE.ExtrudeGeometry(Shantell.generateShapes(toyName.text[i], 0.2), { steps: 1, depth: 0, curveSegments: 6, bevelEnabled: true, bevelThickness: 0.02, bevelSize: 0.02, bevelSegments: 4, bevelOffset: -0.012 }), new THREE.MeshToonMaterial({ color: 0x90A4AE, gradientMap: gradientMap[4] }));
    toyName.symbol[i].top.position.z = 0.041;
    toyName.symbol[i].border = new THREE.Mesh(new THREE.ExtrudeGeometry(Shantell.generateShapes(toyName.text[i], 0.2), { steps: 1, depth: 0, curveSegments: 6, bevelEnabled: true, bevelThickness: 0.035, bevelSize: 0.035, bevelSegments: 4 }), toyNameBorderMaterial);
    toyName.symbol[i].add(toyName.symbol[i].border, toyName.symbol[i].top);
    toyName.symbol[i].scale.set(0, 0, 0);
    toyName.add(toyName.symbol[i]);
  }
  toyName.symbol[0].position.set(-0.3, 0.3, 0);
  toyName.symbol[1].position.set(-0.1, 0.3, 0);
  toyName.symbol[2].position.set(0.09, 0.3, 0);
  toyName.symbol[3].position.set(0.28, 0.3, 0);
  toyName.symbol[4].position.set(-0.34, 0.1, 0);
  toyName.symbol[5].position.set(-0.15, 0.1, 0);
  toyName.symbol[6].position.set(0.02, 0.1, 0);
  toyName.symbol[7].position.set(0.21, 0.1, 0);
  toyName.symbol[8].position.set(-0.62, -0.12, 0);
  toyName.symbol[9].position.set(-0.42, -0.22, 0);
  toyName.symbol[10].position.set(-0.17, -0.22, 0);
  toyName.symbol[11].position.set(0.01, -0.22, 0);
  toyName.symbol[12].position.set(0.35, -0.22, 0);
  toyName.rotation.z = 0.15;
  toyName.position.set(-0.025, -0.005, -2);
  toyName.tween1 = gsap.to(toyName.position, { duration: 0.3 + Math.random() * 0.3, x: -0.015, ease: "power1.inOut", repeat: -1, yoyo: true });
  toyName.tween2 = gsap.to(toyName.position, { duration: 0.3 + Math.random() * 0.3, y: 0.005, ease: "power1.inOut", repeat: -1, yoyo: true });
  toyName.scale.set(1.2, 1.2, 1.2);
  speechBubble[0] = {};
  speechBubble[0].text = [`Чем таким`, `полезным`, `и приятным`, `занять себя`, `сегодня?` ];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0.45, 0.33, 0.48, 0.29, Math.PI * 1.3, Math.PI * 3.2);
  shape.lineTo(0, 0);
  speechBubble[0].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[0].back.position.set(-0.21, 0, -1.5);
  for (let i = 0; i < speechBubble[0].text.length; i++) {
    tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[0].text[i], 0.05), 1);
    tempGeometry[tempGeometry.length - 1].computeBoundingBox();
    tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[0].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  speechBubble[0].text.geometry.translate(0.455, 0.45, 0);
  speechBubble[0].text.position.set(-0.21, 0, -1.5);
  speechBubble[0].back.scale.set(0, 0, 1);
  speechBubble[0].text.scale.set(0, 0, 1);
  speechBubble[1] = {};
  speechBubble[1].text = [`А тебе разве`, `в универ сегодня`, `не нужно?`];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(-0.2, 0.4, 0.62, 0.19, Math.PI * 1.57, Math.PI * 3.5);
  shape.lineTo(0, 0);
  speechBubble[1].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[1].back.position.set(0.15, -0.1, -1.5);
  for (let i = 0; i < speechBubble[1].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[1].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[1].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x1A237E }));
  clearTempGeometries();
  speechBubble[1].text.geometry.translate(-0.2, 0.445, 0);
  speechBubble[1].text.position.set(0.15, -0.1, -1.5);
  speechBubble[1].back.scale.set(0, 0, 1);
  speechBubble[1].text.scale.set(0, 0, 1);
  speechBubble[2] = {};
  speechBubble[2].text = [`Точно!`, `Как я мог`, `забыть`, `про ИТМО?!`];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0.4, 0.3, 0.43, 0.24, Math.PI * 1.3, Math.PI * 3.2);
  shape.lineTo(0, 0);
  speechBubble[2].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[2].back.position.set(-0.21, 0, -1.5);
  for (let i = 0; i < speechBubble[2].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[2].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[2].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blackBasicMaterial);
  clearTempGeometries();
  speechBubble[2].text.geometry.translate(0.405, 0.41, 0);
  speechBubble[2].text.position.set(-0.21, 0, -1.5);
  speechBubble[2].back.scale.set(0, 0, 1);
  speechBubble[2].text.scale.set(0, 0, 1);
  speechBubble[3] = {};
  speechBubble[3].text = [`Воу–воу!`, `Куда это мы`, `спешим?`];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0.1, 0.3, 0.43, 0.18, Math.PI * 1.4, Math.PI * 3.31);
  shape.lineTo(0, 0);
  speechBubble[3].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[3].back.position.set(-0.2, -0.1, -1.5);
  for (let i = 0; i < speechBubble[3].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[3].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[3].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x1A237E }));
  clearTempGeometries();
  speechBubble[3].text.geometry.translate(0.105, 0.295, 0);
  speechBubble[3].text.position.set(-0.2, -0.05, -1.5);
  speechBubble[3].back.scale.set(0, 0, 1);
  speechBubble[3].text.scale.set(0, 0, 1);
  speechBubble[4] = {};
  speechBubble[4].text = [`Читай`, `книги!`];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(-0.1, 0.28, 0.26, 0.15, Math.PI * 1.57, Math.PI * 3.45);
  shape.lineTo(0, 0);
  speechBubble[4].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[4].back.position.set(0.2, -0.15, -1.5);
  for (let i = 0; i < speechBubble[4].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[4].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[4].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x1A237E }));
  clearTempGeometries();
  speechBubble[4].text.geometry.translate(-0.095, 0.29, 0);
  speechBubble[4].text.position.set(0.2, -0.15, -1.5);
  speechBubble[4].back.scale.set(0, 0, 1);
  speechBubble[4].text.scale.set(0, 0, 1);
  speechBubble[5] = {};
  speechBubble[5].text = [`Теперь`, `остался`, `только путь`, `наверх!` ];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(-0.1, 0.35, 0.43, 0.22, Math.PI * 1.55, Math.PI * 3.45);
  shape.lineTo(0, 0);
  speechBubble[5].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[5].back.position.set(0.2, -0.15, -1.5);
  for (let i = 0; i < speechBubble[5].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[5].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[5].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x1A237E }));
  clearTempGeometries();
  speechBubble[5].text.geometry.translate(-0.095, 0.29, 0);
  speechBubble[5].text.position.set(0.19, 0, -1.5);
  speechBubble[5].back.scale.set(0, 0, 1);
  speechBubble[5].text.scale.set(0, 0, 1);
  speechBubble[6] = {};
  speechBubble[6].text = [`Воу!`, `Ты прочитал`, `девять книг!`, `Поздравляем`, `и ждём тебя`, `в ИТМО!` ];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(-0.09, 0.45, 0.50, 0.31, Math.PI * 1.54, Math.PI * 3.46);
  shape.lineTo(0, 0);
  speechBubble[6].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[6].back.position.set(0.2, 0.2, -1.5);
  for (let i = 0; i < speechBubble[6].text.length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[6].text[i], 0.05), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  speechBubble[6].text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x1A237E }));
  clearTempGeometries();
  speechBubble[6].text.geometry.translate(0, 0.61, 0);
  speechBubble[6].text.position.set(0.108, 0.215, -1.5);
  speechBubble[6].back.scale.set(0, 0, 1);
  speechBubble[6].text.scale.set(0, 0, 1);
  speechBubble[8] = {};
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0.275, 0.18, 0.33, 0.09, Math.PI * 1.35, Math.PI * 3.27);
  shape.lineTo(0, 0);
  speechBubble[8].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), whiteBasicMaterial);
  speechBubble[8].back.position.set(-0.23, -0.32, -1.5);
  speechBubble[8].text = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`$#*@%!!!`, 0.05), 1), blackBasicMaterial);
  speechBubble[8].text.geometry.translate(0, 0.14, 0);
  speechBubble[8].text.position.set(-0.23, -0.32, -1.5);
  speechBubble[8].text1 = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`$#*@%!!!`, 0.05), 1), blackBasicMaterial);
  speechBubble[8].text1.geometry.translate(0, -0.218, 0);
  speechBubble[8].text1.position.set(-0.23, -0.32, -1.5);
  speechBubble[8].text2 = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`$#*@%!!!`, 0.05), 1), blackBasicMaterial);
  speechBubble[8].text2.geometry.translate(-0.548, 0.14, 0);
  speechBubble[8].text2.position.set(-0.23, -0.32, -1.5);
  speechBubble[8].back.scale.x = -1;
  speechBubble[8].back.scale.set(0, 0, 1);
  speechBubble[8].text.scale.set(0, 0, 1);
  speechBubble[8].text1.scale.set(0, 0, 1);
  speechBubble[8].text2.scale.set(0, 0, 1);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.1, 0.1, 0.06, 0, Math.PI * 0.5);
  shape.absarc(-0.1, 0.1, 0.06, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.1, -0.1, 0.06, Math.PI, Math.PI * 1.5);
  shape.absarc(0.1, -0.1, 0.06, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.1, -0.1, 0.046, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-0.1, -0.1, 0.046, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-0.1, 0.1, 0.046, Math.PI, Math.PI * 0.5, true);
  shape.absarc(0.1, 0.1, 0.046, Math.PI * 0.5, 0, true);
  shape.lineTo(0.146, -0.1);
  shape.lineTo(0.16, -0.1);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.06, 0.06, 0.06, 0, Math.PI * 0.5);
  shape.absarc(-0.06, 0.06, 0.06, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.06, -0.06, 0.06, Math.PI, Math.PI * 1.5);
  shape.absarc(0.06, -0.06, 0.06, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.06, -0.06, 0.046, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-0.06, -0.06, 0.046, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-0.06, 0.06, 0.046, Math.PI, Math.PI * 0.5, true);
  shape.absarc(0.06, 0.06, 0.046, Math.PI * 0.5, 0, true);
  shape.lineTo(0.106, -0.06);
  shape.lineTo(0.12, -0.06);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 3);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.007, -0.016);
  shape.lineTo(0.007, 0.027);
  shape.lineTo(-0.007, 0.027);
  shape.lineTo(-0.007, -0.016);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.16, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.25);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.007, 0.06);
  shape.lineTo(-0.007, 0.06);
  shape.lineTo(-0.007, -0.033);
  shape.lineTo(-0.044, 0.002);
  shape.lineTo(-0.056, -0.01);
  shape.lineTo(0, -0.066);
  shape.lineTo(0.056, -0.01);
  shape.lineTo(0.044, 0.002);
  shape.lineTo(0.007, -0.033);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 1);
  mainScene.arrowUpHint = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
  clearTempGeometries();
  mainScene.arrowUpHint.visible = false;
  mainScene.arrowUpHint.rotation.z = Math.PI;
  mainScene.arrowUpHint.position.set(0, -0.5, -1.5);
  gsap.to(mainScene.arrowUpHint.scale, { duration: 0.8, x: 0.9, y: 0.9, ease: "power2.inOut", repeat: -1, yoyo: true });
  mainScene.arrowDownHint = mainScene.arrowUpHint.clone();
  mainScene.arrowDownHint.animationTween = [];
  mainScene.arrowDownHint.rotation.z = 0;
  mainScene.arrowDownHint.position.set(0, -0.5, -1.5);
  mainScene.arrowDownHint.visible = false;
  gsap.to(mainScene.arrowDownHint.scale, { duration: 0.8, x: 0.9, y: 0.9, ease: "power2.inOut", repeat: -1, yoyo: true, delay: 0.4 });
  mainScene.swipeHint = new THREE.Object3D();
  mainScene.swipeHint.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(0.16, -0.08);
  shape.lineTo(0.16, -0.1);
  shape.lineTo(0, -0.02);
  shape.lineTo(-0.16, -0.1);
  shape.lineTo(-0.16, -0.08);
  for (let i = 0; i < 6; i++) {
  	mainScene.swipeHint.part[i] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
    mainScene.swipeHint.part[i].position.y = -0.1 * i;
    mainScene.swipeHint.add(mainScene.swipeHint.part[i]);
  }
  mainScene.swipeHint.position.set(0, -0.05, -1.5);
  mainScene.swipeHintDown = mainScene.swipeHint.clone();
  mainScene.swipeHintDown.animationTween = [];
  mainScene.swipeHintDown.rotation.z = Math.PI;
  mainScene.swipeHintDown.position.set(0, -0.65, -1.5);
  let tempText = [`Свайп вверх`,  `для прыжка`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.swipeHint.text = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.swipeHint.text.visible = false;
  mainScene.swipeHint.text.position.set(0, -0.7, -1.3);
  tempText = [`Жми стрелку вверх`,  `для прыжка`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.arrowUpHintText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.arrowUpHintText.visible = false;
  mainScene.arrowUpHintText.position.set(0, -0.7, -1.3);
  tempText = [`Двойной свайп`, `для супер прыжка`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.doubleSwipeHintText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.doubleSwipeHintText.visible = false;
  mainScene.doubleSwipeHintText.position.set(0, -0.7, -1.3);
  tempText = [`Жми стрелку дважды`, `для супер прыжка`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.doubleArrowUpText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.doubleArrowUpText.visible = false;
  mainScene.doubleArrowUpText.position.set(0, -0.7, -1.3);
  tempText = [`Свайп вниз`, `для скольжения`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.slideHintText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.slideHintText.visible = false;
  mainScene.slideHintText.position.set(0, -0.7, -1.3);
  tempText = [`Жми стрелку вниз`, `для скольжения`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.arrowDownText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.arrowDownText.visible = false;
  mainScene.arrowDownText.position.set(0, -0.7, -1.3);
  tempText = [`Свайпай, чтобы`, `управлять полётом`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.flyHintText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.flyHintText.visible = false;
  mainScene.flyHintText.position.set(0, -0.7, -1.3);
  tempText = [`Жми стрелки, чтобы`, `управлять полётом`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.045), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
  }
  mainScene.arrowFlyText = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  mainScene.arrowFlyText.visible = false;
  mainScene.arrowFlyText.position.set(0, -0.7, -1.3);
  tempText = [`ПРОГРАММИРОВАНИЕ`];
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[0], 0.046), 1);
  tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, 0, 0);
  levelName[0] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  levelName[0].visible = false;
  levelName[0].position.set(0, 0.55, -1.3);
  tempText = [`ИТМО`, `и роботы`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.08), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.14 * i, 0);
  }
  levelName[1] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  levelName[1].visible = false;
  levelName[1].position.set(0, 0.2, -1.3);
  tempText = [`КРОНВЕРКСКИЕ`, `БАРСЫ`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.065), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.11 * i, 0);
  }
  levelName[2] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  levelName[2].visible = false;
  levelName[2].position.set(0, 0.2, -1.3);
  tempText = [`ЗАНИМАТЕЛЬНАЯ`, `ФИЗИКА`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.06), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.10 * i, 0);
  }
  levelName[3] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  levelName[3].visible = false;
  levelName[3].position.set(0, 0.4, -1.3);
  tempText = [`Башня`, `ИТМО`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.1), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.19 * i, 0);
  }
  levelName[4] = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true }));
  clearTempGeometries();
  levelName[4].visible = false;
  levelName[4].position.set(0, 0.1, -1.3);
  mainScene.bookHint = new THREE.Object3D();
  mainScene.bookHint.part = [];
  mainScene.bookHint.geometry = new THREE.CircleGeometry(0.1, 8);
  for (let i = 0; i < 10; i++) {
  	mainScene.bookHint.part[i] = new THREE.Mesh(mainScene.bookHint.geometry, blimMaterial[Math.floor(Math.random() * 5)]);
  	mainScene.bookHint.part[i].position.set(0.7 * Math.cos(Math.PI / 5 * i), 0.7 * Math.sin(Math.PI / 5 * i), 0);
  	mainScene.bookHint.part[i].tween = gsap.to(mainScene.bookHint.part[i].scale, { duration: 0.6 + Math.random() * 0.6, x: 0, y: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
  	mainScene.bookHint.add(mainScene.bookHint.part[i]);
  }
  mainScene.bookHint.tween = gsap.to(mainScene.bookHint.rotation, { duration: 18, z: Math.PI * 2, ease: "none", repeat: -1 });
  mainScene.bookHint.position.set(0, -1.55, -6);
  mainScene.bookHint.scale.set(0, 0, 1);
  book[0] = new THREE.Object3D();
  book[0].materialColor = [
  	new THREE.MeshToonMaterial({ color: 0xF9A825, gradientMap: gradientMap[5] }),
  	new THREE.MeshBasicMaterial({ color: 0xE64A19 }),
  ];
  book[0].blimColor = new THREE.Color(0xEF6C00)
  gsap.to(book[0].materialColor[0].color, { duration: 0.3, r: book[0].blimColor.r, g: book[0].blimColor.g, b: book[0].blimColor.b, ease: "power1.inOut", repeat: -1, yoyo: true });
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.45, 0.65, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(0.02, 0, -0.06);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.12);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.14, 0.65, 0.02);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.24, 0, 0);
  book[0].cover = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), book[0].materialColor[0]);
  clearTempGeometries();
  book[0].pages = new THREE.Mesh(new THREE.BoxGeometry(0.47, 0.63, 0.1), whiteBasicMaterial);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.03, 0.64, 0.01);
  tempGeometry[tempGeometry.length - 1].translate(-0.215, 0, -0.055);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.11);
  book[0].cover1 = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), book[0].materialColor[1]);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.25, 0.06);
  tempGeometry[tempGeometry.length - 1].translate(0.02, 0.1, 0.08);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(0.06, 0.06);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.26, 0.24, 0);
  book[0].name = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), book[0].materialColor[1]);
  clearTempGeometries();
  book[0].add(book[0].cover, book[0].cover1, book[0].name, book[0].pages);
  for (let i = 1; i < 24; i++) {
  	book[i] = book[0].clone();
  	const randomScale = Math.random() * 0.2;
  	book[i].scale.set(0.8 + randomScale, 0.8 + randomScale, 0.8 + randomScale);
  	book[i].rotation.set(-0.2, Math.PI * 0.3, 0.2);
    book[i].ready = true;
    book[i].sparkle = [];
    for (let j = 0; j < 12; j++) {
    	book[i].sparkle[j] = new THREE.Mesh(new THREE.ShapeGeometry(sparkleShape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
      book[i].sparkle[j].scale.set(0.05, 0.05, 1);
      book[i].sparkle[j].rotation.y = Math.PI * 0.5;
      worldContainer.add(book[i].sparkle[j]);
      book[i].sparkle[j].visible = false;
    	const randomSize = 0.1 + Math.random() * 0.2;
      tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(randomSize, 0.04);
      tempGeometry[tempGeometry.length - 1].translate(0.5 + randomSize * 0.5, 0, 0);
      tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 6 * j);
    }
    book[i].shine = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[Math.floor(Math.random() * 5)]);
    clearTempGeometries();
    book[i].tween1 = gsap.to(book[i].rotation, { duration: 1 + Math.random() * 2, y: Math.PI * 0.7, ease: "power1.inOut", repeat: -1, yoyo: true });
    book[i].tween2 = gsap.to(book[i].rotation, { duration: 1 + Math.random() * 2, x: 0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    book[i].tween3 = gsap.to(book[i].rotation, { duration: 1 + Math.random() * 2, z: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    book[i].add(book[i].shine);
    gsap.to(book[i].shine.rotation, { duration: 4 + Math.random() * 2, z: Math.PI * 2 * (-1 + Math.round(Math.random()) * 2), ease: "none", repeat: -1 });
    gsap.to(book[i].shine.scale, { duration: 0.3 + Math.random() * 0.7, x: 0.7, y: 0.7, ease: "power1.inOut", repeat: -1, yoyo: true });
    worldContainer.add(book[i]);
  }
  book[0].sparkle = [];
  for (let j = 0; j < 12; j++) {
  	book[0].sparkle[j] = new THREE.Mesh(new THREE.ShapeGeometry(sparkleShape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
    book[0].sparkle[j].scale.set(0.05, 0.05, 1);
    book[0].sparkle[j].visible = false;
    book[0].sparkle[j].rotation.y = Math.PI * 0.5;
    worldContainer.add(book[0].sparkle[j]);
   	const randomSize = 0.1 + Math.random() * 0.2;
    tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(randomSize, 0.04);
    tempGeometry[tempGeometry.length - 1].translate(0.5 + randomSize * 0.5, 0, 0);
    tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI / 6 * j);
  }
  book[0].shine = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimMaterial[Math.floor(Math.random() * 5)]);
  clearTempGeometries();
  gsap.to(book[0].shine.rotation, { duration: 4 + Math.random() * 2, z: Math.PI * 2 * (-1 + Math.round(Math.random()) * 2), ease: "none", repeat: -1 });
  gsap.to(book[0].shine.scale, { duration: 0.3 + Math.random() * 0.7, x: 0.7, y: 0.7, ease: "power1.inOut", repeat: -1, yoyo: true });
  book[0].add(book[0].shine);
  book[0].randomScale = Math.random() * 0.2;
  book[0].scale.set(0.8 + book[0].randomScale, 0.8 + book[0].randomScale, 0.8 + book[0].randomScale);
  book[0].rotation.set(-0.2, Math.PI * 0.3, 0.2);
  book[0].tween1 = gsap.to(book[0].rotation, { duration: 1 + Math.random() * 2, y: Math.PI * 0.7, ease: "power1.inOut", repeat: -1, yoyo: true });
  book[0].tween2 = gsap.to(book[0].rotation, { duration: 1 + Math.random() * 2, x: 0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
  book[0].tween3 = gsap.to(book[0].rotation, { duration: 1 + Math.random() * 2, z: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
  book[0].ready = true;
  book[0].position.set(0, 0.9, -3.95);
  book[1].position.set(0, 3.5, -5.8);
  book[2].position.set(0, 0.9, -8.4);
  book[3].position.set(0, 3.6, -10.7);
  book[4].position.set(0, 2.3, -12.3);
  book[5].position.set(0, 0.9, -14.6);
  book[7].position.set(0, 3.9, -17.2);
  book[8].position.set(0, 3.9, -19.4);
  book[9].position.set(0, 7, -21.3);
  book[10].position.set(0, 3.9, -23.2);
  book[11].position.set(0, 3.9, -25.4);
  book[12].position.set(0, 0.9, -28);
  book[13].position.set(0, 3.5, -29.7);
  book[14].position.set(0, 0.9, -31.7);
  book[15].position.set(0, 3.7, -34);
  book[16].position.set(0, 0.9, -36.1);
  book[18].position.set(0, 3.9, -39);
  book[19].position.set(0, 7, -42.1);
  book[20].position.set(0, 7, -40);
  book[21].position.set(0, 3.9, -43.36);
  book[17].position.set(0, 0.9, -45.45);
  book[6].position.set(0, 3.5, -47.7);
  book[22].position.set(0, 1, -30);
  book[23].position.set(0, 1, -30);
  worldContainer.add(hero, building, guard, book[0]);
  book[0].materialColor_d = [
  	new THREE.MeshToonMaterial({ color: 0xE53935, gradientMap: gradientMap[5] }),
  	new THREE.MeshBasicMaterial({ color: 0xB71C1C }),
  ];
  book[0].blimColor_d = new THREE.Color(0xFF5252)
  gsap.to(book[0].materialColor_d[0].color, { duration: 0.3, r: book[0].blimColor_d.r, g: book[0].blimColor_d.g, b: book[0].blimColor_d.b, ease: "power1.inOut", repeat: -1, yoyo: true });
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.5, 0.65, 0.02);
  tempGeometry[tempGeometry.length - 1].translate(0.02, 0, -0.02);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.04);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.06, 0.65, 0.02);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.265, 0, 0);
  book[0].cover_d = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), book[0].materialColor_d[0]);
  clearTempGeometries();
  book[0].pages_d = new THREE.Mesh(new THREE.BoxGeometry(0.47, 0.63, 0.03), whiteBasicMaterial);
  tempGeometry[tempGeometry.length] = new THREE.BoxGeometry(0.03, 0.64, 0.01);
  tempGeometry[tempGeometry.length - 1].translate(-0.24, 0, -0.02);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.04);
  book[0].cover1_d = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), book[0].materialColor_d[1]);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(`ДИПЛОМ`, 0.045), 1);
  tempGeometry[tempGeometry.length - 1].translate(-0.17, -0.18, 0.04);
  book[0].name_d = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFCC80 }));
  clearTempGeometries();
  book[0].eagle = createSVG(2, 1, 0xFFCC80);
  book[0].eagle.scale.set(0.0003, -0.0003, 0.0003);
  book[0].eagle.position.set(-0.05, 0.14, 0.04);
  book[0].eagle.visible = false;
  book[0].cover1_d.visible = false;
  book[0].pages_d.visible = false;
  book[0].cover_d.visible = false;
  book[0].name_d.visible = false;
  book[11].add(book[0].cover_d, book[0].pages_d, book[0].cover1_d, book[0].name_d, book[0].eagle);
  hero.jumpSparkle = [];
  hero.flySparkle = [];
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.1, 10);
  for (let i = 0; i < 12; i++) {
    hero.flySparkle[i] = new THREE.Mesh(new THREE.ShapeGeometry(sparkleShape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
    hero.flySparkle[i].scale.set(0, 0, 1);
    hero.flySparkle[i].visible = false;
    hero.flySparkle[i].rotation.y = Math.PI * 0.5;
  	hero.jumpSparkle[i] = new THREE.Mesh(tempGeometry[tempGeometry.length - 1], new THREE.MeshBasicMaterial({ color: 0xffffff }));
  	hero.jumpSparkle[i].scale.set(0, 0, 1);
  	hero.jumpSparkle[i].visible = false;
  	hero.jumpSparkle[i].rotation.y = Math.PI * 0.5;
  	worldContainer.add(hero.flySparkle[i], hero.jumpSparkle[i]);
  }
  clearTempGeometries();
  hero.finSparkle = new THREE.Object3D();
  hero.finSparkle.part = [];
  for (let i = 0; i < 24; i++) {
    hero.finSparkle.part[i] = new THREE.Mesh(new THREE.ShapeGeometry(sparkleShape, 1), blimMaterial[Math.floor(Math.random() * 5)]);
    hero.finSparkle.part[i].scale.set(0, 0, 1);
    hero.finSparkle.part[i].position.z = Math.random() * 0.05;
  	hero.finSparkle.add(hero.finSparkle.part[i]);
  }
  hero.finSparkle.rotation.y = Math.PI * 0.5;
  hero.finSparkle.visible = false;
  animateSparkle();
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.5, 0);
  for (let i = 1; i < 13; i++) {
    shape.lineTo(Math.cos(Math.PI / 6 * i - Math.PI / 12) * 1.5, Math.sin(Math.PI / 6 * i - Math.PI / 12) * 1.5);
    shape.lineTo(Math.cos(Math.PI / 6 * i) * 0.5, Math.sin(Math.PI / 6 * i) * 0.5);
  }
  hero.hitSparkle = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), blimMaterial[0]);
  hero.hitSparkle.rotation.y = Math.PI * 0.5;
  hero.hitSparkle.scale.set(0, 0, 1);
  hero.hitSparkle.visible = false;
  gsap.to(hero.hitSparkle.rotation, { duration: 8, z: -Math.PI * 2, ease: "none", repeat: -1 });
  worldContainer.add(hero.finSparkle, hero.hitSparkle);
  tempText = [`МИНУС`, `КНИГА`];
  for (let i = 0; i < 2; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(tempText[i], 0.07), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.12 * i, 0);
  }
  hero.bookSparkle = new THREE.Mesh(new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry), blimBasicMaterial);
  clearTempGeometries();
  hero.bookSparkle.visible = false;
  hero.bookSparkle.position.set(0, 0.05, -1);
  bookCounter.icon = new THREE.Object3D();
  bookCounter.icon.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(0.35, -0.4);
  shape.lineTo(0.35, 0.3);
  shape.lineTo(-0.3, 0.3);
  shape.lineTo(-0.3, -0.35);
  shape.lineTo(-0.25, -0.35);
  shape.lineTo(-0.25, -0.4);
  bookCounter.icon.part[0] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), new THREE.MeshBasicMaterial({ color: 0xF9A825 }));
  bookCounter.icon.part[1] = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 0.6), whiteBasicMaterial);
  bookCounter.icon.part[1].position.set(0.05, -0.05, 0.001);
  bookCounter.icon.part[2] = new THREE.Mesh(new THREE.PlaneGeometry(0.6, 0.7), new THREE.MeshBasicMaterial({ color: 0xFDD835 }));
  bookCounter.icon.part[2].position.set(-0.05, 0.05, 0.002);
  bookCounter.icon.part[3] = new THREE.Mesh(new THREE.PlaneGeometry(0.4, 0.1), new THREE.MeshBasicMaterial({ color: 0xF57C00 }));
  bookCounter.icon.part[3].position.set(-0.05, 0.15, 0.003);
  bookCounter.icon.add(bookCounter.icon.part[3], bookCounter.icon.part[2], bookCounter.icon.part[0], bookCounter.icon.part[1]);
  bookCounter.icon.position.y = 1.5;
  bookCounter.countMaterial = new THREE.MeshBasicMaterial({ color: 0x76FF03 });
  bookCounter.count = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(`0`, 0.46), 1), bookCounter.countMaterial);
  bookCounter.count.position.set(0.5, 1.5, 0);
  bookCounter.name = new THREE.Mesh(new THREE.ShapeGeometry(PressStart.generateShapes(``, 0.2), 0.7), blimBasicMaterial);
  bookCounter.name.position.set(0.14 / 0.12, -1, 0);
  bookCounter.add(bookCounter.name, bookCounter.icon, bookCounter.count);
  bookCounter.position.set(-0.14, Math.tan(mainCamera.fov * Math.PI / 360) - 0.1, -1);
  bookCounter.scale.set(0.12, 0.12, 1);
  let btn = document.createElement("button");
  btn.style.position = 'fixed';
  btn.style.display = 'block';
  btn.style.top = `50%`;
  btn.style.background = `rgba(255, 255, 255, 0)`;
  btn.style.width = `100%`;
  btn.style.height = `50%`;
  btn.style.border = `0px`;
  document.body.appendChild(btn);
  btn.addEventListener('touchend', function(event) {
  	onShare();
  });
  uiScene.add(button[4], button[3], speechBubble[8].text2,speechBubble[8].text1, speechBubble[8].text, mainScene.arrowFlyText, mainScene.arrowDownText, mainScene.doubleArrowUpText, mainScene.arrowUpHintText, hero.bookSparkle, bookCounter, button[2], button[1], speechBubble[6].text, levelName[4], levelName[3], levelName[2], levelName[1], levelName[0], mainScene.flyHintText, mainScene.slideHintText, mainScene.doubleSwipeHintText, speechBubble[5].text, speechBubble[4].text, mainScene.swipeHint.text, speechBubble[3].text, speechBubble[2].text, speechBubble[1].text, speechBubble[0].text, button[0]);
	mainScene.add(speechBubble[8].back, mainScene.arrowDownHint, mainScene.arrowUpHint, speechBubble[6].back, mainScene.bookHint, speechBubble[4].back, mainScene.swipeHint, robot, speechBubble[3].back, speechBubble[2].back, speechBubble[5].back, speechBubble[1].back, speechBubble[0].back, toyName, frame, introContainer, introContainer.orbit[0], introContainer.orbit[1], introContainer.orbit[2]);
	setTimeout(function() {
		hideLoadingScreen();
	}, 100);
}
function animateSparkle() {
  for (let i = 0; i < 24; i++) {
  	if (hero.finSparkle.part[i].position.x === 0 && hero.finSparkle.part[i].position.y === 0) {
  	  const randomTime = 0.5 + Math.random() * 2;
  	  const randomScale = 1 + Math.random();
      const randomAngle = Math.random() * Math.PI * 2;
      gsap.to(hero.finSparkle.part[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power1.out", repeat: 1, yoyo: true });
      gsap.to(hero.finSparkle.part[i].position, { duration: randomTime, x: Math.cos(randomAngle) * (0.5 + randomScale * 0.5), y: Math.sin(randomAngle) * (0.5 + randomScale * 0.5), ease: "power1.out", onComplete: function() {
      	hero.finSparkle.part[i].position.set(0, 0, 0);
      } });
  	}
  }
  setTimeout(function() {
  	animateSparkle();
  }, 100);
}
function goReplay() {
	eraseText(button[2].name, button[2].text, 0.12, 0);
	for (let i = 0; i < button[2].tile.length; i++) {
		const randomTime = 0.2 + Math.random() * 0.2;
		gsap.to(button[2].tile[i].scale, { duration: randomTime, x: 0, y: 0, ease: "power2.in", delay: 0.005 * i + Math.random() * 0.01 });
		gsap.to(button[2].tile[i].position, { duration: randomTime, x: button[2].tile[i].position.x + 1, ease: "power3.in", delay: 0.005 * i + Math.random() * 0.01 });
	}
	gsap.to(robot.position, { duration: 0.8, x: 2, y: -4, ease: "power1.in" });
  gsap.to([speechBubble[6].back.scale, speechBubble[6].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
	gsap.to(button[3].scale, { duration: 0.3, x: 0, y: 0, ease: "power1.in" });
  gsap.to(frame.materialColor.color, { duration: 1, r: blackBasicMaterial.color.r, g: blackBasicMaterial.color.g, b: blackBasicMaterial.color.b, ease: "power1.inOut" });
  frame.back.position.z = -0.02;
	gsap.to(frame.back.material, { duration: 1, opacity: 1, ease: "power1.inOut", onComplete: function() {
    level = 0;
    replaceLevel = 0;
    bookCount = 0;
    worldContainer.remove(levelEnvironment[4], levelEnvironment[0].obstacle[54], levelEnvironment[0].obstacle[49], levelEnvironment[0].obstacle[50], levelEnvironment[0].obstacle[51], levelEnvironment[0].obstacle[52], levelEnvironment[0].obstacle[53], levelEnvironment[0].obstacle[47], levelEnvironment[0].obstacle[48], levelEnvironment[0].obstacle[46], levelEnvironment[0].obstacle[45], levelEnvironment[0].obstacle[44], levelEnvironment[0].obstacle[43]);
    levelEnvironment[4].tower.visible = false;
    worldContainer.add(building, levelEnvironment[0].obstacle[9], levelEnvironment[0].obstacle[8], levelEnvironment[0].obstacle[7], levelEnvironment[0].obstacle[5], levelEnvironment[0].obstacle[4], levelEnvironment[0].obstacle[3], levelEnvironment[0].obstacle[2], levelEnvironment[0].obstacle[1], levelEnvironment[0].obstacle[0], levelEnvironment[0]);
    hero.position.set(0, 0.64, 0.9);
    hero.floor = 0.64;
    hero.rotation.y = Math.PI;
    clearHeroAnimation();
    goHeroIdleAnimation();
    rotationContainer.position.set(-0.9, -2.5, -6);
    rotationContainer.rotation.y = -Math.PI * 0.5;
    rotationContainer.rotation.z = 0;
    worldContainer.position.z = 0;
    hero.head.hat.visible = false;
    hero.topBody.mantle.visible = false;
    hero.topBody.mantle1.visible = false;
    hero.rightHand.shoulder.part[2].visible = false;
    hero.rightHand.shoulder.part[3].visible = false;
    hero.rightHand.arm.part[2].visible = false;
    hero.leftHand.children[0].children[0].visible = false;
    hero.leftHand.children[0].children[1].visible = false;
    hero.leftHand.children[1].children[0].visible = false;
    guard.leftHand.rotation.x = 0;
    guard.leftHand.children[1].rotation.x = 0;
    guard.leftHand.children[1].children[0].rotation.set(0, 0, 0);
    guard.head.rotation.set(0, 0, 0);
    guard.aninationTween1 = gsap.to(guard.head.rotation, { duration: 0.6 + Math.random() * 0.6, x: -0.06, ease: "power1.inOut", repeat: -1, yoyo: true });
    guard.aninationTween2 = gsap.to(guard.head.rotation, { duration: 0.6 + Math.random() * 0.6, z: -0.06, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.head.brows[2].visible = true;
    hero.head.brows[1].visible = false;
    hero.head.mouth[0].visible = true;
    hero.head.mouth[2].visible = false;
    mainScene.swipeHint.position.y = -0.05;
    mainScene.swipeHint.text.material.opacity = 1;
    mainScene.swipeHint.text.material.color.setHex(0x000000);
    mainScene.doubleSwipeHintText.material.opacity = 1;
    mainScene.doubleSwipeHintText.material.color.setHex(0x000000);
    mainScene.slideHintText.material.opacity = 1;
    mainScene.slideHintText.material.color.setHex(0x000000);
    mainScene.flyHintText.material.opacity = 1;
    mainScene.flyHintText.material.color.setHex(0x000000);
    book[0].position.set(0, 0.9, -3.95);
    book[1].position.set(0, 3.5, -5.8);
    book[2].position.set(0, 0.9, -8.4);
    book[3].position.set(0, 3.6, -10.7);
    book[4].position.set(0, 2.3, -12.3);
    book[5].position.set(0, 0.9, -14.6);
    book[7].position.set(0, 3.9, -17.2);
    book[8].position.set(0, 3.9, -19.4);
    book[9].position.set(0, 7, -21.3);
    book[10].position.set(0, 3.9, -23.2);
    book[11].position.set(0, 3.9, -25.4);
    book[12].position.set(0, 0.9, -28);
    book[13].position.set(0, 3.5, -29.7);
    book[14].position.set(0, 0.9, -31.7);
    book[15].position.set(0, 3.7, -34);
    book[16].position.set(0, 0.9, -36.1);
    book[18].position.set(0, 3.9, -39);
    book[19].position.set(0, 7, -42.1);
    book[20].position.set(0, 7, -40);
    book[21].position.set(0, 3.9, -43.36);
    book[17].position.set(0, 0.9, -45.45);
    book[6].position.set(0, 3.5, -47.7);
    book[0].eagle.visible = false;
    book[0].cover1_d.visible = false;
    book[0].pages_d.visible = false;
    book[0].cover_d.visible = false;
    book[0].name_d.visible = false;
    book[11].children[0].visible = true;
    book[11].children[1].visible = true;
    book[11].children[2].visible = true;
    book[11].children[3].visible = true;
    for (let i = 0; i < 24; i++) {
    	book[i].scale.set(0, 0, 0);
    }
    for (let i = 0; i < 22; i++) {
    	book[i].ready = true;
    	const randomScale = 0.8 + Math.random() * 0.2;
    	gsap.to(book[i].scale, { duration: 0.4 + Math.random() * 0.5, x: randomScale, y: randomScale, z: randomScale, ease: "back.out" });
    }
    levelName[0].material.opacity = 1;
    levelName[0].material.color.setHex(0x000000);
    levelName[1].material.opacity = 1;
    levelName[1].material.color.setHex(0x000000);
    levelName[2].material.opacity = 1;
    levelName[2].material.color.setHex(0x000000);
    levelName[3].material.opacity = 1;
    levelName[3].material.color.setHex(0x000000);
    levelName[4].material.opacity = 1;
    levelName[4].material.color.setHex(0x000000);
    for (let i = 0; i < 20; i++) {
     	levelEnvironment[0].matrixNum[i].position.y = 9.8;
    	levelEnvironment[0].matrixNum[i].tween = gsap.to(levelEnvironment[0].matrixNum[i].position, { duration: 2 + Math.random(), y: 0.2, ease: "none", repeat: -1 });
    }
    levelEnvironment[2].ball.position.y = 0.3;
    levelEnvironment[2].ball1.position.y = 0.3;
    levelEnvironment[2].ball2.position.y = 0.3;
    levelEnvironment[2].ball3.position.y = 0.3;
    levelEnvironment[2].ball4.position.y = 0.3;
    slideHint = true;
    flyHint = true;
    doubleJumpHint = true;
    levelEnvironment[3].wall[4].material.color.setHex(0x4A148C);
    levelEnvironment[3].wall[28].material.color.setHex(0x4A148C);
    levelEnvironment[3].pendulum.part[2].rotation.z = 0.5;
    levelEnvironment[3].pendulum1.children[0].rotation.z = 0.5;
    levelEnvironment[0].obstacle[31].part[0].rotation.x = 0;
    levelEnvironment[0].obstacle[33].children[0].rotation.x = 0;
    levelEnvironment[0].obstacle[35].children[0].rotation.x = 0;
    levelEnvironment[0].obstacle[37].part[1].part.rotation.z = 0;
    levelEnvironment[0].obstacle[37].part[2].part.rotation.z = 0;
    levelEnvironment[0].obstacle[37].part[3].part.rotation.z = 0;
    levelEnvironment[0].obstacle[37].container.rotation.z = 0;
    levelEnvironment[0].obstacle[37].container.rotation.x = 0;
    levelEnvironment[0].obstacle[38].children[0].children[0].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[38].children[0].children[1].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[38].children[0].children[2].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[38].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[38].children[0].rotation.x = 0;
    levelEnvironment[0].obstacle[40].children[0].children[0].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[40].children[0].children[1].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[40].children[0].children[2].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[40].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[40].children[0].rotation.x = 0;
    levelEnvironment[0].obstacle[41].children[0].children[0].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[41].children[0].children[1].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[41].children[0].children[2].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[41].children[0].rotation.z = 0;
    levelEnvironment[0].obstacle[41].children[0].rotation.x = 0;
    levelEnvironment[3].pic[12].position.set(24.7, -0.4, -1);
    levelEnvironment[3].pic[11].position.set(33.5, -0.5, -1.5);
    levelEnvironment[3].pic[10].position.set(6, 4.5, -1);
    levelEnvironment[3].pic[9].position.set(35, 0, -1);
    levelEnvironment[3].pic[8].position.set(33.5, 3, -1.5);
    levelEnvironment[3].pic[7].position.set(4, -1.4, -0.5);
    levelEnvironment[3].pic[6].position.set(24, 3, -1.5);
    levelEnvironment[3].pic[5].position.set(28.3, 2, -1.5);
    levelEnvironment[3].pic[4].position.set(19, 1.9, -1.5);
    levelEnvironment[3].pic[3].position.set(13.6, 0.2, -1);
    levelEnvironment[3].pic[2].position.set(16.5, 5, -1.5);
    levelEnvironment[3].pic[1].position.set(8, 2, -1.5);
    levelEnvironment[3].pic[0].position.set(0.3, 2, -1.5);
    for (let k = 0; k < 4; k++) {
      levelEnvironment[0].obstacle[28].part[5 + k].material.color.setHex(0xFF1744);
      levelEnvironment[0].obstacle[42].children[0].children[k].material.color.setHex(0x33691E);
      levelEnvironment[0].obstacle[34].children[k].material.color.setHex(0xD50000);
    }
    for (let i = 0; i < 38; i++) {
      levelEnvironment[3].sparkle[i].scale.set(0, 0, 1);
    }
    hero.topBody.mantle.material = blimMaterial[0];
    hero.topBody.mantle1.material = blimMaterial[1];
    hero.rightHand.shoulder.part[2].material = blimMaterial[0];
    hero.rightHand.shoulder.part[3].material = blimMaterial[0];
    hero.rightHand.arm.part[2].material = blimMaterial[0];
    hero.leftHand.children[0].children[0].material = blimMaterial[0];
    hero.leftHand.children[0].children[1].material = blimMaterial[0];
    hero.leftHand.children[1].children[0].material = blimMaterial[0];
    hero.head.hat.part[0].material = blimMaterial[1];
    hero.head.hat.part[1].material = blimMaterial[2];
    hero.head.hat.part[3].material = blimMaterial[3];
    for (let i = 43; i < levelEnvironment[0].obstacle.length - 1; i++) {
      levelEnvironment[0].obstacle[i].tween.kill();
      levelEnvironment[0].obstacle[i].tween = null;
      levelEnvironment[0].obstacle[i].tween1.kill();
      levelEnvironment[0].obstacle[i].tween1 = null;
    }
    speechBubble[8].back.position.set(-0.23, -0.32, -1.5);
    speechBubble[8].text.position.set(-0.23, -0.32, -1.5);
    mainScene.arrowUpHintText.material.color.setHex(0x000000);
    mainScene.doubleArrowUpText.material.color.setHex(0x000000);
    mainScene.arrowDownText.material.color.setHex(0x000000);
    mainScene.arrowFlyText.material.color.setHex(0x000000);
    mainScene.arrowUpHintText.material.opacity = 1;
    mainScene.doubleArrowUpText.material.opacity = 1;
    mainScene.arrowDownText.material.opacity = 1;
    mainScene.arrowFlyText.material.opacity = 1;
  	gsap.to(frame.back.material, { duration: 0.6, opacity: 0, ease: "power1.inOut", onComplete: function() {
      bookCounter.count.geometry.dispose();
    	bookCounter.count.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(`0`, 0.46), 1);
      gsap.to(bookCounter.icon.position, { duration: 0.3, y: 0, ease: "power2.out" });
      gsap.to(bookCounter.count.position, { duration: 0.3, y: -0.38, ease: "power2.out" });
      levelEnvironment[4].topBlack.material.opacity = 1;
      goLevel1();
  	} });
	} });
}
function hideLoadingScreen() {
  gsap.to(loadingBar.bar.material, { duration: 0.06, opacity: 0, ease: "none", repeat: 4, yoyo: true, onComplete: function() {
  	mainScene.remove(loadingBar);
  	showIntro();
  } });
}
function showIntro() {
	let positionAttribute = [];
  gsap.to(introContainer.logo.children[0].material.color, { duration: 1, r: purpleColor.r, g: purpleColor.g, b: purpleColor.b, ease: "power3.in", onComplete: function() {
 	  for (let i = 0; i < 3; i++) {
 	  	gsap.to(introContainer.orbit[i].scale, { duration: 1, x: 1, y: 1, ease: "back.out", delay: 0.3 * i });
 	    introContainer.sparkle[i].tween = gsap.to(introContainer.sparkle[i].scale, { duration: 0.6 + Math.random() * 0.6, x: 0.7, y: 0.7, ease: "power1.inOut", repeat: -1, yoyo: true });
 	  }
 	  gsap.to(introContainer.logo.children[0].material.color, { duration: 0.5, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "power1.inOut" });
 	  setTimeout(function() {
 	  	gsap.to(introContainer.logo2.children[0].material.color, { duration: 0.1, setHex: 0xFFFFFF, ease: "power1.inOut", repeat: 6, yoyo: true });
 	  	document.body.addEventListener('touchstart', onDocumentTouchStart, false);
 	  	document.body.addEventListener('touchend', onDocumentTouchEnd, false);
      document.body.addEventListener('mousemove', onDocumentMouseMove, false);
 	  	document.body.addEventListener('mousedown', onDocumentMouseDown, false);
 	  	showButton(button[0], 0.08);
 	  }, 500);
  } });
  for (let k = 0; k < introContainer.logo.children.length; k++) {
    positionAttribute[k] = introContainer.logo.children[k].geometry.getAttribute('position');
    const vertex = [];
    for (let i = 0; i < positionAttribute[k].count; i += 3) {
      const randomX = -300 + Math.random() * 600;
      const randomY = -500 + Math.random() * 1000;
      const time = {t: 0};
      const timeRandom = 0.5 + Math.random() * 1;
      const num = i;
      let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
      for (let j = 0; j < 3; j ++) {
        vertex[j].fromBufferAttribute(positionAttribute[k], i + j);
      }
      gsap.from(time, { duration: timeRandom, t: 1, ease: "power4.in", onUpdate: function() {
        introContainer.logo.children[k].geometry.attributes.position.setXYZ(num + 0, vertex[0].x + randomX * time.t, vertex[0].y + randomY * time.t, vertex[0].z);
        introContainer.logo.children[k].geometry.attributes.position.setXYZ(num + 1, vertex[1].x + randomX * time.t, vertex[1].y + randomY * time.t, vertex[1].z);
        introContainer.logo.children[k].geometry.attributes.position.setXYZ(num + 2, vertex[2].x + randomX * time.t, vertex[2].y + randomY * time.t, vertex[2].z);
        introContainer.logo.children[k].geometry.attributes.position.needsUpdate = true;
      } });
    }
  }
}
function hideIntro() {
  for (let i = 0; i < 3; i++) {
  	introContainer.sparkle[i].tween.kill();
  	introContainer.sparkle[i].tween = null;
  	gsap.to(introContainer.sparkle[i].scale, { duration: 0.3 + Math.random() * 0.7, x: 0, y: 0, ease: "power2.in" });
  }
  mainScene.add(rotationContainer);
  gsap.to(introContainer.orbit[0].part[0].material, { duration: 0.05, opacity: 0, ease: "power1.inOut", repeat: 10, yoyo: true });
  gsap.to(introContainer.orbit[1].material, { duration: 0.05, opacity: 0, ease: "power1.inOut", repeat: 8, yoyo: true });
  gsap.to(introContainer.orbit[2].material, { duration: 0.05, opacity: 0, ease: "power1.inOut", repeat: 6, yoyo: true });
  gsap.to(introContainer.orbit[0].part[1].scale, { duration: 1, x: 0, y: 0, ease: "power1.inOut", onComplete: function() {
  	gsap.to([introContainer.logo.children[0].material, introContainer.logo2.children[0].material], { duration: 0.05, opacity: 0, ease: "power1.inOut", repeat: 6, yoyo: true, onComplete: function() {
      uiScene.remove(button[0]);
  	  mainScene.remove(introContainer, introContainer.orbit[0], introContainer.orbit[1], introContainer.orbit[2]);
  	  introContainer.logoTween1.kill();
  	  introContainer.logoTween1 = null;
  	  introContainer.logoTween2.kill();
  	  introContainer.logoTween2 = null;
  	  introContainer.orbitTween.kill();
  	  introContainer.orbitTween = null;
  	  for (let i = 0; i < button[0].tileMaterial.length; i++) {
  	  	button[0].tween[i].kill();
  	  	button[0].tween[i] = null;
  	  }
  	  goStartScene();
  	} });
  } });
}
function goStartScene() {
	mainScene.background.setHex(0x0288D1);
	gsap.to(frame.materialColor.color, { duration: 2.4, r: darkBlueColor.r, g: darkBlueColor.g, b: darkBlueColor.b, ease: "power1.inOut" });
	gsap.to(frame.back.material, { duration: 2, opacity: 0, ease: "power1.inOut", onComplete: function() {
  	setTimeout(function() {
  		gsap.to(building.car.position, { duration: 4, x: -20, ease: "none" });
  		gsap.from(hero.position, { duration: 0.5, x: -1, y: -1, ease: "power2.out" });
      gameSound[4].play();
    	gsap.to([speechBubble[0].back.scale, speechBubble[0].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.3 });
  		hero.visible = true;
  		setTimeout(function() {
  			gsap.from(robot.position, { duration: 0.5, x: 1, y: -4, ease: "power2.out" });
      	gsap.to([speechBubble[0].back.scale, speechBubble[0].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
      	gameSound[2].play();
      	gsap.to([speechBubble[1].back.scale, speechBubble[1].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.3, onComplete: function() {
      		building.car.material1.color.setHex(0x303F9F);
      		building.car.rotation.y = Math.PI;
       	  gsap.to(building.car.position, { duration: 6, x: 20, ease: "none" });
      		hero.head.eyes[1].visible = false;
      		hero.head.eyes[0].visible = true;
      		hero.head.brows[1].visible = true;
      		hero.head.brows[0].visible = false;
      		hero.head.mouth[1].visible = true;
      		hero.head.mouth[0].visible = false;
      	} });
        robot.visible = true;
        setTimeout(function() {
        	gsap.to([speechBubble[1].back.scale, speechBubble[1].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
      	  gameSound[4].play();
      	  gsap.to([speechBubble[2].back.scale, speechBubble[2].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.25, onComplete: function() {
            gsap.to(robot.position, { duration: 0.5, x: 1, y: -4, ease: "power2.in", delay: 1.5 });
            setTimeout(function() {
            	worldContainer.add(levelEnvironment[0].obstacle[9], levelEnvironment[0].obstacle[8], levelEnvironment[0].obstacle[7], levelEnvironment[0].obstacle[5], levelEnvironment[0].obstacle[4], levelEnvironment[0].obstacle[3], levelEnvironment[0].obstacle[2], levelEnvironment[0].obstacle[1], levelEnvironment[0].obstacle[0], levelEnvironment[0]);
            	gsap.to([speechBubble[2].back.scale, speechBubble[2].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
              hero.head.brows[2].visible = true;
              hero.head.brows[1].visible = false;
              hero.head.mouth[0].visible = true;
              hero.head.mouth[1].visible = false;
              gsap.to(rotationContainer.position, { duration: 3, x: -0.9, y: -2.5, z: -6, ease: "power2.in" });
              gsap.to(rotationContainer.rotation, { duration: 0.8, y: -Math.PI * 0.5, z: 0, ease: "power2.inOut", delay: 2.5 });
              building.car.material1.color.setHex(0xD32F2F);
              building.car.rotation.y = 0;
       	      gsap.to(building.car.position, { duration: 4.8, x: -20, ease: "none" });
              clearHeroAnimation();
              goHeroRunAnimation();
              gsap.to(hero.rotation, { duration: 0.6, y: Math.PI, ease: "power2.inOut" });
              gsap.to(hero.position, { duration: 4, x: 0, z: 0.9, ease: "none", onComplete: function() {
                levelEnvironment[0].wall[0].scale.z = 10;
                levelEnvironment[0].wall[5].scale.x = 10;
              	worldContainer.remove(introContainer, building, hero, robot);
              	mainScene.add(robot);
              	rotationContainer.add(hero);
              	clearHeroAnimation();
              	goHeroIdleAnimation();
              	goLevel1();
              } });
            }, 2000);
      	  } });
        }, 3000);
  		}, 3000);
  	}, 1400);
  	for (let i = 0; i < 13; i++) {
  		gsap.to(toyName.symbol[i].position, { duration: 0.5 + Math.random(), z: 2, ease: "power3.in" });
  	}
  } });
	for (let i = 0; i < 8; i++) {
		const randomTime = 0.3 + Math.random() * 0.5;
		gsap.to(toyName.symbol[i].scale, { duration: randomTime, x: 1, y: 1, z: 1, ease: "back.out", delay: 0.05 * i });
		gsap.from(toyName.symbol[i].position, { duration: randomTime, x: 0, y: 0, z: -0.1, ease: "back.out", delay: 0.05 * i });
	}
	const randomTime = 0.3 + Math.random() * 0.5;
	gsap.to(toyName.symbol[8].scale, { duration: randomTime, x: 0.8, y: 0.8, z: 0.8, ease: "back.out", delay: 0.4 });
  gsap.from(toyName.symbol[8].position, { duration: randomTime, x: 0, y: 0, z: -0.1, ease: "back.out", delay: 0.4 });
  for (let i = 9; i < 13; i++) {
  	const randomTime = 0.3 + Math.random() * 0.5;
  	gsap.to(toyName.symbol[i].scale, { duration: randomTime, x: 1.2, y: 1.2, z: 1.2, ease: "back.out", delay: 0.05 * i });
    gsap.from(toyName.symbol[i].position, { duration: randomTime, x: 0, y: 0, z: -0.1, ease: "back.out", delay: 0.05 * i });
  }
}
function goLevel1() {
	gsap.to(frame.materialColor.color, { duration: 4, r: darkGreyColor.r, g: darkGreyColor.g, b: darkGreyColor.b, ease: "none" });
  levelName[0].visible = true;
  gsap.to(levelName[0].material.color, { duration: 0.04, setHex: 0xFF4081, ease: "none", repeat: 8, yoyo: true });
	gsap.to(guard.leftHand.rotation, { duration: 0.3, x: -1.2, ease: "power1.inOut" });
  gsap.to(guard.leftHand.children[1].rotation, { duration: 0.3, x: -0.4, ease: "power1.inOut" });
  gsap.to(guard.leftHand.children[1].children[0].rotation, { duration: 0.3, x: -1.2, y: 1.2, ease: "power1.inOut", onComplete: function() {
  	gameSound[3].play();
  	gsap.to([speechBubble[3].back.scale, speechBubble[3].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", onComplete: function() {
      gameSound[7].play();
      if (touchReady) {
      	mainScene.add(mainScene.swipeHint);
      	uiScene.add(mainScene.swipeHint.text);
        for (let i = 0; i < 6; i++) {
        	mainScene.swipeHint.part[i].tween = gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.5, opacity: 1, ease: "power4.in", repeat: -1, yoyo: true, delay: 0.15 * (5 - i) });
        }
        mainScene.swipeHint.text.visible = true;
        gsap.to(mainScene.swipeHint.text.material.color, { duration: 0.04, setHex: 0x3E2723, ease: "none", repeat: 6, yoyo: true });
      } else {
      	mainScene.arrowUpHint.visible = true;
      	mainScene.arrowUpHint.tween = gsap.to(mainScene.arrowUpHint.material, { duration: 0.5, opacity: 1, ease: "power1.in" });
        mainScene.arrowUpHintText.visible = true;
        gsap.to(mainScene.arrowUpHintText.material.color, { duration: 0.04, setHex: 0x3E2723, ease: "none", repeat: 6, yoyo: true });
      }
      setTimeout(function() {
        gsap.to(levelName[0].material, { duration: 1, opacity: 0, ease: "none", onComplete: function() {
          levelName[0].visible = false;
        } });
        onPlay = true;
        viewY = true;
      }, 100);
  	} });
  } });
  book[22].position.z = -200;
  book[23].position.z = -200;
}
function goLevel2() {
	level = 1;
	levelName[1].visible = true;
	gsap.to(levelName[1].material.color, { duration: 0.04, setHex: 0x76FF03, ease: "none", repeat: 8, yoyo: true });
	gsap.to(frame.materialColor.color, { duration: 4, r: darkBrownColor.r, g: darkBrownColor.g, b: darkBrownColor.b, ease: "none" });
  for (let i = 0; i < 24; i++) {
  	book[i].scale.set(0, 0, 0);
  }
	for (let i = 0; i < 24; i++) {
		book[i].ready = true;
		const randomScale = 0.8 + Math.random() * 0.2;
		gsap.to(book[i].scale, { duration: 0.4 + Math.random() * 0.5, x: randomScale, y: randomScale, z: randomScale, ease: "back.out" });
	}
	setTimeout(function() {
		onPlay = true;
	  gsap.to(levelName[1].material, { duration: 1, opacity: 0, ease: "none", onComplete: function() {
	  	levelName[1].visible = false;
	  } });
	}, 900);
	for (let i = 0; i < 20; i++) {
		levelEnvironment[0].matrixNum[i].tween.kill();
		levelEnvironment[0].matrixNum[i].tween = null;
	}
	book[0].position.set(0, 3.5, -52.7);
	book[1].position.set(0, 6.6, -54.7);
	book[2].position.set(0, 3.5, -56.7);
	book[3].position.set(0, 0.9, -57.7);
	book[4].position.set(0, 3.2, -59.7);
	book[5].position.set(0, 0.9, -61.95);
	book[6].position.set(0, 3.2, -64.1);
	book[7].position.set(0, 3.2, -65.7);
	book[8].position.set(0, 0.9, -68);
	book[9].position.set(0, 3.5, -68.8);
  book[10].position.set(0, 6.6, -70.8);
	book[11].position.set(0, 3.5, -72.8);
	book[12].position.set(0, 0.9, -73.6);
	book[13].position.set(0, 3.9, -75.9);
	book[14].position.set(0, 3.9, -77.7);
	book[15].position.set(0, 6.6, -79.5);
	book[16].position.set(0, 3.9, -81.3);
	book[17].position.set(0, 0.9, -84);
	book[18].position.set(0, 3.5, -85.2);
	book[19].position.set(0, 7, -87.2);
	book[20].position.set(0, 3.5, -89.2);
	book[21].position.set(0, 0.9, -90);
  book[22].position.set(0, 3.9, -92);
	book[23].position.set(0, 3.9, -94);
	clearHeroAnimation();
	goHeroIdleAnimation();
	if (touchReady) {
		mainScene.add(mainScene.swipeHint);
		for (let i = 0; i < 6; i++) {
			mainScene.swipeHint.part[i].tween = gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.5, opacity: 1, ease: "power4.in", repeat: -1, yoyo: true, delay: 0.15 * (5 - i) });
		}
	} else {
		mainScene.arrowUpHint.visible = true;
    mainScene.arrowUpHint.tween = gsap.to(mainScene.arrowUpHint.material, { duration: 0.5, opacity: 1, ease: "power1.in" });
	}
}
function goLevel3() {
	level = 2;
	levelName[2].visible = true;
	gsap.to(levelName[2].material.color, { duration: 0.04, setHex: 0x64FFDA, ease: "none", repeat: 8, yoyo: true });
	gsap.to(frame.materialColor.color, { duration: 4, r: darkBlueColor1.r, g: darkBlueColor1.g, b: darkBlueColor1.b, ease: "none" });
	clearHeroAnimation();
	goHeroIdleAnimation();
	for (let i = 0; i < 24; i++) {
		book[i].scale.set(0, 0, 0);
	}
	for (let i = 0; i < 21; i++) {
		book[i].ready = true;
		const randomScale = 0.8 + Math.random() * 0.2;
		gsap.to(book[i].scale, { duration: 0.4 + Math.random() * 0.5, x: randomScale, y: randomScale, z: randomScale, ease: "back.out" });
	}
	book[0].position.set(0, 0.5, -100.8);
	book[1].position.set(0, 0.9, -103.3);
	book[7].position.set(0, 3.7, -105.3);
	book[2].position.set(0, 0.9, -107);
	book[3].position.set(0, 0.5, -108.8);
	book[8].position.set(0, 0.9, -111.3);
	book[4].position.set(0, 3.5, -112.3);
	book[5].position.set(0, 6.6, -114.3);
	book[6].position.set(0, 3.5, -116.3);
	book[9].position.set(0, 0.9, -117.3);
	book[10].position.set(0, 3.8, -119.3);
	book[11].position.set(0, 0.9, -121.7);
	book[13].position.set(0, 3.5, -122.7);
	book[12].position.set(0, 6.5, -124.7);
	book[14].position.set(0, 3.5, -126.7);
	book[15].position.set(0, 0.9, -127.7);
	book[16].position.set(0, 0.9, -130.8);
	book[18].position.set(0, 3.5, -131.7);
	book[17].position.set(0, 6.5, -133.4);
	book[20].position.set(0, 3.5, -135.6);
	book[19].position.set(0, 0.9, -136.5);
	gameSound[7].play();
  if (touchReady) {
  	mainScene.add(mainScene.swipeHintDown);
  	for (let i = 0; i < 6; i++) {
  		mainScene.swipeHintDown.animationTween[i] = gsap.to(mainScene.swipeHint.children[i].material, { duration: 0.5, opacity: 1, ease: "power4.in", repeat: -1, yoyo: true, delay: 0.15 * (5 - i) });
  	}
  	mainScene.slideHintText.visible = true;
  	uiScene.add(mainScene.slideHintText);
  	gsap.to(mainScene.slideHintText.material.color, { duration: 0.04, setHex: 0xAD1457, ease: "none", repeat: 6, yoyo: true });
  } else {
  	mainScene.arrowDownHint.visible = true;
    mainScene.arrowDownHint.tween = gsap.to(mainScene.arrowDownHint.material, { duration: 0.5, opacity: 1, ease: "power1.in" });
    mainScene.arrowDownText.visible = true;
    gsap.to(mainScene.arrowDownText.material.color, { duration: 0.04, setHex: 0xAD1457, ease: "none", repeat: 6, yoyo: true });
  }
  setTimeout(function() {
  	gsap.to(levelName[2].material, { duration: 1, opacity: 0, ease: "none", onComplete: function() {
	  	levelName[2].visible = false;
	  } });
  	onPlay = true;
  }, 900);
}
const darkPurpleColor = new THREE.Color(0x4A148C);
function goLevel4() {
	level = 3;
	levelName[3].visible = true;
	gsap.to(frame.materialColor.color, { duration: 4, r: darkPurpleColor.r, g: darkPurpleColor.g, b: darkPurpleColor.b, ease: "none" });
	gsap.to(levelName[3].material.color, { duration: 0.04, setHex: 0xC6FF00, ease: "none", repeat: 8, yoyo: true, onComplete: function() {
  	gsap.to(worldContainer.position, { duration: 1, z: 143.1, ease: "power1.out" });
    gsap.to(rotationContainer.rotation, { duration: 1, x: 0, ease: "power1.inOut" });
  	gsap.to(rotationContainer.position, { duration: 1, y: -0.4, ease: "power1.inOut" });
    goHeroFlyIdleAnimation();
    viewY = false;
    moveWorld = false;
    gameSound[7].play();
    if (touchReady) {
  	  mainScene.swipeHint.position.y = 0.8;
  	  mainScene.add(mainScene.swipeHint, mainScene.swipeHintDown);
    	for (let i = 0; i < 6; i++) {
  	  	mainScene.swipeHint.part[i].tween = gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.5, opacity: 1, ease: "power4.in", repeat: -1, yoyo: true, delay: 0.15 * (5 - i) });
  		  mainScene.swipeHintDown.animationTween[i] = gsap.to(mainScene.swipeHint.children[i].material, { duration: 0.5, opacity: 1, ease: "power4.in", repeat: -1, yoyo: true, delay: 0.15 * (5 - i) });
    	}
    	uiScene.add(mainScene.flyHintText);
    	mainScene.flyHintText.visible = true;
  	  gsap.to(mainScene.flyHintText.material.color, { duration: 0.04, setHex: 0xC6FF00, ease: "none", repeat: 6, yoyo: true });
    } else {
    	mainScene.arrowUpHint.position.y = -0.1;
    	mainScene.arrowUpHint.visible = true;
      mainScene.arrowUpHint.tween = gsap.to(mainScene.arrowUpHint.material, { duration: 0.5, opacity: 1, ease: "power1.in" });
    	mainScene.arrowDownHint.visible = true;
      mainScene.arrowDownHint.tween = gsap.to(mainScene.arrowDownHint.material, { duration: 0.5, opacity: 1, ease: "power1.in" });
      mainScene.arrowFlyText.visible = true;
      gsap.to(mainScene.arrowFlyText.material.color, { duration: 0.04, setHex: 0xC6FF00, ease: "none", repeat: 6, yoyo: true });
    }
    setTimeout(function() {
      gsap.to(levelName[3].material, { duration: 1, opacity: 0, ease: "none", onComplete: function() {
	      levelName[3].visible = false;
	    } });
      onPlay = true;
    }, 900);
	} });
  book[0].position.set(0, 3, -144.1);
	book[1].position.set(0, 1, -145.9);
	book[15].position.set(0, -1, -146.3);
	book[7].position.set(0, -3, -148.1);
	book[2].position.set(0, 0.9, -150.5);
	book[3].position.set(0, -1.8, -153);
	book[8].position.set(0, 1.5, -155.1);
	book[19].position.set(0, 4, -156.3);
	book[4].position.set(0, 2.1, -157.7);
	book[5].position.set(0, 0.2, -158.5);
	book[6].position.set(0, -1.7, -160.1);
	book[9].position.set(0, 0.2, -161.9);
	book[16].position.set(0, 3, -163.3);
	book[10].position.set(0, 1.1, -164.5);
	book[20].position.set(0, -0.65, -165.85);
	book[11].position.set(0, -2.5, -167.4);
	book[12].position.set(0, -2.3, -169.6);
	book[13].position.set(0, 0, -171.6);
	book[14].position.set(0, 2.5, -173.6);
	book[17].position.set(0, 0, -175.6);
	book[18].position.set(0, -2.5, -177.2);
  for (let i = 0; i < 24; i++) {
  	book[i].scale.set(0, 0, 0);
  }
	for (let i = 0; i < 21; i++) {
		book[i].ready = true;
		const randomScale = 0.8 + Math.random() * 0.2;
		gsap.to(book[i].scale, { duration: 0.4 + Math.random() * 0.5, x: randomScale, y: randomScale, z: randomScale, ease: "back.out" });
	}
	speechBubble[8].back.position.x = -0.35;
  speechBubble[8].text.position.x = -0.35;
  speechBubble[8].text1.position.x = -0.35;
}
function goLevel5() {
	level = 4;
	flyY = false;
	onPlay = false;
  rotationContainer.position.y = -2.5;
  for (let i = 0; i < 74; i++) {
    levelEnvironment[3].animationTween[i].kill();
    levelEnvironment[3].animationTween[i] = null;
  }
	worldContainer.remove(levelEnvironment[0].obstacle[42], levelEnvironment[0].obstacle[41], levelEnvironment[0].obstacle[40], levelEnvironment[0].obstacle[39], levelEnvironment[0].obstacle[38], levelEnvironment[0].obstacle[37], levelEnvironment[0].obstacle[36], levelEnvironment[0].obstacle[35], levelEnvironment[0].obstacle[34], levelEnvironment[0].obstacle[33], levelEnvironment[0].obstacle[32], levelEnvironment[0].obstacle[31], levelEnvironment[0].obstacle[29], levelEnvironment[0].obstacle[28], levelEnvironment[3]);
  worldContainer.add(levelEnvironment[0].obstacle[54], levelEnvironment[0].obstacle[49], levelEnvironment[0].obstacle[50], levelEnvironment[0].obstacle[51], levelEnvironment[0].obstacle[52], levelEnvironment[0].obstacle[53], levelEnvironment[0].obstacle[47], levelEnvironment[0].obstacle[48], levelEnvironment[0].obstacle[46], levelEnvironment[0].obstacle[45], levelEnvironment[0].obstacle[44], levelEnvironment[0].obstacle[43]);
  levelEnvironment[4].tower.visible = true;
	clearHeroAnimation();
  clearFlyAnimation();
  goHeroIdleAnimation();
  hero.position.z = -0.9
  hero.position.y = 0.64;
  hero.rotation.y = Math.PI * 1.5;
	levelName[4].visible = true;
	gsap.to(levelName[4].material.color, { duration: 0.04, setHex: 0xD500F9, ease: "none", repeat: 8, yoyo: true, onComplete: function() {
		gsap.to(levelName[4].material, { duration: 1.5, opacity: 0, ease: "none", onComplete: function() {
	    levelName[4].visible = false;
	  } });
	  gsap.to(frame.materialColor.color, { duration: 3, r: darkBlueColor.r, g: darkBlueColor.g, b: darkBlueColor.b, ease: "none" });
		gsap.to(levelEnvironment[4].topBlack.material, { duration: 1, opacity: 0, ease: "none", onComplete: function() {
  	  for (let i = 0; i < 24; i++) {
  	  	book[i].scale.set(0, 0, 0);
  	  	book[i].ready = false;
  	  }
  	  for (let i = 0; i < 12; i++) {
  	  	book[i].ready = true;
  	  	const randomScale = 0.8 + Math.random() * 0.2;
  	  	gsap.to(book[i].scale, { duration: 0.4 + Math.random() * 0.5, x: randomScale, y: randomScale, z: randomScale, ease: "back.out" });
  	  }
  	  robot.visible = true;
      robot.scale.set(1.2, 1.2, 1.2);
      robot.position.set(2, -4, -2);
      gsap.to(robot.position, { duration: 0.4, x: 0.7, y: -1.4, ease: "power1.out" });
      gameSound[2].play();
      gsap.to([speechBubble[5].back.scale, speechBubble[5].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.15 });
      setTimeout(function() {
      	book[0].eagle.visible = true;
        book[0].cover1_d.visible = true;
        book[0].pages_d.visible = true;
        book[0].cover_d.visible = true;
        book[0].name_d.visible = true;
        book[11].children[0].visible = false;
        book[11].children[1].visible = false;
        book[11].children[2].visible = false;
        book[11].children[3].visible = false;
        book[11].scale.set(1.3, 1.3, 1);
      	onPlay = true;
        gsap.to(robot.position, { duration: 0.3, x: 2, y: -4, ease: "power1.in" });
       	gsap.to([speechBubble[5].back.scale, speechBubble[5].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
      }, 2000);
    } });
	} });
	hero.finSparkle.position.set(-1.5, 33, -181.9);
	hero.finSparkle.rotation.y = Math.PI * 0.5;
	hero.finSparkle.visible = true;
	hero.finSparkle.scale.set(1.5, 1.5, 1);
  speechBubble[8].back.position.y = -0.17;
  speechBubble[8].text.position.y = -0.17;
  speechBubble[8].text2.position.y = -0.17;
  levelEnvironment[4].hat.scale.set(1.5, 1.5, 1.5);
  levelEnvironment[4].hat.position.set(0.9, 33, 0);
  levelEnvironment[4].hat.rotation.y = -0.1
  levelEnvironment[4].hat.visible = true;
  levelEnvironment[4].hat.tween[0] = gsap.to(levelEnvironment[4].hat.rotation, { duration: 1.5, y: 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].hat.tween[1] = gsap.to(levelEnvironment[4].hat.position, { duration: 1.2, x: 1, ease: "power1.inOut", repeat: -1, yoyo: true });
  levelEnvironment[4].hat.tween[2] = gsap.to(levelEnvironment[4].hat.position, { duration: 1.1, y: levelEnvironment[4].hat.position.y + 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  for (let i = 0; i < 92; i++) {
  	button[1].tile[i].position.set(0.47 - 0.02 * (i % 4) - 0.04 * Math.floor(i / 4), 0.06 - 0.04 * (i % 4), 0);
  }
  for (let i = 0; i < 245; i++) {
  	button[2].tile[i].position.set(0.74 - 0.02 * (i % 7) - 0.04 * Math.floor(i / 7), 0.14 - 0.04 * (i % 7), 0);
  }
  levelEnvironment[0].obstacle[43].position.set(-1, 2, -180.4);
  levelEnvironment[0].obstacle[44].position.set(-1, 4.5, -180.3);
  levelEnvironment[0].obstacle[45].position.set(-1, 7, -180.2);
  levelEnvironment[0].obstacle[46].position.set(-1, 9.5, -180.1);
  levelEnvironment[0].obstacle[47].position.set(-1, 12, -180);
  levelEnvironment[0].obstacle[48].position.set(-1, 14.5, -179.9);
  levelEnvironment[0].obstacle[49].position.set(-1, 17, -179.85);
  levelEnvironment[0].obstacle[50].position.set(-1, 19.5, -179.8);
  levelEnvironment[0].obstacle[51].position.set(-1, 22, -179.75);
  levelEnvironment[0].obstacle[52].position.set(-1, 24.5, -179.7);
  levelEnvironment[0].obstacle[53].position.set(-1, 27, -179.6);
  levelEnvironment[0].obstacle[54].position.x = -1;
  for (let i = 43; i < levelEnvironment[0].obstacle.length - 1; i++) {
    levelEnvironment[0].obstacle[i].ghost = levelEnvironment[0].obstacle[i].position.z;
    levelEnvironment[0].obstacle[i].position.x = -1;
    levelEnvironment[0].obstacle[i].container.position.x = 0;
  }
  levelEnvironment[0].obstacle[43].tween1 = gsap.to(levelEnvironment[0].obstacle[43], { duration: 3.5, ghost: -183.4, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[43].tween = gsap.to(levelEnvironment[0].obstacle[43].position, { duration: 3.5, z: -183.4, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[44].tween1 = gsap.to(levelEnvironment[0].obstacle[44], { duration: 2.6, ghost: -183.5, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[44].tween = gsap.to(levelEnvironment[0].obstacle[44].position, { duration: 2.6, z: -183.5, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[45].tween1 = gsap.to(levelEnvironment[0].obstacle[45], { duration: 3.1, ghost: -183.6, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[45].tween = gsap.to(levelEnvironment[0].obstacle[45].position, { duration: 3.1, z: -183.6, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[46].tween1 = gsap.to(levelEnvironment[0].obstacle[46], { duration: 2.5, ghost: -183.7, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[46].tween = gsap.to(levelEnvironment[0].obstacle[46].position, { duration: 2.5, z: -183.7, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[47].tween1 = gsap.to(levelEnvironment[0].obstacle[47], { duration: 3.2, ghost: -183.8, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[47].tween = gsap.to(levelEnvironment[0].obstacle[47].position, { duration: 3.2, z: -183.8, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[48].tween1 = gsap.to(levelEnvironment[0].obstacle[48], { duration: 2.7, ghost: -183.9, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[48].tween = gsap.to(levelEnvironment[0].obstacle[48].position, { duration: 2.7, z: -183.9, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[49].tween1 = gsap.to(levelEnvironment[0].obstacle[49], { duration: 3.3, ghost: -183.95, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[49].tween = gsap.to(levelEnvironment[0].obstacle[49].position, { duration: 3.3, z: -183.95, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[50].tween1 = gsap.to(levelEnvironment[0].obstacle[50], { duration: 2.4, ghost: -184, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[50].tween = gsap.to(levelEnvironment[0].obstacle[50].position, { duration: 2.4, z: -184, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[51].tween1 = gsap.to(levelEnvironment[0].obstacle[51], { duration: 3.7, ghost: -184.05, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[51].tween = gsap.to(levelEnvironment[0].obstacle[51].position, { duration: 3.7, z: -184.05, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[52].tween1 = gsap.to(levelEnvironment[0].obstacle[52], { duration: 2.8, ghost: -184.1, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[52].tween = gsap.to(levelEnvironment[0].obstacle[52].position, { duration: 2.8, z: -184.1, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
  levelEnvironment[0].obstacle[53].tween1 = gsap.to(levelEnvironment[0].obstacle[53], { duration: 3.8, ghost: -184.15, ease: "none", repeat: -1, yoyo: true });
  levelEnvironment[0].obstacle[53].tween = gsap.to(levelEnvironment[0].obstacle[53].position, { duration: 3.8, z: -184.15, ease: "none", repeat: -1, yoyo: true, delay: 0.8 });
}
function goHeroFlyIdleAnimation() {
	hero.animation = `idleFly`;
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 1, x: 0.03, ease: "power1.inOut" });
  hero.animationTween[2] = gsap.to(hero.head.rotation, { duration: 1, x: 0.1, z: 0.1, ease: "power1.inOut" });
  hero.animationTween[6] = gsap.to(hero.rotation, { duration: 1, x: 0, ease: "power1.inOut" });
	hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 1, x: 0.2, ease: "power1.inOut" });
  hero.animationTween[7] = gsap.to(hero.rightHand.rotation, { duration: 1, z: -0.5, ease: "power1.inOut" });
  hero.animationTween[8] = gsap.to(hero.leftHand.rotation, { duration: 1, z: 0.5, ease: "power1.inOut" });
  hero.animationTween[3] = gsap.to(hero.rightLeg.rotation, { duration: 1, x: -0.3, ease: "power1.inOut" });
  hero.animationTween[9] = gsap.to(hero.leftLeg.rotation, { duration: 1, x: -0.1, ease: "power1.inOut" });
  hero.animationTween[10] = gsap.to(hero.rightLeg.part[1].rotation, { duration: 1, x: 1.5, ease: "power1.inOut" });
  hero.animationTween[0] = gsap.to(hero.leftLeg.children[1].rotation, { duration: 1, x: 0.7, ease: "power1.inOut" });
  hero.animationTween[5] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 1, x: -0.9, ease: "power1.inOut", onComplete: function() {
    clearHeroAnimation();
  	hero.animationTween[0] = gsap.to(hero.topBody.position, { duration: 0.6, y: -0.01, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[1] = gsap.to(hero.rightHand.rotation, { duration: 0.6 + Math.random(), x: 0.4, z: -0.3, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[2] = gsap.to(hero.leftHand.rotation, { duration: 1 + Math.random(), x: 0.4, z: 0.3, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[3] = gsap.to(hero.rightHand.arm.rotation, { duration: 1 + Math.random(), x: -1.6, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[4] = gsap.to(hero.leftHand.children[1].rotation, { duration: 1 + Math.random(), x: -1.6, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[5] = gsap.to(hero.topBody.rotation, { duration: 1.2, x: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[8] = gsap.to(hero.rotation, { duration: 2.5, x: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[10] = gsap.to(hero.rightLeg.rotation, { duration: 1.5 + Math.random() * 0.5, x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[9] = gsap.to(hero.leftLeg.rotation, { duration: 1.5 + Math.random() * 0.5, x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[12] = gsap.to(hero.rightLeg.part[1].rotation, { duration: 1.5 + Math.random() * 0.5, x: 1.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[11] = gsap.to(hero.leftLeg.children[1].rotation, { duration: 1.5 + Math.random() * 0.5, x: 0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[6] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), x: -0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[7] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), z: -0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  } });
}
let flyHint = true;
function goHeroFlyUpAnimation() {
	gameSound[5].play();
	gameSound[13].play();
	for (let i = 0; i < 12; i++) {
    if (hero.flySparkle[i].tween !== undefined && hero.flySparkle[i].tween !== null) {
    	hero.flySparkle[i].tween.kill();
    	hero.flySparkle[i].tween = null;
    	hero.flySparkle[i].tween1.kill();
    	hero.flySparkle[i].tween1 = null;
    }
    hero.flySparkle[i].visible = true;
  	hero.flySparkle[i].scale.set(0, 0, 1);
    hero.flySparkle[i].position.set(0, hero.position.y + Math.random() * 0.5, hero.position.z - worldContainer.position.z - 0.5 + Math.random());
    const randomTime = 0.4 + Math.random() * 0.6;
    const randomScale = 0.5 + Math.random();
    hero.flySparkle[i].tween = gsap.to(hero.flySparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
    hero.flySparkle[i].tween1 = gsap.to(hero.flySparkle[i].position, { duration: randomTime, y: hero.flySparkle[i].position.y - 1 - Math.random() * 1.5, z: hero.flySparkle[i].position.z - 0.1 * randomTime, ease: "power2.out", onComplete: function() {
      hero.flySparkle[i].tween.kill();
      hero.flySparkle[i].tween = null;
      hero.flySparkle[i].tween1.kill();
      hero.flySparkle[i].tween1 = null;
      hero.flySparkle[i].visible = false;
    } });
  }
	clearFlyAnimation();
	hero.animation = `holdFly`;
	if (flyHint) {
		flyHint = false;
		if (touchReady) {
	  	for (let i = 0; i < 6; i++) {
  	    mainScene.swipeHintDown.animationTween[i].kill();
  	  	mainScene.swipeHintDown.animationTween[i] = null;
        gsap.to(mainScene.swipeHintDown.children[i].material, { duration: 0.3, opacity: 0, ease: "none" });
        mainScene.swipeHint.part[i].tween.kill();
  	  	mainScene.swipeHint.part[i].tween = null;
        gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.3, opacity: 0, ease: "none" });
      }
      gsap.to(mainScene.flyHintText.material, { duration: 0.3, opacity: 0, ease: "power2.in", onComplete: function() {
        mainScene.remove(mainScene.swipeHint, mainScene.swipeHintDown);
        uiScene.remove(mainScene.flyHintText);
      } });
		} else {
			mainScene.arrowUpHint.tween.kill();
  		mainScene.arrowUpHint.tween = null;
  		mainScene.arrowDownHint.tween.kill();
  		mainScene.arrowDownHint.tween = null;
      gsap.to([mainScene.arrowUpHint.material, mainScene.arrowDownHint.material, mainScene.arrowFlyText.material], { duration: 0.3, opacity: 0, ease: "none", onComplete: function() {
        mainScene.arrowUpHint.visible = false;
        mainScene.arrowDownHint.visible = false;
        mainScene.arrowFlyText.visible = false;
      } });
		}
	}
	hero.animationTween[6] = gsap.to([hero.rotation, hero.head.rotation], { duration: 0.2, x: 0, ease: "power1.inOut" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.05, x: 0.1, ease: "power1.inOut" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.05, x: -1, ease: "power1.inOut" });
  hero.animationTween[3] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation], { duration: 0.05, x: 2.3, ease: "power1.inOut" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.05, x: 0.5, z: -0.8, ease: "power1.out" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.05, x: 0.5, z: 0.8, ease: "power1.out" });
  hero.animationTween[0] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.05, x: -2, ease: "power1.out", onComplete: function() {
  	if (!moveWorld) {
  		moveWorld = true;
  		flyY = true;
  	}
  	hero.flyAnimation = gsap.to(hero.position, { duration: 0.5, y: hero.position.y + 2, ease: "power1.out" });
  	hero.animationTween[3] = gsap.to(hero.topBody.rotation, { duration: 0.2, x: 0.4, ease: "power1.out" });
  	hero.animationTween[0] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.2, x: 0.4, ease: "power1.out" });
    hero.animationTween[1] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation, hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.2, x: 0, ease: "power1.out" });
    hero.animationTween[2] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.2, x: 0.3, ease: "power1.out", onComplete: function() {
    	if (hero.animation == `holdFly`) {
    		clearHeroAnimation();
    		goHeroFlyIdleAnimation();
    	}
    } });
  } });
}
function clearFlyAnimation() {
	if (hero.flyAnimation !== undefined && hero.flyAnimation !== null) {
		hero.flyAnimation.kill();
		hero.flyAnimation = null;
	}
}
function goHeroFlyDownAnimation() {
	gameSound[5].play();
	gameSound[13].play();
	for (let i = 0; i < 12; i++) {
    if (hero.flySparkle[i].tween !== undefined && hero.flySparkle[i].tween !== null) {
    	hero.flySparkle[i].tween.kill();
    	hero.flySparkle[i].tween = null;
    	hero.flySparkle[i].tween1.kill();
    	hero.flySparkle[i].tween1 = null;
    }
    hero.flySparkle[i].visible = true;
  	hero.flySparkle[i].scale.set(0, 0, 1);
    hero.flySparkle[i].position.set(0, hero.position.y - Math.random() * 0.5, hero.position.z - worldContainer.position.z - 0.5 + Math.random());
    const randomTime = 0.4 + Math.random() * 0.6;
    const randomScale = 0.5 + Math.random();
    hero.flySparkle[i].tween = gsap.to(hero.flySparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
    hero.flySparkle[i].tween1 = gsap.to(hero.flySparkle[i].position, { duration: randomTime, y: hero.flySparkle[i].position.y + Math.random(), z: hero.flySparkle[i].position.z - 0.1 * randomTime, ease: "power2.out", onComplete: function() {
      hero.flySparkle[i].tween.kill();
     	hero.flySparkle[i].tween = null;
     	hero.flySparkle[i].tween1.kill();
     	hero.flySparkle[i].tween1 = null;
    	hero.flySparkle[i].visible = false;
    } });
  }
	clearFlyAnimation();
	hero.animation = `holdFly`;
	if (flyHint) {
		flyHint = false;
		if (touchReady) {
	  	for (let i = 0; i < 6; i++) {
  	    mainScene.swipeHintDown.animationTween[i].kill();
  	  	mainScene.swipeHintDown.animationTween[i] = null;
        gsap.to(mainScene.swipeHintDown.children[i].material, { duration: 0.3, opacity: 0, ease: "none" });
        mainScene.swipeHint.part[i].tween.kill();
  	  	mainScene.swipeHint.part[i].tween = null;
        gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.3, opacity: 0, ease: "none" });
      }
      gsap.to(mainScene.flyHintText.material, { duration: 0.3, opacity: 0, ease: "power2.in", onComplete: function() {
        mainScene.remove(mainScene.swipeHint, mainScene.swipeHintDown);
        uiScene.remove(mainScene.flyHintText);
      } });
		} else {
			mainScene.arrowUpHint.tween.kill();
  		mainScene.arrowUpHint.tween = null;
  		mainScene.arrowDownHint.tween.kill();
  		mainScene.arrowDownHint.tween = null;
      gsap.to([mainScene.arrowUpHint.material, mainScene.arrowDownHint.material, mainScene.arrowFlyText.material], { duration: 0.3, opacity: 0, ease: "none", onComplete: function() {
        mainScene.arrowUpHint.visible = false;
        mainScene.arrowDownHint.visible = false;
        mainScene.arrowFlyText.visible = false;
      } });
		}
	}
	if (!moveWorld) {
		moveWorld = true;
		flyY = true;
	}
	hero.animationTween[6] = gsap.to(hero.rotation, { duration: 0.2, x: 0, ease: "power1.inOut" });
  hero.animationTween[7] = gsap.to(hero.head.rotation, { duration: 0.2, x: 0.4, ease: "power1.inOut" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.2, x: -0.4, ease: "power1.inOut" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.2, x: -0.2, ease: "power1.inOut" });
  hero.animationTween[3] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation], { duration: 0.2, x: 2.3, ease: "power1.inOut" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.2, x: 1, z: -0.8, ease: "power1.out" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.2, x: 1, z: 0.8, ease: "power1.out" });
  hero.flyAnimation = gsap.to(hero.position, { duration: 0.5, y: hero.position.y - 2, ease: "power1.out" });
  hero.animationTween[0] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.2, x: -2, ease: "power1.out", onComplete: function() {
  	if (hero.animation == `holdFly`) {
  		clearHeroAnimation();
  		goHeroFlyIdleAnimation();
  	}
  } });
}
function goHeroFlyHitDownAnimation() {
	gameSound[10].play();
	if (hero.hitSparkle.tween !== undefined && hero.hitSparkle.tween !== null) {
  	hero.hitSparkle.tween.kill();
  	hero.hitSparkle.tween = null;
  }
  hero.hitSparkle.scale.set(0, 0, 1);
	hero.hitSparkle.visible = true;
  hero.hitSparkle.position.set(0, hero.position.y + 1.3, hero.position.z - worldContainer.position.z);
  hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.1, y: 1.1, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
  hero.hitSparkle.tween.kill();
    hero.hitSparkle.tween = null;
    hero.hitSparkle.visible = false;
  } });
	clearHeroAnimation();
	hero.animation = `hitDown`;
  hero.animationTween[7] = gsap.to(hero.head.rotation, { duration: 0.2, x: -0.5, ease: "power1.out" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.2, x: 0, ease: "power1.out" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.2, x: -1.5, ease: "power2.out" });
  hero.animationTween[3] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation], { duration: 0.2, x: 0.9, ease: "power2.out" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.2, x: -1, z: 0, ease: "power2.out" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.2, x: -1, z: 0, ease: "power1.out" });
  hero.flyAnimation = gsap.to(hero.position, { duration: 0.5, y: hero.position.y - 1, ease: "power1.out" });
  hero.animationTween[0] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.2, x: -0.9, ease: "power2.out" });
  hero.animationTween[6] = gsap.to(hero.rotation, { duration: 0.2, x: 0.8, ease: "power2.out", onComplete: function() {
  	if (hero.animation == `hitDown`) {
  		clearHeroAnimation();
  		goHeroFlyIdleAnimation();
  	}
  } });
}
function goHeroFlyHitUpAnimation() {
	gameSound[10].play();
	if (hero.hitSparkle.tween !== undefined && hero.hitSparkle.tween !== null) {
  	hero.hitSparkle.tween.kill();
  	hero.hitSparkle.tween = null;
  }
  hero.hitSparkle.scale.set(0, 0, 1);
	hero.hitSparkle.visible = true;
  hero.hitSparkle.position.set(0, hero.position.y - 0.64, hero.position.z - worldContainer.position.z);
  hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.1, y: 1.1, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
  hero.hitSparkle.tween.kill();
    hero.hitSparkle.tween = null;
    hero.hitSparkle.visible = false;
  } });
	clearHeroAnimation();
	hero.animation = `hitUp`;
  hero.animationTween[7] = gsap.to(hero.head.rotation, { duration: 0.2, x: 0.5, ease: "power1.out" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.2, x: 0, ease: "power1.out" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.2, x: 0.2, ease: "power2.out" });
  hero.animationTween[3] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation], { duration: 0.2, x: 0.9, ease: "power2.out" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.2, x: -1, z: 0, ease: "power2.out" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.2, x: -1, z: 0, ease: "power1.out" });
  hero.flyAnimation = gsap.to(hero.position, { duration: 0.5, y: hero.position.y + 1, ease: "power1.out" });
  hero.animationTween[0] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.2, x: -0.9, ease: "power2.out" });
  hero.animationTween[6] = gsap.to(hero.rotation, { duration: 0.2, x: -0.2, ease: "power2.out", onComplete: function() {
  	if (hero.animation == `hitUp`) {
  		clearHeroAnimation();
  		goHeroFlyIdleAnimation();
  	}
  } });
}
function goHeroIdleAnimation() {
	hero.animation = `idle`;
	hero.animationTween[0] = gsap.to(hero.position, { duration: 0.15, y: hero.floor, ease: "power1.inOut" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.15, x: 0.03, ease: "power1.inOut" });
  if (level == 4 && hero.floor < 30) {
  	hero.animationTween[2] = gsap.to(hero.head.rotation, { duration: 0.15, x: -0.4, z: 0.03, ease: "power1.inOut" });
  } else {
  	hero.animationTween[2] = gsap.to(hero.head.rotation, { duration: 0.15, x: 0.03, z: 0.03, ease: "power1.inOut" });
  }
	hero.animationTween[3] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation, hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.15, x: 0, ease: "power1.inOut" });
  hero.animationTween[6] = gsap.to(hero.rightHand.rotation, { duration: 0.15, x: 0.2, z: -0.15, ease: "power1.inOut" });
  hero.animationTween[4] = gsap.to(hero.leftHand.rotation, { duration: 0.15, x: 0.2, z: 0.15, ease: "power1.inOut" });
  hero.animationTween[5] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.15, x: -0.4, y: 0,  ease: "power1.inOut", onComplete: function() {
    clearHeroAnimation();
  	hero.animationTween[0] = gsap.to(hero.topBody.position, { duration: 0.6, y: -0.01, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[1] = gsap.to(hero.rightHand.rotation, { duration: 0.6 + Math.random(), x: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[2] = gsap.to(hero.leftHand.rotation, { duration: 0.6 + Math.random(), x: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[3] = gsap.to(hero.rightHand.arm.rotation, { duration: 0.6 + Math.random(), x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[4] = gsap.to(hero.leftHand.children[1].rotation, { duration: 0.6 + Math.random(), x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[5] = gsap.to(hero.topBody.rotation, { duration: 1.2, x: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
    if (level == 4 && hero.floor < 30) {
      hero.animationTween[6] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), x: -0.34, ease: "power1.inOut", repeat: -1, yoyo: true });
    } else {
    	hero.animationTween[6] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), x: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
    }
    hero.animationTween[7] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), z: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
    if (level == 4 && hero.floor > 30) goFin();
  } });
}
function goHeroRunAnimation() {
	hero.animation = `run`;
	hero.animationTween[0] = gsap.to(hero.position, { duration: 0.25, y: hero.floor, ease: "power1.inOut" });
	hero.animationTween[1] = gsap.to(hero.head.rotation, { duration: 0.25, x: 0, z: 0, ease: "power1.inOut" });
	hero.animationTween[2] = gsap.to(hero.topBody.rotation, { duration: 0.25, x: 0.2, ease: "power1.inOut" });
	hero.animationTween[3] = gsap.to(hero.rightLeg.part[1].rotation, { duration: 0.25, x: 0.8, ease: "power1.inOut" });
	hero.animationTween[4] = gsap.to(hero.leftLeg.children[1].rotation, { duration: 0.25, x: 1.2, ease: "power1.inOut" });
  hero.animationTween[5] = gsap.to(hero.leftLeg.rotation, { duration: 0.25, x: 0.8, ease: "power1.inOut" });
  hero.animationTween[6] = gsap.to(hero.rightHand.rotation, { duration: 0.25, x: 1.2, ease: "power1.inOut" });
  hero.animationTween[7] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.25, x: -1, ease: "power1.inOut" });
  hero.animationTween[8] = gsap.to(hero.leftHand.rotation, { duration: 0.25, x: -1, ease: "power1.inOut" });
  if (level < 4) hero.animationTween[10] = gsap.to(hero.rotation, { duration: 0.25, x: 0, y: Math.PI, ease: "power2.inOut" });
	hero.animationTween[9] = gsap.to(hero.rightLeg.rotation, { duration: 0.25, x: -0.8, ease: "power1.inOut", onComplete: function() {
		clearHeroAnimation();
		hero.animationTween[0] = gsap.to(hero.rightLeg.rotation, { duration: 0.4, x: 0.8, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[1] = gsap.to(hero.rightLeg.part[1].rotation, { duration: 0.4, x: 1.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[2] = gsap.to(hero.leftLeg.rotation, { duration: 0.4, x: -0.8, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[3] = gsap.to(hero.leftLeg.children[1].rotation, { duration: 0.4, x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.4, x: -1, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[5] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 2, x: 0, ease: "power1.in", repeat: -1, yoyo: true });
    hero.animationTween[6] = gsap.to(hero.leftHand.rotation, { duration: 0.4, x: 1.2, ease: "power1.inOut", repeat: -1, yoyo: true });
    hero.animationTween[7] = gsap.to(hero.position, { duration: 0.2, y: hero.floor + 0.02, ease: "power1.inOut", repeat: -1, yoyo: true });
	} });
}
let bookHint = true;
let doubleJumpHint = true;
let slideHint = true;
function goHeroJumpAnimation() {
	gameSound[5].play();
	hero.animation = `jumpUp`;
	let easeIn = "power2.in";
	let easeOut = "power2.out";
	if (level == 4) {
		easeIn = "power1.in";
		easeOut = "power1.out";
		if (hero.floor > 0.64) gsap.to(hero.obstacle.position, { duration: 1, x: 8, ease: "power1.inOut" });
    for (let i = 43; i < levelEnvironment[0].obstacle.length; i++) {
    	if (levelEnvironment[0].obstacle[i].position.y < hero.position.y + 2.5 && levelEnvironment[0].obstacle[i].position.x < 0 && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].ghost)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.3) {
    		if (i < 54) {
    			levelEnvironment[0].obstacle[i].tween.pause();
    			levelEnvironment[0].obstacle[i].tween1.pause();
    			if (Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z)) > levelEnvironment[0].obstacle[i].box[0] * 0.5 - 0.2) {
    				if ((levelEnvironment[0].obstacle[i].position.z - (hero.position.z - worldContainer.position.z)) < 0) {
    					gsap.to(levelEnvironment[0].obstacle[i].container.position, { duration: 0.8, x: -(Math.abs(levelEnvironment[0].obstacle[i].position.z - (hero.position.z - worldContainer.position.z)) - (levelEnvironment[0].obstacle[i].box[0] * 0.5 - 0.2)), ease: "power1.inOut" });
    				} else {
    					gsap.to(levelEnvironment[0].obstacle[i].container.position, { duration: 0.8, x: Math.abs(levelEnvironment[0].obstacle[i].position.z - (hero.position.z - worldContainer.position.z)) - (levelEnvironment[0].obstacle[i].box[0] * 0.5 - 0.2), ease: "power1.inOut" });
    				}
    			}
    		}
    	  hero.animation = `none`;
    		gsap.to(levelEnvironment[0].obstacle[i].position, { duration: 0.8, x: 0, ease: "power1.in" });
    		break;
    	} else if (levelEnvironment[0].obstacle[i].position.y < hero.position.y + 2.5 && levelEnvironment[0].obstacle[i].position.x < 0 && !(Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].ghost)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.3)) {
    		hero.animation = `fall`;
    		break;
    	}
    }
	}
	if (level == 1 && doubleJumpHint) {
		onPlay = false;
	}
	hero.animationTween[0] = gsap.to(hero.topBody.rotation, { duration: 0.05, x: 0.3, ease: "power1.in" });
  hero.animationTween[1] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.05, x: -1, ease: "power1.in" });
  hero.animationTween[2] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: 0.05, x: 2, ease: "power1.in" });
  hero.animationTween[3] = gsap.to(hero.position, { duration: 0.05, y: hero.floor - 0.18, ease: "power1.in" });
  hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.05, x: 1, ease: "power1.in" });
  hero.animationTween[5] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.05, x: -0.7, ease: "power1.in" });
  hero.animationTween[7] = gsap.to(hero.rotation, { duration: 0.05, x: 0, ease: "power2.inOut" });
  if (level < 4) hero.animationTween[8] = gsap.to(hero.rotation, { duration: 0.05, y: Math.PI, ease: "power2.inOut" });
  hero.animationTween[6] = gsap.to(hero.head.rotation, { duration: 0.05, x: 0.4, ease: "power1.in", onComplete: function() {
    let fallTime = 0.5;
    if (level < 4) {
      if (hero.floor > 0.64) fallTime = (hero.position.y + 3.54) / 3.54 * 0.5;
      hero.floor = 0.64;
    } 
    clearHeroAnimation();
    if (!moveWorld && level != 4) moveWorld = true;
    for (let i = 0; i < 12; i++) {
    	hero.jumpSparkle[i].visible = true;
    	hero.jumpSparkle[i].position.set(0, hero.position.y - 0.46, hero.position.z - worldContainer.position.z);
    	const randomTime = 0.2 + Math.random() * 0.4;
    	const randomScale = 0.3 + Math.random();
    	gsap.to(hero.jumpSparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
      gsap.to(hero.jumpSparkle[i].position, { duration: randomTime, y: hero.jumpSparkle[i].position.y + Math.random() * 1.5, z: hero.jumpSparkle[i].position.z - 1 + Math.random() * 2, ease: "power2.out", onComplete: function() {
    	  hero.jumpSparkle[i].visible = false;
      } });
    }
    hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.5, x: 0, ease: "power1.out" });
    hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation, hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: 0.5, x: 0, ease: "power2.out" });
    hero.animationTween[3] = gsap.to(hero.head.rotation, { duration: 0.5, x: -0.2, ease: "power1.out" });
    hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.5, x: -1.5, ease: "power1.out" });
    hero.animationTween[0] = gsap.to(hero.position, { duration: 0.5, y: hero.position.y + 3.54, ease: easeOut, onComplete: function() {
    	if (level == 1 && doubleJumpHint) {
        doubleJumpHint = false;
    		moveWorld = false;
    		onPlay = false;
    		gameSound[7].play();
    		if (touchReady) {
    			uiScene.add(mainScene.doubleSwipeHintText);
    			mainScene.doubleSwipeHintText.visible = true;
    			gsap.to(mainScene.doubleSwipeHintText.material.color, { duration: 0.04, setHex: 0xFFFFFF, ease: "none", repeat: 7, yoyo: true, onComplete: function() {
    				onPlay = true;
    			} });
    		} else {
    			mainScene.doubleArrowUpText.visible = true;
    			gsap.to(mainScene.doubleArrowUpText.material.color, { duration: 0.04, setHex: 0xFFFFFF, ease: "none", repeat: 7, yoyo: true, onComplete: function() {
    				onPlay = true;
    			} });
    		}
    	} else {
    	  if (level == 4 && hero.floor > 0.64) fallTime = 0.5 + hero.floor / 2.5 * 0.03;
      	if (hero.animation == `jumpUp` || hero.animation == `fall`) {
    	  	hero.animation = `jumpDown`;
    	  	clearHeroAnimation();
    	  	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: fallTime, x: 0.3, ease: "power2.in" });
     	  	hero.animationTween[2] = gsap.to(hero.head.rotation, { duration: fallTime, x: 0.4, ease: "power1.in" });
          hero.animationTween[3] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: fallTime, x: 1, ease: "power1.in" });
          hero.animationTween[4] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: fallTime, x: 2, ease: "power2.in" });
    	  	hero.animationTween[5] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: fallTime, x: -1, ease: "power2.in" });
    	  	if (level == 4 && hero.floor > 0.64) {
    	  		if (hero.position.z < -0.9) {
    	  		  gsap.to(hero.rotation, { duration: fallTime, x: 0, y: Math.PI * 2, ease: "power1.in" });
    	  		} else {
    	  			gsap.to(hero.rotation, { duration: fallTime, x: 0, y: Math.PI, ease: "power1.in" });
    	  		}
    	  	}
    	    if (level == 4 && hero.floor > 6) gameSound[14].play();
    	  	hero.animationTween[0] = gsap.to(hero.position, { duration: fallTime, y: 0.46, ease: easeIn, onComplete: function() {
    		  	if (hero.animation == `jumpDown`) {
    		    	clearHeroAnimation();
    		    	if (bookHint) {
    		  	  	onPlay = false;
    		  	  	moveWorld = false;
    		    		bookHint = false;
    		    		gameSound[6].play();
    		  	  	goHeroIdleAnimation();
    		  	  	robot.visible = true;
    		  	  	robot.scale.set(1.2, 1.2, 1.2);
    		  	  	robot.position.set(2, -4, -2);
    		  	  	gsap.to(bookCounter.icon.position, { duration: 0.3, y: 0, ease: "power2.out" });
                gsap.to(bookCounter.count.position, { duration: 0.3, y: -0.38, ease: "power2.out" });
    		  	  	gsap.to(robot.position, { duration: 0.4, x: 0.7, y: -1.4, ease: "power1.out" });
                gameSound[2].play();
                gsap.to([mainScene.bookHint.scale, speechBubble[4].back.scale, speechBubble[4].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.15 });
                setTimeout(function() {
                	gsap.to(robot.position, { duration: 0.3, x: 2, y: -4, ease: "power1.in" });
                	gsap.to([mainScene.bookHint.scale, speechBubble[4].back.scale, speechBubble[4].text.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in", onComplete: function() {
              	  	clearHeroAnimation();
              	  	goHeroRunAnimation();
              	  	onPlay = true;
              	  	moveWorld = true;
                	} });
                }, 1400);
    		    	} else {
    		    		if (level < 4) {
    		    			gameSound[6].play();
    		    			goHeroRunAnimation();
    		    		} else {
    		    			if (hero.floor > 0.64) {
    		    				gameSound[11].play();
    		    				hero.floor = 0.64;
    		    			  onPlay = false;
    		    		  	gsap.to([hero.rightHand.rotation, hero.rightHand.arm.rotation, hero.leftHand.rotation, hero.leftHand.children[1].rotation], { duration: 0.1, x: -0.4, ease: "power1.out" });
	                  gsap.to(hero.topBody.rotation, { duration: 0.1, x: 0.6, ease: "power1.out" });
                    gsap.to(hero.head.rotation, { duration: 0.1, x: 0.7, z: 0, ease: "power2.out", onComplete: function() {
                    	gsap.to(hero.head.rotation, { duration: 0.4, x: 0.4, ease: "power1.in" });
                      gsap.to(hero.topBody.rotation, { duration: 0.4, x: 0.3, ease: "power1.in" });
                      gsap.to([hero.rightHand.rotation, hero.rightHand.arm.rotation, hero.leftHand.rotation, hero.leftHand.children[1].rotation], { duration: 0.4, x: 0, ease: "power1.in", onComplete: function() {
                       	hero.animationTween[0] = gsap.to(hero.topBody.rotation, { duration: 0.5 + Math.random() * 0.5, x: 0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
                        hero.animationTween[1] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.5 + Math.random() * 0.5, x: -0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
    	                  gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.5, x: -0.4, ease: "power1.inOut", repeat: 2, yoyo: true, onComplete: function() {
                          gsap.to([speechBubble[8].back.scale, speechBubble[8].text.scale, speechBubble[8].text2.scale], { duration: 0.2, x: 0, y: 0, ease: "back.in" });
                          clearHeroAnimation();
                          goHeroRunAnimation();
                          gsap.to(hero.rotation, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, x: 0, y: Math.PI * 1.5, ease: "none" });
                          gsap.to(hero.position, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, z: -0.9, ease: "none", onComplete: function() {
                          	clearHeroAnimation();
                          	goHeroIdleAnimation();
                          } });
                          for (let i = 43; i < levelEnvironment[0].obstacle.length - 1; i++) {
                          	if (levelEnvironment[0].obstacle[i].position.x > 0) {
                        	    levelEnvironment[0].obstacle[i].position.y += 2.5;
                          	  levelEnvironment[0].obstacle[i].container.position.x = 0;
                             	gsap.to(levelEnvironment[0].obstacle[i].position, { duration: 1, y: 2 + 2.5 * (i - 43), x: -1, ease: "power1.out", delay: 0.2 * (i - 43) });
                          	}
                          }
                          setTimeout(function() {
                          	onPlay = true;
                          }, 1050);
                      	} });
                      } });
                    } });
                    gsap.to(rotationContainer.scale, { duration: 0.05, x: 1.15, y: 1.15, z: 1.15, repeat: 5, yoyo: true, ease: "power1.inOut" });
                    takeBookAway();
                    goGlitch = true;
                    if (hero.hitSparkle.tween !== undefined && hero.hitSparkle.tween !== null) {
  		                hero.hitSparkle.tween.kill();
  		                hero.hitSparkle.tween = null;
  	                }  
  	                hero.hitSparkle.scale.set(0, 0, 1);
  	                hero.hitSparkle.visible = true;
  		              hero.hitSparkle.position.set(0, hero.position.y - 0.4, hero.position.z - worldContainer.position.z );
                    hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.2, y: 1.2, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
      	              hero.hitSparkle.tween.kill();
      	              hero.hitSparkle.tween = null;
                      hero.hitSparkle.visible = false;
                      goGlitch = false;
                      gameSound[15].play();
                      if (hero.position.z > -0.9) {
                      	speechBubble[8].back.position.x = -(hero.position.z + 0.9) / 7 + 0.15;
                      	speechBubble[8].text.position.x = -(hero.position.z + 0.9) / 7 + 0.15;
                      	gsap.to([speechBubble[8].back.scale, speechBubble[8].text.scale], { duration: 0.3, x: 1, y: 1, ease: "back.out" });
                      } else {
                      	speechBubble[8].back.position.x = -(hero.position.z + 0.9) / 7 - 0.15;
                      	speechBubble[8].text2.position.x = -(hero.position.z + 0.9) / 7 - 0.15;
                      	gsap.to(speechBubble[8].back.scale, { duration: 0.3, x: -1, y: 1, ease: "back.out" });
                      	gsap.to(speechBubble[8].text2.scale, { duration: 0.3, x: 1, y: 1, ease: "back.out" });
                      }
                    } });
                    for (let i = 0; i < 12; i++) {
                     	hero.jumpSparkle[i].visible = true;
    	                hero.jumpSparkle[i].position.set(0, hero.position.y - 0.46, hero.position.z - worldContainer.position.z);
    	                const randomTime = 0.2 + Math.random() * 0.4;
    	                const randomScale = 0.3 + Math.random();
                    	gsap.to(hero.jumpSparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
                      gsap.to(hero.jumpSparkle[i].position, { duration: randomTime, y: hero.jumpSparkle[i].position.y + Math.random() * 1.5, z: hero.jumpSparkle[i].position.z - 1 + Math.random() * 2, ease: "power2.out", onComplete: function() {
    	                  hero.jumpSparkle[i].visible = false;
                      } });
                    }
                    gsap.to(hero.rightLeg.rotation, { duration: 0.05, x: -1.5, ease: "power1.out" });
                    gsap.to(hero.rightLeg.part[1].rotation, { duration: 0.05, x: 1.5, ease: "power1.out" });
                    gsap.to(hero.leftLeg.rotation, { duration: 0.05, x: 0, ease: "power1.out" });
                    gsap.to(hero.leftLeg.children[1].rotation, { duration: 0.05, x: 1.85, ease: "power1.out" });
                    gsap.to(hero.position, { duration: 0.05, y: 0.37, ease: "power1.in" });
    		    			} else {
    		    				clearHeroAnimation();
    		    				goHeroIdleAnimation();
    		    			}
    		    		} 
    		    	}
    		  	}
    	  	} });
    	  } else {
    	  	hero.animation = `jumpDown`;
          hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.25, x: 0.3, ease: "power2.in" });
          hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.25, x: -1, ease: "power2.in" });
          hero.animationTween[3] = gsap.to(hero.head.rotation, { duration: 0.25, x: 0.4, ease: "power1.in" });
          hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.25, x: 1, ease: "power1.in" });
          hero.animationTween[5] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: 0.25, x: 2, ease: "power2.in" });
          hero.animationTween[0] = gsap.to(hero.position, { duration: 0.25, y: hero.floor + 2.32, ease: "power1.in" });
    	  } 
    	}
    } });
  } });
}
function goHeroDoubleJumpAnimation() {
	hero.animation = `doubleJump`;
	gameSound[5].play();
	gameSound[13].play();
	hero.animationTween[0] = gsap.to(hero.topBody.rotation, { duration: 0.05, x: 0.3, ease: "power1.in" });
  hero.animationTween[1] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.05, x: -1, ease: "power1.in" });
  hero.animationTween[2] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: 0.05, x: 2, ease: "power1.in" });
  hero.animationTween[3] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.05, x: 1, ease: "power1.in" });
  hero.animationTween[4] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.05, x: -0.7, ease: "power1.in" });
  hero.animationTween[5] = gsap.to(hero.head.rotation, { duration: 0.05, x: 0.4, ease: "power1.in", onComplete: function() {
    for (let i = 0; i < 12; i++) {
    	if (hero.flySparkle[i].tween !== undefined && hero.flySparkle[i].tween !== null) {
    		hero.flySparkle[i].tween.kill();
    		hero.flySparkle[i].tween = null;
    		hero.flySparkle[i].tween1.kill();
    		hero.flySparkle[i].tween1 = null;
    	}
    	hero.flySparkle[i].visible = true;
  	  hero.flySparkle[i].scale.set(0, 0, 1);
    	hero.flySparkle[i].position.set(0, hero.position.y + Math.random() * 0.5, hero.position.z - worldContainer.position.z - 0.5 + Math.random());
    	const randomTime = 0.4 + Math.random() * 0.6;
    	const randomScale = 0.5 + Math.random();
    	hero.flySparkle[i].tween = gsap.to(hero.flySparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
      hero.flySparkle[i].tween1 = gsap.to(hero.flySparkle[i].position, { duration: randomTime, y: hero.flySparkle[i].position.y - Math.random() * 1.5, z: hero.flySparkle[i].position.z - 0.1 * randomTime, ease: "power2.out", onComplete: function() {
      	hero.flySparkle[i].tween.kill();
      	hero.flySparkle[i].tween = null;
      	hero.flySparkle[i].tween1.kill();
      	hero.flySparkle[i].tween1 = null;
      	hero.flySparkle[i].visible = false;
      } });
    }
    clearHeroAnimation();
    if (!moveWorld) {
    	if (touchReady) {
    	  for (let i = 0; i < 6; i++) {
  		  	mainScene.swipeHint.part[i].tween.kill();
    			mainScene.swipeHint.part[i].tween = null;
          gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.3, opacity: 0, ease: "none" });
        }
        gsap.to(mainScene.doubleSwipeHintText.material, { duration: 0.3, opacity: 0, ease: "power2.in", onComplete: function() {
          mainScene.remove(mainScene.swipeHint);
          uiScene.remove(mainScene.doubleSwipeHintText);
        } });
    	} else {
    		mainScene.arrowUpHint.tween.kill();
  		  mainScene.arrowUpHint.tween = null;
        gsap.to([mainScene.arrowUpHint.material, mainScene.doubleArrowUpText.material], { duration: 0.3, opacity: 0, ease: "none", onComplete: function() {
          mainScene.arrowUpHint.visible = false;
          mainScene.doubleArrowUpText.visible = false;
        } });
    	}
    	moveWorld = true;
    }
    hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.5, x: 0, ease: "power1.out" });
    hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation, hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: 0.5, x: 0, ease: "power2.out" });
    hero.animationTween[3] = gsap.to(hero.head.rotation, { duration: 0.5, x: -0.2, ease: "power1.out" });
    hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.5, x: -1.5, ease: "power1.out" });
    hero.animationTween[0] = gsap.to(hero.position, { duration: 0.5, y: hero.position.y + 3, ease: "power1.out", onComplete: function() {
      clearHeroAnimation();
      goHeroFallDownAnimation();
    } });
  } });
}
function goHeroFlyFallAnimation(dir) {
	takeBookAway();
	gameSound[9].play();
	goGlitch = true;
	if (hero.hitSparkle.tween !== undefined && hero.hitSparkle.tween !== null) {
  	hero.hitSparkle.tween.kill();
  	hero.hitSparkle.tween = null;
  }
  hero.hitSparkle.scale.set(0, 0, 1);
	hero.hitSparkle.visible = true;
  hero.hitSparkle.position.set(0, hero.position.y + 0.31, hero.position.z - worldContainer.position.z - 0.5 );
  hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.8, y: 1.8, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
  hero.hitSparkle.tween.kill();
    hero.hitSparkle.tween = null;
    hero.hitSparkle.visible = false;
  } });
	clearFlyAnimation();
	hero.animation = `none`;
	gsap.to(rotationContainer.scale, { duration: 0.05, x: 1.15, y: 1.15, z: 1.15, repeat: 5, yoyo: true, ease: "power1.inOut" });
  hero.head.brows[1].visible = true;
  hero.head.brows[2].visible = false;
  hero.head.mouth[1].visible = true;
  hero.head.mouth[0].visible = false;
	hero.animationTween[7] = gsap.to(hero.head.rotation, { duration: 0.4, x: 0.2, ease: "power1.out" });
	hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: 0.4, x: 0, ease: "power1.out" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.4, x: -1.5, ease: "power1.out" });
  hero.animationTween[3] = gsap.to([hero.rightLeg.part[1].rotation, hero.leftLeg.children[1].rotation], { duration: 0.4, x: 0.9, ease: "power1.out" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.4, x: -1, z: 0, ease: "power1.out" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.4, x: -1, z: 0, ease: "power1.out" });
  hero.animationTween[0] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.4, x: -0.9, ease: "power1.out" });
  hero.animationTween[6] = gsap.to(hero.rotation, { duration: 0.4, x: 0.3, ease: "power1.out", onComplete: function() {
    	gameSound[15].play();
    	if (hero.position.y < 1.5) {
    	  speechBubble[8].back.position.y = hero.position.y / 6 + 0.15;
      	speechBubble[8].text.position.y = hero.position.y / 6 + 0.15;
      	gsap.to([speechBubble[8].back.scale, speechBubble[8].text.scale], { duration: 0.3, x: 1, y: 1, ease: "back.out" });
    	} else {
        speechBubble[8].back.position.y = hero.position.y / 6 + 0.1;
      	speechBubble[8].text1.position.y = hero.position.y / 6 + 0.1;
      	gsap.to(speechBubble[8].back.scale, { duration: 0.3, x: 1, y: -1, ease: "back.out" });
        gsap.to(speechBubble[8].text1.scale, { duration: 0.3, x: 1, y: 1, ease: "back.out" });
    	}
   		goGlitch = false;
  		clearHeroAnimation();
  		goHeroFlyIdleAnimation();
  } });
  gsap.to(worldContainer.position, { duration: 1, z: worldContainer.position.z - dir, ease: "power1.out", onComplete: function() {
  	gsap.to([speechBubble[8].text1.scale, speechBubble[8].back.scale, speechBubble[8].text.scale], { duration: 0.2, x: 0, y: 0, ease: "back.in" });
  	hero.head.brows[2].visible = true;
  	hero.head.brows[1].visible = false;
  	hero.head.mouth[0].visible = true;
  	hero.head.mouth[1].visible = false;
  	onPlay = true;
  	moveWorld = true;
  } });
}
function goHeroFallAnimation(dir) {
	takeBookAway();
	gameSound[9].play();
  goGlitch = true;
  if (level < 4) {
  	if (hero.hitSparkle.tween !== undefined && hero.hitSparkle.tween !== null) {
  		hero.hitSparkle.tween.kill();
  		hero.hitSparkle.tween = null;
  	}
  	hero.hitSparkle.scale.set(0, 0, 1);
  	hero.hitSparkle.visible = true;
  	if (dir > 0) {
    	hero.hitSparkle.position.set(0, hero.position.y + 0.31, hero.position.z - worldContainer.position.z - 0.5 );
      hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.8, y: 1.8, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
      	hero.hitSparkle.tween.kill();
      	hero.hitSparkle.tween = null;
        hero.hitSparkle.visible = false;
      } });
  	} else {
  		hero.hitSparkle.position.set(0, hero.position.y + 1.3, hero.position.z - worldContainer.position.z );
      hero.hitSparkle.tween = gsap.to(hero.hitSparkle.scale, { duration: 0.2, x: 1.8, y: 1.8, ease: "elastic.out", repeat: 1, yoyo: true, onComplete: function() {
      	hero.hitSparkle.tween.kill();
      	hero.hitSparkle.tween = null;
        hero.hitSparkle.visible = false;
      } });
  	}
  }
	if (dir > 0) gsap.to(worldContainer.position, { duration: 0.5, z: worldContainer.position.z - dir, ease: "none" });
  if (hero.animation == `jumpSlide`) {
  	gsap.to(hero.position, { duration: 0.1, y: 0.64, ease: "power1.in" });
  }
	hero.animation = `none`;
	gsap.to(rotationContainer.scale, { duration: 0.05, x: 1.15, y: 1.15, z: 1.15, repeat: 5, yoyo: true, ease: "power1.inOut" });
  hero.head.brows[1].visible = true;
  hero.head.brows[2].visible = false;
  hero.head.mouth[1].visible = true;
  hero.head.mouth[0].visible = false;
	gsap.to(hero.rotation, { duration: 0.5, x: 0, y: Math.PI, ease: "power1.in", onComplete: function() {
		gameSound[15].play();
		gsap.to([speechBubble[8].back.scale, speechBubble[8].text.scale], { duration: 0.3, x: 1, y: 1, ease: "back.out" });
	} });
  gsap.to([hero.rightHand.rotation, hero.rightHand.arm.rotation, hero.leftHand.rotation, hero.leftHand.children[1].rotation], { duration: 0.1, x: -1, ease: "power1.out" });
	gsap.to(hero.topBody.rotation, { duration: 0.1, x: -0.2, ease: "power1.out" });
  gsap.to(hero.head.rotation, { duration: 0.1, x: -0.5, z: 0, ease: "power2.out", onComplete: function() {
  	gsap.to(hero.head.rotation, { duration: 0.4, x: 0.4, ease: "power1.in" });
    gsap.to(hero.topBody.rotation, { duration: 0.4, x: 0.3, ease: "power1.in" });
    gsap.to([hero.rightHand.rotation, hero.rightHand.arm.rotation, hero.leftHand.rotation, hero.leftHand.children[1].rotation], { duration: 0.4, x: 0, ease: "power1.in", onComplete: function() {
    	goGlitch = false;
    	hero.animationTween[0] = gsap.to(hero.topBody.rotation, { duration: 0.5 + Math.random() * 0.5, x: 0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
      hero.animationTween[1] = gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.5 + Math.random() * 0.5, x: -0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
    	gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: 0.5, x: -0.4, ease: "power1.inOut", repeat: 2, yoyo: true, onComplete: function() {
        gsap.to([speechBubble[8].back.scale, speechBubble[8].text.scale], { duration: 0.2, x: 0, y: 0, ease: "back.in" });
        hero.head.brows[2].visible = true;
        hero.head.brows[1].visible = false;
        hero.head.mouth[0].visible = true;
        hero.head.mouth[1].visible = false;
        clearHeroAnimation();
        goHeroRunAnimation();
        if (level < 4) onPlay = true;
        moveWorld = true;
    	} });
    } });
  } });
  gsap.to(hero.rightLeg.rotation, { duration: 0.5, x: -1.5, ease: "power1.out" });
  gsap.to(hero.rightLeg.part[1].rotation, { duration: 0.5, x: 1.5, ease: "power1.out" });
  gsap.to(hero.leftLeg.rotation, { duration: 0.5, x: 0, ease: "power1.out" });
  gsap.to(hero.leftLeg.children[1].rotation, { duration: 0.5, x: 1.85, ease: "power1.out" });
  gsap.to(hero.position, { duration: 0.4, y: 0.37, ease: "power1.in", delay: 0.1 });
}
function goHeroFallDownAnimation() {
	hero.animation = `jumpDown`;
	const fallTime = hero.position.y / 3.54 * 0.5;
  hero.animationTween[1] = gsap.to(hero.topBody.rotation, { duration: fallTime, x: 0.3, ease: "power2.in" });
  hero.animationTween[2] = gsap.to([hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: fallTime, x: -1, ease: "power2.in" });
  hero.animationTween[3] = gsap.to(hero.head.rotation, { duration: fallTime, x: 0.4, ease: "power1.in" });
  hero.animationTween[4] = gsap.to([hero.rightHand.rotation, hero.leftHand.rotation], { duration: fallTime, x: 1, ease: "power1.in" });
  hero.animationTween[5] = gsap.to([hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation], { duration: fallTime, x: 2, ease: "power2.in" });
  hero.animationTween[6] = gsap.to(hero.rotation, { duration: 0.15, x: 0, y: Math.PI, ease: "power2.inOut" });
  hero.animationTween[0] = gsap.to(hero.position, { duration: fallTime, y: 0.46, ease: "power1.in", onComplete: function() {
    gameSound[6].play();
    clearHeroAnimation();
    goHeroRunAnimation();
  } });
}
function goHeroSlideAnimation() {
  hero.animation = `jumpSlide`;
  gameSound[12].play();
	if (!moveWorld) moveWorld = true;
	if (slideHint) {
		slideHint = false;
		if (touchReady) {
		  for (let i = 0; i < 6; i++) {
  	    mainScene.swipeHintDown.animationTween[i].kill();
  	  	mainScene.swipeHintDown.animationTween[i] = null;
        gsap.to(mainScene.swipeHintDown.children[i].material, { duration: 0.3, opacity: 0, ease: "none" });
      }
      gsap.to(mainScene.slideHintText.material, { duration: 0.3, opacity: 0, ease: "power2.in", onComplete: function() {
        mainScene.remove(mainScene.swipeHintDown);
        uiScene.remove(mainScene.slideHintText);
      } });
		} else {
			mainScene.arrowDownHint.tween.kill();
      mainScene.arrowDownHint.tween = null;
      gsap.to([mainScene.arrowDownHint.material, mainScene.arrowDownText.material], { duration: 0.3, opacity: 0, ease: "none", onComplete: function() {
        mainScene.arrowDownHint.visible = false;
        mainScene.arrowDownText.visible = false;
      } });
		}
	}
	for (let i = 0; i < 12; i++) {
    	hero.jumpSparkle[i].visible = true;
    	hero.jumpSparkle[i].position.set(0, hero.position.y - 0.46, hero.position.z - worldContainer.position.z);
    	const randomTime = 0.3 + Math.random() * 0.5;
    	const randomScale = 0.3 + Math.random();
    	gsap.to(hero.jumpSparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
      gsap.to(hero.jumpSparkle[i].position, { duration: randomTime, y: hero.jumpSparkle[i].position.y + Math.random(), z: hero.jumpSparkle[i].position.z + Math.random() * 1.5, ease: "power2.out", onComplete: function() {
    	hero.jumpSparkle[i].visible = false;
    } });
	}
	hero.animationTween[0] = gsap.to(hero.rotation, { duration: 0.3, x: 1.2, y: Math.PI - 0.5, ease: "power2.inOut" });
  hero.animationTween[1] = gsap.to(hero.position, { duration: 0.3, y: hero.floor - 0.45, ease: "power2.inOut" });
  hero.animationTween[2] = gsap.to(hero.rightLeg.rotation, { duration: 0.3, x: -1.1, ease: "power2.inOut" });
  hero.animationTween[3] = gsap.to(hero.head.rotation, { duration: 0.3, x: 0.8, ease: "power1.inOut" });
  hero.animationTween[4] = gsap.to(hero.rightLeg.part[1].rotation, { duration: 0.3, x: 1.6, ease: "power1.inOut" });
  hero.animationTween[5] = gsap.to(hero.leftLeg.rotation, { duration: 0.3, x: -0.2, ease: "power2.inOut" });
  hero.animationTween[4] = gsap.to(hero.rightHand.rotation, { duration: 0.3, x: 2.5, ease: "power1.inOut" });
  hero.animationTween[5] = gsap.to(hero.leftHand.rotation, { duration: 0.3, x: -1, ease: "power1.inOut" });
  hero.animationTween[6] = gsap.to(hero.leftHand.children[1].rotation, { duration: 0.3, x: -1, ease: "power1.inOut" });
  hero.animationTween[7] = gsap.to([hero.topBody.rotation, hero.rightHand.arm.rotation, hero.leftLeg.children[1].rotation], { duration: 0.3, x: 0, ease: "power1.inOut" });
  setTimeout(function() {
  	if (hero.animation == `jumpSlide`) {
  	  clearHeroAnimation();
  	  goHeroRunAnimation();
  	}
	}, 1000);
}
function walkObstacle() {
	hero.animation = `none`;
	clearHeroAnimation();
	goHeroRunAnimation();
	moveWorld = true;
}
function clearHeroAnimation() {
	while (hero.animationTween.length > 0) {
		hero.animationTween[hero.animationTween.length - 1].kill();
		hero.animationTween[hero.animationTween.length - 1] = null;
		hero.animationTween.pop();
	}
}
let bookLevelOffset = [0, 22, 46, 67, 88, 88];
function takeBook(chosen) {
	gameSound[8].play();
	book[chosen].ready = false;
	gsap.to(book[chosen].scale, { duration: 0.1, x: 0, y: 0, z: 0, ease: "back.in" });
  for (let i = 0; i < 12; i++) {
  	book[chosen].sparkle[i].visible = true;
  	book[chosen].sparkle[i].position.set(0, book[chosen].position.y, book[chosen].position.z);
  	const randomTime = 0.3 + Math.random() * 0.7;
  	const randomScale = 1 + Math.random();
  	gsap.to(book[chosen].sparkle[i].scale, { duration: randomTime * 0.5, x: randomScale, y: randomScale, ease: "power2.out", repeat: 1, yoyo: true });
    gsap.to(book[chosen].sparkle[i].position, { duration: randomTime, y: book[chosen].sparkle[i].position.y - 2 + Math.random() * 4, z: book[chosen].sparkle[i].position.z - 1.5 + Math.random() * 3, ease: "power2.out", onComplete: function() {
    	book[chosen].sparkle[i].visible = false;
    } });
  }
  bookCount ++;
  bookCounter.count.material = blimBasicMaterial;
  gsap.to(bookCounter.count.position, { duration: 0.1, y: 0.38, ease: "none" });
  gsap.to(bookCounter.count.scale, { duration: 0.1, y: 0, ease: "none", onComplete: function() {
  	bookCounter.count.geometry.dispose();
  	bookCounter.count.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(`${bookCount}`, 0.46), 1);
    bookCounter.count.position.y = -0.38;
    gsap.to(bookCounter.count.scale, { duration: 0.1, y: 1, ease: "none", onComplete: function() {
    	bookCounter.count.material = bookCounter.countMaterial;
    } });
  } });
  if (bookCounter.count.tween !== undefined && bookCounter.count.tween !== null) {
  	bookCounter.count.tween.kill();
  	bookCounter.count.tween = null;
  }
  bookCounter.icon.scale.set(1, 1, 1);
  bookCounter.count.tween = gsap.to(bookCounter.icon.scale, { duration: 0.05, x: 1.5, y: 1.5, ease: "none", repeat: 3, yoyo: true, onComplete: function() {
    bookCounter.count.tween.kill();
  	bookCounter.count.tween = null;
  } });
  bookCounter.name.geometry.dispose();
  for (let i = 0; i < bookName[chosen + bookLevelOffset[level]].length; i++) {
  	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(bookName[chosen + bookLevelOffset[level]][i], 0.24), 1);
  	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.4 * i, 0);
  }
  bookCounter.name.geometry = new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry);
  clearTempGeometries();
  bookCounter.name.scale.set(1, 1, 1);
  if (bookCounter.name.tween !== undefined && bookCounter.name.tween !== null) {
  	bookCounter.name.tween.kill();
  	bookCounter.name.tween = null;
  }
  bookCounter.name.tween = gsap.to(bookCounter.name.scale, { duration: 0.1, x: 0, y: 0, ease: "back.in", delay: 0.8, onComplete: function() {
    bookCounter.name.tween.kill();
  	bookCounter.name.tween = null;
  } });
}
function takeBookAway() {
	if (bookCount > 1) {
  	hero.bookSparkle.visible = true;
  	setTimeout(function() {
		  hero.bookSparkle.visible = false;
	  }, 400);
	  bookCount --;
    bookCounter.count.material = blimBasicMaterial;
    gsap.to(bookCounter.count.scale, { duration: 0.1, y: 0, ease: "none", onComplete: function() {
  	  bookCounter.count.geometry.dispose();
    	bookCounter.count.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(`${bookCount}`, 0.46), 1);
      bookCounter.count.position.y = 0.38;
      gsap.to(bookCounter.count.position, { duration: 0.1, y: -0.38, ease: "none" });
      gsap.to(bookCounter.count.scale, { duration: 0.1, y: 1, ease: "none", onComplete: function() {
      	bookCounter.count.material = bookCounter.countMaterial;
      } });
    } });
    if (bookCounter.count.tween !== undefined && bookCounter.count.tween !== null) {
    	bookCounter.count.tween.kill();
    	bookCounter.count.tween = null;
    }
    bookCounter.icon.scale.set(1, 1, 1);
    bookCounter.count.tween = gsap.to(bookCounter.icon.scale, { duration: 0.05, x: 1.2, y: 1.2, ease: "none", repeat: 3, yoyo: true, onComplete: function() {
      bookCounter.count.tween.kill();
    	bookCounter.count.tween = null;
    } });
	}
}
function goFin() {
	level = 5;
  goPlay = false;
	clearHeroAnimation();
  goHeroRunAnimation();
  if (hero.position.z > -0.9) {
  	gsap.to(hero.rotation, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, x: 0, y: Math.PI * 0.5, ease: "none" });
  } else {
  	gsap.to(hero.rotation, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, x: 0, y: Math.PI * 2.5, ease: "none" });
  }
  gsap.to(rotationContainer.position, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, x: -0.9, ease: "power1.inOut" });
  gsap.to(frame.materialColor.color, { duration: 4, r: darkPurpleColor.r, g: darkPurpleColor.g, b: darkPurpleColor.b, ease: "none" });
  gsap.to(hero.position, { duration: 0.1 + Math.abs(hero.position.z + 0.9) * 0.4, z: -0.9, ease: "none", onComplete: function() {
    gsap.to([bookCounter.icon.position, bookCounter.count.position], { duration: 0.5, y: 1.5, ease: "power2.in" });
    gsap.to(rotationContainer.position, { duration: 1.9, x: -1.2, ease: "power1.inOut" });
    hero.head.brows[2].visible = false;
    hero.head.brows[1].visible = true;
    hero.head.mouth[0].visible = false;
    hero.head.mouth[2].visible = true;
    clearHeroAnimation();
    hero.animation = `none`;
    gsap.to(hero.head.rotation, { duration: 1, x: 0, y: 0, z: 0, ease: "power1.inOut" });
	  gsap.to([hero.topBody.rotation, hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation, hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.3, x: 0, ease: "power1.inOut" });
    gsap.to(hero.rightHand.rotation, { duration: 1.3, x: 0.2, z: -0.2, ease: "power1.inOut" });
    gsap.to(hero.leftHand.rotation, { duration: 1.3, x: 0.2, z: 0.2, ease: "power1.inOut" });
    gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 1.3, x: -0.4, y: -1.4, ease: "power1.inOut" });
    gsap.to([hero.topBody.rotation, hero.leftLeg.children[1].rotation, hero.rightLeg.part[1].rotation, hero.rightLeg.rotation, hero.leftLeg.rotation], { duration: 0.3, x: 0, ease: "power1.inOut", onComplete: function() {
      levelEnvironment[4].hat.tween[0].kill();
      levelEnvironment[4].hat.tween[0] = null;
      levelEnvironment[4].hat.tween[1].kill();
      levelEnvironment[4].hat.tween[1] = null;
      levelEnvironment[4].hat.tween[2].kill();
      levelEnvironment[4].hat.tween[2] = null;
      gameSound[17].play();
      gsap.to(hero.finSparkle.position, { duration: 1, x: -0.5, y: 31.5, ease: "power1.inOut" });
      gsap.to(levelEnvironment[4].hat.position, { duration: 1, x: 0.95, y: hero.position.y + 1.25, z: 0.1, ease: "power1.inOut" });
      gsap.to(levelEnvironment[4].hat.rotation, { duration: 1, y: 0, ease: "power1.inOut" });
      gsap.to(levelEnvironment[4].hat.scale, { duration: 1, x: 0.8, y: 0.8, z: 0.8, ease: "power1.inOut", onComplete: function() {
      	levelEnvironment[4].hat.visible = false;
      	hero.head.hat.part[0].material = blimMaterial[1];
      	hero.head.hat.part[1].material = blimMaterial[2];
      	hero.head.hat.part[3].material = blimMaterial[3];
      	gsap.to(hero.finSparkle.scale, { duration: 0.5, x: 0, y: 0, ease: "none" });
      	hero.head.hat.visible = true;
      	hero.topBody.mantle.visible = true;
      	hero.topBody.mantle1.visible = true;
      	hero.rightHand.shoulder.part[2].visible = true;
      	hero.rightHand.shoulder.part[3].visible = true;
      	hero.rightHand.arm.part[2].visible = true;
      	hero.leftHand.children[0].children[0].visible = true;
      	hero.leftHand.children[0].children[1].visible = true;
      	hero.leftHand.children[1].children[0].visible = true;
      	setTimeout(function() {
      		hero.head.hat.part[0].material = heroFinMaterial[0];
      	  hero.head.hat.part[1].material = heroFinMaterial[0];
      	  hero.head.hat.part[3].material = heroFinMaterial[2];
      	  gameSound[16].play();
      	  gsap.to(hero.finSparkle.scale, { duration: 0.4, x: 1.5, y: 1.5, ease: "back.out" });
          hero.topBody.mantle.material = heroFinMaterial[0];
      	  hero.topBody.mantle1.material = heroFinMaterial[1];
          hero.rightHand.shoulder.part[2].material = heroFinMaterial[0];
        	hero.rightHand.shoulder.part[3].material = heroFinMaterial[0];
        	hero.rightHand.arm.part[2].material = heroFinMaterial[0];
        	hero.leftHand.children[0].children[0].material = heroFinMaterial[0];
        	hero.leftHand.children[0].children[1].material = heroFinMaterial[0];
        	hero.leftHand.children[1].children[0].material = heroFinMaterial[0];
        	gsap.to(rotationContainer.position, { duration: 0.6, x: -1.3, z: 4.3, ease: "back.out" });
        	gsap.to(hero.topBody.rotation, { duration: 1, x: 0.03, ease: "power1.inOut" });
          gsap.to(hero.head.rotation, { duration: 1, x: 0.03, z: 0.03, ease: "power1.inOut" });
          gsap.to(hero.rightHand.rotation, { duration: 1, x: 0.2, z: -0.15, ease: "power1.inOut" });
          gsap.to(hero.leftHand.rotation, { duration: 1, x: 0.2, z: 0.15, ease: "power1.inOut" });
          gsap.to([hero.rightHand.arm.rotation, hero.leftHand.children[1].rotation], { duration: 0.6, x: -0.4, y: 0,  ease: "power1.inOut", onComplete: function() {
  	        hero.animationTween[1] = gsap.to(hero.rightHand.rotation, { duration: 0.6 + Math.random(), x: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[2] = gsap.to(hero.leftHand.rotation, { duration: 0.6 + Math.random(), x: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[3] = gsap.to(hero.rightHand.arm.rotation, { duration: 0.6 + Math.random(), x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[4] = gsap.to(hero.leftHand.children[1].rotation, { duration: 0.6 + Math.random(), x: 0, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[5] = gsap.to(hero.topBody.rotation, { duration: 1.2, x: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[6] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), x: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
            hero.animationTween[0] =  gsap.to(hero.head.rotation, { duration: 1 + Math.random(), z: -0.03, ease: "power1.inOut", repeat: -1, yoyo: true });
          } });
          speechBubble[6].text.geometry.dispose();
          speechBubble[6].words = [`Воу!`, `Ты прочитал`, resultText[bookCount - 1], `Поздравляем,`, `ты закончил`, `ИТМО!` ];
          for (let i = 0; i < speechBubble[6].words.length; i++) {
           	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(PressStart.generateShapes(speechBubble[6].words[i], 0.05), 1);
          	tempGeometry[tempGeometry.length - 1].computeBoundingBox();
  	        tempGeometry[tempGeometry.length - 1].translate(-0.5 * tempGeometry[tempGeometry.length - 1].boundingBox.max.x, -0.08 * i, 0);
          }
          speechBubble[6].text.geometry = new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry);
          clearTempGeometries();
          speechBubble[6].text.geometry.translate(0, 0.61, 0);
          robot.visible = true;
          robot.scale.set(0.75, 0.75, 0.75);
          robot.position.set(2, -4, -1);
          gsap.to(robot.position, { duration: 0.6, x: 0.25, y: -0.62, ease: "power1.out", delay: 0.2 });
          gameSound[2].play();
          gsap.to([speechBubble[6].back.scale, speechBubble[6].text.scale], { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 0.55, onComplete: function() {
          	showButton(button[2], 0.12);
          	setTimeout(function() {
          		showButton(button[1], 0.076);
          		setTimeout(function() {
          			button[3].ready = true;
          			gsap.to(button[3].scale, { duration: 0.3, x: 1, y: 1, ease: "back.out" });
          		}, 800);
          	}, 2000);
          } });
      	}, 500);
      } });
    } });
  } });
  gsap.to(rotationContainer.position, { duration: 2.5, z: 4, y: rotationContainer.position.y + 1.65, ease: "power1.inOut" });
  gsap.to(rotationContainer.rotation, { duration: 2.5, x: -0.2, y: -1.2, ease: "power1.inOut" });
}
function typeText(text, object, size, step) {
  object.geometry.dispose();
	object.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(text.slice(0, step), size), 1)
	if (step < text.length) {
		setTimeout(function() {
			typeText(text, object, size, step += 1);
		}, 50);
	}
}
function eraseText(text, object, size, step) {
	object.geometry.dispose();
	object.geometry = new THREE.ShapeGeometry(PressStart.generateShapes(text.slice(0, text.length - step), size), 1)
	if (step < text.length) {
		setTimeout(function() {
			eraseText(text, object, size, step += 1);
		}, 50);
	}
}
function showButton(object, textSize) {
	for (let i = 0; i < object.tile.length; i++) {
		const randomTime = 0.2 + Math.random() * 0.2;
		gsap.to(object.tile[i].scale, { duration: randomTime, x: 1, y: 1, ease: "power2.in", delay: 0.005 * i + Math.random() * 0.01 });
    gsap.from(object.tile[i].position, { duration: randomTime, x: object.tile[i].position.x - 1, ease: "power3.in", delay: 0.005 * i + Math.random() * 0.01 });
	}
	setTimeout(function() {
		typeText(object.name, object.text, textSize, 0);
		setTimeout(function() {
			object.ready = true;
		}, object.tile.length * 1.5 + 40);
	}, object.tile.length * 6);
}
function clickButton(object, func, textSize) {
	for (let i = 0; i < object.tile.length; i++) {
		gsap.to(object.tile[i].scale, { duration: 0.1, x: 0.2, y: 0.2, ease: "power2.out", repeat: 1, yoyo: true, delay: 0.001 * (object.tile.length - i) });
	}
	setTimeout(function() {
		requestAnimationFrame(function() {
			eraseText(object.name, object.text, textSize, 0);
			for (let i = 0; i < object.tile.length; i++) {
				const randomTime = 0.2 + Math.random() * 0.2;
				gsap.to(object.tile[i].scale, { duration: randomTime, x: 0, y: 0, ease: "power2.in", delay: 0.005 * i + Math.random() * 0.01 });
			  gsap.to(object.tile[i].position, { duration: randomTime, x: object.tile[i].position.x + 1, ease: "power3.in", delay: 0.005 * i + Math.random() * 0.01 });
			}
			func();
		});
	}, object.tile.length * 1 + 200);
}
function loadSVG(url) { return new Promise(resolve => { new THREE.SVGLoader().load(url, resolve) }) }
function clearTempGeometries() { while (tempGeometry.length > 0) { tempGeometry[tempGeometry.length - 1].dispose(); tempGeometry.pop(); } }
function bendGeometry(geometry, axis, angle) { let theta = 0; if (angle !== 0) { const v = geometry.attributes.position.array; for (let i = 0; i < v.length; i += 3) { let x = v[i]; let y = v[i + 1]; let z = v[i + 2]; switch (axis) { case "x": theta = z * angle; break; case "y": theta = x * angle; break; default: theta = x * angle; break; } let sinTheta = Math.sin(theta); let cosTheta = Math.cos(theta); switch (axis) { case "x": v[i] = x; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = -(y - 1.0 / angle) * sinTheta; break; case "y": v[i] = -(z - 1.0 / angle) * sinTheta; v[i + 1] = y; v[i + 2] = (z - 1.0 / angle) * cosTheta + 1.0 / angle; break; default: v[i] = -(y - 1.0 / angle) * sinTheta; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = z; break; } } geometry.attributes.position.needsUpdate = true; } }
function createSVG(chosen, details, color) {
	const svgPic = new THREE.Object3D();
	svgPic.material = new THREE.MeshBasicMaterial({ color: color, transparent: true });
	for (let i = 0; i < path[chosen].length; i++) {
		const newPath = path[chosen][i];
		const shapes = newPath.toShapes(true);
		for (let j = 0; j < shapes.length; j++) {
			const mesh = new THREE.Mesh(new THREE.ShapeGeometry(shapes[j], details), svgPic.material);
			svgPic.add(mesh);
		};
	}
	return svgPic;
}
document.addEventListener('keydown', function(event) {
	if ((event.code == 'ArrowUp') && !event.repeat) {
  	if (onPlay) {
			if (!(level == 4 && hero.animation == `run`) && !(level == 2 && slideHint) && (level == 0 || level == 1 || level == 2 || level == 4) && (hero.animation == `idle` || hero.animation == `run`)) {
  			clearHeroAnimation();
  			goHeroJumpAnimation();
  			if (worldContainer.position.z == 0) {
  				guard.aninationTween1.kill();
  				guard.aninationTween1 = null;
  				guard.aninationTween2.kill();
  				guard.aninationTween2 = null;
  				gsap.to(guard.leftHand.rotation, { duration: 1, x: 0, ease: "power1.inOut" });
          gsap.to(guard.leftHand.children[1].rotation, { duration: 1, x: -0.1, ease: "power1.inOut" });
          gsap.to(guard.leftHand.children[1].children[0].rotation, { duration: 1, x: 0, y: 0, ease: "power1.inOut" });
  				gsap.to(guard.head.rotation, { duration: 1, x: -0.7, z: 0, ease: "power1.inOut", onComplete: function() {
  					gsap.to(guard.head.rotation, { duration: 1, x: 0.5, z: 0, ease: "power1.inOut" });
  				} });
  				gsap.to([speechBubble[3].back.scale, speechBubble[3].text.scale], { duration: 0.2, x: 0, y: 0, ease: "back.in" });
  				mainScene.arrowUpHint.tween.kill();
  				mainScene.arrowUpHint.tween = null;
        	gsap.to([mainScene.arrowUpHint.material, mainScene.arrowUpHintText.material], { duration: 0.3, opacity: 0, ease: "none", onComplete: function() {
            mainScene.arrowUpHint.visible = false;
            mainScene.arrowUpHintText.visible = true;
          } });
  			}
  		} else if ((level == 1 || level == 2) && hero.animation == `jumpUp`) {
  			clearHeroAnimation();
  			goHeroDoubleJumpAnimation();
  		} else if (level == 3) {
  			clearHeroAnimation();
  			goHeroFlyUpAnimation();
  		}
		} 
	} else if ((event.code == 'ArrowDown') && !event.repeat) {
		if (onPlay) {
			if ((hero.animation == `idle` || hero.animation == `run`) && (level == 2)) {
				clearHeroAnimation();
				goHeroSlideAnimation();
			} else if (level == 3) {
				clearHeroAnimation();
				goHeroFlyDownAnimation();
			}
		}
	}
});
let hammertime = new Hammer(document.body);
hammertime.get('swipe').set({ enable: true, direction: Hammer.DIRECTION_VERTICAL });
hammertime.on('swipe', (e) => {
	if (onPlay) {
  	if (e.direction == 8) {
  		if (!(level == 4 && hero.animation == `run`) && !(level == 2 && slideHint) && (level == 0 || level == 1 || level == 2 || level == 4) && (hero.animation == `idle` || hero.animation == `run`)) {
  			clearHeroAnimation();
  			goHeroJumpAnimation();
  			if (worldContainer.position.z == 0) {
  				guard.aninationTween1.kill();
  				guard.aninationTween1 = null;
  				guard.aninationTween2.kill();
  				guard.aninationTween2 = null;
  				gsap.to(guard.leftHand.rotation, { duration: 1, x: 0, ease: "power1.inOut" });
          gsap.to(guard.leftHand.children[1].rotation, { duration: 1, x: -0.1, ease: "power1.inOut" });
          gsap.to(guard.leftHand.children[1].children[0].rotation, { duration: 1, x: 0, y: 0, ease: "power1.inOut" });
  				gsap.to(guard.head.rotation, { duration: 1, x: -0.7, z: 0, ease: "power1.inOut", onComplete: function() {
  					gsap.to(guard.head.rotation, { duration: 1, x: 0.5, z: 0, ease: "power1.inOut" });
  				} });
  				gsap.to([speechBubble[3].back.scale, speechBubble[3].text.scale], { duration: 0.2, x: 0, y: 0, ease: "back.in" });
  				for (let i = 0; i < 6; i++) {
  					mainScene.swipeHint.part[i].tween.kill();
  					mainScene.swipeHint.part[i].tween = null;
        	  gsap.to(mainScene.swipeHint.part[i].material, { duration: 0.3, opacity: 0, ease: "none" });
          }
          gsap.to(mainScene.swipeHint.text.material, { duration: 0.3, opacity: 0, ease: "power2.in", onComplete: function() {
          	mainScene.remove(mainScene.swipeHint);
          	uiScene.remove(mainScene.swipeHint.text);
          } });
  			}
  		} else if ((level == 1 || level == 2) && hero.animation == `jumpUp`) {
  			clearHeroAnimation();
  			goHeroDoubleJumpAnimation();
  		} else if (level == 3) {
  			clearHeroAnimation();
  			goHeroFlyUpAnimation();
  		}
  	} else if (e.direction == 16) {
  		if ((hero.animation == `idle` || hero.animation == `run`) && (level == 2)) {
		    clearHeroAnimation();
		    goHeroSlideAnimation();
  		} else if (level == 3) {
  			clearHeroAnimation();
  			goHeroFlyDownAnimation();
  		}
  	}
	}
});
let shareReady = false;
let touchReady = false;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();
function onDocumentMouseDown(event) {
  event.preventDefault();
  if (!touchReady) {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, mainCamera);
    let intersects = raycaster.intersectObject(button[2].plane);
    if (intersects.length > 0 && button[2].ready) {
    	window.open("https://itmo.ru/");
    }
    checkInteractive(raycaster);
  }
}
function onDocumentTouchStart(event) {
  touchReady = true;
  event.preventDefault();
  mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);
  checkInteractive(raycaster);
}
function checkInteractive(raycaster) {
  intersects = raycaster.intersectObject(button[0].plane);
  if (intersects.length > 0 && button[0].ready) {
    button[0].ready = false;
    gameSound[0].play();
    gameSound[1].play();
   // gsap.to(button[4].scale, { duration: 0.3, x: 0.8, y: 0.8, ease: "back.out" });
    clickButton(button[0], hideIntro, 0.08);
  }
  intersects = raycaster.intersectObject(button[1].plane);
  if (intersects.length > 0 && button[1].ready) {
  	gameSound[0].play();
  	button[1].ready = false;
  	button[2].ready = false;
  	button[3].ready = false;
  	clickButton(button[1], goReplay, 0.076);
  }
  intersects = raycaster.intersectObject(button[3].plane);
  if (intersects.length > 0 && button[3].ready) {
  	shareReady = true;
  }
  intersects = raycaster.intersectObject(button[4].plane);
  if (intersects.length > 0 && button[4].scale.x == 0.8) {
    if (button[4].icon1.visible) {
      button[4].icon1.visible = false;
      button[4].icon.visible = true;
      Howler.mute(true);
      muted = "true";
    } else if (button[4].icon.visible) {
      button[4].icon.visible = false;
      button[4].icon1.visible = true;
      Howler.mute(false);
      muted = "false";
    }
    localStorage.setItem('savedMuted', muted);
  }
}
function onDocumentMouseMove(event) {
	event.preventDefault();
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	raycaster.setFromCamera(mouse, mainCamera);
	for (let i = 0; i < 5; i++) {
		intersects = raycaster.intersectObject(button[i].plane);
		if (intersects.length > 0 && button[i].ready) {
			document.body.style.cursor = "pointer";
		}
	}
}
function onDocumentTouchEnd(event) {
	event.preventDefault();
	raycaster.setFromCamera(mouse, mainCamera);
	let intersects = raycaster.intersectObject(button[2].plane);
	if (intersects.length > 0 && button[2].ready) {
		window.open("https://itmo.ru/");
	}
}
visibilityChange();
function visibilityChange() {
  let hidden = 'hidden';
  if (hidden in document) document.addEventListener('visibilitychange', onchange);
  else if ((hidden = 'mozHidden') in document) document.addEventListener('mozvisibilitychange', onchange);
  else if ((hidden = 'webkitHidden') in document) document.addEventListener('webkitvisibilitychange', onchange);
  else if ((hidden = 'msHidden') in document) document.addEventListener('msvisibilitychange', onchange);
  else if ('onfocusin' in document) document.onfocusin = document.onfocusout = onchange;
  else window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;
  function onchange(evt) {
    let v = false;
    let h = true;
    let evtMap = {
      focus: v,
      focusin: v,
      pageshow: v,
      blur: h,
      focusout: h,
      pagehide: h
    };
    evt = evt || window.event;
    let windowHidden = false;
    if (evt.type in evtMap) {
      windowHidden = evtMap[evt.type];
    } else {
      windowHidden = this[hidden];
    }
    if (windowHidden) {
      Howler.mute(true);
    } else {
      if (button[4] !== undefined && button[4].icon1.visible) Howler.mute(false);
    }
  }
  if (document[hidden] !== undefined) {
    onchange({
      type: document[hidden] ? 'blur' : 'focus'
    });
  }
}
let oldWindow;
onWindowResize();
function onWindowResize() {
  if (oldWindow != document.body.clientWidth / document.body.clientHeight) {
  	if (document.body.clientHeight / document.body.clientWidth >= 1.449) {
    	mainCamera.fov = (Math.atan((window.innerHeight / 2) / ((window.innerWidth / 2) / (Math.tan(50 * Math.PI / 360))))) * 360 / Math.PI;
      renderPixelatedPass.setPixelSize(document.body.clientWidth / 240 * window.devicePixelRatio);
      renderPixelatedPass1.setPixelSize(document.body.clientWidth / 240 * window.devicePixelRatio);
  	} else {
  		mainCamera.fov = 68.10827;
  	  renderPixelatedPass.setPixelSize(document.body.clientHeight / 348 * window.devicePixelRatio);
  	  renderPixelatedPass1.setPixelSize(document.body.clientHeight / 348 * window.devicePixelRatio);
   	}
   	if (window.innerHeight * window.devicePixelRatio > 1080) {
   		mainRenderer.setPixelRatio(1080 / window.innerHeight);
   	} else {
   		mainRenderer.setPixelRatio(window.devicePixelRatio);
   	}
   	if (bookCounter !== undefined) bookCounter.position.set(-0.14, Math.tan(mainCamera.fov * Math.PI / 360) - 0.1, -1);
    if (button[4] !== undefined) button[4].position.set((Math.tan(mainCamera.fov * Math.PI / 360) / document.body.clientHeight * document.body.clientWidth - 0.1) * 2, (Math.tan(mainCamera.fov * Math.PI / 360) - 0.08) * 2, -2);
    uiRenderer.setPixelRatio(window.devicePixelRatio);
    mainCamera.aspect = document.body.clientWidth / document.body.clientHeight;
    mainCamera.updateProjectionMatrix();
    mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
    uiRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
    pixelComposer.setSize(document.documentElement.clientWidth, document.documentElement.clientHeight);
    glitchComposer.setSize(document.documentElement.clientWidth, document.documentElement.clientHeight);
    oldWindow = document.body.clientWidth / document.body.clientHeight;
  }
}
async function onShare() {
	if (shareReady && button[3].scale.x == 1) {
		shareReady = false;
		gameSound[0].play();
		gsap.to(button[3].scale, { duration: 0.15, x: 0.9, y: 0.9, ease: "power2.out" });
	  uiScene.remove(speechBubble[6].text);
	  mainScene.add(speechBubble[6].text);
	  pixelComposer.render(delta);
		const dataUrl = mainRenderer.domElement.toDataURL();
    const blob = await (await fetch(dataUrl)).blob();
		const filesArray = [new File([blob], 'result.jpg', { type: blob.type, lastModified: new Date().getTime() })];
		const shareData = {
			text: 'Проведи день в ИТМО вместе со мной!',
			url: window.location.href,
			files: filesArray
		};
		mainScene.remove(speechBubble[6].text);
	  uiScene.add(speechBubble[6].text);
		try {
			await navigator.share(shareData);
		} catch (err) {}
		setTimeout(function() {
			gsap.to(button[3].scale, { duration: 0.1, x: 1, y: 1, ease: "power2.in" });
		}, 300);
	}
}
let replaceLevel = 0;
const clock = new THREE.Clock();
let delta;
loop();
function loop() {
	document.body.style.cursor = "default";
	delta = clock.getDelta() * 0.001;
	visibilityChange();
	if (oldWindow !== document.body.clientWidth / document.body.clientHeight) onWindowResize();
  if (moveWorld) {
  	if (level == 0 && (worldContainer.position.z + 3000 * delta) >= 52.5) {
  		worldContainer.position.z = 52.5;
  		moveWorld = false;
  		goLevel2();
  	} else if (level == 1 && (worldContainer.position.z + 3000 * delta) >= 100.1) {	
  		worldContainer.position.z = 100.1;
  		moveWorld = false;
  		goLevel3();
  	}  else {
  		worldContainer.position.z += 3000 * delta;
  	}
  	if (level == 0) {
  		if (worldContainer.position.z >= 48) {
  			onPlay = false;
  		}
  		if (worldContainer.position.z >= 38.5 && replaceLevel == 0) {
  			replaceLevel = 1;
  			worldContainer.add(levelEnvironment[1], levelEnvironment[0].obstacle[17], levelEnvironment[0].obstacle[16], levelEnvironment[0].obstacle[15], levelEnvironment[0].obstacle[14], levelEnvironment[0].obstacle[13], levelEnvironment[0].obstacle[12], levelEnvironment[0].obstacle[10], levelEnvironment[0].obstacle[11]);
  		}
  	}
  	if (level == 1) {
  		if (worldContainer.position.z >= 58.5 && replaceLevel == 1) {
  			replaceLevel = 2;
  			worldContainer.remove(levelEnvironment[0].obstacle[9], levelEnvironment[0].obstacle[8], levelEnvironment[0].obstacle[7], levelEnvironment[0].obstacle[6], levelEnvironment[0].obstacle[5], levelEnvironment[0].obstacle[4], levelEnvironment[0].obstacle[3], levelEnvironment[0].obstacle[2], levelEnvironment[0].obstacle[1], levelEnvironment[0].obstacle[0], levelEnvironment[0]);
  		}
  		if (worldContainer.position.z >= 96) {
  			onPlay = false;
  		}
  		if (worldContainer.position.z >= 89.1 && replaceLevel == 2) {
  			replaceLevel = 3;
  			levelEnvironment[2].tweenAnimation[0] = gsap.to([levelEnvironment[2].ball.position, levelEnvironment[2].ball2.position], { duration: 0.8, y: 6, ease: "power1.out", repeat: -1, yoyo: true });
        levelEnvironment[2].tweenAnimation[1] = gsap.to([levelEnvironment[2].ball.rotation, levelEnvironment[2].ball2.rotation], { duration: 3, x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[2].tweenAnimation[2] = gsap.to([levelEnvironment[2].ball.rotation, levelEnvironment[2].ball2.rotation], { duration: 4, y: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[2].tweenAnimation[3] = gsap.to([levelEnvironment[2].ball1.position, levelEnvironment[2].ball3.position], { duration: 0.7, y: 5, ease: "power1.out", repeat: -1, yoyo: true });
        levelEnvironment[2].tweenAnimation[4] = gsap.to([levelEnvironment[2].ball1.rotation, levelEnvironment[2].ball3.rotation], { duration: 4, x: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[2].tweenAnimation[5] = gsap.to([levelEnvironment[2].ball1.rotation, levelEnvironment[2].ball3.rotation], { duration: 3, y: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[2].tweenAnimation[6] = gsap.to(levelEnvironment[2].ball4.position, { duration: 0.6, y: 4, ease: "power1.out", repeat: -1, yoyo: true });
        levelEnvironment[2].tweenAnimation[7] = gsap.to(levelEnvironment[2].ball4.rotation, { duration: 3, x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[2].tweenAnimation[8] = gsap.to(levelEnvironment[2].ball4.rotation, { duration: 5, y: -Math.PI * 2, ease: "none", repeat: -1 });
  			worldContainer.add(levelEnvironment[0].obstacle[27], levelEnvironment[0].obstacle[26], levelEnvironment[0].obstacle[25], levelEnvironment[0].obstacle[24], levelEnvironment[0].obstacle[23], levelEnvironment[0].obstacle[22], levelEnvironment[0].obstacle[21], levelEnvironment[0].obstacle[20], levelEnvironment[0].obstacle[19], levelEnvironment[0].obstacle[18], levelEnvironment[2]);
  		}
  	}
  	if (level == 2) {
  		if (worldContainer.position.z >= 104.1 && replaceLevel == 3) {
  			replaceLevel = 4;
  			worldContainer.remove(levelEnvironment[1], levelEnvironment[0].obstacle[17], levelEnvironment[0].obstacle[16], levelEnvironment[0].obstacle[15], levelEnvironment[0].obstacle[14], levelEnvironment[0].obstacle[13], levelEnvironment[0].obstacle[12], levelEnvironment[0].obstacle[10], levelEnvironment[0].obstacle[11]);
  		}
  		if (worldContainer.position.z >= 133.1 && replaceLevel == 4) {
  			replaceLevel = 5;
  			levelEnvironment[3].animationTween[0] = gsap.to(levelEnvironment[3].wall[4].material.color, { duration: 0.3, r: levelEnvironment[3].blueBlimColor.r, g: levelEnvironment[3].blueBlimColor.g, b: levelEnvironment[3].blueBlimColor.b, ease: "none", repeat: -1, yoyo: true });
        levelEnvironment[3].animationTween[1] = gsap.to(levelEnvironment[3].wall[28].material.color, { duration: 0.5, r: levelEnvironment[3].greenlimColor.r, g: levelEnvironment[3].greenlimColor.g, b: levelEnvironment[3].greenlimColor.b, ease: "none", repeat: -1, yoyo: true });
        levelEnvironment[3].animationTween[2] = gsap.to([levelEnvironment[3].pendulum.part[2].rotation, levelEnvironment[3].pendulum1.children[0].rotation], { duration: 2, z: -0.5, ease: "power1.inOut", repeat: -1, yoyo: true });
        levelEnvironment[3].animationTween[3] = gsap.to(levelEnvironment[0].obstacle[31].part[0].rotation, { duration: 5, x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[4] = gsap.to(levelEnvironment[0].obstacle[33].children[0].rotation, { duration: 7, x: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[5] = gsap.to(levelEnvironment[0].obstacle[35].children[0].rotation, { duration: 6, x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[6] = gsap.to(levelEnvironment[0].obstacle[37].part[1].part.rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[7] = gsap.to(levelEnvironment[0].obstacle[37].part[2].part.rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[8] = gsap.to(levelEnvironment[0].obstacle[37].part[3].part.rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[9] = gsap.to(levelEnvironment[0].obstacle[37].container.rotation, { duration: 1 + Math.random(), z: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[10] = gsap.to(levelEnvironment[0].obstacle[37].container.rotation, { duration: 1 + Math.random(), x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[11] = gsap.to(levelEnvironment[0].obstacle[38].children[0].children[0].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[12] = gsap.to(levelEnvironment[0].obstacle[38].children[0].children[1].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[13] = gsap.to(levelEnvironment[0].obstacle[38].children[0].children[2].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[14] = gsap.to(levelEnvironment[0].obstacle[38].children[0].rotation, { duration: 1 + Math.random(), z: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[15] = gsap.to(levelEnvironment[0].obstacle[38].children[0].rotation, { duration: 1 + Math.random(), x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[16] = gsap.to(levelEnvironment[0].obstacle[40].children[0].children[0].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[17] = gsap.to(levelEnvironment[0].obstacle[40].children[0].children[1].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[18] = gsap.to(levelEnvironment[0].obstacle[40].children[0].children[2].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[19] = gsap.to(levelEnvironment[0].obstacle[40].children[0].rotation, { duration: 1 + Math.random(), z: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[20] = gsap.to(levelEnvironment[0].obstacle[40].children[0].rotation, { duration: 1 + Math.random(), x: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[21] = gsap.to(levelEnvironment[0].obstacle[41].children[0].children[0].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[22] = gsap.to(levelEnvironment[0].obstacle[41].children[0].children[1].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[23] = gsap.to(levelEnvironment[0].obstacle[41].children[0].children[2].children[0].rotation, { duration: 0.5 + Math.random() * 0.5, z: Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[24] = gsap.to(levelEnvironment[0].obstacle[41].children[0].rotation, { duration: 1 + Math.random(), z: -Math.PI * 2, ease: "none", repeat: -1 });
        levelEnvironment[3].animationTween[25] = gsap.to(levelEnvironment[0].obstacle[41].children[0].rotation, { duration: 1 + Math.random(), x: Math.PI * 2, ease: "none", repeat: -1 });
        for (let k = 0; k < 13; k++) {
        	levelEnvironment[3].animationTween[k + 26] = gsap.to(levelEnvironment[3].pic[k].position, { duration: 0.5 + Math.random() * 0.5, x: levelEnvironment[3].pic[k].position.x + 0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
        	levelEnvironment[3].animationTween[k + 26] = gsap.to(levelEnvironment[3].pic[k].position, { duration: 0.5 + Math.random() * 0.5, y: levelEnvironment[3].pic[k].position.y + 0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
        }
        for (let k = 0; k < 4; k++) {
        	levelEnvironment[3].animationTween[k + 29] = gsap.to([levelEnvironment[0].obstacle[28].part[5 + k].material.color, levelEnvironment[0].obstacle[42].children[0].children[k].material.color], { duration: 0.3, r: levelEnvironment[3].yellowBlimColor.r, g: levelEnvironment[3].yellowBlimColor.g, b: levelEnvironment[3].yellowBlimColor.b, ease: "none", repeat: -1, yoyo: true, delay: k * 0.09 });
        	levelEnvironment[3].animationTween[k + 33] = gsap.to(levelEnvironment[0].obstacle[34].children[k].material.color, { duration: 0.3, r: levelEnvironment[3].blueBlimColor.r, g: levelEnvironment[3].blueBlimColor.g, b: levelEnvironment[3].blueBlimColor.b, ease: "none", repeat: -1, yoyo: true, delay: k * 0.09 });
        }
        for (let i = 0; i < 38; i++) {
  	      const randomScale = 1.5 + Math.random();
  	      levelEnvironment[3].animationTween[i + 37] = gsap.to(levelEnvironment[3].sparkle[i].scale, { duration: 0.6 + Math.random() * 0.6, x: randomScale, y: randomScale, ease: "power1.inOut", repeat: -1, yoyo: true });
        }
  			worldContainer.add(levelEnvironment[0].obstacle[42], levelEnvironment[0].obstacle[41], levelEnvironment[0].obstacle[40], levelEnvironment[0].obstacle[39], levelEnvironment[0].obstacle[38], levelEnvironment[0].obstacle[37], levelEnvironment[0].obstacle[36], levelEnvironment[0].obstacle[35], levelEnvironment[0].obstacle[34], levelEnvironment[0].obstacle[33], levelEnvironment[0].obstacle[32], levelEnvironment[0].obstacle[31], levelEnvironment[0].obstacle[29], levelEnvironment[0].obstacle[28], levelEnvironment[3]);
   		}
  	}
  	if (level == 3) {
  		if (worldContainer.position.z >= 181) {
  		  moveWorld = false;
  		  worldContainer.position.z = 181;
  		  goLevel5();
  		}
  		if (worldContainer.position.z >= 146.1 && replaceLevel == 5) {
  			replaceLevel = 6;
  			for (let i = 0; i < 9; i++) {
  				levelEnvironment[2].tweenAnimation[i].kill();
  				levelEnvironment[2].tweenAnimation[i] = null;
  			}
  			worldContainer.remove(levelEnvironment[0].obstacle[27], levelEnvironment[0].obstacle[26], levelEnvironment[0].obstacle[25], levelEnvironment[0].obstacle[24], levelEnvironment[0].obstacle[23], levelEnvironment[0].obstacle[22], levelEnvironment[0].obstacle[21], levelEnvironment[0].obstacle[20], levelEnvironment[0].obstacle[19], levelEnvironment[0].obstacle[18], levelEnvironment[2]);
   		}
   		if (worldContainer.position.z >= 174 && replaceLevel == 6) {
   			replaceLevel = 7;
   		  worldContainer.add(levelEnvironment[4]);
   		}
   		if (worldContainer.position.z >= 178 && replaceLevel == 7) {
   			gsap.to(frame.materialColor.color, { duration: 1, r: blackBasicMaterial.color.r, g: blackBasicMaterial.color.g, b: blackBasicMaterial.color.b, ease: "none" });
   		}
  	}
  }
  if (level < 4 && viewY) {
  	rotationContainer.position.y = -2.5 - (hero.position.y - 0.64) * 0.7;
  	for (let i = 0; i < 24; i++) {
  		if (book[i].ready && Math.abs(hero.position.z - 0.2 - (book[i].position.z + worldContainer.position.z)) < 0.4 && Math.abs(hero.position.y + 0.31 - book[i].position.y) < 1.3) {
  			takeBook(i);
  		}
  	}
  }
  if (flyY && level < 4) {
  	rotationContainer.position.y = -0.4 - (hero.position.y - 0.64) * 0.3;
  	for (let i = 0; i < 24; i++) {
  		if (book[i].ready && Math.abs(hero.position.z - 0.2 - (book[i].position.z + worldContainer.position.z)) < 0.4 && Math.abs(hero.position.y + 0.31 - book[i].position.y) < 1.3) {
  			takeBook(i);
  		}
  	}
  }
  if (level > 3) {
  	for (let i = 0; i < 12; i++) {
  		if (book[i].ready) book[i].position.set(levelEnvironment[0].obstacle[i + 43].position.x , levelEnvironment[0].obstacle[i + 43].position.y + 1.5, levelEnvironment[0].obstacle[i + 43].position.z - levelEnvironment[0].obstacle[i + 43].container.position.x);
  	}
  	for (let i = 0; i < 12; i++) {
  		if (book[i].ready && book[i].position.x == 0 && Math.abs(hero.position.z - (book[i].position.z + worldContainer.position.z)) < 0.7 && Math.abs(hero.position.y + 0.31 - book[i].position.y) < 1.3) {
  			takeBook(i);
  		}
  	}
  }
  if (level == 4) {
  	rotationContainer.position.y = -2.5 - (hero.position.y - 0.64);
  	rotationContainer.position.x = -0.9 + (hero.position.z + 0.9) * 0.4;
    for (let i = 43; i < levelEnvironment[0].obstacle.length; i++) {
  	  if (levelEnvironment[0].obstacle[i].position.x == 0 && levelEnvironment[0].obstacle[i].ready && hero.animation == `jumpDown` && Math.abs(hero.position.z - (worldContainer.position.z + (levelEnvironment[0].obstacle[i].position.z - levelEnvironment[0].obstacle[i].container.position.x + 3000 * delta))) < levelEnvironment[0].obstacle[i].box[0] * 0.5 && hero.position.y - 0.64 <= levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1]) {
        hero.obstacle = levelEnvironment[0].obstacle[i];
    	  hero.floor = 0.64 + levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1];
        if (hero.floor > 30) onPlay = false;
  	    if (i < 54) {
    	    levelEnvironment[0].obstacle[i].tween.play();
    	    levelEnvironment[0].obstacle[i].tween1.play();
   	    }
        hero.offsetX = hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z);
  	    gameSound[6].play();
  	    clearHeroAnimation();
        goHeroIdleAnimation();
        break;
      }
  	}
  }
  if (levelEnvironment[0] !== undefined && level < 4) {
    for (let i = 0; i < levelEnvironment[0].obstacle.length; i++) {
      if (levelEnvironment[0].obstacle[i].ready && hero.animation == `jumpDown` && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 && hero.position.y - 0.64 < levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] + 0.1) {
    	  moveWorld = false;
    	  hero.position.y = levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] + 0.64;
    	  hero.obstacle = levelEnvironment[0].obstacle[i];
  	    hero.floor = 0.64 + levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1];
        gameSound[6].play();
        walkObstacle();
      }
    }
  }
  if (hero.floor > 0.64 && (hero.animation == `run` || hero.animation == `jumpSlide`) && hero.obstacle.position.z + worldContainer.position.z + 3000 * delta - hero.position.z > hero.obstacle.box[0] * 0.5 + 0.1) {
  	hero.floor = 0.64;
  	clearHeroAnimation();
  	goHeroFallDownAnimation();
  }
  if (onPlay) {
  	if (level == 3) {
  		if (hero.position.y > 4) {
  			clearFlyAnimation();
  			goHeroFlyHitDownAnimation()
  		}
  		if (hero.position.y < -4) {
  			clearFlyAnimation();
  			goHeroFlyHitUpAnimation();
  		}
  		for (let i = 0; i < levelEnvironment[0].obstacle.length; i++) {
        if (levelEnvironment[0].obstacle[i].ready && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.3 && hero.position.y + 1.3 > levelEnvironment[0].obstacle[i].position.y && levelEnvironment[0].obstacle[i].position.y > hero.position.y && hero.position.y + 1.3 < levelEnvironment[0].obstacle[i].position.y + 0.2) {
        
        	clearFlyAnimation();
        	goHeroFlyHitDownAnimation();
        	break;
        }
  	  	if (levelEnvironment[0].obstacle[i].ready && hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.3 && hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta) >= levelEnvironment[0].obstacle[i].box[0] * 0.5 && (Math.abs(hero.position.y + 0.31 - (levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] * 0.5)) < 0.95 + levelEnvironment[0].obstacle[i].box[1] * 0.5) && hero.position.y - 0.64 < levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] && ((hero.animation != `jumpSlide` && hero.animation != `holdSlide`) || (hero.animation == `jumpSlide` && levelEnvironment[0].obstacle[i].position.y < 1))) {
  		  	moveWorld = false;
  		  	onPlay = false;
  		  	clearHeroAnimation();
  		  	goHeroFlyFallAnimation(levelEnvironment[0].obstacle[i].dir - 3000 * delta);
  		  	break;
  	  	}
  	  	if (levelEnvironment[0].obstacle[i].ready && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.3 && hero.position.y - 0.64 < levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] && hero.position.y - 0.64 > levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] - 0.2) {
  	  		clearFlyAnimation();
  	  		goHeroFlyHitUpAnimation();
  	  		break;
  	  	}
  		}
  	} else {
    	if (level == 4) {
    		if (hero.floor > 0.64 && hero.animation == `idle` && hero.obstacle !== undefined) {
    			hero.position.z = worldContainer.position.z + hero.obstacle.position.z + hero.offsetX;
    		}
    	}
  	  for (let i = 0; i < levelEnvironment[0].obstacle.length; i++) {
   	  	if (level < 4 && levelEnvironment[0].obstacle[i].ready && hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta) < levelEnvironment[0].obstacle[i].box[0] * 0.5 + 0.6 && hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta) >= levelEnvironment[0].obstacle[i].box[0] * 0.5 && (Math.abs(hero.position.y + 0.31 - (levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] * 0.5)) < 0.95 + levelEnvironment[0].obstacle[i].box[1] * 0.5) && hero.position.y - 0.64 < levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] && ((hero.animation != `jumpSlide` && hero.animation != `holdSlide`) || (hero.animation == `jumpSlide` && levelEnvironment[0].obstacle[i].position.y < 1))) {
  	    	moveWorld = false;
  	    	onPlay = false;
  	    	clearHeroAnimation();
  	    	goHeroFallAnimation(levelEnvironment[0].obstacle[i].dir - 3000 * delta);
  	    	break;
  	  	}
  	  	if (level < 4 && levelEnvironment[0].obstacle[i].ready && (hero.animation == `jumpUp` || hero.animation == `doubleJump`) && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z + 3000 * delta)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 && hero.position.y + 1.2 > levelEnvironment[0].obstacle[i].position.y && levelEnvironment[0].obstacle[i].position.y > hero.position.y) {
  	  		moveWorld = false;
  	  		onPlay = false;
  	  		clearHeroAnimation();
  	  		goHeroFallAnimation(0);
  	  		break;
    	  }
    	  if (levelEnvironment[0].obstacle[i].ready && Math.abs(hero.position.z - (worldContainer.position.z + levelEnvironment[0].obstacle[i].position.z)) < levelEnvironment[0].obstacle[i].box[0] * 0.5 && hero.position.y - 0.64 < levelEnvironment[0].obstacle[i].position.y + levelEnvironment[0].obstacle[i].box[1] && (hero.animation == `jumpSlide` || hero.animation == `holdSlide`)) {
    	    hero.obstacle = levelEnvironment[0].obstacle[i];
    	    hero.animation = `holdSlide`;
    	  	break;
    	  }
  	  }
  	  if (hero.animation == `holdSlide` && hero.obstacle.position.z + worldContainer.position.z - hero.position.z > hero.obstacle.box[0] * 0.5 + 1) {
  	  	clearHeroAnimation();
  	  	if (worldContainer.position.z < 140) {
  	  	  goHeroRunAnimation();
  	  	} else {
  	  		onPlay = false;
  	  		goLevel4();
  	  	}
  	  }
  	}
	}
  if (goGlitch) {
  	glitchComposer.render();
  } else {
    pixelComposer.render(delta);
  }
  uiRenderer.render(uiScene, mainCamera);
  requestAnimationFrame(loop);
}
const bookName = [ [`Учим JavaScript`], [`Чистый код`], [`Построение`, `алгоритмов`], [`Питон`, `для начинающих`], [`Кодеры`, `за работой`], [`Основы`, `информатики`], [`Java`, `для начинающих`], [`C++ для чайников`], [`Ненормальный`, `React`], [`Что такое`, `рефакторинг?`], [`Макросы`, `и шмакросы`], [`Как работает`, `Async/Await?`], [`Клонирование`, `объектов JS`], [`Зачем нам`, `Reactive?`], [`Математическая`, `криптография`], [`C# это легко!`], [`Какой ты`, `программист?`], [`Date Science`, `& Big Data`], [`Не шути`, `с Фортраном`], [`Весёлые`, `репозитории`], [`Переходим на Go`], [`Java для сисадминов`], [`Три закона`, `робототехники`], [`Экзоскелеты`, `как в кино`], [`Роботы и жизнь`], [`Семиотика`, `для чайников`], [`Восстание`, `машин`], [`Робот-сгибатель`, `из будущего`], [`Сервоприводы`, `для домохозяек`], [`Беспилотники`, `и чайки`], [`Космические`, `андроиды`], [`Робот`, `на все руки`], [`Мечты`, `о трансформерах`], [`Дроны`, `на рыбалке`], [`Зловещая долина`], [`Беспилотные`, `пылесосы`], [`Кибернетика`, `для чайников`], [`Компьютерное`, `зрение`], [`Как починить`, `робо-пса`], [`Искуственный`, `интеллект`], [`100 роботов`, `будущего`], [`Микроконтроллеры`], [`Как дружить`, `с роботами`], [`Кибернетические`, `черепахи`], [`Робот-паук`, `на Arduino`], [`Machine Learning`], [`История бокса`], [`Лучшие нокауты`], [`Почему ринг`, `квадратный?`], [`Спортивные`, `вкусняхи`], [`Как правильно`, `надувать мяч`], [`Пинг-понг`, `для новичков`], [`Самые высокие`, `баскетболисты`], [`Шахматные задачи`], [`Анатомия пилатеса`], [`Почему мячи`, `круглые?`], [`Спортивная`, `медицина`], [`Занимательная`, `физкультура`], [`Королевский`, `гамбит`], [`Мой любимый`, `хоккей`], [`Настольная книга`, `тренера`], [`Развитие`, `мышц кора`], [`Легенды`, `футбола`], [`Бесседы`, `о фитнесе`], [`Армрестлинг.`, `Шаги к успеху`], [`Волейбол`, `и песок`], [`Футбольные`, `истории`], [`Магнитные`, `монополи`], [`Что такое`, `«белые дыры»`], [`Квантовая`, `механика`], [`Неполнота`, `и относительность`], [`Какого цвета`, `атомы`], [`Телепортация`, `энергии`], [`Измеряем`, `скорость света`], [`Квантовая`, `теория поля`], [`Червоточины`,`и черные дыры`], [`Динамика`, `крыльев мотылька`], [`Настоящий`, `цвет луны`], [`Ядерный синтез`, `для чайников`], [`Теория всего`], [`Пугающее`, `дальнодействие`], [`Очарованные`, `кварки`], [`О задаче`, `трёх тел`], [`Светим лазером`, `сквозь стену`], [`Как устроены`, `атомные часы?`], [`Антиматерия`, `и бариогенезис`], [`Звуковой луч –`, `реально ли это?`], [`История физики`], [`Диалоги Платона`], [`Математические`, `начала`], [`О вращениях`, `небесных тел`], [`Аристотелев`, `корпус`], [`Тайна`, `мироздания`], [`Сущность теории`, `относительности`], [`Тетрабиблос`], [`Краткая история`, `времени`], [`Альмагест`], [`Красная книга`, `лекций`], [`Диалог о двух`, `системах мира`], [`ДИПЛОМ`], ];
const resultText = [ `одну книгу!`, `две книги!`, `три книги!`, `четыре книги!`, `пять книг!`, `шесть книг!`, `семь книг!`, `восемь книг!`, `девять книг!`, `десять книг!`, `11 книг!`, `12 книг!`, `13 книг!`, `14 книг!`, `15 книг!`, `16 книг!`, `17 книг!`, `18 книг!`, `19 книг!`, `20 книг!`, `21 книгу!`, `22 книги!`, `23 книги!`, `24 книги!`, `25 книг!`, `26 книг!`, `27 книг!`, `28 книг!`, `29 книг!`, `30 книг!`, `31 книгу!`, `32 книги!`, `33 книги!`, `34 книги!`, `35 книг!`, `36 книг!`, `37 книг!`, `38 книг!`, `39 книг!`, `40 книг!`, `41 книгу!`, `42 книги!`, `43 книги!`, `44 книги!`, `45 книг!`, `46 книг!`, `47 книг!`, `48 книг!`, `49 книг!`, `50 книг!`, `51 книгу!`, `52 книги!`, `53 книги!`, `54 книги!`, `55 книг!`, `56 книг!`, `57 книг!`, `58 книг!`, `59 книг!`, `60 книг!`, `61 книгу!`, `62 книги!`, `63 книги!`, `64 книги!`, `65 книг!`, `66 книг!`, `67 книг!`, `68 книг!`, `69 книг!`, `70 книг!`, `71 книгу!`, `72 книги!`, `73 книги!`, `74 книги!`, `75 книг!`, `76 книг!`, `77 книг!`, `78 книг!`, `79 книг!`, `80 книг!`, `81 книгу!`, `82 книги!`, `83 книги!`, `84 книги!`, `85 книг!`, `86 книг!`, `87 книг!`, `88 книг!`, `89 книг!`, `90 книг!`, `91 книгу!`, `92 книги!`, `93 книги!`, `94 книги!`, `95 книг!`, `96 книг!`, `97 книг!`, `98 книг!`, `99 книг!`, `100 книг!`,]